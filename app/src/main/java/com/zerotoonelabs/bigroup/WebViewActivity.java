package com.zerotoonelabs.bigroup;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.authui.payment.PublicUtilitiesFragment;
import com.zerotoonelabs.bigroup.fragment.PayQuickFragment;

import static android.app.PendingIntent.getActivity;

public class WebViewActivity extends AppCompatActivity {

    private WebView webView;
    private String redirectUrl;
    private String returnUrl = "https://www.google.com";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        getSupportActionBar().setTitle(R.string.ejemesiyac_platej);

        redirectUrl = getIntent().getStringExtra("URL");
        webView = findViewById(R.id.webView);

        webView.setHorizontalScrollBarEnabled(false);
        webView.setVerticalScrollBarEnabled(false);
        webView.setScrollContainer(false);
        webView.setOnTouchListener((v, event) -> (event.getAction() == MotionEvent.ACTION_MOVE));

        webView.loadUrl(redirectUrl);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.equals(returnUrl)){
                    WebViewActivity.this.finish();
                    return true;
                } else {
                    return false;
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        webView.goBack();
        super.onBackPressed();
        //propertyid
//        String token = Library.GetPhoneMemoryOne("token");
//        String propertyid = Library.GetPhoneMemoryOne("propertyid");
//        String contractId = Library.GetPhoneMemoryOne("contractId");

        return true;
    }

    @Override
    public void onBackPressed() {
        //setResult(RESULT_OK);
        webView.goBack();
      //  super.onBackPressed();


//        String serviceId = null;
//        String serviceNumber = null;
//        Intent intent = new Intent(this, Kassa24Activity.class);
//        //Intent intent = Library.intent;
//        Library.serviceId = serviceId;
//        Library.serviceNumber = serviceNumber;
//        intent.putExtra("serviceID", serviceId);
//        intent.putExtra("contractNumber", serviceNumber);
//        startActivity(intent);

//        Intent intent = Library.intent;
//        intent.putExtra("serviceID", Library.serviceId);
//        intent.putExtra("contractNumber", Library.serviceNumber);
//        startActivity(intent);

    }

}
