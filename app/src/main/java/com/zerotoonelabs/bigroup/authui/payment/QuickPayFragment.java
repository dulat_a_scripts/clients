package com.zerotoonelabs.bigroup.authui.payment;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.authui.authcommon.ServiceListAdapter;
import com.zerotoonelabs.bigroup.databinding.QuickPayFragBinding;
import com.zerotoonelabs.bigroup.util.AutoClearedValue;
import com.zerotoonelabs.bigroup.entity.BankAccount;
import com.zerotoonelabs.bigroup.entity.Status;
import com.zerotoonelabs.bigroup.entity.kassa24.Service;

import javax.inject.Inject;

import static android.app.Activity.RESULT_OK;

public class QuickPayFragment extends Fragment {

    private static final String TOKEN_KEY = "token";
    private static final String CONTRACT_KEY = "contract_id";
    private static final String PROPERTY_KEY = "property_id";
    private static final int BROWSER_RC = 12;

    String LOG = "QuickPayFragment";

    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private AutoClearedValue<QuickPayFragBinding> binding;
    private AutoClearedValue<ServiceListAdapter> adapter;

    private PaymentViewModel viewModel;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.i(LOG, "onActivityCreated()");
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(PaymentViewModel.class);
        Bundle args = getArguments();
        if (args != null && args.containsKey(TOKEN_KEY) && args.containsKey(CONTRACT_KEY) && args.containsKey(PROPERTY_KEY)) {
            String token = args.getString(TOKEN_KEY);
            String contractId = args.getString(CONTRACT_KEY);
            int propertyId = args.getInt(PROPERTY_KEY);
            viewModel.setToken(token);
            viewModel.setPaymentId(contractId, propertyId);
        }

        setupAdapter();
        initObservables(viewModel);

        binding.get().button11.setOnClickListener(v -> {
            viewModel.setTransactionId();
        });
    }

    private void setupAdapter() {
        Log.i(LOG, "setupAdapter()");
        ServiceListAdapter adapter = new ServiceListAdapter(new ServiceListAdapter.ServiceClickCallback() {
            @Override
            public void onClick(Service utility) {

            }

            @Override
            public void onPayClick(Service utility, int position) {
                showDialog(utility, position);
            }
        });
        this.adapter = new AutoClearedValue<>(this, adapter);
        binding.get().serviceList.setAdapter(adapter);
    }

    private void initObservables(PaymentViewModel viewModel) {

        Log.i(LOG, "initObservables()");
        viewModel.getServiceIds().observe(this, resource -> {
            binding.get().setResource(resource);

        });

        viewModel.getBankAccount().observe(this, resource -> {

            viewModel.setHistoryId(resource == null || resource.data == null ? -1 : resource.data.getId());
            if (resource != null && resource.data != null && resource.status == Status.SUCCESS) {
                BankAccount bankAccount = resource.data;

                String number = bankAccount.getNumber();
                String ErcNumber = bankAccount.getErcNumber();
                String AlsecoNumber = bankAccount.getAlsecoNumber();

                //ekranizasiya

                if(number.equals("") || number.equals(null)){
                    number = "";
                }

                if(ErcNumber.equals("") || ErcNumber.equals(null)){
                    ErcNumber = "";
                }

                if(AlsecoNumber.equals("") || AlsecoNumber.equals(null)){
                    AlsecoNumber = "";
                }

                if (!number.equals("")) {
                    viewModel.setUserId("2518", resource.data.getNumber());
                } else if (!ErcNumber.equals("")) {
                    viewModel.setUserId("138", resource.data.getErcNumber());
                } else {
                    viewModel.setMessage(R.string.client_number_not_found);
                }
            }
        });

        viewModel.getServices().observe(this, listResource -> {
            binding.get().setResource(listResource);
            binding.get().setResultCount(listResource == null || listResource.data == null
                    ? 0
                    : listResource.data.services.service.size());
            adapter.get().replace(listResource == null || listResource.data == null
                    ? null
                    : listResource.data.services.service);
            if (listResource != null && listResource.data != null
                    && listResource.data.invoiceId != null) {
                viewModel.setInvoiceId(listResource.data.invoiceId);
            }
            binding.get().executePendingBindings();
        });

        viewModel.getTransaction().observe(this, resource -> {
            binding.get().setResource(resource);
            if (resource != null && resource.data != null && resource.data.formUrl != null) {
                openBrowser(resource.data.formUrl);
                viewModel.getTransaction().removeObservers(this);
            }
        });

        viewModel.getMessage().observe(this, resId -> {
            if (resId != null) {
                Toast.makeText(getActivity(), resId, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void openBrowser(String url) {

        Log.i(LOG, "openBrowser()");
        if (url == null) {
            return;
        }

        Intent browserIntent = new Intent(getActivity(), BrowserActivity.class);
        browserIntent.putExtra(BrowserActivity.URL_KEY, url);
        startActivityForResult(browserIntent, BROWSER_RC);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(LOG, "onActivityResult()");
        if (requestCode == BROWSER_RC) {
            if (resultCode == RESULT_OK) {
                binding.get().button11.setText(R.string.pay_button_label2);
                viewModel.retry();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void showDialog(Service utility, int position) {
        Log.i(LOG, "showDialog()");
        if (!isAdded()) return;

        View view = getLayoutInflater().inflate(R.layout.payment_dialog, null);
        final EditText editText = view.findViewById(R.id.editText4);
        if (utility.getTotalSum() != 0) {
            editText.setText(String.valueOf(utility.getTotalSum()));
        }

        new AlertDialog.Builder(getActivity())
                .setView(view)
                .setPositiveButton(android.R.string.ok, (dialog1, which) -> {
                    String input = editText.getText().toString();

                    int sum = Integer.parseInt(TextUtils.isEmpty(input) ? "0" : input);
                    utility.setTotalSum(sum);

                    adapter.get().notifyItemChanged(position);

                    binding.get().button11.setText(getString(R.string.pay_button_label, viewModel.getTotalSum() + ""));
                })
                .show();
    }

    public QuickPayFragment() {
        // Required empty public constructor
    }

    public static QuickPayFragment newInstance(String token, int param1, String contractId) {
        QuickPayFragment fragment = new QuickPayFragment();
        Bundle args = new Bundle();
        args.putString(TOKEN_KEY, token);
        args.putString(CONTRACT_KEY, contractId);
        args.putInt(PROPERTY_KEY, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(LOG, "onCreateView()");
        // Inflate the layout for this fragment
        QuickPayFragBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.quick_pay_frag, container, false);
        binding = new AutoClearedValue<>(this, dataBinding);

        return dataBinding.getRoot();
    }

}
