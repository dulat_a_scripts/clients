package com.zerotoonelabs.bigroup.authui.biserviceauthoruzed;


import android.app.Activity;
import android.arch.lifecycle.ViewModelProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.zerotoonelabs.bigroup.BuildConfig;
import com.zerotoonelabs.bigroup.Library.Fragments.Notification_fragments;
import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.Library.Retrofit_request.Get_photo_guid;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.authui.SingleFragmentActivity;
import com.zerotoonelabs.bigroup.authui.authcommon.AuthNavController;
import com.zerotoonelabs.bigroup.authui.pdfviewer.PdfViewerViewModel;
import com.zerotoonelabs.bigroup.databinding.FragmentCallBinding;
import com.zerotoonelabs.bigroup.di.Injectable;
import com.zerotoonelabs.bigroup.util.AutoClearedValue;
import com.zerotoonelabs.bigroup.entity.BiServiceContactData;
import com.zerotoonelabs.bigroup.entity.Status;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.zerotoonelabs.bigroup.authui.HomeActivity.CURRENT_FLAT;

/**
 * A fragment with a Google +1 button.
 */
public class CallServiceFragment extends Fragment implements Injectable {

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    Context context;
    String token;
    String url = "http://webapi.bi-group.org/api/v1/MobileClient/getServiceCompanyContactDetails/";
    int propertyId;
    SharedPreferences sharedPreferences;
    private AutoClearedValue<FragmentCallBinding> binding;
    private PdfViewerViewModel viewerViewModel;
    Gson gson;
    @Inject
    AuthNavController navigationController;

    public CallServiceFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        //setupActionBar();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        Log.i("dh",menu.toString());
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //FragmentManager fm = getActivity().getSupportFragmentManager();
        //fm.popBackStack();
        //getActivity().onBackPressed();
        //navigationController.navigateToBiServicetwo();
        getActivity().onBackPressed();
        String extraKey = SingleFragmentActivity.BISERVICE_EXTRA;
        Intent intent = new Intent(getActivity(), SingleFragmentActivity.class);
        intent.putExtra(extraKey, true);
        startActivity(intent);
        //Log.i("ds","" + item.getItemId());
        return super.onOptionsItemSelected(item);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentCallBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_call, container, false);
        binding = new AutoClearedValue<>(this, dataBinding);
        context = getContext();
        gson = new Gson();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        token = sharedPreferences.getString("token", "");
        return binding.get().getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewerViewModel = viewModelFactory.create(PdfViewerViewModel.class);
        propertyId = sharedPreferences.getInt(CURRENT_FLAT, -1);
        getServiceContacts();
        binding.get().managerCallIcon.setOnClickListener(this::launchPhone);
        binding.get().managerPhoneNumber.setOnClickListener(this::launchPhone);
        binding.get().priseListCard.setOnClickListener(this::downloadPriseList);
        binding.get().tvPriceLists.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent viewIntent =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse("https://sales.bi-group.org/ru-kz/bi_service"));
                startActivity(viewIntent);
            }
        });


        binding.get().sendMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGmail(binding.get().sendMail.getText().toString());
            }
        });

        viewerViewModel.getPriseListPdf().observe(this, fileResource -> {
            if (fileResource != null && fileResource.data != null && fileResource.status == Status.SUCCESS) {
                openPdf(fileResource.data, "application/pdf");
            }
        });
    }

//    private void setupActionBar() {
//        Toolbar toolbar = ((Activity)Library.context).findViewById(R.id.toolbar);
//        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
//        actionBar.setDisplayHomeAsUpEnabled(true);
//        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);
//
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Notification_fragments notification_fragmentss = new Notification_fragments();
//
//                android.support.v4.app.FragmentManager fragmentManager = Library.fragmentManager;
//                fragmentManager.beginTransaction().replace(R.id.containerAuth, notification_fragmentss).commit();
//
//            }
//        });
//    }


    public void openGmail(String email) {

        Intent intent=new Intent(Intent.ACTION_SEND);
        String[] recipients={email};
        intent.putExtra(Intent.EXTRA_EMAIL, recipients);
        intent.putExtra(Intent.EXTRA_SUBJECT,"Subject text here...");
        intent.putExtra(Intent.EXTRA_TEXT,"Body of the content here...");
        intent.putExtra(Intent.EXTRA_CC,"mailcc@gmail.com");
        intent.setType("text/html");
        intent.setPackage("com.google.android.gm");
        startActivity(Intent.createChooser(intent, "Send mail"));

    }

    private void getServiceContacts() {
        JsonArrayRequest arrayRequest = new JsonArrayRequest(url + propertyId, response -> {
            try {
                JSONObject object = (JSONObject) response.get(0);
                BiServiceContactData contactData = gson.fromJson(object.toString(), BiServiceContactData.class);
                setContactData(contactData);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, Throwable::printStackTrace) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> header = new HashMap<>();
                if (!token.isEmpty()) {
                    header.put("Token", token);
                }
                return header;
            }

        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(arrayRequest);
    }

    private void setContactData(BiServiceContactData contactData) {
        if (binding.get() == null) {
            return;
        }

        if (contactData != null) {
            binding.get().setContacts(contactData);
            binding.get().setHasContackts(true);
//zz
            String manager_guid = contactData.getManagerGuid();

            Retrofit client = Library.NewRetrofitclient();
            Get_photo_guid get_photo_guid = client.create(Get_photo_guid.class);
            Call<Object> call = get_photo_guid.getdecodeImage(token,manager_guid);

            call.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    Gson gson = new Gson();
                    String decodeobj = Library.getAsString(gson.toJson(response.body()));
                    setManagerPhoto(decodeobj);
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    binding.get().managerAvatar.setImageResource(R.drawable.home_title_gradient_background);
                }
            });

            String director_guid = contactData.getDirectorGuid();

            Get_photo_guid get_photo_guids = client.create(Get_photo_guid.class);
            Call<Object> calla = get_photo_guids.getdecodeImage(token,director_guid);

            calla.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    Gson gson = new Gson();
                    String decodeobj = Library.getAsString(gson.toJson(response.body()));
                    setDirectorPhoto(decodeobj);
                    //setDirectorPhoto(contactData.getDirectorServSluzhbPhoto());
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    binding.get().directorAvatar.setImageResource(R.drawable.home_title_gradient_background);
                }
            });


//            setManagerPhoto(contactData.getManagerPhoto());
//            setDirectorPhoto(contactData.getDirectorServSluzhbPhoto());
        } else {
            showHasNoContackts();
        }
    }

    private void showHasNoContackts() {
        binding.get().setHasContackts(false);
    }

    private void setDirectorPhoto(String directorServSluzhbPhoto) {
        Log.d("TEST", "d_" + directorServSluzhbPhoto);
        if (directorServSluzhbPhoto != null) {
            binding.get().directorAvatar.setImageBitmap(Bitmap.createScaledBitmap(baseToBitmap(directorServSluzhbPhoto), 200, 200, false));
        }
    }

    private void setManagerPhoto(String managerPhoto) {
        Log.d("TEST", "m_" + managerPhoto);
        if (managerPhoto != null) {
            binding.get().managerAvatar.setImageBitmap(Bitmap.createScaledBitmap(baseToBitmap(managerPhoto), 50, 50, false));
        }
    }

    private Bitmap baseToBitmap(String base64) {
        byte[] temp = Base64.decode(base64, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(temp, 0, temp.length);
    }

    public void launchPhone(View view) {
        String phoneNum = binding.get().getContacts().getManagerPhone();
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phoneNum));
        startActivity(intent);
    }

    private void downloadPriseList(View view) {
        String fileName = File.separator + "Prise list.pdf";
        File file = new File(getActivity().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS) + fileName);
        if (file.exists()) {
            openPdf(file, "application/pdf");
        } else {
            viewerViewModel.setPriseListPdfId(token);
        }
    }

    private void openPdf(File file, String type) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        Uri uri = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID + ".fileprovider", file);
        intent.setDataAndType(uri, type);

        PackageManager pm = getActivity().getPackageManager();
        if (intent.resolveActivity(pm) != null) {
            startActivity(intent);
        } else {
            Toast.makeText(getActivity(), "Не удалось открыть файл", Toast.LENGTH_SHORT).show();
        }
    }

}
