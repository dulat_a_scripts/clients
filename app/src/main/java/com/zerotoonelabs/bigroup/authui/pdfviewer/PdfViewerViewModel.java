package com.zerotoonelabs.bigroup.authui.pdfviewer;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;

import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.repository.FlatRepository;
import com.zerotoonelabs.bigroup.util.AbsentLiveData;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.Resource;

import java.io.File;

import javax.inject.Inject;

public class PdfViewerViewModel extends AndroidViewModel {

    private final MutableLiveData<PdfId> pdfId;
    private final MutableLiveData<StatusPdfID> statusPdfId;
    private final MutableLiveData<PdfId> apartmentSheetPdfId;
    private final MutableLiveData<TPassportId> tPassportId;
    private final MutableLiveData<String> priseListPdfId;

    private final LiveData<Resource<File>> pdf;
    private final LiveData<Resource<File>> statusPdf;
    private final LiveData<Resource<File>> apartmentSheetPdf;
    private final LiveData<Resource<File>> tPassportPdf;
    private final LiveData<Resource<File>> priseListPdf;

    @Inject
    public PdfViewerViewModel(Application application, FlatRepository repository) {
        super(application);
        pdfId = new MutableLiveData<>();
        apartmentSheetPdfId = new MutableLiveData<>();
        tPassportId = new MutableLiveData<>();
        statusPdfId = new MutableLiveData<>();
        priseListPdfId = new MutableLiveData<>();

        statusPdf = Transformations.switchMap(statusPdfId, input -> {
            if (input.isEmpty()) return AbsentLiveData.create();
            return repository.loadStatusPdf(application.getApplicationContext(), input.token, input.contractId, input.propertyId, input.type);
        });
        pdf = Transformations.switchMap(pdfId, input -> {
            if (input.isEmpty()) return AbsentLiveData.create();
            return repository.loadPdf(application.getApplicationContext(), input.token, input.pdfId);
        });
        //xx
        apartmentSheetPdf = Transformations.switchMap(apartmentSheetPdfId, input -> {
            if (input.isEmpty()) return AbsentLiveData.create();
            return repository.loadApartmentSheetPdf(application.getApplicationContext(), input.token, input.pdfId);
        });

        tPassportPdf = Transformations.switchMap(tPassportId, input -> {
            if (input.isEmpty()) return AbsentLiveData.create();

            if(Library.propertyid != 0){
                return repository.loadApartmentSheetPdf(application.getApplicationContext(), input.token, Library.propertyid);
            }else{
                return repository.loadTechPassportPdf(application.getApplicationContext(), input.token, input.propertyId, input.contractId);
            }

        });

        priseListPdf = Transformations.switchMap(priseListPdfId, input -> {
            if (input.isEmpty()) return AbsentLiveData.create();
            return repository.loadPriseListPdf(application.getApplicationContext(), priseListPdfId.getValue());
        });
    }

    void setPdfId(String token, int Id) {
        PdfId update = new PdfId(token, Id);
        if (Objects.equals(pdfId.getValue(), update)) return;
        pdfId.setValue(update);
    }

    void setApartmentSheetPdfId(String token, int propertyId) {
        PdfId update = new PdfId(token, propertyId);
        if (Objects.equals(apartmentSheetPdfId.getValue(), update)) return;
        apartmentSheetPdfId.setValue(update);
    }

    void setStatusPdfId(String token, String contractId, int propertyId, int type) {
        StatusPdfID update = new StatusPdfID(token, contractId, propertyId, type);
        if (Objects.equals(statusPdfId.getValue(), update)) return;
        statusPdfId.setValue(update);
    }

    void settPassportId(String token, String contractId, int propertyId) {
        TPassportId update = new TPassportId(token, contractId, propertyId);
        if (Objects.equals(tPassportId.getValue(), update)) return;
        tPassportId.setValue(update);
    }

    public void setPriseListPdfId(String token) {
        if (Objects.equals(priseListPdfId.getValue(), token)) return;
        priseListPdfId.setValue(token);
    }

    public LiveData<Resource<File>> getPdf() {
        return pdf;
    }

    public LiveData<Resource<File>> getStatusPdf() {
        return statusPdf;
    }

    public LiveData<Resource<File>> getApartmentSheetPdf() {
        return apartmentSheetPdf;
    }

    public LiveData<Resource<File>> gettPassportPdf() {
        return tPassportPdf;
    }

    public LiveData<Resource<File>> getPriseListPdf() {
        return priseListPdf;
    }

    static class TPassportId {
        String token;
        String contractId;
        int propertyId;

        public TPassportId(String token, String contractId, int propertyId) {
            this.token = token;
            this.contractId = contractId;
            this.propertyId = propertyId;
        }

        boolean isEmpty() {
            return token == null || contractId == null || propertyId == -1;
        }
    }

    static class StatusPdfID {
        String token;
        String contractId;
        int propertyId;
        int type;

        public StatusPdfID(String token, String contractId, int propertyId, int type) {
            this.token = token;
            this.contractId = contractId;
            this.propertyId = propertyId;
            this.type = type;
        }

        boolean isEmpty() {
            return token == null || contractId == null || propertyId == -1 || type == -1;
        }
    }

    static class PdfId {
        String token;
        int pdfId;

        public PdfId(String token, int pdfId) {
            this.token = token;
            this.pdfId = pdfId;
        }

        boolean isEmpty() {
            return token == null || pdfId == -1;
        }
    }
}
