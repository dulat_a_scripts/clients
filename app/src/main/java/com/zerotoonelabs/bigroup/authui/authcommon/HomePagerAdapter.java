package com.zerotoonelabs.bigroup.authui.authcommon;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.zerotoonelabs.bigroup.authui.home.FlatInfoFragment;
import com.zerotoonelabs.bigroup.authui.home.FlatStatusFragment;

public class HomePagerAdapter extends FragmentPagerAdapter {
    private FlatInfoFragment flatInfoFragment;
    private FlatStatusFragment flatStatusFragment;

    public HomePagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
        flatStatusFragment = new FlatStatusFragment();
        flatInfoFragment = new FlatInfoFragment();
    }

    public HomePagerAdapter(FragmentManager fm, String contractId, int propertyId) {
        super(fm);
        flatInfoFragment = FlatInfoFragment.newInstance(propertyId, contractId);
        flatStatusFragment = FlatStatusFragment.newInstance(propertyId, contractId);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return flatInfoFragment;
        } else {
            return flatStatusFragment;
        }
    }


    public void loadOtherFlat(int propertyId, String contractId){
        if (flatInfoFragment != null && flatStatusFragment != null) {


                flatInfoFragment.loadOtherFlat(propertyId, contractId);
                flatStatusFragment.loadOtherFlat(propertyId, contractId);


        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title;
        if (position == 0) {
            title = "Информация";
        } else {
            title = "Активность";
        }
        return title;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
