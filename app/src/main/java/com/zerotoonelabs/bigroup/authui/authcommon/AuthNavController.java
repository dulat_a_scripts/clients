package com.zerotoonelabs.bigroup.authui.authcommon;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.api.PaymentScheduleResponse;
import com.zerotoonelabs.bigroup.authui.SingleFragmentActivity;
import com.zerotoonelabs.bigroup.authui.biserviceauthoruzed.AuthorizedBiServiceFragment;
import com.zerotoonelabs.bigroup.authui.biserviceauthoruzed.CallServiceFragment;
import com.zerotoonelabs.bigroup.authui.biserviceauthoruzed.NewApplicationFragment;
import com.zerotoonelabs.bigroup.authui.payment.PaymentFragment;
import com.zerotoonelabs.bigroup.authui.payment.PublicUtilitiesFragment;
import com.zerotoonelabs.bigroup.authui.payment.utilities.UtilitiesFragment;
import com.zerotoonelabs.bigroup.authui.salesdepartment.SalesDepartmentFragment;
import com.zerotoonelabs.bigroup.entity.CloudPayRequest;
import com.zerotoonelabs.bigroup.entity.RequrentPayment;
import com.zerotoonelabs.bigroup.entity.club.Partner;
import com.zerotoonelabs.bigroup.fragments.RequrentPaymentFragment;
import com.zerotoonelabs.bigroup.fragments.club.AllPartnersFragment;
import com.zerotoonelabs.bigroup.fragments.club.BiClubFragment;
import com.zerotoonelabs.bigroup.fragments.club.PartnerDetailsFragment;
import com.zerotoonelabs.bigroup.fragments.peny.CloudPaymentFragment;
import com.zerotoonelabs.bigroup.ui.apartments.ApartmentFragment;
import com.zerotoonelabs.bigroup.ui.favourites.FavoriteApartmentsFragment;
import com.zerotoonelabs.bigroup.ui.profile.ProfileFragment;
import com.zerotoonelabs.bigroup.ui.realestate.RealEstateFragment;
import com.zerotoonelabs.bigroup.ui.sales.SalesFragment;
import com.zerotoonelabs.bigroup.ui.sendrequest.SendRequestFragment;
import com.zerotoonelabs.bigroup.entity.User;

import javax.inject.Inject;

public class AuthNavController {
    private final FragmentManager fragmentManager;
    private final int containerId;

    @Inject
    public AuthNavController(SingleFragmentActivity activity) {
        this.containerId = R.id.flContent;
        fragmentManager = activity.getSupportFragmentManager();
    }

    public void navigateToPayment(String token, int propertyId, String contractId) {
        PaymentFragment fragment = PaymentFragment.newInstance(token, contractId, propertyId);
        String tag = "Оплата";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToPublicUtilities(String token, int propertyId, String contractId) {
        PublicUtilitiesFragment fragment = PublicUtilitiesFragment.newInstance(token, contractId, propertyId);
        String tag = "Коммунальные услуги";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToUtilitiesFragment(String token, int invoiceId) {
        UtilitiesFragment fragment = UtilitiesFragment.newInstance(invoiceId, token);
        String tag = "Коммунальные услуги";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(containerId, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    public void navigateToNewBiServiceApplication() {
        NewApplicationFragment fragment = new NewApplicationFragment();
        String tag = "Новая заявка BI Service";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(containerId, fragment, tag)
                .addToBackStack(tag)
                .commitAllowingStateLoss();
    }

    public void navigateToCallService() {
        CallServiceFragment fragment = new CallServiceFragment();
        String tag = "Контактные данные";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                //.add(containerId, fragment, tag)
                .addToBackStack(tag)
               .commitAllowingStateLoss();

    }

    public void navigateToBiService() {
        AuthorizedBiServiceFragment fragment = AuthorizedBiServiceFragment.newInstance(null, null);
        String tag = "BI Service";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }
//zz
    public void navigateToBiServicetwo() {
        AuthorizedBiServiceFragment fragment = AuthorizedBiServiceFragment.newInstance(null, null);
        String tag = "BI Service";
        fragmentManager.beginTransaction()
               // .addToBackStack(null)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commit();
    }

    public void navigateToSales(String token) {
        SalesDepartmentFragment fragment = SalesDepartmentFragment.newInstance(token);
        String tag = "Заявка в отдел продаж";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToCall() {
        SendRequestFragment fragment = new SendRequestFragment();
        String tag = "Заказать звонок";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToSales() {
        SalesFragment fragment = new SalesFragment();
        String tag = SalesFragment.class.getSimpleName();
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToReqPaymentDetails(RequrentPayment payment) {
        RequrentPaymentFragment fragment = RequrentPaymentFragment.newInstance(payment);
        String tag = "Подписка";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToPartnerDetails(Partner partner) {
        PartnerDetailsFragment fragment = PartnerDetailsFragment.newInstance(partner);
        String tag = "Детали партнера";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToCloudPayment(CloudPayRequest response) {
        CloudPaymentFragment fragment = CloudPaymentFragment.newInstance(response);
        String tag = "Платеж";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToAllPartners(String type) {
        AllPartnersFragment fragment = AllPartnersFragment.newInstance(type);
        String tag = "Все партнеры";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToApartments() {
        ApartmentFragment fragment = new ApartmentFragment();
        String tag = "Подбор квартиры";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToRealEstate() {
        RealEstateFragment fragment = new RealEstateFragment();
        String tag = "Объекты";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToProfile(User user) {
        ProfileFragment fragment = ProfileFragment.newInstance(user);
        String tag = "Профиль";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToFavorites(String token) {
        FavoriteApartmentsFragment fragment = FavoriteApartmentsFragment.newInstance(token);
        String tag = "Избранное";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();

    }
}
