package com.zerotoonelabs.bigroup.authui.biserviceauthoruzed;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.authui.SingleFragmentActivity;
import com.zerotoonelabs.bigroup.authui.authcommon.AuthNavController;
import com.zerotoonelabs.bigroup.databinding.FragmentAuthorizedBiserviceBinding;
import com.zerotoonelabs.bigroup.di.Injectable;
import com.zerotoonelabs.bigroup.ui.common.AuthorizedBiServicePagerAdapter;
import com.zerotoonelabs.bigroup.util.AutoClearedValue;

import javax.inject.Inject;

/**
 * Created by mac on 15.11.2017.
 */

public class AuthorizedBiServiceFragment extends  Fragment implements Injectable {

    @Inject
    AuthNavController navigationController;
    public static final String TOKEN_KEY = "token";

    public AuthorizedBiServiceFragment() {
    }

    public static AuthorizedBiServiceFragment newInstance(String param1, String param2) {
        AuthorizedBiServiceFragment fragment = new AuthorizedBiServiceFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    AutoClearedValue<FragmentAuthorizedBiserviceBinding> binding;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        FragmentAuthorizedBiserviceBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_authorized_biservice, container, false);
        binding = new AutoClearedValue<>(this, dataBinding);
        return dataBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        AuthorizedBiServicePagerAdapter adapter = new AuthorizedBiServicePagerAdapter(getActivity().getSupportFragmentManager());
        binding.get().viewPager.setAdapter(adapter);
        binding.get().tabLayout.setupWithViewPager(binding.get().viewPager);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.call_bi_service, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.call_service:
                navigationController.navigateToCallService();
                break;
            case 16908332:
                SingleFragmentActivity activity = (SingleFragmentActivity) getActivity();
                activity.onSupportNavigateUp();
                break;
        }
        return true;
    }
}
