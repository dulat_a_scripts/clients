package com.zerotoonelabs.bigroup.authui.authcommon;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.UtilityBillsDetailItemBinding;
import com.zerotoonelabs.bigroup.ui.common.DataBoundListAdapter;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.Utility;

public class UtilityListAdapter extends DataBoundListAdapter<Utility, UtilityBillsDetailItemBinding> {

    private final UtilityClickCallback apartmentClickCallback;

    public UtilityListAdapter(UtilityClickCallback apartmentClickCallback) {
        this.apartmentClickCallback = apartmentClickCallback;
    }

    @Override
    protected UtilityBillsDetailItemBinding createBinding(ViewGroup parent) {
        UtilityBillsDetailItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.utility_bills_detail_item,
                        parent, false);
        binding.getRoot().setOnClickListener(v -> {
            Utility repo = binding.getUtility();
            if (repo != null && apartmentClickCallback != null) {
                apartmentClickCallback.onClick(repo);
            }
        });

        binding.editText3.setOnClickListener(v -> {
            Utility utility = binding.getUtility();
            int position = binding.getPosition();
            if (apartmentClickCallback != null) {
                apartmentClickCallback.onPayClick(utility, position);
            }
        });

        return binding;
    }

    @Override
    protected void bind(UtilityBillsDetailItemBinding binding, Utility item, int position) {
        binding.setUtility(item);
        binding.setPosition(position);
    }

    @Override
    protected boolean areItemsTheSame(Utility oldItem, Utility newItem) {
        return Objects.equals(oldItem.getId(), newItem.getId());
    }

    @Override
    protected boolean areContentsTheSame(Utility oldItem, Utility newItem) {
        return Objects.equals(oldItem.getName(), newItem.getName());
    }

    public interface UtilityClickCallback {
        void onClick(Utility utility);
        void onPayClick(Utility utility, int position);
    }
}