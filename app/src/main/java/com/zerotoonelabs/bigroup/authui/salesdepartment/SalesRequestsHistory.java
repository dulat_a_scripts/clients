package com.zerotoonelabs.bigroup.authui.salesdepartment;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.zerotoonelabs.bigroup.BuildConfig;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.authui.authcommon.SalesRequestHistoryListAdapter;
import com.zerotoonelabs.bigroup.databinding.SalesRequestsHistoryFragBinding;
import com.zerotoonelabs.bigroup.di.Injectable;
import com.zerotoonelabs.bigroup.entity.SalesRequestHistory;
import com.zerotoonelabs.bigroup.util.AutoClearedValue;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Base64;

import javax.inject.Inject;

public class SalesRequestsHistory extends Fragment implements Injectable {

    private static final String TOKEN_KEY = "token";

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    AutoClearedValue<SalesRequestsHistoryFragBinding> binding;
    AutoClearedValue<SalesRequestHistoryListAdapter> adapter;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        SalesViewModel viewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(SalesViewModel.class);
        Bundle args = getArguments();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String propertyGuid = sharedPreferences.getString("propertyGuid", null);
        String userId = sharedPreferences.getString("applicationUserId", null);
        if (args != null && args.containsKey(TOKEN_KEY) && propertyGuid != null && userId != null) {
            viewModel.setHistoryId(args.getString(TOKEN_KEY), userId, propertyGuid);
        } else {
            viewModel.setHistoryId(null, null, null);
        }

        SalesRequestHistoryListAdapter adapter = new SalesRequestHistoryListAdapter(clickCallback);
        this.adapter = new AutoClearedValue<>(this, adapter);
        binding.get().historyList.setAdapter(adapter);
        initObservables(viewModel);
    }

    SalesRequestHistoryListAdapter.SalesRequestHistoryClickCallback clickCallback = new SalesRequestHistoryListAdapter.SalesRequestHistoryClickCallback() {
        @Override
        public void onClick(SalesRequestHistory item) {
            try {
                if (item.pdfUrl != null) {
                    byte[] ar = android.util.Base64.decode(item.pdfUrl, 0);

                    File file = new File(getActivity().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS) + "spravka_" + item.number);

                    OutputStream out = new FileOutputStream(file.getPath());
                    out.write(ar);
                    out.close();


                    openPdf(file, "application/pdf");
                }
            } catch (Exception e){
                e.printStackTrace();
            }



        }
    };

    private void openPdf(File file, String type) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        Uri uri = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID + ".fileprovider", file);
        intent.setDataAndType(uri, type);

        PackageManager pm = getActivity().getPackageManager();
        if (intent.resolveActivity(pm) != null) {
            startActivity(intent);
        } else {
            Toast.makeText(getActivity(), "Не удалось открыть файл", Toast.LENGTH_SHORT).show();
        }
    }

    private void initObservables(SalesViewModel viewModel) {
        viewModel.getHistory().observe(this, listResource -> {
            binding.get().setResource(listResource);
            binding.get().setResultCount(listResource == null || listResource.data == null ? 0 : listResource.data.size());
            adapter.get().replace(listResource == null ? null : listResource.data);
            binding.get().executePendingBindings();
        });
    }

    public SalesRequestsHistory() {
        // Required empty public constructor
    }

    public static SalesRequestsHistory newInstance(String param1) {
        SalesRequestsHistory fragment = new SalesRequestsHistory();
        Bundle args = new Bundle();
        args.putString(TOKEN_KEY, param1);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        SalesRequestsHistoryFragBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.sales_requests_history_frag, container, false);

        binding = new AutoClearedValue<>(this, dataBinding);
        return dataBinding.getRoot();
    }
}
