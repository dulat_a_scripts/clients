package com.zerotoonelabs.bigroup.authui.biserviceauthoruzed;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.zerotoonelabs.bigroup.entity.BankAccount;
import com.zerotoonelabs.bigroup.entity.User;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.ui.common.AuthorizedBiServiceListAdapter;
import com.zerotoonelabs.bigroup.entity.BiServiceApplication;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.zerotoonelabs.bigroup.authui.HomeActivity.CURRENT_FLAT;
import static com.zerotoonelabs.bigroup.authui.HomeActivity.CURRENT_USER;

/**
 * Created by mac on 15.11.2017.
 */

public class ApplicationsHistoryFragment extends Fragment implements SharedPreferences.OnSharedPreferenceChangeListener {


    private SharedPreferences sharedPreferences;
    Gson gson;
    String token;
    User currentUser;
    int propertyId;
    Context context;

    TextView message;
    RecyclerView recyclerView;

    public ApplicationsHistoryFragment() {
        // Required empty public constructor
    }

    public static ApplicationsHistoryFragment newInstance(String param1, String param2) {
        ApplicationsHistoryFragment fragment = new ApplicationsHistoryFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = getContext();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        View view = inflater.inflate(R.layout.fragment_sales_department_history, container, false);
        currentUser = getCurrentUser();
        message = view.findViewById(R.id.message);
        initRecView(view);
        return view;
    }


    private void initRecView(View view) {
        recyclerView = view.findViewById(R.id.recycleView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        AuthorizedBiServiceListAdapter adapter = new AuthorizedBiServiceListAdapter(null, false);
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getBiServiceApplications(propertyId);
        this.sharedPreferences.registerOnSharedPreferenceChangeListener(this);
    }

    private User getCurrentUser() {
        String user = sharedPreferences.getString(CURRENT_USER, "");
        gson = new Gson();
        propertyId = sharedPreferences.getInt(CURRENT_FLAT, -1);
        return gson.fromJson(user, User.class);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    private void getBiServiceApplications(int propertyId) {
        String propertyGuid = sharedPreferences.getString("propertyGuid", null);
        if (propertyGuid != null) {
            requestBiServiceApplications(propertyGuid);
        } else {

            try{
                String contractId = currentUser.getUserName();
                String url = "http://webapi.bi-group.org/api/v1/MobileClient/GetActiveBankingAccount?property_id=" + propertyId + "&contractId=" + contractId;
                JsonArrayRequest arrayRequest = new JsonArrayRequest(url, response -> {
                    List<BiServiceApplication> res = new ArrayList<>();
                    try {
                        JSONObject object = response.getJSONObject(0);
                        BankAccount bankAccount = gson.fromJson(object.toString(), BankAccount.class);
                        requestBiServiceApplications(bankAccount.getPropertyGuid());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    setContentToAdapters(res);
                }, Throwable::printStackTrace) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> header = new HashMap<>();
                        token = sharedPreferences.getString("token", "");
                        System.out.println(token);
                        if (!token.isEmpty()) {
                            header.put("Token", token);
                        }
                        return header;
                    }
                };
                RequestQueue queue = Volley.newRequestQueue(context);
                queue.add(arrayRequest);
            }catch (Exception e){

            }

        }
    }

    private void requestBiServiceApplications(String propertyGuid) {

        try{
            String appUserId = currentUser.getApplicationUserId();
            String url = "http://webapi.bi-group.org/api/v1/MobileClient/getBiServiceRequestsClosed?ApplicationUserId=" + appUserId + "&PropertyGuid=" + propertyGuid;
            JsonArrayRequest arrayRequest = new JsonArrayRequest(url, response -> {
                List<BiServiceApplication> res = new ArrayList<>();
                for (int i = 0; i < response.length(); i++) {
                    try {
                        BiServiceApplication application = gson.fromJson(response.get(i).toString(), BiServiceApplication.class);
                        res.add(application);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                setContentToAdapters(res);
            }, Throwable::printStackTrace) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> header = new HashMap<>();
                    token = sharedPreferences.getString("token", "");
                    System.out.println(token);
                    if (!token.isEmpty()) {
                        header.put("Token", token);
                    }
                    return header;
                }
            };

            RequestQueue queue = Volley.newRequestQueue(context);
            queue.add(arrayRequest);
        }catch (Exception e){

        }


    }

    private void setContentToAdapters(List<BiServiceApplication> res) {        if (!res.isEmpty()) {
        message.setVisibility(View.GONE);
        recyclerView.setAdapter(new AuthorizedBiServiceListAdapter(res, true));
    } else {
        message.setVisibility(View.VISIBLE);
        recyclerView.setAdapter(new AuthorizedBiServiceListAdapter(res, true));
    }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(CURRENT_FLAT)){
            propertyId = sharedPreferences.getInt(CURRENT_FLAT, -1);
            getBiServiceApplications(propertyId);
        }
        this.sharedPreferences.registerOnSharedPreferenceChangeListener(this);
    }
}
