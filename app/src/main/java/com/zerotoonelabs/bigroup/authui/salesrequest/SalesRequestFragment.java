package com.zerotoonelabs.bigroup.authui.salesrequest;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;

public class SalesRequestFragment extends Fragment {

    public SalesRequestFragment() {
        // Required empty public constructor
    }

    public static SalesRequestFragment newInstance(String param1, String param2) {
        SalesRequestFragment fragment = new SalesRequestFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.sales_request_frag, container, false);
    }

}
