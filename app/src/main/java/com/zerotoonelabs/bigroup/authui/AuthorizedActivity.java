package com.zerotoonelabs.bigroup.authui;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.adapters.FlatListAdapter;
import com.zerotoonelabs.bigroup.authui.biserviceauthoruzed.AuthorizedBiServiceFragment;
import com.zerotoonelabs.bigroup.authui.biserviceauthoruzed.FragmentInteraction;
import com.zerotoonelabs.bigroup.authui.salesdepartment.SalesDepartmentFragment;
import com.zerotoonelabs.bigroup.ui.MainActivity;
import com.zerotoonelabs.bigroup.util.CollapsibleCard;
import com.zerotoonelabs.bigroup.entity.Flat;
import com.zerotoonelabs.bigroup.entity.User;

import java.util.List;

import javax.inject.Inject;

import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.zerotoonelabs.bigroup.authui.HomeActivity.CURRENT_FLAT;
import static com.zerotoonelabs.bigroup.authui.HomeActivity.CURRENT_USER;

public class AuthorizedActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, FragmentInteraction, HasSupportFragmentInjector {

    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Override
    public DispatchingAndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }

    User currentUser;
    SharedPreferences sharedPreferences;
    FlatListAdapter flatAdapter;
    Gson gson;

    private DrawerLayout mDrawerLayout;
    AuthorizedBiServiceFragment biServiceFragment = new AuthorizedBiServiceFragment();
    SalesDepartmentFragment salesDepartmentFragment = new SalesDepartmentFragment();
    CollapsibleCard collapsibleCard;


    String LOG = "AuthorizedActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(LOG, "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authorized);
        getCurrentUser();

        setFlats();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setupToolbar(toolbar);
        setupDrawer();
        setToggle(toolbar);
    }

    private void setFlats() {
        String token = sharedPreferences.getString("token", null);
        String contractId = sharedPreferences.getString("contractId", null);
        HomeViewModel viewModel = ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel.class);

        if (token != null && contractId != null) {
            viewModel.setFlats(token, contractId);
        } else {
            logout();
        }
        viewModel.getFlats().observe(this, listResource -> {
            viewModel.setMyFlats(listResource == null ? null : listResource.data);
            if (listResource != null && listResource.data != null) {
                setupAdapter(listResource.data, contractId);
            }
        });
    }

    public void getCurrentUser() {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String userString = sharedPreferences.getString(CURRENT_USER, "");
        if (!userString.isEmpty()) {
            gson = new Gson();
            currentUser = gson.fromJson(userString, User.class);
        }
    }

    private void setupToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    private void setupDrawer() {
        mDrawerLayout = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        if (currentUser != null) {
            initHeaderView(headerView);
        }
        navigationView.setNavigationItemSelectedListener(this);
        if (getIntent().hasExtra("id")) {
            onNavigationItemSelected(navigationView.getMenu().findItem(getIntent().getIntExtra("id", 0)));
        }
    }

    private void setToggle(Toolbar toolbar) {
        mDrawerLayout = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();
    }

    private void setupAdapter(List<Flat> flats, String contractId) {
        setFlatAdapter(flats);
        collapsibleCard.setCardList(flatAdapter);
        flatAdapter.replace(flats);
        chosenFlat(flats);
    }

    private void initHeaderView(View headerView) {
        setUserInfo(headerView);
        collapsibleCard = headerView.findViewById(R.id.collapsible_card);
    }

    private void setUserInfo(View headerView) {
        TextView fullName = headerView.findViewById(R.id.fullName);
        CircleImageView avatar = headerView.findViewById(R.id.avatar);
        try {
            fullName.setText(currentUser.getLastName());
            avatar.setImageBitmap(currentUser.getBitmapAva());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setFlatAdapter(List<Flat> flats) {
        flatAdapter = new FlatListAdapter(flat -> {
            for (int i = 0; i < flats.size(); i++) {
                if (flats.get(i).equals(flat)) {
                    flats.get(i).setSelected(true);
                } else {
                    flats.get(i).setSelected(false);
                }
            }
            String title = newTitle(flat);
            storeChosenFlat(flat);
            collapsibleCard.setCardTitle(title);
            collapsibleCard.expand();
            flatAdapter.replace(flats);
            flatAdapter.notifyDataSetChanged();
            this.closeDrawer();
        });

    }

    private void storeChosenFlat(Flat flat) {
        sharedPreferences.edit().putInt(CURRENT_FLAT, flat.getPropertyId()).commit();
    }

    private String newTitle(Flat flat) {
        String title = "";
        title += flat.getRealEstateName() + "\n" + flat.getType().toLowerCase() + " " + flat.getFlatNumber();
        return title;
    }

    private Flat chosenFlat(List<Flat> flats) {
        int currentFlat = sharedPreferences.getInt(CURRENT_FLAT, -1);
        if (currentFlat > 0) {
            for (Flat flat : flats) {
                if (flat.getPropertyId() == currentFlat) {
                    flat.setSelected(true);
                    collapsibleCard.setCardTitle(newTitle(flat));
                    return flat;
                }
            }
        }
        return null;
    }

    @Override
    public boolean onSupportNavigateUp() {
        openDrawer();
        return true;
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    private void openDrawer() {
        if (mDrawerLayout != null) {
            mDrawerLayout.openDrawer(GravityCompat.START);
        }
    }

    private void closeDrawer() {
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        Log.i(LOG, "onNavigationItemSelected(" + item.getItemId() + ")");
        int id = item.getItemId();
        setTitle(item.getTitle());
        switch (id) {
            case R.id.nav_main:
                startActivity(new Intent(this, HomeActivity.class));
                break;
            case R.id.nav_bi_service:
                getSupportFragmentManager().beginTransaction().replace(R.id.container, biServiceFragment).commit();
                break;
//            case R.id.nav_book_call:
//                break;
//            case R.id.nav_spec_offers:
//                break;
//            case R.id.nav_favourites:
//                break;
            case R.id.nav_payment:
                break;
//            case R.id.nav_sales_dep:
//                getSupportFragmentManager().beginTransaction().replace(R.id.container, salesDepartmentFragment).commit();
//                break;
//            case R.id.nav_ref:
//                break;
//            case R.id.nav_fird_flat:
//                break;
//            case R.id.logo:
//                logout();
//                break;
        }

        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void logout() {
        PreferenceManager.getDefaultSharedPreferences(this)
                .edit()
                .clear()
                .apply();
        launchMainAct();
    }

    public void setTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void changeFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.container, fragment).addToBackStack("").commitAllowingStateLoss();
    }

    private void launchMainAct() {
        Intent i = new Intent(this, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        finish();
    }
}
