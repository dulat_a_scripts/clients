package com.zerotoonelabs.bigroup.authui.salesdepartment;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.SendSalesRequestFragBinding;
import com.zerotoonelabs.bigroup.di.Injectable;
import com.zerotoonelabs.bigroup.util.AutoClearedValue;
import com.zerotoonelabs.bigroup.entity.SalesRequestType;
import com.zerotoonelabs.bigroup.entity.Status;

import javax.inject.Inject;

public class SendSalesRequestFragment extends Fragment implements Injectable {

    private static final String TOKEN_KEY = "token";

    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private SalesViewModel viewModel;
    AutoClearedValue<ArrayAdapter<SalesRequestType>> adapter;
    AutoClearedValue<SendSalesRequestFragBinding> binding;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(SalesViewModel.class);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String iin = sharedPreferences.getString("iin", null);
        String propertyGuid = sharedPreferences.getString("propertyGuid", null);
        String contractGuid = sharedPreferences.getString("contractId", null);
        viewModel.setContractGuid(contractGuid);
        viewModel.setIin(iin);
        viewModel.setPropertyGuid(propertyGuid);

        Bundle args = getArguments();
        if (args != null && args.containsKey(TOKEN_KEY)) {
            viewModel.setToken(args.getString(TOKEN_KEY));
        }

        ArrayAdapter<SalesRequestType> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.adapter = new AutoClearedValue<>(this, adapter);
        binding.get().catalogSpinner.setAdapter(adapter);

        initObservables(viewModel);

        binding.get().button12.setOnClickListener(v -> {
            sendSalesRequestPressed(viewModel);
        });
    }

    private void sendSalesRequestPressed(SalesViewModel viewModel) {

        SalesRequestType salesRequestType = (SalesRequestType) binding.get().catalogSpinner.getSelectedItem();
        if (salesRequestType == null) {
            viewModel.setMessage(R.string.select_sales_request_type_message);
            return;
        }
        viewModel.setSalesRequestId(salesRequestType.guid);
    }

    private void initObservables(SalesViewModel viewModel) {
        viewModel.getRequestTypes().observe(this, listResource -> {
            if (listResource != null && listResource.data != null) {
                adapter.get().addAll(listResource.data);
            } else {
                adapter.get().clear();
            }
            adapter.get().notifyDataSetChanged();
        });

        viewModel.getSalesRequestMessage().observe(this, resource -> {
            binding.get().setResource(resource);
            if (resource != null && resource.data != null && resource.status == Status.SUCCESS) {
                showMessage(resource.data, true);
                return;
            }
            if (resource != null && resource.message != null && resource.status == Status.ERROR) {
                showMessage(resource.message, false);
            }
        });
    }

    private void showMessage(String message, boolean bool) {
        if (!isAdded()) return;

        new AlertDialog.Builder(getActivity())
                .setMessage(bool ? getString(R.string.sales_request_send_message, message) : message)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, null)
                .create()
                .show();
    }

    public SendSalesRequestFragment() {
        // Required empty public constructor
    }

    public static SendSalesRequestFragment newInstance(String param1) {
        SendSalesRequestFragment fragment = new SendSalesRequestFragment();
        Bundle args = new Bundle();
        args.putString(TOKEN_KEY, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        SendSalesRequestFragBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.send_sales_request_frag, container, false);
        binding = new AutoClearedValue<>(this, dataBinding);
        return dataBinding.getRoot();
    }
}
