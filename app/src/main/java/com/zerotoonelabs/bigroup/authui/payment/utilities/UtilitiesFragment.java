package com.zerotoonelabs.bigroup.authui.payment.utilities;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.authui.authcommon.UtilityListAdapter;
import com.zerotoonelabs.bigroup.databinding.UtilitiesFragBinding;
import com.zerotoonelabs.bigroup.di.Injectable;
import com.zerotoonelabs.bigroup.ui.PaymentFormActivity;
import com.zerotoonelabs.bigroup.util.AutoClearedValue;
import com.zerotoonelabs.bigroup.entity.Utility;

import javax.inject.Inject;

public class UtilitiesFragment extends Fragment implements Injectable {

    private static final String TOKEN_KEY = "token";
    private static final String INVOICE_KEY = "invoice";
    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private AutoClearedValue<UtilityListAdapter> adapter;
    private AutoClearedValue<UtilitiesFragBinding> binding;
    private UtilitiesViewModel viewModel;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(UtilitiesViewModel.class);
        Bundle args = getArguments();
        if (args != null && args.containsKey(TOKEN_KEY) && args.containsKey(INVOICE_KEY)) {
            String token = args.getString(TOKEN_KEY);
            int invoiceId = args.getInt(INVOICE_KEY);
            viewModel.setUtilityId(token, invoiceId);
        }
        setupAdapter();
        initObservable(viewModel);

        binding.get().button11.setOnClickListener(v -> {
            launchPayment();
        });
    }

    private void launchPayment() {
        int amount = viewModel.getTotalSum();
        if (amount == 0) return;

        Intent intent = new Intent(getActivity(), PaymentFormActivity.class);
        intent.putExtra(PaymentFormActivity.AMOUNT_KEY, amount);
        startActivity(intent);
    }

    private void setupAdapter() {
        UtilityListAdapter adapter = new UtilityListAdapter(new UtilityListAdapter.UtilityClickCallback() {
            @Override
            public void onClick(Utility utility) {

            }

            @Override
            public void onPayClick(Utility utility, int position) {
                showDialog(utility, position);
            }
        });
        this.adapter = new AutoClearedValue<>(this, adapter);
        binding.get().utilityList.setAdapter(adapter);
    }

    private void initObservable(UtilitiesViewModel viewModel) {
        viewModel.getUtilities().observe(this, listResource -> {
            binding.get().setResource(listResource);
            binding.get().setResultCount(listResource == null || listResource.data == null ? 0 : listResource.data.size());
            adapter.get().replace(listResource == null ? null : listResource.data);
            binding.get().executePendingBindings();
        });
    }

    private void showDialog(Utility utility, int position) {
        if (!isAdded()) return;

        View view = getLayoutInflater().inflate(R.layout.payment_dialog, null);
        final EditText editText = view.findViewById(R.id.editText4);
        if (utility.getTotalSum() != 0) {
            editText.setText(String.valueOf(utility.getTotalSum()));
        }

        new AlertDialog.Builder(getActivity())
                .setView(view)
                .setPositiveButton(android.R.string.ok, (dialog1, which) -> {
                    String input = editText.getText().toString();

                    int sum = Integer.parseInt(TextUtils.isEmpty(input) ? "0" : input);
                    utility.setTotalSum(sum);

                    adapter.get().notifyItemChanged(position);

                    binding.get().button11.setText(getString(R.string.pay_button_label, viewModel.getTotalSum() + ""));
                })
                .show();
    }

    public UtilitiesFragment() {
        // Required empty public constructor
    }

    public static UtilitiesFragment newInstance(int param1, String param2) {
        UtilitiesFragment fragment = new UtilitiesFragment();
        Bundle args = new Bundle();
        args.putInt(INVOICE_KEY, param1);
        args.putString(TOKEN_KEY, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        UtilitiesFragBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.utilities_frag, container, false);
        binding = new AutoClearedValue<>(this, dataBinding);

        return dataBinding.getRoot();
    }

}
