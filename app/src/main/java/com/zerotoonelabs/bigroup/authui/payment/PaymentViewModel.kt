package com.zerotoonelabs.bigroup.authui.payment

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import com.zerotoonelabs.bigroup.R
import com.zerotoonelabs.bigroup.repository.PaymentRepository
import com.zerotoonelabs.bigroup.ui.common.SingleLiveEvent
import com.zerotoonelabs.bigroup.util.AbsentLiveData
import com.zerotoonelabs.bigroup.util.Objects
import com.zerotoonelabs.bigroup.entity.*
import com.zerotoonelabs.bigroup.entity.kassa24.Invoices
import com.zerotoonelabs.bigroup.entity.kassa24.Service
import java.util.*
import javax.inject.Inject

class PaymentViewModel @Inject constructor(val repository: PaymentRepository) : ViewModel() {

    private var token: String? = null
    private var invoiceId: String? = null

    private val paymentId = MutableLiveData<PaymentId>()
    private val historyId = MutableLiveData<HistoryId>()
    private val userId = MutableLiveData<UserId>()
    private val transactionId = MutableLiveData<TransactionId>()
    private val message = SingleLiveEvent<Int>()


    private val bankAccount: LiveData<Resource<BankAccount>>
    private val history: LiveData<Resource<List<PaymentHistory>>>
    private val services: LiveData<Resource<Invoices.Invoice>>
    private val transaction: LiveData<Resource<PaymentResponse>>
    private val serviceIds: LiveData<Resource<List<ServiceId>>>

    init {
        bankAccount = Transformations.switchMap(paymentId) { input ->
            if (input.isEmpty()) AbsentLiveData.create<Resource<BankAccount>>()
            repository.loadBankAccount(input.token, input.propertyId, input.contractId)
        }

        history = Transformations.switchMap(historyId) {  input ->
            repository.loadPaymentHistory(input.token, input.bankAccountId)
        }

        services = Transformations.switchMap(userId) { input ->
            repository.loadCheckUser(input.serviceId, input.userId)
        }

        transaction = Transformations.switchMap(transactionId) { input ->
            repository.loadTransaction(input.serviceId, input.amount, input.userId, input.invoiceId, input.services)
        }

        serviceIds = Transformations.switchMap(paymentId) { input ->
            repository.loadServiceIds(input.token)
        }
    }

    fun setToken(input: String) {
        token = input
    }

    fun setPaymentId(contractId: String, propertyId: Int) {
        val token = this.token
        val update = PaymentId(token, contractId, propertyId)
        if (Objects.equals(paymentId.value, update)) return
        paymentId.value = update
    }

    fun setHistoryId(bankAccountId: Int) {
        val token = this.token

        val update = HistoryId(token!!, bankAccountId)
        if (Objects.equals(historyId.value, update)) return
        historyId.value = update
    }

    fun setUserId(serviceId: String, arg1: String) {
        val update = UserId(serviceId, arg1)
        if (Objects.equals(userId, update)) return
        userId.value = update
    }

    fun setTransactionId() {
        val amount = getTotalSum()
        val usrId = userId.value
        val services = getTransactionServices()
        val invoiceId = this.invoiceId ?: return
        if (amount <= 0 || services == null || services.isEmpty()) {
            setMessage(R.string.empty_amount_message)
            return
        }
        val update = TransactionId(usrId?.serviceId, usrId?.userId, amount, invoiceId, services)
        if (Objects.equals(transactionId.value, update)) return
        transactionId.setValue(update)
    }

    fun getInvoiceId() = invoiceId

    fun setInvoiceId(input: String) {
        invoiceId = input
    }

    fun getTransaction() = transaction

    fun getBankAccount() = bankAccount

    fun getHistory() = history

    fun getServices() = services

    fun getServiceIds() = serviceIds

    private fun getTransactionServices(): List<Service>? {
        val invoice = getServices().getValue()
        val serviceList = invoice?.data?.services?.service
        if (serviceList == null || serviceList.isEmpty()) return null
        val services = ArrayList<Service>()
        for (service in serviceList) {
            if (service.totalSum > 0) services.add(service)
        }
        return services
    }

    fun getTotalSum(): Int {
        var total = 0
        val services = getServices().getValue()
        if (services?.data != null) {
            val serviceList = services.data.services.service
            for (service in serviceList) {
                total += service.getTotalSum()
            }
        }
        return total
    }

    fun getMessage() = message

    fun setMessage(resId: Int?) {
        if (resId != null) message.value = resId
    }

    fun retry() {
        userId.value = userId.value
    }


    private data class PaymentId(val token: String?, val contractId: String?, val propertyId: Int) {
        fun isEmpty(): Boolean {
            return token.isNullOrEmpty() || contractId.isNullOrEmpty()
        }
    }

    private data class HistoryId(val token: String, val bankAccountId: Int)

    private data class TransactionId(val serviceId: String?,
                                     val userId: String?,
                                     val amount: Int,
                                     val invoiceId: String?,
                                     val services: List<Service>?)

    private data class UserId(val serviceId: String, val userId: String)
}