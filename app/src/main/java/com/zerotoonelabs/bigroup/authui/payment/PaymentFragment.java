package com.zerotoonelabs.bigroup.authui.payment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.authui.authcommon.AuthNavController;
import com.zerotoonelabs.bigroup.di.Injectable;

import javax.inject.Inject;

public class PaymentFragment extends Fragment implements Injectable {

    private static final String TOKEN_KEY = "token";
    private static final String CONTRACT_KEY = "contract_id";
    private static final String PROPERTY_KEY = "property_id";

    @Inject
    AuthNavController navigationController;

    public PaymentFragment() {
        // Required empty public constructor
    }

    public static PaymentFragment newInstance(String token, String contractId, int propertyId) {
        PaymentFragment fragment = new PaymentFragment();
        Bundle args = new Bundle();
        args.putString(TOKEN_KEY, token);
        args.putString(CONTRACT_KEY, contractId);
        args.putInt(PROPERTY_KEY, propertyId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.payment_frag, container, false);
        Bundle args = getArguments();

        if (args != null && args.containsKey(TOKEN_KEY) && args.containsKey(CONTRACT_KEY) && args.containsKey(PROPERTY_KEY)) {
            String token = args.getString(TOKEN_KEY);
            String contractId = args.getString(CONTRACT_KEY);
            int propertyId = args.getInt(PROPERTY_KEY);

            view.findViewById(R.id.public_utilities).setOnClickListener((View v) -> {
                navigationController.navigateToPublicUtilities(token, propertyId, contractId);
            });
        }
        return view;
    }

}
