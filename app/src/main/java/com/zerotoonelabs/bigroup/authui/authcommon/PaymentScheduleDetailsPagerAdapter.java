package com.zerotoonelabs.bigroup.authui.authcommon;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.zerotoonelabs.bigroup.authui.paymentscheduledetails.PaymentSchedule1Fragment;
import com.zerotoonelabs.bigroup.authui.paymentscheduledetails.PaymentSchedule2Fragment;
import com.zerotoonelabs.bigroup.fragments.peny.PenyItemFragment;

public class PaymentScheduleDetailsPagerAdapter extends FragmentPagerAdapter {
    private String contractId;
    private String token;
    private String propertyGuid;
    private int propertyId;

    public PaymentScheduleDetailsPagerAdapter(FragmentManager fm, String token, String contractId, String propertyGuid) {
        super(fm);
        this.contractId = contractId;
        this.token = token;
        this.propertyGuid = propertyGuid;

    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0)
            return PaymentSchedule1Fragment.newInstance(token, contractId, propertyGuid);
        else if (position == 1)
            return PaymentSchedule2Fragment.newInstance(token, contractId, propertyGuid);
        else
            return PenyItemFragment.newInstance2(token, contractId, propertyGuid);

    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title;
        if (position == 0)
            title = "График платежей";
        else if (position == 1)
            title = "История";
        else
            title = "Подписки";
        return title;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
