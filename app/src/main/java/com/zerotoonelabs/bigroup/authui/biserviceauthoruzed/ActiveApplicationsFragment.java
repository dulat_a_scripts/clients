package com.zerotoonelabs.bigroup.authui.biserviceauthoruzed;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.entity.BankAccount;
import com.zerotoonelabs.bigroup.entity.User;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.ui.common.AuthorizedBiServiceListAdapter;
import com.zerotoonelabs.bigroup.entity.BiServiceApplication;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import static com.zerotoonelabs.bigroup.authui.HomeActivity.CURRENT_FLAT;
import static com.zerotoonelabs.bigroup.authui.HomeActivity.CURRENT_USER;

/**
 * Created by mac on 15.11.2017.
 */

public class ActiveApplicationsFragment extends Fragment implements SharedPreferences.OnSharedPreferenceChangeListener {


    private SharedPreferences sharedPreferences;
    Gson gson;
    String token;
    User currentUser;
    int propertyId;
    Context context;

    TextView message;
    RecyclerView recyclerView;
    Button button;
    ProgressBar progressBar;
    SwipeRefreshLayout swipeRefreshLayout;

    AuthorizedBiServiceListAdapter adapter;


    public ActiveApplicationsFragment() {
        // Required empty public constructor
    }

    public static ActiveApplicationsFragment newInstance(String param1, String param2) {
        ActiveApplicationsFragment fragment = new ActiveApplicationsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = getContext();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        View view = inflater.inflate(R.layout.fragment_active_applications, container, false);

        progressBar = view.findViewById(R.id.progressBar5);

        progressBar.setVisibility(View.VISIBLE);

        View view1 = inflater.inflate(R.layout.universal_dialog,container,false);

        Library.newcontext = getActivity();

        Library.viewdialog = view1;

        Library.activity = view;

        Library.token = sharedPreferences.getString("token", null);

        String applicationUserId = sharedPreferences.getString("applicationUserId", null);
        Library.applicationUserId = applicationUserId;

        swipeRefreshLayout = view.findViewById(R.id.refresh);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //getBiServiceApplications(propertyId);
                String propertyGuid = sharedPreferences.getString("propertyGuid", null);

                    requestBiServiceApplications(propertyGuid);
                   // adapter.notifyDataSetChanged();


                swipeRefreshLayout.setRefreshing(false);
            }
        });


        currentUser = getCurrentUser();
        message = view.findViewById(R.id.message);
        initRecView(view);
        initButton(view);
        return view;
    }

    private void initButton(View view) {
        button = view.findViewById(R.id.b_new_application);
        button.setOnClickListener(v -> {
            startActivityForResult(new Intent(getContext(), NewBiServiceApplicationActivity.class), 10);
            //navigationController.navigateToNewBiServiceApplication();
           // Intent intent = new Intent(new Intent(getContext(), NewBiServiceApplicationActivity.class));
            //intent.putExtra(NewBiServiceApplicationActivity.PROFILE_EXTRA, user);
            //startActivity(intent);
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 10) {
            getBiServiceApplications(propertyId);
        }
    }

    private void initRecView(View view) {
        recyclerView = view.findViewById(R.id.recycleView);
        //recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        AuthorizedBiServiceListAdapter adapter = new AuthorizedBiServiceListAdapter(null, true);
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getBiServiceApplications(propertyId);
        this.sharedPreferences.registerOnSharedPreferenceChangeListener(this);
    }

    private User getCurrentUser() {
        String user = sharedPreferences.getString(CURRENT_USER, "");
        gson = new Gson();
        propertyId = sharedPreferences.getInt(CURRENT_FLAT, -1);
        return gson.fromJson(user, User.class);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    public void getBiServiceApplications(int propertyId) {
        String propertyGuid = sharedPreferences.getString("propertyGuid", null);
        if (propertyGuid != null) {
            requestBiServiceApplications(propertyGuid);
        } else {
            String contractId = currentUser.getUserName();
            String url = "http://webapi.bi-group.org/api/v1/MobileClient/GetActiveBankingAccount?property_id=" + propertyId + "&contractId=" + contractId;
            JsonArrayRequest arrayRequest = new JsonArrayRequest(url, response -> {
                List<BiServiceApplication> res = new ArrayList<>();
                try {
                    JSONObject object = response.getJSONObject(0);
                    BankAccount bankAccount = gson.fromJson(object.toString(), BankAccount.class);
                    requestBiServiceApplications(bankAccount.getPropertyGuid());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                setContentToAdapters(res);
            }, Throwable::printStackTrace) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> header = new HashMap<>();
                    token = sharedPreferences.getString("token", "");

                    Library.token = token;


                    System.out.println(token);
                    if (!token.isEmpty()) {
                        header.put("Token", token);
                    }
                    return header;
                }
            };
            RequestQueue queue = Volley.newRequestQueue(context);
            queue.add(arrayRequest);
        }
    }

    public void requestBiServiceApplications(String propertyGuid) {
        String appUserId = currentUser.getApplicationUserId();
        String url = "http://webapi.bi-group.org/api/v1/MobileClient/getBiServiceRequestsActual?ApplicationUserId=" + appUserId + "&PropertyGuid=" + propertyGuid;
        JsonArrayRequest arrayRequest = new JsonArrayRequest(url, response -> {
            List<BiServiceApplication> res = new ArrayList<>();
            for (int i = 0; i < response.length(); i++) {
                try {
                    BiServiceApplication application = gson.fromJson(response.get(i).toString(), BiServiceApplication.class);
                    res.add(application);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            setContentToAdapters(res);
        }, Throwable::printStackTrace) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> header = new HashMap<>();
                token = sharedPreferences.getString("token", "");
                System.out.println(token);
                if (!token.isEmpty()) {
                    header.put("Token", token);
                }
                return header;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(arrayRequest);
    }

    private void setContentToAdapters(List<BiServiceApplication> res) {

        if (res.size() > 0) {

            List<BiServiceApplication> news = new ArrayList();

            for(int z = 0;z < res.size();z++){

                String opisanie = res.get(z).getOpisanie();
                String data = res.get(z).getData();


                if(opisanie.length() > 2 || data.length() > 2 || !opisanie.isEmpty() || !data.isEmpty()){
                    news.add(res.get(z));
                }

            }


            adapter = new AuthorizedBiServiceListAdapter(news, true);

            message.setVisibility(View.GONE);
            recyclerView.setAdapter(adapter);

            progressBar.setVisibility(View.GONE);
        }
        //else {
//            message.setVisibility(View.GONE);
//            recyclerView.setAdapter(new AuthorizedBiServiceListAdapter(res, true));
//            progressBar.setVisibility(View.GONE);
//        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(CURRENT_FLAT)) {
            propertyId = sharedPreferences.getInt(CURRENT_FLAT, -1);
            getBiServiceApplications(propertyId);
        }
        this.sharedPreferences.registerOnSharedPreferenceChangeListener(this);
    }
}
