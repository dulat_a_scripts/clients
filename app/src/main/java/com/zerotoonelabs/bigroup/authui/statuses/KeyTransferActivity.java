package com.zerotoonelabs.bigroup.authui.statuses;

import android.arch.lifecycle.LiveData;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.zerotoonelabs.bigroup.BiApplication;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.api.ApiResponse;
import com.zerotoonelabs.bigroup.authui.SingleHomeFragmentActivity;
import com.zerotoonelabs.bigroup.repository.BiRepository;
import com.zerotoonelabs.bigroup.entity.KeyTransfer;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KeyTransferActivity extends AppCompatActivity {

    @Inject
    BiRepository repository;

    LiveData<ApiResponse<List<KeyTransfer>>> liveData;
    TextView textView1, textView2, textView3, textView;

    String contractId, propertyId, token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_key_transfer);
        ((BiApplication) getApplicationContext()).dispatchingAndroidInjector.inject(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        textView = findViewById(R.id.textView);
        textView1 = findViewById(R.id.textView1);
        textView2 = findViewById(R.id.textView2);
        textView3 = findViewById(R.id.textView3);
        Intent intent = getIntent();
        contractId = intent.getStringExtra("contractId");
        propertyId = intent.getStringExtra("propertyId");
        token = PreferenceManager.getDefaultSharedPreferences(this).getString("token", null);

        repository.loadKeyTransfer(token, propertyId).enqueue(new Callback<List<KeyTransfer>>() {
            @Override
            public void onResponse(Call<List<KeyTransfer>> call, Response<List<KeyTransfer>> response) {
                List<KeyTransfer> data = response.body();
                if (data == null || data.isEmpty() || data.get(0) == null) return;

                KeyTransfer keyTransfer = data.get(0);
                bindData(keyTransfer);
            }

            @Override
            public void onFailure(Call<List<KeyTransfer>> call, Throwable t) {

            }
        });

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(KeyTransferActivity.this, SingleHomeFragmentActivity.class);
                intent.putExtra(SingleHomeFragmentActivity.STATUS_PDF, 5);
                startActivity(intent);
            }
        });

    }

    private void bindData(@NonNull KeyTransfer keyTransfer) {
        textView1.setText(keyTransfer.getNomerDogovora());
        String price = formatPrice(keyTransfer.getSummaDogovora()) + " тг.";
        textView2.setText(price);
        textView3.setText(formatDate(keyTransfer.getDataNachPeredachiKluchei()));
    }

    private String formatPrice(Integer price) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
        symbols.setGroupingSeparator(' ');
        formatter.setDecimalFormatSymbols(symbols);
        return formatter.format(price);
    }

    private String formatDate(String input) {
        if (input == null) return "";
//                2019-03-03T00:00:00
        SimpleDateFormat outputSdf = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
        SimpleDateFormat inputSdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());

        try {
            Date inputDate = inputSdf.parse(input);
            return outputSdf.format(inputDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
