package com.zerotoonelabs.bigroup.authui.home;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;

import com.zerotoonelabs.bigroup.repository.FlatRepository;
import com.zerotoonelabs.bigroup.ui.common.SingleLiveEvent;
import com.zerotoonelabs.bigroup.util.AbsentLiveData;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.Flat;
import com.zerotoonelabs.bigroup.entity.FlatScheme;
import com.zerotoonelabs.bigroup.entity.Manager;
import com.zerotoonelabs.bigroup.entity.Resource;

import java.util.List;

import javax.inject.Inject;

public class FlatViewModel extends AndroidViewModel {

    private final MutableLiveData<FlatId> flatId;
    private final MutableLiveData<ManagerId> managerId;
    private final MutableLiveData<ManagerId> managerId2;
    private SingleLiveEvent<Integer> message = new SingleLiveEvent<Integer>();
    private final LiveData<Resource<Flat>> flat;
    private final LiveData<Resource<Manager>> manager;
    private final LiveData<Resource<Manager>> manager2;
    private final LiveData<Resource<List<FlatScheme>>> flatSchemes;
    private String token;

    @Inject
    public FlatViewModel(final Application application, final FlatRepository repository) {
        super(application);
        flatId = new MutableLiveData<>();
        managerId = new MutableLiveData<>();
        managerId2 = new MutableLiveData<>();
        flat = Transformations.switchMap(flatId, input -> {
            if (input.isEmpty()) {
                return AbsentLiveData.create();
            }
            return repository.loadFlat(application.getApplicationContext(), input.token, input.propertyId, input.contractId);
        });
        flatSchemes = Transformations.switchMap(flatId, input -> {
            if (input.isEmpty()) return AbsentLiveData.create();
            return repository.loadFlatSchemes(input.token, input.propertyId, input.contractId);
        });
        manager = Transformations.switchMap(managerId, input -> {
            if (input.isEmpty()) {
                return AbsentLiveData.create();
            }
            return repository.loadManager(input.token, input.realEstateId, input.contractId);
        });
        manager2 = Transformations.switchMap(managerId2, input -> {
            if (input.isEmpty()) {
                return AbsentLiveData.create();
            }
            return repository.loadManager2(input.token, input.realEstateId, input.contractId);
        });
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    void setFlatId(int propertyId, String contractId) {
        String token = this.token;
        FlatId update = new FlatId(token, propertyId, contractId);
        if (Objects.equals(flatId.getValue(), update)) return;
        flatId.setValue(update);
    }

    void setManagerId(String realEstateId, String contractId) {
        String token = this.token;
        ManagerId update = new ManagerId(token, realEstateId, contractId);
        if (Objects.equals(managerId.getValue(), update)) return;
        managerId.setValue(update);

    }

    void setManagerId2(String respId) {
        String contractId = managerId.getValue().contractId;
        String token = this.token;
        ManagerId update = new ManagerId(token, respId, contractId);
        if (Objects.equals(managerId2.getValue(), update)) return;
        managerId2.setValue(update);
    }

    public LiveData<Resource<Flat>> getFlat() {
        return flat;
    }

    public LiveData<Resource<Manager>> getManager() {
        return manager;
    }

    public LiveData<Resource<Manager>> getManager2() {
        return manager2;
    }

    public LiveData<Resource<List<FlatScheme>>> getFlatSchemes() {
        return flatSchemes;
    }

    public SingleLiveEvent<Integer> getMessage() {
        return message;
    }

    public void setMessage(Integer resId) {
        if (resId != null)
            message.setValue(resId);
    }

    static class FlatId {
        String token;
        int propertyId;
        String contractId;

        public FlatId(String token, int propertyId, String contractId) {
            this.token = token;
            this.propertyId = propertyId;
            this.contractId = contractId;
        }

        boolean isEmpty() {
            return token == null || propertyId == 0 || contractId == null;
        }
    }

    static class ManagerId {
        String token;
        String realEstateId;
        String contractId;

        public ManagerId(String token, String realEstateId, String contractId) {
            this.token = token;
            this.realEstateId = realEstateId;
            this.contractId = contractId;
        }

        boolean isEmpty() {
            return token == null || realEstateId == null || contractId == null;
        }
    }
}