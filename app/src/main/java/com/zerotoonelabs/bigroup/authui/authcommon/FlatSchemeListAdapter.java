package com.zerotoonelabs.bigroup.authui.authcommon;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.FlatSchemeItemBinding;
import com.zerotoonelabs.bigroup.ui.common.DataBoundListAdapter;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.FlatScheme;

public class FlatSchemeListAdapter extends DataBoundListAdapter<FlatScheme, FlatSchemeItemBinding> {

    private final FlatSchemeClickCallback flatClickCallback;

    public FlatSchemeListAdapter(FlatSchemeClickCallback flatClickCallback) {
        this.flatClickCallback = flatClickCallback;
    }

    @Override
    protected FlatSchemeItemBinding createBinding(ViewGroup parent) {
        FlatSchemeItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.flat_scheme_item,
                        parent, false);
        binding.getRoot().setOnClickListener(v -> {
            FlatScheme repo = binding.getFlatScheme();
            if (repo != null && flatClickCallback != null) {
                flatClickCallback.onClick(repo);
            }
        });
        return binding;
    }


    @Override
    protected void bind(FlatSchemeItemBinding binding, FlatScheme item, int position) {
        binding.setFlatScheme(item);
    }

    @Override
    protected boolean areItemsTheSame(FlatScheme oldItem, FlatScheme newItem) {
        return Objects.equals(oldItem.id, newItem.id);
    }

    @Override
    protected boolean areContentsTheSame(FlatScheme oldItem, FlatScheme newItem) {
        return Objects.equals(oldItem.name, newItem.name);
    }

    public interface FlatSchemeClickCallback {
        void onClick(FlatScheme flat);
    }
}