package com.zerotoonelabs.bigroup.authui.salesdepartment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.ui.common.SalesDepartmentPagerAdapter;

public class SalesDepartmentFragment extends android.support.v4.app.Fragment {

    private static final String TOKEN_KEY = "token";

    public SalesDepartmentFragment() {
        // Required empty public constructor
    }

    private TabLayout tabLayout;
    private ViewPager viewPager;

    public static SalesDepartmentFragment newInstance(String param1) {
        SalesDepartmentFragment fragment = new SalesDepartmentFragment();
        Bundle args = new Bundle();
        args.putString(TOKEN_KEY, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle args = getArguments();
        if (args != null && args.containsKey(TOKEN_KEY)) {
            String token = args.getString(TOKEN_KEY);
            SalesDepartmentPagerAdapter adapter = new SalesDepartmentPagerAdapter(getChildFragmentManager(), token);
            setupAdapter(adapter);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sales_department, container, false);
        tabLayout = view.findViewById(R.id.tab_layout);
        viewPager = view.findViewById(R.id.container);
        return view;
    }

    private void setupAdapter(SalesDepartmentPagerAdapter adapter) {
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager, true);
    }
}