package com.zerotoonelabs.bigroup.authui;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.zerotoonelabs.bigroup.repository.FlatRepository;
import com.zerotoonelabs.bigroup.util.AbsentLiveData;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.Flat;
import com.zerotoonelabs.bigroup.entity.Resource;

import java.util.List;

import javax.inject.Inject;

public class HomeViewModel extends ViewModel {

    private final MutableLiveData<FlatsId> flatsId;
    private final LiveData<Resource<List<Flat>>> flats;
    private final MutableLiveData<Flat> currentFlat;

    private List<Flat> myFlats;

    //1 q vizov
    @Inject
    public HomeViewModel(FlatRepository repository) {
        currentFlat = new MutableLiveData<>();
        flatsId = new MutableLiveData<>();
        flats = Transformations.switchMap(flatsId, input -> {
            if (input.isEmpty()) return AbsentLiveData.create();
            return repository.loadFlats(input.token, input.contractId);
        });
    }

    public MutableLiveData<Flat> getCurrentFlat() {
        return currentFlat;
    }

    public void setCurrentFlat(Flat flat) {
        if (flat == null) return;

        try{
            currentFlat.setValue(flat);
        }catch (Exception e){

        }


    }

    public List<Flat> updateFlats(Flat selectedFlat) {
        List<Flat> flats = myFlats;
        if (flats == null) return null;

        for (Flat flat : flats) {
            flat.setSelected(flat.equals(selectedFlat));
        }
        setCurrentFlat(selectedFlat);

        return flats;
    }

    void setMyFlats(List<Flat> myFlats) {
        this.myFlats = myFlats;
    }

    void setFlats(String token, String contractId) {
        FlatsId update = new FlatsId(contractId, token);
        if (Objects.equals(flatsId.getValue(), update)) return;
        flatsId.setValue(update);
    }

    public LiveData<Resource<List<Flat>>> getFlats() {
        return flats;
    }

    static class FlatsId {
        String contractId;
        String token;

        public FlatsId(String contractId, String token) {
            this.contractId = contractId;
            this.token = token;
        }

        boolean isEmpty() {
            return token == null || contractId == null;
        }
    }
}
