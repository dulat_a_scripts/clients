package com.zerotoonelabs.bigroup.authui.home;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.api.ApiResponse;
import com.zerotoonelabs.bigroup.api.PaymentScheduleResponse;
import com.zerotoonelabs.bigroup.api.RestClient;
import com.zerotoonelabs.bigroup.authui.SingleHomeFragmentActivity;
import com.zerotoonelabs.bigroup.authui.paymentschedule.PaymentScheduleViewModel;
import com.zerotoonelabs.bigroup.authui.statuses.IntroductoryStatusActivity;
import com.zerotoonelabs.bigroup.authui.statuses.KeyTransferActivity;
import com.zerotoonelabs.bigroup.authui.statuses.ServiceStatusActivity;
import com.zerotoonelabs.bigroup.authui.statuses.StatusGetDocumentsActivity;
import com.zerotoonelabs.bigroup.authui.statuses.StatusOpendDoorActivity;
import com.zerotoonelabs.bigroup.databinding.FlatStatusFragBinding;
import com.zerotoonelabs.bigroup.di.Injectable;
import com.zerotoonelabs.bigroup.repository.BiRepository;
import com.zerotoonelabs.bigroup.util.AutoClearedValue;
import com.zerotoonelabs.bigroup.entity.StatusAuth.StatusAuth;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FlatStatusFragment extends Fragment implements Injectable {

    private static final String PROPERTY_KEY = "property_id";
    private static final String CONTRACT_KEY = "contract_id";

    private TextView currentPaymentAmount;
    private TextView currentPaymentContractId;


    @Inject
    ViewModelProvider.Factory viewModelFactory;
    AutoClearedValue<FlatStatusFragBinding> binding;
    private PaymentScheduleViewModel mViewModel;

    public FlatStatusFragment() {}

    @Inject
    BiRepository repository;
    MutableLiveData<ApiResponse<StatusAuth>> liveData = new MutableLiveData<>();
    //LiveData<ApiResponse<StatusAuth>> liveData;

    String token, contractId;
    int propertyId;

    public static FlatStatusFragment newInstance(int propertyId, String contractId) {
        FlatStatusFragment fragment = new FlatStatusFragment();
        Bundle args = new Bundle();
        args.putInt(PROPERTY_KEY, propertyId);
        args.putString(CONTRACT_KEY, contractId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FlatStatusFragBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.flat_status_frag, container, false);
        binding = new AutoClearedValue<>(this, dataBinding);

        View view = inflater.inflate(R.layout.flat_status_frag, container, false);

        currentPaymentAmount = view.findViewById(R.id.textView126);
        currentPaymentContractId = view.findViewById(R.id.textView127);

        return dataBinding.getRoot();
    }

    private void initObservables(PaymentScheduleViewModel viewModel) {
        viewModel.getPaymentSchedule().observe(this, resource -> {
            binding.get().setPayment(resource == null || resource.data == null ? null : resource.data);
            binding.get().executePendingBindings();
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(PaymentScheduleViewModel.class);
        token = PreferenceManager.getDefaultSharedPreferences(getContext()).getString("token", null);
        Bundle args = getArguments();
        if (args != null && args.containsKey(PROPERTY_KEY) && token != null) {
            int propertyId = args.getInt(PROPERTY_KEY);
            String contractId = args.getString(CONTRACT_KEY);
            this.propertyId = propertyId;
            this.contractId = contractId;

            mViewModel.setPropertyId(token, contractId, propertyId);
        } else {
            mViewModel.setPropertyId(null, null, -1);
        }


        initObservables(mViewModel);

        repository.loadFlatStatus(token, contractId, String.valueOf(propertyId)).observe(getActivity(), flatStatus -> {
            liveData.setValue(flatStatus);
        });

        liveData.observe(getActivity(), flatStatus -> {
            if (flatStatus != null && flatStatus.body != null) {
                if (flatStatus.body.getStatus1().getDate() != null) {
                    String date = formatDate(flatStatus.body.getStatus1().getDate().substring(0, 10));
                    flatStatus.body.getStatus1().setDate(date);
                } else
                    flatStatus.body.getStatus1().setDate(null);

                if (flatStatus.body.getStatus2().getDate() != null) {
                    String date = formatDate(flatStatus.body.getStatus2().getDate().substring(0, 10));
                    flatStatus.body.getStatus2().setDate(date);
                } else
                    flatStatus.body.getStatus2().setDate(null);

                if (flatStatus.body.getStatus3().getDate() != null) {
                    String date = formatDate(flatStatus.body.getStatus3().getDate().substring(0, 10));
                    flatStatus.body.getStatus3().setDate(date);
                } else
                    flatStatus.body.getStatus3().setDate(null);

                if (flatStatus.body.getStatus4().getDate() != null) {
                    String date = formatDate(flatStatus.body.getStatus4().getDate().substring(0, 10));
                    flatStatus.body.getStatus4().setDate(date);
                } else
                    flatStatus.body.getStatus4().setDate(null);

                if (flatStatus.body.getStatus5().getDate() != null) {
                    String date = formatDate(flatStatus.body.getStatus5().getDate().substring(0, 10));
                    flatStatus.body.getStatus5().setDate(date);
                } else
                    flatStatus.body.getStatus5().setDate(null);

                binding.get().setFlatStatus(flatStatus.body);
            }
        });
        binding.get().l1.setOnClickListener(view -> {
            Intent intent = new Intent(getContext(), IntroductoryStatusActivity.class);
            intent.putExtra("contractId", contractId);
            intent.putExtra("propertyId", String.valueOf(propertyId));
            startActivity(intent)
            ;
        });
        binding.get().l2.setOnClickListener(view -> {
            Intent intent = new Intent(getContext(), StatusOpendDoorActivity.class);
            startActivity(intent)
            ;
        });
        binding.get().l3.setOnClickListener(view -> {
            Intent intent = new Intent(getContext(), KeyTransferActivity.class);
            intent.putExtra("contractId", contractId);
            intent.putExtra("propertyId", String.valueOf(propertyId));
            startActivity(intent)
            ;
        });
        binding.get().l4.setOnClickListener(view -> {
            Intent intent = new Intent(getContext(), ServiceStatusActivity.class);
            startActivity(intent);
        });
        binding.get().l5.setOnClickListener(view -> {
            Intent intent = new Intent(getContext(), StatusGetDocumentsActivity.class);
            intent.putExtra("contractId", contractId);
            intent.putExtra("propertyId", String.valueOf(propertyId));
            startActivity(intent)
            ;
        });
        binding.get().textView125.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), SingleHomeFragmentActivity.class);
            intent.putExtra(SingleHomeFragmentActivity.PAYMENT_GRAPH, true);
            startActivity(intent);
        });
        binding.get().monthlyPayment.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), SingleHomeFragmentActivity.class);
            intent.putExtra(SingleHomeFragmentActivity.PAYMENT_SCHEDULE, true);
            startActivity(intent);
        });

        binding.get().textView225.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), SingleHomeFragmentActivity.class);
            intent.putExtra(SingleHomeFragmentActivity.PENY, true);
            startActivity(intent);
        });
    }

    private String formatDate(String input) {
        if (input == null) return null;
        SimpleDateFormat outputSdf = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
        SimpleDateFormat inputSdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

        try {
            Date inputDate = inputSdf.parse(input);
            return outputSdf.format(inputDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void loadOtherFlat(int propertyId, String contractId) {


            mViewModel.setPropertyId(token, contractId, propertyId);

            repository.loadFlatStatus(token, contractId, String.valueOf(propertyId)).observe(getActivity(), flatStatus -> {
                this.contractId = contractId;
                this.propertyId = propertyId;
                liveData.setValue(flatStatus);
            });




    }
}
