package com.zerotoonelabs.bigroup.authui.authcommon;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.InstalmentItemBinding;
import com.zerotoonelabs.bigroup.ui.common.DataBoundListAdapter;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.Instalment;

public class InstalmentListAdapter extends DataBoundListAdapter<Instalment, InstalmentItemBinding> {

    private final InstalmentClickCallback instalmentClickCallback;

    public InstalmentListAdapter(InstalmentClickCallback instalmentClickCallback) {
        this.instalmentClickCallback = instalmentClickCallback;
    }

    @Override
    protected InstalmentItemBinding createBinding(ViewGroup parent) {
        InstalmentItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.instalment_item,
                        parent, false);
        binding.getRoot().setOnClickListener(v -> {
            Instalment repo = binding.getInstalment();
            if (repo != null && instalmentClickCallback != null) {
                instalmentClickCallback.onClick(repo);
            }
        });

        return binding;
    }

    @Override
    protected void bind(InstalmentItemBinding binding, Instalment item, int position) {
        binding.setInstalment(item);
    }

    @Override
    protected boolean areItemsTheSame(Instalment oldItem, Instalment newItem) {
        return Objects.equals(oldItem.date, newItem.date);
    }

    @Override
    protected boolean areContentsTheSame(Instalment oldItem, Instalment newItem) {
        return Objects.equals(oldItem.stage, newItem.stage);
    }

    public interface InstalmentClickCallback {
        void onClick(Instalment utility);
    }
}