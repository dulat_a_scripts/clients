package com.zerotoonelabs.bigroup.authui.payment.utilities;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.zerotoonelabs.bigroup.repository.PaymentRepository;
import com.zerotoonelabs.bigroup.util.AbsentLiveData;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.Resource;
import com.zerotoonelabs.bigroup.entity.Utility;

import java.util.List;

import javax.inject.Inject;

public class UtilitiesViewModel extends ViewModel {

    private final MutableLiveData<UtilityId> utilityId;
    private final LiveData<Resource<List<Utility>>> utilities;

    @Inject
    public UtilitiesViewModel(PaymentRepository repository) {
        utilityId = new MutableLiveData<>();
        utilities = Transformations.switchMap(utilityId, input -> {
            if (input.isEmpty()) return AbsentLiveData.create();
            return repository.loadUtilities(input.token, input.invoiceId);
        });
    }

    void setUtilityId(String token, int invoiceId) {
        UtilityId update = new UtilityId(token, invoiceId);
        if (Objects.equals(utilityId.getValue(), update)) return;
        utilityId.setValue(update);
    }

    LiveData<Resource<List<Utility>>> getUtilities() {
        return utilities;
    }

    int getTotalSum() {
        int total = 0;
        if (getUtilities().getValue() != null && getUtilities().getValue().data != null) {
            List<Utility> utilities = getUtilities().getValue().data;
            for (Utility utility : utilities) {
                total += utility.getTotalSum();
            }
        }
        return total;
    }

    static class UtilityId {
        String token;
        int invoiceId;

        public UtilityId(String token, int invoiceId) {
            this.token = token;
            this.invoiceId = invoiceId;
        }

        boolean isEmpty() {
            return token == null || invoiceId == -1;
        }
    }
}
