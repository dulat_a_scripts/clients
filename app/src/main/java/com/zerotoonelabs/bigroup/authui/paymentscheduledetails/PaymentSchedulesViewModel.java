package com.zerotoonelabs.bigroup.authui.paymentscheduledetails;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.zerotoonelabs.bigroup.repository.PaymentRepository;
import com.zerotoonelabs.bigroup.util.AbsentLiveData;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.Instalment;
import com.zerotoonelabs.bigroup.entity.InstalmentHistory;
import com.zerotoonelabs.bigroup.entity.Resource;

import java.util.List;

import javax.inject.Inject;

public class PaymentSchedulesViewModel extends ViewModel {

    private final MutableLiveData<InstalmentId> instalmentId;
    private final LiveData<Resource<List<Instalment>>> instalments;
    private final LiveData<Resource<List<InstalmentHistory>>> instalmentHistory;

    @Inject
    public PaymentSchedulesViewModel(PaymentRepository repository) {
        instalmentId = new MutableLiveData<>();
        instalments = Transformations.switchMap(instalmentId, input -> {
            if (input.isEmpty()) return AbsentLiveData.create();
            return repository.loadPaymentSchedules(input.token, input.contractId, input.propertyGuid);
        });
        instalmentHistory = Transformations.switchMap(instalmentId, input -> {
            if (input.isEmpty()) return AbsentLiveData.create();
            return repository.loadPaymentScheduleHistory(input.token, input.contractId, input.propertyGuid);
        });
    }

    public LiveData<Resource<List<Instalment>>> getInstalments() {
        return instalments;
    }

    void setInstalmentId(String token, String contractId, String propertyGuid) {
        InstalmentId update = new InstalmentId(token, contractId, propertyGuid);
        if (Objects.equals(instalmentId.getValue(), update)) return;
        instalmentId.setValue(update);
    }

    public LiveData<Resource<List<InstalmentHistory>>> getInstalmentHistory() {
        return instalmentHistory;
    }


    static class InstalmentId {
        String token;
        String contractId;
        String propertyGuid;

        public InstalmentId(String token, String contractId, String propertyGuid) {
            this.token = token;
            this.contractId = contractId;
            this.propertyGuid = propertyGuid;
        }

        boolean isEmpty() {
            return token == null || contractId == null;
        }
    }
}
