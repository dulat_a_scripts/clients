package com.zerotoonelabs.bigroup.authui.authcommon;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.UtilityBillsHistoryItemBinding;
import com.zerotoonelabs.bigroup.ui.common.DataBoundListAdapter;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.PaymentHistory;

public class PaymentHistoryListAdapter extends DataBoundListAdapter<PaymentHistory, UtilityBillsHistoryItemBinding> {

    private final PaymentHistoryClickCallback apartmentClickCallback;

    public PaymentHistoryListAdapter(PaymentHistoryClickCallback apartmentClickCallback) {
        this.apartmentClickCallback = apartmentClickCallback;
    }

    @Override
    protected UtilityBillsHistoryItemBinding createBinding(ViewGroup parent) {
        UtilityBillsHistoryItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.utility_bills_history_item,
                        parent, false);
        binding.getRoot().setOnClickListener(v -> {
            PaymentHistory repo = binding.getPaymentHistory();
            if (repo != null && apartmentClickCallback != null) {
                apartmentClickCallback.onClick(repo);
            }
        });

        binding.textView49.setOnClickListener(v -> {

        });
        return binding;
    }

    @Override
    protected void bind(UtilityBillsHistoryItemBinding binding, PaymentHistory item, int position) {
        binding.setPaymentHistory(item);
    }

    @Override
    protected boolean areItemsTheSame(PaymentHistory oldItem, PaymentHistory newItem) {
        return Objects.equals(oldItem.getId(), newItem.getId());
    }

    @Override
    protected boolean areContentsTheSame(PaymentHistory oldItem, PaymentHistory newItem) {
        return Objects.equals(oldItem.getTimeStamp(), newItem.getTimeStamp());
    }

    public interface PaymentHistoryClickCallback {
        void onClick(PaymentHistory apartment);
    }
}