package com.zerotoonelabs.bigroup.authui.pdfviewer;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.zerotoonelabs.bigroup.BuildConfig;
import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.authui.HomeActivity;
import com.zerotoonelabs.bigroup.databinding.PdfViewerFragBinding;
import com.zerotoonelabs.bigroup.di.Injectable;
import com.zerotoonelabs.bigroup.util.AutoClearedValue;
import com.zerotoonelabs.bigroup.entity.Status;

import java.io.File;

import javax.inject.Inject;

public class PdfViewerFragment extends Fragment implements Injectable {

    private static final String TOKEN_KEY = "token";
    private static final String PDF_KEY = "pdf";
    private static final String PROPERTY_KEY = "property_id";
    private static final String STATUS_TYPE = "status_type";
    private static final String CONTRACT_KEY = "contract_id";

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private AutoClearedValue<PdfViewerFragBinding> binding;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        PdfViewerViewModel viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(PdfViewerViewModel.class);
        Bundle args = getArguments();
        if (args != null && args.containsKey(TOKEN_KEY)) {
            String token = args.getString(TOKEN_KEY);

            if (args.containsKey(STATUS_TYPE)) {
                int propertyId = args.getInt(PROPERTY_KEY);
                String contractId = args.getString(CONTRACT_KEY);
                int type = args.getInt(STATUS_TYPE);
                viewModel.setStatusPdfId(token, contractId, propertyId, type);
            } else if (args.containsKey(PDF_KEY)) {
                int pdfId = args.getInt(PDF_KEY);
                viewModel.setPdfId(token, pdfId);
            } else if (args.containsKey(CONTRACT_KEY) && args.containsKey(PROPERTY_KEY)) {
                String contractId = args.getString(CONTRACT_KEY);
                int propertyId = args.getInt(PROPERTY_KEY);
                viewModel.settPassportId(token, contractId, propertyId);
            } else if (args.containsKey(PROPERTY_KEY)) {
                int propertyId = args.getInt(PROPERTY_KEY);
                viewModel.setApartmentSheetPdfId(token, propertyId);
            }
        }

        viewModel.getStatusPdf().observe(this, fileResource -> {
            binding.get().setSearchResource(fileResource);
            if (fileResource != null && fileResource.data != null && fileResource.status == Status.SUCCESS) {
                openPdf(fileResource.data, "application/pdf");
            }
        });

        viewModel.getPdf().observe(this, fileResource -> {
            binding.get().setSearchResource(fileResource);
            if (fileResource != null && fileResource.data != null && fileResource.status == Status.SUCCESS) {
                openPdf(fileResource.data, "application/pdf");
            }
        });


//xx
        viewModel.gettPassportPdf().observe(this, fileResource -> {
            binding.get().setSearchResource(fileResource);
            if (fileResource != null && fileResource.data != null && fileResource.status == Status.SUCCESS) {

                if(Library.propertyid != 0){
                    openPdf(fileResource.data, "image/*");
                }else{
                    openPdf(fileResource.data, "application/pdf");
                }

            } else {
                viewModel.getApartmentSheetPdf().observe(this, fileResource1 -> {
                    binding.get().setSearchResource(fileResource1);
                    if (fileResource1 != null && fileResource1.data != null && fileResource1.status == Status.SUCCESS) {
                        openPdf(fileResource1.data, "image/*");
                    }
                });
            }
        });
    }

    private void openPdf(File file, String type) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        Uri uri = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID + ".fileprovider", file);
        intent.setDataAndType(uri, type);

        PackageManager pm = getActivity().getPackageManager();
        if (intent.resolveActivity(pm) != null) {
            startActivity(intent);
        } else {
            Toast.makeText(getActivity(), "Не удалось открыть файл", Toast.LENGTH_SHORT).show();
        }
    }

    public PdfViewerFragment() {
        // Required empty public constructor
    }

    public static PdfViewerFragment newInstance(String token, int pdfId) {
        PdfViewerFragment fragment = new PdfViewerFragment();
        Bundle args = new Bundle();
        args.putString(TOKEN_KEY, token);
        args.putInt(PDF_KEY, pdfId);
        fragment.setArguments(args);
        return fragment;
    }

    public static PdfViewerFragment newInstanceStatus(String token, int propertyID, String contractID, int type) {
        PdfViewerFragment fragment = new PdfViewerFragment();
        Bundle args = new Bundle();
        args.putString(TOKEN_KEY, token);
        args.putInt(PROPERTY_KEY, propertyID);
        args.putString(CONTRACT_KEY, contractID);
        args.putInt(STATUS_TYPE, type);
        fragment.setArguments(args);
        return fragment;
    }


    public static PdfViewerFragment newInstance2(String token, int propertyId) {
        PdfViewerFragment fragment = new PdfViewerFragment();
        Bundle args = new Bundle();
        args.putString(TOKEN_KEY, token);
        args.putInt(PROPERTY_KEY, propertyId);
        fragment.setArguments(args);
        return fragment;
    }

    public static PdfViewerFragment newInstance3(String token, String contractId, int propertyId) {
        PdfViewerFragment fragment = new PdfViewerFragment();
        Bundle args = new Bundle();
        args.putString(TOKEN_KEY, token);
        args.putInt(PROPERTY_KEY, propertyId);
        args.putString(CONTRACT_KEY, contractId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        PdfViewerFragBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.pdf_viewer_frag, container, false);
        binding = new AutoClearedValue<>(this, dataBinding);
        dataBinding.textView106.setOnClickListener(v -> {
            if (!isAdded()) return;
            navigateBack();
        });
        // Inflate the layout for this fragment
        return dataBinding.getRoot();
    }

    private void navigateBack() {
        Intent i = new Intent(getActivity(), HomeActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        new Handler().postDelayed(() -> {
            startActivity(i);
        }, 250);
    }

}
