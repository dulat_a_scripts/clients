package com.zerotoonelabs.bigroup.authui;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.squareup.picasso.Picasso;
import com.zerotoonelabs.bigroup.BiApplication;
import com.zerotoonelabs.bigroup.Library.Adapters.Notification_adapter;
import com.zerotoonelabs.bigroup.Library.Fragments.Notification_fragments;
import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.Library.Retrofit_request.GetCheckAccount;
import com.zerotoonelabs.bigroup.Library.Retrofit_request.GetFlat_int;
import com.zerotoonelabs.bigroup.Library.Retrofit_request.Get_notification_interf;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.adapters.FlatListAdapter;
import com.zerotoonelabs.bigroup.api.PicasoClient;
import com.zerotoonelabs.bigroup.api.RestClient;
import com.zerotoonelabs.bigroup.api.RetrofitApi;
import com.zerotoonelabs.bigroup.authui.authcommon.HomePagerAdapter;
import com.zerotoonelabs.bigroup.authui.authcommon.PaymentPagerAdapter;
import com.zerotoonelabs.bigroup.authui.home.FlatInfoFragment;
import com.zerotoonelabs.bigroup.authui.payment.PublicUtilitiesFragment;
import com.zerotoonelabs.bigroup.databinding.HomeActBinding;
import com.zerotoonelabs.bigroup.databinding.HomeNavHeaderBinding;
import com.zerotoonelabs.bigroup.entity.BankAccount;
import com.zerotoonelabs.bigroup.entity.FlatImgResponse;
import com.zerotoonelabs.bigroup.fragments.club.BiClubFragment;
import com.zerotoonelabs.bigroup.ui.MainActivity;
import com.zerotoonelabs.bigroup.ui.common.NavigationController;
import com.zerotoonelabs.bigroup.util.CommonDialog;
import com.zerotoonelabs.bigroup.entity.Flat;
import com.zerotoonelabs.bigroup.entity.User;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import io.fabric.sdk.android.services.concurrency.AsyncTask;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        HasSupportFragmentInjector, FlatInfoFragment.OnFlatLoaded {

    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;

    @Inject
    ViewModelProvider.Factory viewModelFactory;


    private HomeViewModel mViewModel;

    @Override
    public DispatchingAndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }

    public static final String CURRENT_USER = "currentUser";
    public static final String CURRENT_FLAT = "currentFlat";
    private DrawerLayout mDrawerLayout;
    private ImageView flatImageView;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private HomeActBinding binding;
    private HomeNavHeaderBinding headerBinding;
    private FlatListAdapter adapter;
    private HomePagerAdapter pagerAdapter;
    private PaymentPagerAdapter paymentPagerAdapter;
    SharedPreferences sharedPreferences = null;
    ViewPager viewPager = null;
    TabLayout tabLayout = null;
    String LOG = "HomeActivity";
    String token = "", contractId = "";
    int propertyId = 0;
    User user = null;
    FrameLayout frameLayout = null;
    String request_response = null;

    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(LOG, "onCreate()");
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.home_act);

        binding.authAppBar.setVisibility(View.GONE);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        user = getCurrentUser(sharedPreferences);
        token = sharedPreferences.getString("token", null);
        Library.SavePhoneMemoryOne("token",token);
        String userid = sharedPreferences.getString("applicationUserId", null);
        Library.SavePhoneMemoryOne("userid",userid);
        contractId = sharedPreferences.getString("contractId", null);
        Library.SavePhoneMemoryOne("contractId",contractId);
        if (user == null || token == null || contractId == null) {
            logout();
            return;
        }

        setupToolbarMain();

        pagerAdapter = new HomePagerAdapter(getSupportFragmentManager());

        viewPager = findViewById(R.id.viewpager);
        tabLayout = findViewById(R.id.tabs);
        frameLayout = findViewById(R.id.fragment_navigation_drawer);
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel.class);


        adapter = new FlatListAdapter(this::handleFlatClick);
        binding.flatList.setAdapter(adapter);
        initObservables(mViewModel, token, contractId);
//zz
        flatImageView = findViewById(R.id.backdrop);
        setupDrawer(user);

        CommonDialog commonDialog = new CommonDialog(this);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey("firstLogin")) {
            boolean firstLogin = bundle.getBoolean("firstLogin");
            if (firstLogin) {
                commonDialog.showThanksDialog();
            }
        }




    }


    private void getBankingAccountId() {
        Retrofit retrofitApi = new Retrofit.Builder()
                .baseUrl(RetrofitApi.url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RetrofitApi response = retrofitApi.create(RetrofitApi.class);
        response.getBankAccount(token, propertyId, contractId).enqueue(new Callback<List<BankAccount>>() {
            @Override
            public void onResponse(Call<List<BankAccount>> call, retrofit2.Response<List<BankAccount>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    response.body();

                    try{
                        sharedPreferences.edit().putInt("BankingAccountId", response.body().get(0).getId()).commit();
                        sharedPreferences.edit().putString("ContractNumber", response.body().get(0).getContractNumber()).commit();
                        sharedPreferences.edit().putString("propertyGuid", response.body().get(0).getPropertyGuid()).commit();
                        sharedPreferences.edit().putString("number", response.body().get(0).getNumber()).commit();
                        sharedPreferences.edit().putString("contractNumber", response.body().get(0).getContractNumber()).commit();
                        sharedPreferences.edit().putString("ercNumber", response.body().get(0).getErcNumber()).commit();
                        sharedPreferences.edit().putString("applicationUserId", response.body().get(0).getApplicationUserId()).commit();
                    }catch(Exception e){
                                    Log.i("error","error line 186");
                    }



                    //new Get_Download_Flat().execute();
                    //Log.i("dd","constractnumber" + response.body().toString());

                }
            }

            @Override
            public void onFailure(Call<List<BankAccount>> call, Throwable t) {
                Toast.makeText(HomeActivity.this, t.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

//zz


    private void initObservables(HomeViewModel viewModel, String token, String contractId) {
        viewModel.setFlats(token, contractId);

        viewModel.getFlats().observe(this, listResource -> {

            try{
                binding.setResource(listResource);
                viewModel.setMyFlats(listResource == null ? null : listResource.data);
                viewModel.setCurrentFlat(
                        listResource == null || listResource.data == null || listResource.data.size() < 1
                                ? null
                                : listResource.data.get(0));
                adapter.replace(listResource == null ? null : listResource.data);
                binding.executePendingBindings();
            }catch (Exception e){

            }



        });
//xx
        viewModel.getCurrentFlat().observe(this, flat -> {
            if (flat == null) return;


            try{
                collapsingToolbarLayout.setTitle(flat.getRealEstateName());
                headerBinding.textView129.setText(flat.getFlatTitle());
                pagerAdapter.loadOtherFlat(flat.getPropertyId(), flat.getContractId());
            }catch (Exception e){

            }

        });
    }

    private void handleFlatClick(Flat flat) {
        closeDrawer();
//xx
        boolean expanded = binding.getExpanded();
        binding.setExpanded(!expanded);
        headerBinding.setExpanded(!expanded);

        if (mViewModel.updateFlats(flat) != null) {
            adapter.replace(mViewModel.updateFlats(flat));
            adapter.notifyDataSetChanged();
            headerBinding.textView129.setText(flat.getFlatTitle());
            pagerAdapter.loadOtherFlat(flat.getPropertyId(), flat.getContractId());
        }
    }
//zz
    public User getCurrentUser(SharedPreferences sharedPreferences) {
        String userString = sharedPreferences.getString(CURRENT_USER, null);
        if (userString != null) {
            return new Gson().fromJson(userString, User.class);
        }
        return null;
    }

    private void setupToolbarMain() {
        collapsingToolbarLayout = findViewById(R.id.ctl);
        Toolbar mToolbar = findViewById(R.id.toolbarMain);
        setSupportActionBar(mToolbar);
        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        ab.setDisplayHomeAsUpEnabled(true);
        FrameLayout layout = collapsingToolbarLayout.findViewById(R.id.opacity);
        layout.setAlpha(0.3f);
//        ab.setTitle(name);
    }

    private void setupToolbar(int name) {
        Toolbar mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setTitle(name);
    }

    private void setupDrawer(User user) {
        mDrawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        View headerView = navigationView.getHeaderView(0);

        headerBinding = HomeNavHeaderBinding.bind(headerView);
        headerBinding.setFullName(user.getFullName());

        loadProfileImage(headerBinding.avatar, user);

        headerView.setOnClickListener(v -> {
            navigateToProfile(user);
        });
        TextView textView129 = headerView.findViewById(R.id.textView129);
        textView129.setOnClickListener(v -> {
            boolean expanded = binding.getExpanded();
            binding.setExpanded(!expanded);
            headerBinding.setExpanded(!expanded);
        });

    }

    // Setup the fragment content for FrameLayout
    private void setupFragmentLayout() {
        binding.appBarMain.setVisibility(View.GONE);
        binding.authAppBar.setVisibility(View.VISIBLE);
        frameLayout.setVisibility(View.VISIBLE);
    }

    private void goToMainPage(){
        binding.appBarMain.setVisibility(View.VISIBLE);
        binding.authAppBar.setVisibility(View.GONE);
        frameLayout.setVisibility(View.GONE);
        adapter = new FlatListAdapter(this::handleFlatClick);
        binding.flatList.setAdapter(adapter);
        initObservables(mViewModel, token, contractId);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Log.i(LOG, "onNavigationItemSelected(" + item.getItemId() +  ")");

        String extraKey;
        int id = item.getItemId();
        Intent intent = new Intent(this, SingleFragmentActivity.class);
        Fragment fragment = null;
        Class fragmentClass = null;
//        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        switch (id) {
            case R.id.nav_main:
                /*binding.appBarMain.setVisibility(View.VISIBLE);
                binding.authAppBar.setVisibility(View.GONE);
                frameLayout.setVisibility(View.GONE);
                adapter = new FlatListAdapter(this::handleFlatClick);
                binding.flatList.setAdapter(adapter);
                initObservables(mViewModel, token, contractId);*/
                goToMainPage();
                closeDrawer();
                return true;
            case R.id.nav_bi_service:
                extraKey = SingleFragmentActivity.BISERVICE_EXTRA;
                intent.putExtra(extraKey, true);
//                setupFragmentLayout();
//                fragment = AuthorizedBiServiceFragment.newInstance(null, null);
//                String serviceId = sharedPreferences.getString("serviceId", null);
//                if (isEmpty(serviceId)) {
//                    showMessageDialog(R.string.cant_open_bi_service);
//                    break;
//                }
                break;
            case R.id.nav_book_call:
                extraKey = SingleFragmentActivity.SEND_REQUEST_EXTRA;
                intent.putExtra(extraKey, true);
                break;
            case R.id.nav_spec_offers:
                extraKey = SingleFragmentActivity.STOCKS_EXTRA;
                intent.putExtra(extraKey, true);
                break;
            case R.id.nav_favourites:
                extraKey = SingleFragmentActivity.FAVORITES_EXTRA;
                intent.putExtra(extraKey, true);
                break;
            case R.id.nav_payment: // ---------- Оплата комм платежей
                setupToolbar(R.string.payment_title);
                setupFragmentLayout();
                fragment = PublicUtilitiesFragment.newInstance(token, contractId, propertyId);
//                extraKey = SingleFragmentActivity.PAYMENT_EXTRA;
//                intent.putExtra(extraKey, true);
                break;
            case R.id.nav_club:
                //extraKey = SingleFragmentActivity.CLUB;
                //intent.putExtra(extraKey, true);
                setupToolbar(R.string.bi_club);
                setupFragmentLayout();
                fragment = BiClubFragment.newInstance();
                break;
            case R.id.nav_sales_dep:
                extraKey = SingleFragmentActivity.SALES_EXTRA;
                intent.putExtra(extraKey, true);
                break;
            case R.id.nav_objects:
                extraKey = SingleFragmentActivity.REALESTATE_EXTRA;
                intent.putExtra(extraKey, true);
                break;
            case R.id.nav_fird_flat:
                extraKey = SingleFragmentActivity.APARTMENT_EXTRA;
                intent.putExtra(extraKey, true);
                break;
            case R.id.push_notification:
//                FragmentManager manager = getSupportFragmentManager();
//                FragmentTransaction transaction = manager.beginTransaction();
                FrameLayout frameLayout = findViewById(R.id.containerAuth);
                frameLayout.removeAllViews();
                Notification_fragments notification_fragments = new Notification_fragments();
                setupToolbar(R.string.push_notification);
                setupFragmentLayout();
                fragment = notification_fragments;
//                String tag = "Уведомления";
//                transaction.replace(R.id.containerAuth , notification_fragments , tag);
//                transaction.commit();

                //fragment_navigation_drawer

//                binding.authAppBar.setVisibility(View.VISIBLE);
//                binding.appBarMain.setVisibility(View.GONE);
//                frameLayout.setVisibility(View.VISIBLE);


                //closeDrawer();
                break;
            case R.id.logout_menu_item:
                showLogoutDialog();
                return true;
            default:
        }






        if (fragment != null || fragmentClass != null){
//            try {
//                fragment = (Fragment) fragmentClass.newInstance();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.
                    beginTransaction().
                    replace(R.id.containerAuth, fragment).commit();
            closeDrawer();
        } else {
            closeDrawer();
            mHandler.postDelayed(() -> {
                startActivity(intent);
                overridePendingTransition(0, 0);
            }, 250);
        }
        return true;
    }


    private void showMessageDialog(int resId) {
        new AlertDialog.Builder(this)
                .setMessage(resId)
                .setCancelable(false)
                .setPositiveButton("Назад", (dialog, which) -> onBackPressed())
                .create()
                .show();
    }


    @Override
    public boolean onSupportNavigateUp() {
        Log.i(LOG, "onSupportNavigateUp()");
        openDrawer();
        propertyId = sharedPreferences.getInt("currentFlat", -1);
        getBankingAccountId();
        return true;
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    private void openDrawer() {
        if (mDrawerLayout != null) {
            mDrawerLayout.openDrawer(GravityCompat.START);
        }
    }

    private void closeDrawer() {
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    private Handler mHandler = new Handler();

    private void navigateToProfile(User user) {
        closeDrawer();
        mHandler.postDelayed(() -> {
            Intent intent = new Intent(this, SingleFragmentActivity.class);
            intent.putExtra(SingleFragmentActivity.PROFILE_EXTRA, user);
            startActivity(intent);
        }, 250);
    }

    private void showLogoutDialog() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.logout_message)
                .setPositiveButton("Да", (dialog, which) -> {
                    logout();
                })
                .setNegativeButton("Отмена", null)
                .create()
                .show();
    }

    private void logout() {
        PreferenceManager.getDefaultSharedPreferences(this)
                .edit()
                .clear()
                .apply();

        launchMainAct();
    }

//xx
    @Override
    public void onFlatLoaded(Flat flat) {

        flatImageView.setImageResource(R.drawable.placeholder);

//        Library.SavePhoneMemoryOne(flat.getContractNumber(),"not");
//        Library.SavePhoneMemoryOne("bitmap","not");

        String phonebitmap = Library.GetPhoneMemoryOne("bitmap");

        if(phonebitmap.equals("not")){

        }else{
            flatImageView.setImageBitmap(Library.storeimage);
            try {

                Bitmap bitmap = BitmapFactory.decodeByteArray(Library.storebyte, 0, Library.storebyte.length);
                Bitmap newbitmap = Bitmap.createScaledBitmap(bitmap, flatImageView.getWidth(), flatImageView.getHeight(), false);

                flatImageView.setImageBitmap(newbitmap);
            } catch (Exception e){
                //AriadnaErrorHandlerUtil.registerError(e, "getUserData");
                e.printStackTrace();
                flatImageView.setImageResource(R.drawable.home_title_gradient_background);
            }

            return;

        }

        Intent intent = new Intent("UPDATE_UI");
        intent.putExtra("propertyId", sharedPreferences.getInt("currentFlat", -1));
        BiApplication.getAppContext().sendBroadcast(intent);
        if (flatImageView != null) {
            if (flat.getRealEstateName().toLowerCase().contains("паркинг")) {
                RestClient.getRestProvider().getParkingImage(token).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        try {
                            byte[] decodedString = Base64.decode(response.body().string(), Base64.DEFAULT);
                            if(response.body().string() != ""){
                                Library.SavePhoneMemoryOne("bitmap",response.body().string());
                            }

                            Library.storebyte = decodedString;


                            Bitmap bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            Bitmap newbitmap = Bitmap.createScaledBitmap(bitmap, flatImageView.getWidth(), flatImageView.getHeight(), false);

                                Library.storeimage = newbitmap;


                            flatImageView.setImageBitmap(newbitmap);
                        } catch (Exception e){
                            //AriadnaErrorHandlerUtil.registerError(e, "getUserData");
                            e.printStackTrace();
                            flatImageView.setImageResource(R.drawable.home_title_gradient_background);
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                               // flatImageView.setImageDrawable();

                    }
                });
            }
       //zz
            if (!flat.getRealEstateName().toLowerCase().contains("паркинг")){   //квартиры загрузка фото

                String bitmaptwo = Library.GetPhoneMemoryOne(flat.getContractNumber());

                if(bitmaptwo.equals("not")){

                }else{
                    byte[] decodedString = Base64.decode(bitmaptwo, Base64.DEFAULT);

                    Bitmap bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    flatImageView.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 600, 400,false));

                    return;
                }



                RestClient.getRestProvider().getFlatImage(token, contractId, propertyId).enqueue(new Callback<FlatImgResponse[]>() {
                    @Override
                    public void onResponse(Call<FlatImgResponse[]> call, Response<FlatImgResponse[]> response) {

                      // Log.i("response","resp: " + response.body()[0].getFlatImage().toString());
                        try {

                            //oshibka

                            if(response.body()[0].getFlatImage() != null){

                                byte[] decodedString = Base64.decode(response.body()[0].getFlatImage(), Base64.DEFAULT);

                                if (response.body()[0].getFlatImage().equals("")) {

                                }else{

                                    Library.SavePhoneMemoryOne(flat.getContractNumber(),response.body()[0].getFlatImage());

                                }

                                Bitmap bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                flatImageView.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 600, 400,false));

                            }

                        } catch (Exception e){
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<FlatImgResponse[]> call, Throwable t) {

                    }
                });
            }
        }else{
            flatImageView.setImageResource(R.drawable.home_title_gradient_background);
        }
    }

    private void loadProfileImage(ImageView imageView, User user) {
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.ic_account_circle)
                .error(R.drawable.ic_account_circle);

        Glide.with(this)
                .asBitmap()
                .apply(options)
                .load(user.getBitmapAva())
                .into(imageView);
    }

    private void launchMainAct() {
        Intent i = new Intent(this, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(i);
        finish();
        overridePendingTransition(0, 0);
    }
}
