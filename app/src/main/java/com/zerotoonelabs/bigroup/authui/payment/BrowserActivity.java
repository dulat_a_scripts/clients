package com.zerotoonelabs.bigroup.authui.payment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.zerotoonelabs.bigroup.BiApplication;
import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.Library.Retrofit_request.Send_Woopay_history;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.api.RestClient;
import com.zerotoonelabs.bigroup.api.RetrofitApi;
import com.zerotoonelabs.bigroup.entity.Wooppay.AuthToken;
import com.zerotoonelabs.bigroup.entity.Wooppay.FrameForm;
import com.zerotoonelabs.bigroup.entity.Wooppay.WooppayCheck;
import com.zerotoonelabs.bigroup.ui.MainActivity;

import org.codehaus.jackson.map.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class BrowserActivity extends AppCompatActivity {

    public static final String URL_KEY = "url";
    SharedPreferences sharedPreferences;
    String token;
    int operationId = 0;
    String url = "", operationData = "";
    List<String> cookie = new ArrayList<>();
    String javascript = "";
    String responsesend = null;
    WooppayCheck wooppayCheck;
    ProgressBar progressBar;



    private AppCompatActivity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
        setContentView(R.layout.browser_act);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(R.string.payment_online);
        progressBar = findViewById(R.id.progress_bar);
        WebView webView = findViewById(R.id.webview);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(BrowserActivity.this);
        token = sharedPreferences.getString("Authorization", null);
        operationId = sharedPreferences.getInt("operationId", 0);
        javascript = sharedPreferences.getString("javascript", "");

        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey(URL_KEY)) {
            url = extras.getString(URL_KEY);
            operationData = extras.getString("operationData");

        }
        final String[] webPage = new String[1];

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAllowContentAccess(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setLoadsImagesAutomatically(true);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getRealSize(size);
        int width = size.x;
        int height = size.y;


        progressBar.setVisibility(View.VISIBLE);


        RetrofitApi retrofitApi = BiApplication.initializeRetrofitApi(RetrofitApi.urlWooppay);
        retrofitApi.payAction(
                token,
                String.valueOf(operationId),
                "card",
                (int)dpFromPx(BrowserActivity.this,width),
                (int)dpFromPx(BrowserActivity.this,height),
                url,
                operationData)
                .enqueue(new Callback<FrameForm>() {
            @Override
            public void onResponse(Call<FrameForm> call, Response<FrameForm> response) {
                if (response.body() != null && response.code() == 200) {
                    cookie = response.headers().values("Set-Cookie");
                    try{
                    String str = cookie.get(0).split(";")[0];
                    CookieSyncManager.createInstance(webView.getContext());
                    CookieManager cookieManager = CookieManager.getInstance();
                    cookieManager.setCookie("https://pci-ws.wooppay.com/cashin/logon", str);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            cookieManager.flush();
                        }
                        webView.setWebContentsDebuggingEnabled(true);
                    }
                    } catch (Exception ex){}
                    webPage[0] = response.body().getFrameform().toString().replaceFirst("<iframe", javascript + "<iframe") + "<script type='text/javascript'>window.onload(document.getElementById('replenishmentForm').submit());</script>";
                    String url="https://pci-ws.wooppay.com";
                   // String url="http://test.wooppay.com";
                    Log.d("TEST", webPage[0]);
                    webView.loadDataWithBaseURL(url+"/cashin/logon", webPage[0], "text/html", "UTF-8", url+"/cashin/logon");

                    progressBar.setVisibility(View.GONE);

                } else {
                    if (response.code() == 500) {
                        Toast.makeText(BrowserActivity.this, response.message().toString(), Toast.LENGTH_LONG).show();
                    } else
                        Toast.makeText(BrowserActivity.this, response.message().toString(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<FrameForm> call, Throwable t) {
                Toast.makeText(BrowserActivity.this, t.toString(), Toast.LENGTH_LONG).show();
            }
        });

        webView.setWebChromeClient(new WebChromeClient() {

            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setTitle("Ваша транзакция обработана")
                        .setMessage("Статус вы можете посмотреть в истории платежей")
                        .setPositiveButton("OK", (arg0, arg1) -> {
                            arg0.dismiss();
                            activity.finish();
                        }).show();
                result.cancel();

                new Download().execute();

                return true;
            }

            public void onProgressChanged(WebView view, int progress) {
                progressBar.setProgress(progress);
                if (progress == 100) {

                } else {
                    progressBar.setVisibility(View.VISIBLE);

                    progressBar.setVisibility(View.GONE);
                }
            }
        });
    }

    public static float dpFromPx(final Context context, final int px) {
        return px / context.getResources().getDisplayMetrics().density;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        super.onBackPressed();
    }

    public class Download extends AsyncTask {
        @Override
        protected String doInBackground(Object[] objects) {

            Retrofit client = Library.NewRetrofitclient();

            Send_Woopay_history send_woopay_history = client.create(Send_Woopay_history.class);

            String newtoken = Library.Phone_memory.getString("token", null);

            String sendjson = Library.Phone_memory.getString("sendjson", null);

            Integer txn_id = Library.Phone_memory.getInt("txn_id", 0);

            String propertyGuid = Library.Phone_memory.getString("propertyGuid", null);

            String ApplicationUserId = Library.Phone_memory.getString("applicationUserId", null);

            Integer serv_id = sharedPreferences.getInt("service_id", 1883);

            Integer operationids = operationId = sharedPreferences.getInt("operationId", 0);

            WooppayCheck wooppayChecking = Library.wooppayCheck;
            wooppayChecking.setTxn_id(txn_id.toString());
            wooppayChecking.setPropertyGuid(propertyGuid);
            wooppayChecking.setApplicationUserId(ApplicationUserId);
            wooppayChecking.setService_id(serv_id);
            wooppayChecking.setOperationId(String.valueOf(operationids));


            Call<Object> objectCall2 = send_woopay_history.getRequest(wooppayChecking,newtoken);

            try {

                Gson gson = new Gson();

                responsesend = gson.toJson(objectCall2.execute().body());

                objectCall2.execute();



            } catch (Exception e) {

            }

            return "not";
        }

        @Override
        protected void onPostExecute(Object o) {

            Log.i("response","response" + responsesend);


            super.onPostExecute(o);
        }
    }



}
