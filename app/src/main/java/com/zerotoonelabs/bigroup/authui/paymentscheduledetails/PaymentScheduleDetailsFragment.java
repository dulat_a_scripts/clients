package com.zerotoonelabs.bigroup.authui.paymentscheduledetails;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.authui.authcommon.PaymentScheduleDetailsPagerAdapter;

public class PaymentScheduleDetailsFragment extends Fragment {

    private static final String TOKEN_KEY = "token";
    private static final String CONTRACT_KEY = "contract";
    private static final String PROPERTY_GUID_KEY = "property";
    private static final String PROPERTY_ID = "PROPERTY_ID";

    private TabLayout tabLayout;
    private ViewPager viewPager;

    public PaymentScheduleDetailsFragment() {
        // Required empty public constructor
    }

    public static PaymentScheduleDetailsFragment newInstance(String token, String contractId, String param3) {
        PaymentScheduleDetailsFragment fragment = new PaymentScheduleDetailsFragment();
        Bundle args = new Bundle();
        args.putString(TOKEN_KEY, token);
        args.putString(CONTRACT_KEY, contractId);
        args.putString(PROPERTY_GUID_KEY, param3);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle args = getArguments();
        if (args != null && args.containsKey(TOKEN_KEY) && args.containsKey(CONTRACT_KEY) && args.containsKey(PROPERTY_GUID_KEY)) {
            String token = args.getString(TOKEN_KEY);
            String contractId = args.getString(CONTRACT_KEY);
            String propertyGuid = args.getString(PROPERTY_GUID_KEY);

            PaymentScheduleDetailsPagerAdapter adapter = new PaymentScheduleDetailsPagerAdapter(getChildFragmentManager(), token, contractId, propertyGuid);
            setupAdapter(adapter);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.payment_schedule_details_frag, container, false);
        tabLayout = view.findViewById(R.id.tab_layout);
        viewPager = view.findViewById(R.id.container);
        viewPager.setPageMargin(16);
        return view;
    }

    public static int convertDip2Pixels(Context context, int dip) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip, context.getResources().getDisplayMetrics());
    }

    private void setupAdapter(PaymentScheduleDetailsPagerAdapter adapter) {
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager, true);
    }

}
