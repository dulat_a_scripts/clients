package com.zerotoonelabs.bigroup.authui.home;

import android.app.AlertDialog;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.zerotoonelabs.bigroup.AppExecutors;
import com.zerotoonelabs.bigroup.BuildConfig;
import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.Library.Retrofit_request.Get_tex_passp;
import com.zerotoonelabs.bigroup.Library.Retrofit_request.Getpdf_list_int;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.api.PicasoClient;
import com.zerotoonelabs.bigroup.api.RestClient;
import com.zerotoonelabs.bigroup.api.Webservice;
import com.zerotoonelabs.bigroup.authui.SingleHomeFragmentActivity;
import com.zerotoonelabs.bigroup.authui.authcommon.FlatSchemeListAdapter;
import com.zerotoonelabs.bigroup.databinding.FlatInfoFragBinding;
import com.zerotoonelabs.bigroup.di.Injectable;
import com.zerotoonelabs.bigroup.entity.Resource;
import com.zerotoonelabs.bigroup.interfaces.IClickCallback;
import com.zerotoonelabs.bigroup.util.AutoClearedValue;
import com.zerotoonelabs.bigroup.entity.Flat;
import com.zerotoonelabs.bigroup.entity.Manager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.inject.Inject;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.text.TextUtils.isEmpty;

public class FlatInfoFragment extends Fragment implements Injectable {
    private static final String PROPERTY_KEY = "property_id";
    private static final String CONTRACT_KEY = "contract_id";

    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private FlatViewModel mViewModel;
    private AutoClearedValue<FlatInfoFragBinding> binding;
    private AutoClearedValue<FlatSchemeListAdapter> adapter;
    private OnFlatLoaded mListener;

    public static CircleImageView logoImg;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String token = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("token", null);
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(FlatViewModel.class);
        mViewModel.setToken(token);
        Bundle args = getArguments();
        if (args != null && args.containsKey(PROPERTY_KEY)) {
            int propertyId = args.getInt(PROPERTY_KEY);
            String contractId = args.getString(CONTRACT_KEY);
            mViewModel.setFlatId(propertyId, contractId);
            initObservables(mViewModel, contractId);
        }

        setupAdapter(mViewModel);

        logoImg = binding.get().flatLogoImage;

        binding.get().textView97.setOnClickListener(v -> {
            Manager manager = binding.get().getManager();
            showContactDialog(manager);
        });
        binding.get().textView99.setOnClickListener(v -> {
            Manager manager = binding.get().getManager2();
            showContactDialog(manager);
        });
        binding.get().textView95.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), SingleHomeFragmentActivity.class);
            intent.putExtra(SingleHomeFragmentActivity.VISIT_OBJ_EXTRA, true);
            startActivity(intent);
        });
        binding.get().textView89.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), SingleHomeFragmentActivity.class);
            intent.putExtra(SingleHomeFragmentActivity.APARTMENT_SHEET_KEY, true);
            startActivity(intent);
        });
        binding.get().textView90.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), SingleHomeFragmentActivity.class);
            intent.putExtra(SingleHomeFragmentActivity.T_PASSPORT_KEY, true);
            startActivity(intent);

        });

        //xx
        //loadApartmentSheetPdf

        //Library.SavePhoneMemoryOne("contractId",contractId);
        String contractid = Library.GetPhoneMemoryOne("contractId");
        String propertyid = Library.GetPhoneMemoryOne("propertyid");


    }

    private void setupAdapter(FlatViewModel viewModel) {
        Intent intent = new Intent(getActivity(), SingleHomeFragmentActivity.class);
        FlatSchemeListAdapter adapter = new FlatSchemeListAdapter(flat -> {
            intent.putExtra(SingleHomeFragmentActivity.PDF_EXTRA, flat.id);
            startActivity(intent);
        });
        this.adapter = new AutoClearedValue<>(this, adapter);

        binding.get().textView92.setCardList(adapter);
        if (!(adapter.getItemCount() > 0))
            binding.get().textView92.setVisibility(View.GONE);
    }

    public AppExecutors appExecutors;
//xx
    public void loadOtherFlat(int propertyId, String contractId) {
        if (mViewModel == null) {
            return;
        }
        Library.propertyid = propertyId;
        String token = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("token", null);

        Retrofit client = Library.NewRetrofitclient();
        Getpdf_list_int getpdf_list_int = client.create(Getpdf_list_int.class);
        Call<ResponseBody> call = getpdf_list_int.getPdf(token,String.valueOf(propertyId));

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.body() == null){

                }else{


                    //binding.get().textView89.setVisibility(View.VISIBLE);
                    //binding.get().textView90.setVisibility(View.GONE);

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

        Get_tex_passp get_tex_passp = client.create(Get_tex_passp.class);
        Call<ResponseBody> call2 = get_tex_passp.getPdf(token,contractId,String.valueOf(propertyId));

        call2.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.body() == null){

                }else{
                   // binding.get().textView89.setVisibility(View.GONE);
                    Library.propertyid = 0;
                    binding.get().textView90.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });


        mViewModel.setFlatId(propertyId, contractId);
        initObservables(mViewModel, contractId);
    }



    private void initObservables(FlatViewModel viewModel, String contractId) {
        viewModel.getMessage().observe(this, integer -> {
            if (integer != null) {
                Toast.makeText(getActivity(), integer, Toast.LENGTH_SHORT).show();
            }
        });
        //xx
        viewModel.getFlat().observe(this, flatResource -> {
            binding.get().setResource(flatResource);
            binding.get().setFlat(flatResource == null ? null : flatResource.data);
            setFlat(flatResource == null ? null : flatResource.data);
            if (flatResource != null && flatResource.data != null) {
                viewModel.setManagerId(flatResource.data.getRealEstateGuid(), contractId);
                viewModel.setManagerId2(flatResource.data.getResponsibleGuid());

            }
            binding.get().executePendingBindings();
        });

        viewModel.getFlatSchemes().observe(this, listResource -> {
//            binding.get().setResource(listResource);
            adapter.get().replace(listResource == null ? null : listResource.data);
            binding.get().executePendingBindings();
        });

        viewModel.getManager().observe(this, managerResource -> {
//            binding.get().setResource(managerResource);
            binding.get().setManager(managerResource == null ? null : managerResource.data);
            binding.get().executePendingBindings();
        });

        viewModel.getManager2().observe(this, managerResource -> {
//            binding.get().setResource(managerResource);
            binding.get().setManager2(managerResource == null ? null : managerResource.data);
            binding.get().executePendingBindings();
        });
    }

    public FlatInfoFragment() {
        // Required empty public constructor
    }

    public static FlatInfoFragment newInstance(int propertyId, String contractId) {
        FlatInfoFragment fragment = new FlatInfoFragment();
        Bundle args = new Bundle();
        args.putInt(PROPERTY_KEY, propertyId);
        args.putString(CONTRACT_KEY, contractId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FlatInfoFragBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.flat_info_frag, container, false);
        binding = new AutoClearedValue<>(this, dataBinding);
        dataBinding.textView68.setOnClickListener(view -> openMap(container, dataBinding.textView68.getText().toString()));

        //parking image
//        Glide.with(this)
//                .load("http://webapi.bi-group.org/api/v1/MobileClient/GetParkingPicture")
//                .into(binding.get().flatLogoImage);

        Picasso.get().load("http://webapi.bi-group.org/api/v1/MobileClient/GetParkingPicture").into(binding.get().flatLogoImage);



        return dataBinding.getRoot();
    }

    private void openMap(ViewGroup p, String s) {
        Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + s);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        p.getContext().startActivity(mapIntent);

    }

    private void showContactDialog(Manager manager) {
        if (!isAdded() || (manager == null || (manager.getPhone() == null && manager.getEmail() == null))) {
            mViewModel.setMessage(R.string.tel_not_found_message);
            return;
        }

        CharSequence[] options = {
                isEmpty(manager.getPhone()) ? getString(R.string.tel_is_not_available) : manager.getPhone(),
                isEmpty(manager.getEmail()) ? getString(R.string.email_is_not_available) : manager.getEmail()
        };

        new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.contact_with, manager.getName()))
                .setItems(options, (dialog, which) -> {
                    if (which == 0) {
                        launchPhone(options[0].toString());
                    } else {
                        launchEmail(options[1].toString());
                    }
                })
                .create()
                .show();
    }

    private void launchPhone(@NonNull String phone) {
        if (phone.equals(getString(R.string.tel_is_not_available))) {
            mViewModel.setMessage(R.string.tel_not_found_message);
            return;
        }

        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phone));
        startActivity(intent);
    }

    private void launchEmail(@NonNull String email) {
        if (email.equals(getString(R.string.email_is_not_available))) {
            mViewModel.setMessage(R.string.email_not_found_message);
            return;
        }

        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.email_subject));
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public void setFlat(Flat flat) {
        if (mListener != null && flat != null) {
            mListener.onFlatLoaded(flat);

            PreferenceManager.getDefaultSharedPreferences(getContext()).edit().putString("ADD", flat.getAddress()).putString("DOG", flat.getContractNumber()).apply();
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFlatLoaded) {
            mListener = (OnFlatLoaded) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFlatLoaded");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFlatLoaded {
        void onFlatLoaded(Flat flat);
    }
}