package com.zerotoonelabs.bigroup.authui.payment;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.authui.authcommon.AuthNavController;
import com.zerotoonelabs.bigroup.authui.authcommon.PaymentHistoryListAdapter;
import com.zerotoonelabs.bigroup.databinding.UtilityBillsHistoryFragBinding;
import com.zerotoonelabs.bigroup.util.AutoClearedValue;

import javax.inject.Inject;

public class PaymentHistoryFragment extends Fragment {

    private static final String TOKEN_KEY = "token";
    private static final String CONTRACT_KEY = "contract_id";
    private static final String PROPERTY_KEY = "property_id";

    @Inject
    ViewModelProvider.Factory viewModelFactory;
    @Inject
    AuthNavController navController;

    private AutoClearedValue<PaymentHistoryListAdapter> adapter;
    private AutoClearedValue<UtilityBillsHistoryFragBinding> binding;
    String LOG = "PaymentHistoryFragment";

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.i(LOG, "onActivityCreated()");
        super.onActivityCreated(savedInstanceState);
        PaymentViewModel mViewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(PaymentViewModel.class);
        Bundle args = getArguments();
        if (args != null && args.containsKey(TOKEN_KEY)) {
            String token = args.getString(TOKEN_KEY);
            initAdapter(token);
        }

        initObservables(mViewModel);
    }

    private void initAdapter(String token) {
        Log.i(LOG, "initAdapter()");
        PaymentHistoryListAdapter adapter = new PaymentHistoryListAdapter(
                paymentHistory -> navController.navigateToUtilitiesFragment(token, paymentHistory.getId()));
//        PaymentHistoryAdapter adapter = new PaymentHistoryAdapter();

        this.adapter = new AutoClearedValue(this, adapter);
        binding.get().utilityBillsList.setAdapter(adapter);
    }

    private void initObservables(PaymentViewModel viewModel) {
        Log.i(LOG, "initObservables()");
        viewModel.getHistory().observe(this, listResource -> {
            binding.get().setResource(listResource);
            binding.get().setResultCount(listResource == null || listResource.data == null ? 0 : listResource.data.size());
            adapter.get().replace(listResource == null ? null : listResource.data);
            binding.get().executePendingBindings();
        });
    }

    public PaymentHistoryFragment() {
        // Required empty public constructor
    }

    public static PaymentHistoryFragment newInstance(String token, int param1, String contractId) {
        PaymentHistoryFragment fragment = new PaymentHistoryFragment();
        Bundle args = new Bundle();
        args.putString(TOKEN_KEY, token);
        args.putString(CONTRACT_KEY, contractId);
        args.putInt(PROPERTY_KEY, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(LOG, "onCreateView()");
        UtilityBillsHistoryFragBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.utility_bills_history_frag, container, false);
        binding = new AutoClearedValue<>(this, dataBinding);
        // Inflate the layout for this fragment
        return dataBinding.getRoot();
    }

}
