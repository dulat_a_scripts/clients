package com.zerotoonelabs.bigroup.authui.paymentschedule;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.zerotoonelabs.bigroup.api.PaymentScheduleResponse;
import com.zerotoonelabs.bigroup.repository.PaymentRepository;
import com.zerotoonelabs.bigroup.ui.common.SingleLiveEvent;
import com.zerotoonelabs.bigroup.util.AbsentLiveData;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.Resource;

import javax.inject.Inject;

public class PaymentScheduleViewModel extends ViewModel {

    private final MutableLiveData<PaymentScheduleId> propertyId = new MutableLiveData<>();
    private SingleLiveEvent<String> message = new SingleLiveEvent<>();
    private final LiveData<Resource<PaymentScheduleResponse>> paymentSchedule;

    @Inject
    public PaymentScheduleViewModel(PaymentRepository repository) {
        paymentSchedule = Transformations.switchMap(propertyId, input -> {
            if (input.isEmpty()) return AbsentLiveData.create();
            return repository.loadPaymentSchedule(input.token, input.contractId, input.propertyId);
        });
    }

    public void setPropertyId(String token, String contractId, int propertyId) {
        PaymentScheduleId update = new PaymentScheduleId(token, contractId, propertyId);
        if (Objects.equals(this.propertyId.getValue(), update)) return;
        this.propertyId.setValue(update);
    }

    public LiveData<Resource<PaymentScheduleResponse>> getPaymentSchedule() {
        return paymentSchedule;
    }

    public void retry() {
        if (propertyId.getValue() != null) {
            propertyId.setValue(propertyId.getValue());
        }
    }


    public SingleLiveEvent<String> getMessage() {
        return message;
    }

    public void setMessage(String resId) {
        if (resId != null)
            message.setValue(resId);
    }

    static class PaymentScheduleId {
        String token;
        String contractId;
        int propertyId;

        public PaymentScheduleId(String token, String contractId, int propertyId) {
            this.token = token;
            this.contractId = contractId;
            this.propertyId = propertyId;
        }

        boolean isEmpty() {
            return token == null || contractId == null || propertyId == -1;
        }
    }
}
