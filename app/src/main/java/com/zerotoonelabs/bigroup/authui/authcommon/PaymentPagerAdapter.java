package com.zerotoonelabs.bigroup.authui.authcommon;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.zerotoonelabs.bigroup.authui.payment.PaymentHistoryFragment;
import com.zerotoonelabs.bigroup.authui.payment.QuickPayFragment;
import com.zerotoonelabs.bigroup.fragment.HistoryPaymentFragment;
import com.zerotoonelabs.bigroup.fragment.PayQuickFragment;

public class PaymentPagerAdapter extends FragmentPagerAdapter {
    private String contractId;
    private String token;
    private int propertyId;
    String LOG = "PaymentPagerAdapter";

    public PaymentPagerAdapter(FragmentManager fm, String token, String contractId, int propertyId) {
        super(fm);
        this.contractId = contractId;
        this.token = token;
        this.propertyId = propertyId;
    }

    @Override
    public Fragment getItem(int position) {
        Log.i(LOG, "getItem()");
        switch (position) {
            case 0: // Быстрая оплата
            return PayQuickFragment.newInstance(token, propertyId, contractId);
//            QuickPayFragment.newInstance(token, propertyId, contractId);
            case 1: // История
            return HistoryPaymentFragment.newInstance(token, propertyId, contractId);
//            PaymentHistoryFragment.newInstance(token, propertyId, contractId);
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title;
        if (position == 0) {
            title = "Быстрая оплата";
        } else {
            title = "История";
        }
        return title;
    }

    @Override
    public int getCount() {
        return 2;
    }

}
