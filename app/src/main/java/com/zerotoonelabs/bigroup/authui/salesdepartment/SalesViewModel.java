package com.zerotoonelabs.bigroup.authui.salesdepartment;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.text.TextUtils;

import com.zerotoonelabs.bigroup.repository.FlatRepository;
import com.zerotoonelabs.bigroup.ui.common.SingleLiveEvent;
import com.zerotoonelabs.bigroup.util.AbsentLiveData;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.Resource;
import com.zerotoonelabs.bigroup.entity.SalesRequestHistory;
import com.zerotoonelabs.bigroup.entity.SalesRequestType;

import java.util.List;

import javax.inject.Inject;

public class SalesViewModel extends ViewModel {

    private final MutableLiveData<String> token;
    private final MutableLiveData<SalesRequestId> salesRequestId;
    private final MutableLiveData<HistoryId> historyId;
    private final LiveData<Resource<List<SalesRequestType>>> requestTypes;
    private final LiveData<Resource<List<SalesRequestHistory>>> history;
    private final LiveData<Resource<String>> salesRequestMessage;
    private SingleLiveEvent<Integer> message = new SingleLiveEvent<>();
    private String iin;
    private String contractGuid;
    private String propertyGuid;

    @Inject
    public SalesViewModel(FlatRepository repository) {
        token = new MutableLiveData<>();
        historyId = new MutableLiveData<>();
        salesRequestId = new MutableLiveData<>();
        requestTypes = Transformations.switchMap(token, input -> {
            if (TextUtils.isEmpty(input)) return AbsentLiveData.create();
            return repository.loadSalesReqTypes(input);
        });
        history = Transformations.switchMap(historyId, input -> {
            if (input.isEmpty()) return AbsentLiveData.create();
            return repository.loadSalesRequestHistory(input.token, input.userId, input.propertyId);
        });

        salesRequestMessage = Transformations.switchMap(salesRequestId, input -> {
            if (input.isEmpty()) return AbsentLiveData.create();
            return repository.sendSalesRequest(input.token, input.iin, input.typeId, input.contractGuid, input.propertyGuid);
        });
    }

    String getIin() {
        return iin;
    }

    void setIin(String iin) {
        this.iin = iin;
    }

    String getContractGuid() {
        return contractGuid;
    }

    void setContractGuid(String contractGuid) {
        this.contractGuid = contractGuid;
    }

    String getPropertyGuid() {
        return propertyGuid;
    }

    void setPropertyGuid(String propertyGuid) {
        this.propertyGuid = propertyGuid;
    }

    void setToken(String input) {
        token.setValue(input);
    }

    LiveData<Resource<List<SalesRequestType>>> getRequestTypes() {
        return requestTypes;
    }

    void setHistoryId(String token, String userId, String propertyId) {
        HistoryId update = new HistoryId(userId, token, propertyId);
        if (Objects.equals(historyId.getValue(), update)) return;
        historyId.setValue(update);
    }

    void setSalesRequestId(String typeId) {
        String iin = this.iin;
        String propertyGuid = this.propertyGuid;
        String contractGuid = this.contractGuid;
        String token = this.token.getValue();

        SalesRequestId update = new SalesRequestId(iin, propertyGuid, contractGuid, typeId, token);
        if (Objects.equals(salesRequestId.getValue(), update)) return;
        salesRequestId.setValue(update);
    }

    public SingleLiveEvent<Integer> getMessage() {
        return message;
    }

    public void setMessage(Integer resId) {
        if (resId != null)
            message.setValue(resId);
    }

    public LiveData<Resource<List<SalesRequestHistory>>> getHistory() {
        return history;
    }

    public LiveData<Resource<String>> getSalesRequestMessage() {
        return salesRequestMessage;
    }

    static class SalesRequestId {
        private String iin;
        private String propertyGuid;
        private String contractGuid;
        private String typeId;
        private String token;

        SalesRequestId(String iin, String propertyGuid, String contractGuid, String typeId, String token) {
            this.iin = iin;
            this.propertyGuid = propertyGuid;
            this.contractGuid = contractGuid;
            this.typeId = typeId;
            this.token = token;
        }

        boolean isEmpty() {
            return iin == null || propertyGuid == null || contractGuid == null || typeId == null || token == null;
        }
    }

    static class HistoryId {
        String userId;
        String token;
        String propertyId;

        public HistoryId(String userId, String token, String propertyId) {
            this.userId = userId;
            this.token = token;
            this.propertyId = propertyId;
        }

        boolean isEmpty() {
            return userId == null || token == null || propertyId == null;
        }
    }
}
