package com.zerotoonelabs.bigroup.authui.authcommon;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.ServiceItemBinding;
import com.zerotoonelabs.bigroup.ui.common.DataBoundListAdapter;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.kassa24.Service;

public class ServiceListAdapter extends DataBoundListAdapter<Service, ServiceItemBinding> {

    private final ServiceClickCallback serviceClickCallback;

    public ServiceListAdapter(ServiceClickCallback serviceClickCallback) {
        this.serviceClickCallback = serviceClickCallback;
    }

    @Override
    protected ServiceItemBinding createBinding(ViewGroup parent) {
        ServiceItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.service_item,
                        parent, false);
        binding.getRoot().setOnClickListener(v -> {
            Service repo = binding.getService();
            if (repo != null && serviceClickCallback != null) {
                serviceClickCallback.onClick(repo);
            }
        });

        binding.editText3.setOnClickListener(v -> {
            Service utility = binding.getService();
            int position = binding.getPosition();
            if (serviceClickCallback != null) {
                serviceClickCallback.onPayClick(utility, position);
            }
        });

        return binding;
    }

    @Override
    protected void bind(ServiceItemBinding binding, Service item, int position) {
        binding.setService(item);
        binding.setPosition(position);
    }

    @Override
    protected boolean areItemsTheSame(Service oldItem, Service newItem) {
        return Objects.equals(oldItem.getServiceId(), newItem.getServiceId());
    }

    @Override
    protected boolean areContentsTheSame(Service oldItem, Service newItem) {
        return Objects.equals(oldItem.getServiceName(), newItem.getServiceName());
    }

    public interface ServiceClickCallback {
        void onClick(Service utility);
        void onPayClick(Service utility, int position);
    }
}