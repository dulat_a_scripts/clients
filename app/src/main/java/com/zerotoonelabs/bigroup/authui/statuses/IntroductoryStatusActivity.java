package com.zerotoonelabs.bigroup.authui.statuses;

import android.arch.lifecycle.LiveData;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.zerotoonelabs.bigroup.BiApplication;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.api.ApiResponse;
import com.zerotoonelabs.bigroup.authui.SingleHomeFragmentActivity;
import com.zerotoonelabs.bigroup.repository.BiRepository;
import com.zerotoonelabs.bigroup.ui.common.StatusesAdapter;
import com.zerotoonelabs.bigroup.entity.statusesPred.Contract;
import com.zerotoonelabs.bigroup.entity.statusesPred.DopSoglashenie;
import com.zerotoonelabs.bigroup.entity.statusesPred.Platezhi;
import com.zerotoonelabs.bigroup.entity.statusesPred.StatusPred;
import com.zerotoonelabs.bigroup.entity.statusesPred.Uslugi;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class IntroductoryStatusActivity extends AppCompatActivity {

    @Inject
    BiRepository repository;
    LiveData<ApiResponse<List<StatusPred>>> liveData;
    String contractId, propertyId, token;
    RecyclerView /*recyclerView1, */recyclerView2, recyclerView3, recyclerView4;
    TextView textView1, textView2, servicesText, paymentsText;
    private RecyclerView.LayoutManager mLayoutManager;
    TextView one,two,three;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_introductory_status);
        ((BiApplication) getApplicationContext()).dispatchingAndroidInjector.inject(this);
        token = PreferenceManager.getDefaultSharedPreferences(this).getString("token", null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        recyclerView1 = (RecyclerView) findViewById(R.id.recycleView1);
        recyclerView2 = (RecyclerView) findViewById(R.id.recycleView2);
        recyclerView3 = (RecyclerView) findViewById(R.id.recycleView3);
        recyclerView4 = (RecyclerView) findViewById(R.id.recycleView4);
        textView1 = (TextView) findViewById(R.id.textView1);
        textView2 = (TextView) findViewById(R.id.textView2);
        mLayoutManager = new LinearLayoutManager(this);
//        recyclerView1.setLayoutManager(mLayoutManager);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView2.setLayoutManager(mLayoutManager);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView3.setLayoutManager(mLayoutManager);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView4.setLayoutManager(mLayoutManager);
        Intent intent = getIntent();

        one = findViewById(R.id.one);
        two = findViewById(R.id.two);
        three = findViewById(R.id.three);

        textView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(IntroductoryStatusActivity.this, SingleHomeFragmentActivity.class);
                intent.putExtra(SingleHomeFragmentActivity.STATUS_PDF, 1);
                startActivity(intent);
            }
        });

        textView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(IntroductoryStatusActivity.this, SingleHomeFragmentActivity.class);
                intent.putExtra(SingleHomeFragmentActivity.STATUS_PDF, 2);
                startActivity(intent);
            }
        });

        contractId = intent.getStringExtra("contractId");
        propertyId = intent.getStringExtra("propertyId");
        liveData = repository.loadStatusPred(token, contractId, propertyId);
        liveData.observe(this, result -> {
            if (result != null && result.body != null) {
                List<StatusPred> list = result.body;
                List<Contract> contractList = list.get(0).getContract();
                List<DopSoglashenie> dopSoglashenieList = list.get(1).getDopSoglashenie();
                List<Uslugi> uslugiList = list.get(2).getUslugi();
                if(uslugiList.size() > 0){
                    servicesText.setVisibility(View.VISIBLE);
                }

                if(contractList.size() > 0){
                    textView1.setVisibility(View.VISIBLE);
                    textView2.setVisibility(View.VISIBLE);
                }else{
                    textView1.setVisibility(View.GONE);
                    textView2.setVisibility(View.GONE);
                }

                if(dopSoglashenieList.size() > 1){

                    one.setVisibility(View.VISIBLE);
                    two.setVisibility(View.VISIBLE);
                    three.setVisibility(View.VISIBLE);
                    StatusesAdapter adapter2 = new StatusesAdapter(2, new ArrayList<>(), dopSoglashenieList, new ArrayList<>(), new ArrayList<>());
                    recyclerView2.setAdapter(adapter2);
                }else{
                    //textView1.setVisibility(View.GONE);
                    //textView2.setVisibility(View.GONE);
                    one.setVisibility(View.GONE);
                    two.setVisibility(View.GONE);
                    three.setVisibility(View.GONE);
                }



                List<Platezhi> platezhiList = list.get(3).getPlatezhi();
                if(platezhiList.size() > 0){
                    paymentsText.setVisibility(View.VISIBLE);
                    StatusesAdapter adapter4 = new StatusesAdapter(4, new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), platezhiList);
                    recyclerView4.setAdapter(adapter4);
                }

                if(uslugiList.size() > 0){
                    StatusesAdapter adapter3 = new StatusesAdapter(3, new ArrayList<>(), new ArrayList<>(), uslugiList, new ArrayList<>());
                    recyclerView3.setAdapter(adapter3);
                }

//                StatusesAdapter adapter = new StatusesAdapter(1, contractList, new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
//                recyclerView1.setAdapter(adapter);



            }
        });


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
