package com.zerotoonelabs.bigroup.authui.paymentscheduledetails;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.api.PaymentScheduleResponse;
import com.zerotoonelabs.bigroup.api.RestClient;
import com.zerotoonelabs.bigroup.authui.authcommon.InstalmentListAdapter;
import com.zerotoonelabs.bigroup.databinding.PaymentSchedule1FragBinding;
import com.zerotoonelabs.bigroup.di.Injectable;
import com.zerotoonelabs.bigroup.util.AutoClearedValue;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentSchedule1Fragment extends Fragment implements Injectable {

    private static final String TOKEN_KEY = "token";
    private static final String CONTRACT_KEY = "contract";
    private static final String PROPERTY_GUID_KEY = "property";
    private static final String PROPERTY_ID = "PROPERTY_ID";

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private AutoClearedValue<PaymentSchedule1FragBinding> binding;
    private AutoClearedValue<InstalmentListAdapter> adapter;

    private String formatPrice(DecimalFormat formatter, float price) {
        return formatter.format(price);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        PaymentSchedulesViewModel viewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(PaymentSchedulesViewModel.class);
        Bundle args = getArguments();
        if (args != null && args.containsKey(TOKEN_KEY) && args.containsKey(CONTRACT_KEY) && args.containsKey(PROPERTY_GUID_KEY)) {
            String token = args.getString(TOKEN_KEY);
            String contractId = args.getString(CONTRACT_KEY);
            String propertyGuid = args.getString(PROPERTY_GUID_KEY);
            int propertyId = PreferenceManager.getDefaultSharedPreferences(getContext()).getInt("currentFlat", -1);

            viewModel.setInstalmentId(token, contractId, propertyGuid);

            RestClient.getRestProvider().loadPaymentSchedule(token, contractId, propertyId).enqueue(new Callback<PaymentScheduleResponse>() {
                @Override
                public void onResponse(Call<PaymentScheduleResponse> call, Response<PaymentScheduleResponse> response) {
                    try {
                        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
                        symbols.setGroupingSeparator(' ');
                        formatter.setDecimalFormatSymbols(symbols);

                        double dTotalValue = Double.parseDouble(response.body().totalValue);
                        int totalValue = (int) dTotalValue;


                        binding.get().totalAmount.setText(formatPrice(formatter, totalValue) + " \u20B8");
                        binding.get().restAmount.setText(formatPrice(formatter, response.body().currentPayment.leftover) + " \u20B8");
                        binding.get().accountNum.setText(response.body().payment.number);
                        Log.d("TEST", "_"+response.body().payment.number);
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<PaymentScheduleResponse> call, Throwable t) {

                }
            });

        } else {
            viewModel.setInstalmentId(null, null, null);
        }

        InstalmentListAdapter adapter = new InstalmentListAdapter(utility -> {

        });
        this.adapter = new AutoClearedValue<>(this, adapter);
        binding.get().instalmentList.setAdapter(adapter);

        viewModel.getInstalments().observe(this, listResource -> {
            binding.get().setResource(listResource);
            this.adapter.get().replace(listResource == null ? null : listResource.data);
            binding.get().executePendingBindings();
        });


    }

    public PaymentSchedule1Fragment() {
        // Required empty public constructor
    }

    public static PaymentSchedule1Fragment newInstance(String param1, String param2, String param3) {
        PaymentSchedule1Fragment fragment = new PaymentSchedule1Fragment();
        Bundle args = new Bundle();
        args.putString(TOKEN_KEY, param1);
        args.putString(CONTRACT_KEY, param2);
        args.putString(PROPERTY_GUID_KEY, param3);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        PaymentSchedule1FragBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.payment_schedule1_frag, container, false);
        binding = new AutoClearedValue<>(this, dataBinding);
        // Inflate the layout for this fragment
        return dataBinding.getRoot();
    }

}
