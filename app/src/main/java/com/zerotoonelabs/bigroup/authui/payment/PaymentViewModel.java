//package com.zerotoonelabs.bigroup.authui.payment;
//
//import android.arch.lifecycle.LiveData;
//import android.arch.lifecycle.MutableLiveData;
//import android.arch.lifecycle.Transformations;
//import android.arch.lifecycle.ViewModel;
//import android.text.TextUtils;
//
//import com.zerotoonelabs.bigroup.R;
//import com.zerotoonelabs.bigroup.repository.PaymentRepository;
//import com.zerotoonelabs.bigroup.ui.common.SingleLiveEvent;
//import com.zerotoonelabs.bigroup.util.AbsentLiveData;
//import com.zerotoonelabs.bigroup.util.Objects;
//import com.zerotoonelabs.bigroup.vo.BankAccount;
//import com.zerotoonelabs.bigroup.vo.PaymentHistory;
//import com.zerotoonelabs.bigroup.vo.PaymentResponse;
//import com.zerotoonelabs.bigroup.vo.Resource;
//import com.zerotoonelabs.bigroup.vo.ServiceId;
//import com.zerotoonelabs.bigroup.vo.kassa24.CheckUserResponse;
//import com.zerotoonelabs.bigroup.vo.kassa24.Invoices;
//import com.zerotoonelabs.bigroup.vo.kassa24.Service;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.inject.Inject;
//
//public class PaymentViewModel extends ViewModel {
//
//    private final MutableLiveData<PaymentId> paymentId;
//    private final MutableLiveData<HistoryId> historyId;
//    private final MutableLiveData<String> userId;
//    private final MutableLiveData<TransactionId> transactionId;
//    private SingleLiveEvent<Integer> message = new SingleLiveEvent<>();
//    private final LiveData<Resource<BankAccount>> bankAccount;
//    private final LiveData<Resource<List<PaymentHistory>>> history;
//    private final LiveData<Resource<Invoices.Invoice>> services;
//    private final LiveData<Resource<PaymentResponse>> transaction;
//    private final LiveData<Resource<List<ServiceId>>> serviceIds;
//    private String token;
//    private String invoiceId;
//
//    @Inject
//    public PaymentViewModel(PaymentRepository repository) {
//        paymentId = new MutableLiveData<>();
//        historyId = new MutableLiveData<>();
//        userId = new MutableLiveData<>();
//        transactionId = new MutableLiveData<>();
//
//        serviceIds = Transformations.switchMap(paymentId, input -> repository.loadServiceIds(input.token));
//
//        bankAccount = Transformations.switchMap(paymentId, input -> {
//            if (input.isEmpty()) return AbsentLiveData.create();
//            return repository.loadBankAccount(input.token, input.propertyId, input.contractId);
//        });
//
//        history = Transformations.switchMap(historyId, input -> {
//            if (input.isEmpty()) return AbsentLiveData.create();
//            return repository.loadPaymentHistory(input.token, input.bankAccountId);
//        });
//        services = Transformations.switchMap(userId, input -> {
//            if (TextUtils.isEmpty(input)) return AbsentLiveData.create();
//            return repository.loadCheckUser(input);
//        });
//
//        transaction = Transformations.switchMap(transactionId, input -> {
//            if (input.isEmpty()) return AbsentLiveData.create();
//            return repository.loadTransaction(input.serviceId, input.amount, input.userId, input.invoiceId, input.services);
//        });
//    }
//
//    public String getToken() {
//        return token;
//    }
//
//    public void setToken(String token) {
//        this.token = token;
//    }
//
//    void setPaymentId(String contractId, int propertyId) {
//        String token = this.token;
//        PaymentId update = new PaymentId(contractId, propertyId, token);
//        if (Objects.equals(paymentId.getValue(), update)) return;
//        paymentId.setValue(update);
//    }
//
//    public void setHistoryId(int bankAccountId) {
//        String token = this.token;
//        HistoryId update = new HistoryId(token, bankAccountId);
//        if (Objects.equals(historyId.getValue(), update)) return;
//        historyId.setValue(update);
//    }
//
//    void setUserId(String input) {
//        if (Objects.equals(userId, input)) return;
//        userId.setValue(input);
//    }
//
//    void setTransactionId() {
//        String serviceId = "138";
//        int amount = getTotalSum();
//        String usrId = userId.getValue();
//        List<Service> services = getTransactionServices();
//        String invoiceId = this.invoiceId;
//        if (amount <= 0 || services == null || services.isEmpty()) {
//            setMessage(R.string.empty_amount_message);
//            return;
//        }
//        TransactionId update = new TransactionId(serviceId, usrId, amount, services, invoiceId);
//        if (Objects.equals(transactionId.getValue(), update)) return;
//        transactionId.setValue(update);
//    }
//
//    public String getInvoiceId() {
//        return invoiceId;
//    }
//
//    public void setInvoiceId(String invoiceId) {
//        this.invoiceId = invoiceId;
//    }
//
//    public LiveData<Resource<BankAccount>> getBankAccount() {
//        return bankAccount;
//    }
//
//    LiveData<Resource<List<PaymentHistory>>> getHistory() {
//        return history;
//    }
//
//    LiveData<Resource<Invoices.Invoice>> getServices() {
//        return services;
//    }
//
//    private List<Service> getTransactionServices() {
//        Resource<Invoices.Invoice> invoice = getServices().getValue();
//        if (invoice != null && invoice.data != null) {
//            List<Service> services = new ArrayList<>();
//            List<Service> serviceList = invoice.data.services.service;
//            for (Service service : serviceList) {
//                if (service.getTotalSum() > 0) {
//                    services.add(service);
//                }
//            }
//            return services;
//        }
//        return null;
//    }
//
//    int getTotalSum() {
//        int total = 0;
//        Resource<Invoices.Invoice> checkUserResponse = getServices().getValue();
//        if (checkUserResponse != null && checkUserResponse.data != null) {
//            List<Service> serviceList = checkUserResponse.data.services.service;
//            for (Service service : serviceList) {
//                total += service.getTotalSum();
//            }
//        }
//        return total;
//    }
//
//    LiveData<Resource<PaymentResponse>> getTransaction() {
//        return transaction;
//    }
//
//    public SingleLiveEvent<Integer> getMessage() {
//        return message;
//    }
//
//    public void setMessage(Integer resId) {
//        if (resId != null)
//            message.setValue(resId);
//    }
//
//    public void retry() {
//        if (userId.getValue() != null) {
//            userId.setValue(userId.getValue());
//        }
//    }
//
//    static class TransactionId {
//        String serviceId;
//        String userId;
//        int amount;
//        String invoiceId;
//        List<Service> services;
//
//        public TransactionId(String serviceId, String userId, int amount, List<Service> services, String invoiceId) {
//            this.serviceId = serviceId;
//            this.userId = userId;
//            this.amount = amount;
//            this.services = services;
//            this.invoiceId = invoiceId;
//        }
//
//        boolean isEmpty() {
//            return serviceId == null || userId == null || invoiceId == null || amount == 0 || services == null || services.isEmpty();
//        }
//    }
//
//    static class HistoryId {
//        String token;
//        int bankAccountId;
//
//        public HistoryId(String token, int bankAccountId) {
//            this.token = token;
//            this.bankAccountId = bankAccountId;
//        }
//
//        boolean isEmpty() {
//            return token == null || bankAccountId == -1;
//        }
//    }
//
//    static class PaymentId {
//        String contractId;
//        int propertyId;
//        String token;
//
//        public PaymentId(String contractId, int propertyId, String token) {
//            this.contractId = contractId;
//            this.propertyId = propertyId;
//            this.token = token;
//        }
//
//        boolean isEmpty() {
//            return contractId == null || propertyId == -1 || token == null;
//        }
//    }
//}
