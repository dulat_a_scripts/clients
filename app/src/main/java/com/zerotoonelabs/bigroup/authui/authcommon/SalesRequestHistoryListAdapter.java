package com.zerotoonelabs.bigroup.authui.authcommon;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import com.zerotoonelabs.bigroup.BuildConfig;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.SalesReqItemBinding;
import com.zerotoonelabs.bigroup.ui.common.DataBoundListAdapter;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.SalesRequestHistory;

import java.io.File;

public class SalesRequestHistoryListAdapter extends DataBoundListAdapter<SalesRequestHistory, SalesReqItemBinding> {

    private final SalesRequestHistoryClickCallback historyClickCallback;

    public SalesRequestHistoryListAdapter(SalesRequestHistoryClickCallback historyClickCallback) {
        this.historyClickCallback = historyClickCallback;
    }

    @Override
    protected SalesReqItemBinding createBinding(ViewGroup parent) {
        SalesReqItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.sales_req_item,
                        parent, false);
        binding.getRoot().setOnClickListener(v -> {
            SalesRequestHistory repo = binding.getHistory();
            if (repo != null && historyClickCallback != null) {
                historyClickCallback.onClick(repo);
            }
        });

        return binding;
    }

    @Override
    protected void bind(SalesReqItemBinding binding, SalesRequestHistory item, int position) {
        binding.setHistory(item);
    }

    @Override
    protected boolean areItemsTheSame(SalesRequestHistory oldItem, SalesRequestHistory newItem) {
        return Objects.equals(oldItem.number, newItem.number);
    }

    @Override
    protected boolean areContentsTheSame(SalesRequestHistory oldItem, SalesRequestHistory newItem) {
        return Objects.equals(oldItem.createdAt, newItem.createdAt);
    }

    public interface SalesRequestHistoryClickCallback {
        void onClick(SalesRequestHistory item);
    }
}