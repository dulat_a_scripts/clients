package com.zerotoonelabs.bigroup.authui.payment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.authui.authcommon.AuthNavController;
import com.zerotoonelabs.bigroup.authui.authcommon.PaymentPagerAdapter;
import com.zerotoonelabs.bigroup.di.Injectable;

import javax.inject.Inject;

public class PublicUtilitiesFragment extends Fragment {

    private static final String TOKEN_KEY = "token";
    private static final String CONTRACT_KEY = "contract_id";
    private static final String PROPERTY_KEY = "property_id";
    String LOG = "PublicUtilitiesFragment";

    private TabLayout tabLayout;
    private ViewPager viewPager;
    PaymentPagerAdapter adapter = null;
    String token = "", contractId = "";
    int propertyId = 0;

    public PublicUtilitiesFragment() {
        // Required empty public constructor
    }

    public static PublicUtilitiesFragment newInstance(String token, String contractId, int propertyId) {
        PublicUtilitiesFragment fragment = new PublicUtilitiesFragment();
        Bundle args = new Bundle();
        args.putString(TOKEN_KEY, token);
        args.putString(CONTRACT_KEY, contractId);
        args.putInt(PROPERTY_KEY, propertyId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.i(LOG, "onActivityCreated()");
        super.onActivityCreated(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            token = args.getString(TOKEN_KEY);
            contractId = args.getString(CONTRACT_KEY);
            propertyId = args.getInt(PROPERTY_KEY);

            adapter = new PaymentPagerAdapter(getChildFragmentManager(), token, contractId, propertyId);
            setupAdapter(adapter);
        }
    }

    private void setupAdapter(PaymentPagerAdapter adapter) {
        Log.i(LOG, "setupAdapter()");
        viewPager.setAdapter(adapter);
        viewPager.getAdapter().notifyDataSetChanged();
        tabLayout.setupWithViewPager(viewPager, true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(LOG, "onCreateView()");
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.public_utilities_frag, container, false);
        tabLayout = view.findViewById(R.id.tab_layout);
        viewPager = view.findViewById(R.id.container);
        return view;
    }


}
