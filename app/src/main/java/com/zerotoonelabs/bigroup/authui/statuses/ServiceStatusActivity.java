package com.zerotoonelabs.bigroup.authui.statuses;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.zerotoonelabs.bigroup.R;

public class ServiceStatusActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_status);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
