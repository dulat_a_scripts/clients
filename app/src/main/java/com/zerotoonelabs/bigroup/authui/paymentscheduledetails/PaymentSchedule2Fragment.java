package com.zerotoonelabs.bigroup.authui.paymentscheduledetails;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.authui.authcommon.InstalmentHistoryListAdapter;
import com.zerotoonelabs.bigroup.databinding.PaymentSchedule2FragBinding;
import com.zerotoonelabs.bigroup.di.Injectable;
import com.zerotoonelabs.bigroup.util.AutoClearedValue;

import javax.inject.Inject;

public class PaymentSchedule2Fragment extends Fragment implements Injectable {

    private static final String TOKEN_KEY = "token";
    private static final String CONTRACT_KEY = "contract";
    private static final String PROPERTY_GUID_KEY = "property";

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private AutoClearedValue<PaymentSchedule2FragBinding> binding;
    private AutoClearedValue<InstalmentHistoryListAdapter> adapter;

    public PaymentSchedule2Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        PaymentSchedulesViewModel viewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(PaymentSchedulesViewModel.class);
        Bundle args = getArguments();
        if (args != null && args.containsKey(TOKEN_KEY) && args.containsKey(CONTRACT_KEY) && args.containsKey(PROPERTY_GUID_KEY)) {
            String token = args.getString(TOKEN_KEY);
            String contractId = args.getString(CONTRACT_KEY);
            String propertyGuid = args.getString(PROPERTY_GUID_KEY);

            viewModel.setInstalmentId(token, contractId, propertyGuid);
        } else {
            viewModel.setInstalmentId(null, null, null);
        }

        InstalmentHistoryListAdapter adapter = new InstalmentHistoryListAdapter(utility -> {

        });
        this.adapter = new AutoClearedValue<>(this, adapter);
        binding.get().instalmentHistoryList.setAdapter(adapter);

        viewModel.getInstalmentHistory().observe(this, listResource -> {
            binding.get().setResource(listResource);
            this.adapter.get().replace(listResource == null ? null : listResource.data);
            binding.get().executePendingBindings();
        });
    }

    public static PaymentSchedule2Fragment newInstance(String param1, String param2, String param3) {
        PaymentSchedule2Fragment fragment = new PaymentSchedule2Fragment();
        Bundle args = new Bundle();
        args.putString(TOKEN_KEY, param1);
        args.putString(CONTRACT_KEY, param2);
        args.putString(PROPERTY_GUID_KEY, param3);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        PaymentSchedule2FragBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.payment_schedule2_frag, container, false);
        binding = new AutoClearedValue<>(this, dataBinding);
        // Inflate the layout for this fragment
        return dataBinding.getRoot();
    }

}
