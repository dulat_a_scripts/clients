package com.zerotoonelabs.bigroup.authui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.authui.authcommon.HomeNavigationController;

import javax.inject.Inject;

import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public class SingleHomeFragmentActivity extends AppCompatActivity implements HasSupportFragmentInjector {


    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;

    @Override
    public DispatchingAndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }

    @Inject
    HomeNavigationController navigationController;

    public static final String VISIT_OBJ_EXTRA = "visit_obj";
    public static final String PDF_EXTRA = "pdf";
    public static final String STATUS_PDF = "status_pdf";
    public static final String T_PASSPORT_KEY = "t_passport";
    public static final String APARTMENT_SHEET_KEY = "apartment_sheet";
    public static final String PAYMENT_SCHEDULE = "payment_schedule";
    public static final String PAYMENT_GRAPH = "payment_graph";

    public static final String PENY = "peny";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_home_fragment_act);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setElevation(0);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String contractId = sharedPreferences.getString("contractId", null);
        int propertyId = sharedPreferences.getInt("currentFlat", -1);
        String token = sharedPreferences.getString("token", null);
        String propertyGuid = sharedPreferences.getString("propertyGuid", null);
        Bundle extras = getIntent().getExtras();
        if (extras != null && token != null && contractId != null && propertyId != -1) {
            if (extras.containsKey(VISIT_OBJ_EXTRA)) {
                navigationController.navigateToVisitObject(token);
                actionBar.setTitle(R.string.visit_object);
            } else if (extras.containsKey(PDF_EXTRA)) {
                int pdfId = extras.getInt(PDF_EXTRA);
                navigationController.navigateToPdfViewer(token, pdfId);
                actionBar.setTitle(R.string.pdf_title);
            } else if (extras.containsKey(APARTMENT_SHEET_KEY)) {
                navigationController.navigateToPdfViewer3(token, propertyId);
                actionBar.setTitle(R.string.pdf_title);
            } else if (extras.containsKey(T_PASSPORT_KEY)) {
                navigationController.navigateToPdfViewer2(token, propertyId, contractId);
                actionBar.setTitle(R.string.pdf_title);
            } else if (extras.containsKey(STATUS_PDF)) {
                navigationController.navigateToPdfViewerStatus(token, propertyId, contractId, extras.getInt(STATUS_PDF));
                actionBar.setTitle(R.string.pdf_title);
            } else if (extras.containsKey(PAYMENT_SCHEDULE)) {
                navigationController.navigateToPaymentSchedule(token, contractId, propertyId);
                actionBar.setTitle(R.string.payment_current_title);
            } else if (extras.containsKey(PAYMENT_GRAPH)) {
                navigationController.navigateToPaymentScheduleDetails(token, contractId, propertyGuid);
                actionBar.setTitle(R.string.payment_schedule_title);
            } else if (extras.containsKey(PENY)) {
                navigationController.navigateToPeny(token, contractId, propertyGuid);
                actionBar.setTitle("Пеня");
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
