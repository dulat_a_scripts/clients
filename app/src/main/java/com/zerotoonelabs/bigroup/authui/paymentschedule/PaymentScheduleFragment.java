package com.zerotoonelabs.bigroup.authui.paymentschedule;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.Library.Request_parsers.ApiKeyParser;
import com.zerotoonelabs.bigroup.Library.Request_parsers.Get_comission_parser;
import com.zerotoonelabs.bigroup.Library.Retrofit_request.GetCheckAccount;
import com.zerotoonelabs.bigroup.Library.Retrofit_request.GetPaymentsMainPage_int;
import com.zerotoonelabs.bigroup.Library.Retrofit_request.Get_api_key;
import com.zerotoonelabs.bigroup.Library.Retrofit_request.Get_comission;
import com.zerotoonelabs.bigroup.Library.Retrofit_request.Set_podpicka;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.api.PaymentScheduleResponse;
import com.zerotoonelabs.bigroup.api.RestClient;
import com.zerotoonelabs.bigroup.authui.HomeActivity;
import com.zerotoonelabs.bigroup.authui.SingleFragmentActivity;
import com.zerotoonelabs.bigroup.databinding.PaymentScheduleFragBinding;
import com.zerotoonelabs.bigroup.di.Injectable;
import com.zerotoonelabs.bigroup.entity.AutoPayRequest;
import com.zerotoonelabs.bigroup.entity.CloudPayRequest;
import com.zerotoonelabs.bigroup.entity.User;
import com.zerotoonelabs.bigroup.interfaces.IClickCallback;
import com.zerotoonelabs.bigroup.util.AutoClearedValue;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import okhttp3.Credentials;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import ru.cloudpayments.sdk.Card;
import ru.cloudpayments.sdk.CardFactory;
import ru.cloudpayments.sdk.ICard;
import ru.cloudpayments.sdk.IPayment;
import ru.cloudpayments.sdk.PaymentFactory;
import ru.cloudpayments.sdk.PaymentWidget;
import ru.cloudpayments.sdk.business.connector.CloudPaymentsSession;
import ru.cloudpayments.sdk.business.domain.model.BaseResponse;
import ru.cloudpayments.sdk.business.domain.model.billing.CardsAuthConfirmResponse;
import ru.cloudpayments.sdk.business.domain.model.billing.CardsAuthRequest;
import ru.cloudpayments.sdk.business.domain.model.billing.CardsAuthResponse;
import ru.cloudpayments.sdk.utils.Logger;
import ru.cloudpayments.sdk.view.PaymentTaskListener;

import static android.text.TextUtils.isEmpty;
import static com.zerotoonelabs.bigroup.authui.HomeActivity.CURRENT_USER;

public class PaymentScheduleFragment extends Fragment implements Injectable {

    String request_value = null;
    boolean checkpar = false;
    Button button11;
    String contractNumber_string = null;
    String Iin_string = null;
    Integer constract_int = 0;
    ArrayList<String> sendarray = new ArrayList<>();
    String PublicId_int = "null";
    Boolean IsSberbank_bool = false;
    String apptoken = null;
    String request_response = null;
    ProgressBar progressBar;
    static Timer timer = new Timer();
    Integer fix = 0;
    Integer time = 0;
    String TotalValue = null;
    String Amount = null;
    boolean Check_status = false;

    private static final String TOKEN_KEY = "token";
    private static final String CONTRACT_KEY = "contract";
    private static final String PROPERTY_KEY = "property";
    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private AutoClearedValue<PaymentScheduleFragBinding> binding;
    private PaymentScheduleViewModel viewModel;

    private Calendar selectedDate;

    private double fee;

    public PaymentScheduleFragment() {}

    public static PaymentScheduleFragment newInstance(String token, String contractId, int propertyId) {
        PaymentScheduleFragment fragment = new PaymentScheduleFragment();
        Bundle args = new Bundle();
        args.putString(TOKEN_KEY, token);
        args.putString(CONTRACT_KEY, contractId);
        args.putInt(PROPERTY_KEY, propertyId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.info_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
//zz
        if(id == R.id.action_info)
            Toast.makeText(getActivity(), "1.Платежные карточки ДБ АО \"Сбербанк\":\n\u0009до 10 000 тенге\t-1%\n\u0009более 10 000\u0009-300 тенге\n2.Платежные карточки других банков:\u0009-2.4%", Toast.LENGTH_LONG).show();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAttach(Context context) {

        new Download().execute();
        super.onAttach(context);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(PaymentScheduleViewModel.class);

        Bundle args = getArguments();
        if (args != null && args.containsKey(TOKEN_KEY) && args.containsKey(PROPERTY_KEY) && args.containsKey(CONTRACT_KEY)) {
            String token = args.getString(TOKEN_KEY);
            String contractId = args.getString(CONTRACT_KEY);
            int propertyId = args.getInt(PROPERTY_KEY);

            viewModel.setPropertyId(token, contractId, propertyId);
        } else {
            viewModel.setPropertyId(null, null, -1);
        }

        binding.get().setRetryCallback(viewModel::retry);
        initObservables(viewModel);
        binding.get().amountEt.setOnClickListener( v -> binding.get().amountEt.setSelection(binding.get().amountEt.getText().length()));
        binding.get().button10.setOnClickListener(v -> {
            payPressed();
        });
        binding.get().sberSection.setOnClickListener(v -> {
            binding.get().sberCheck.setChecked(true);
            binding.get().otherCheck.setChecked(false);
            DownloadComission("sber");

        });
        binding.get().otherSection.setOnClickListener(v -> {
            binding.get().sberCheck.setChecked(false);
            binding.get().otherCheck.setChecked(true);
            DownloadComission("other");
        });

        binding.get().saveCardSection.setOnClickListener(v -> binding.get().saveCardCheck.setChecked(!binding.get().saveCardCheck.isChecked()));
        binding.get().autoPaySection.setOnClickListener(v -> binding.get().autoPaycCheck.setChecked(!binding.get().autoPaycCheck.isChecked()));
//zz
        binding.get().autoPaycCheck.setOnCheckedChangeListener((buttonView, isChecked) -> {
                binding.get().addData.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                binding.get().uvedsection.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                binding.get().uvedsection2.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                binding.get().emailEt.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                binding.get().dateSection.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                preFillAutoPay();

                binding.get().button10.setText(isChecked ?
                        binding.get().button10.getText().toString() + " и создать ежемесячный платеж" :
                        binding.get().button10.getText().toString().replaceAll(" и создать ежемесячный платеж", ""));
        });
        binding.get().dateSection.setOnClickListener(v -> openDatePicker());
        binding.get().dateEt.setOnClickListener(v -> openDatePicker());

        binding.get().amountEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    DownloadComission(binding.get().sberCheck.isChecked() ? "sber" : "other");
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }
//xx
    private void preFillAutoPay(){
        Calendar currentDate = Calendar.getInstance();
        selectedDate = Calendar.getInstance();
        selectedDate.set(Calendar.YEAR, currentDate.get(Calendar.YEAR));
        selectedDate.set(Calendar.MONTH, currentDate.get(Calendar.MONTH) + 1);
        selectedDate.set(Calendar.DAY_OF_MONTH, currentDate.get(Calendar.DAY_OF_MONTH) - 1);

        User user = getCurrentUser(PreferenceManager.getDefaultSharedPreferences(getContext()));

        binding.get().emailEt.setText(user.getEmail());
        binding.get().dateEt.setText(new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault()).format(selectedDate.getTime()));
        binding.get().helpText.setText(getResources().getString(R.string.autopaytext));
    }

    public User getCurrentUser(SharedPreferences sharedPreferences) {
        String userString = sharedPreferences.getString(CURRENT_USER, null);
        if (userString != null) {
            return new Gson().fromJson(userString, User.class);
        }
        return null;
    }

    private String formatPrice(DecimalFormat formatter, double price) {
        return formatter.format(price);
    }



    public void DownloadComission(String type){
        Retrofit client = Library.NewRetrofitclient();
        Get_comission get_comission = client.create(Get_comission.class);
        Double toPay = Double.parseDouble(binding.get().amountEt.getText().toString().replaceAll(" ", ""));
        Long ls = (long)Math.floor(toPay);
        String token = Library.GetPhoneMemoryOne("token");
        Call<Get_comission_parser> get_comission_parserCall = get_comission.getData(token,ls);
        get_comission_parserCall.enqueue(new Callback<Get_comission_parser>() {
            @Override
            public void onResponse(Call<Get_comission_parser> call, Response<Get_comission_parser> response) {
                Double SberBankKomisiya = response.body().getSberBankKomisiya();
                Double AmountSberBankTotal = response.body().getAmountSberBankTotal();
                Double AnotherBankKomisiya = response.body().getAnotherBankKomisiya();
                Double AmountAnotherBankTotal = response.body().getAmountAnotherBankTotal();

                countFee(type,SberBankKomisiya,AmountSberBankTotal,AnotherBankKomisiya,AmountAnotherBankTotal);
            }

            @Override
            public void onFailure(Call<Get_comission_parser> call, Throwable t) {
                try{
                    Library.showToast(getContext(),"не удалось выщитать комиссию проверьте подключение к интернету");
                }catch (Exception e){

                }

            }
        });
    }



    private void countFee(String type,Double SberBankKomisiya,Double AmountSberBankTotal, Double AnotherBankKomisiya, Double AmountAnotherBankTotal){

        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
        symbols.setGroupingSeparator(' ');
        formatter.setDecimalFormatSymbols(symbols);

        double toPay = Double.parseDouble(binding.get().amountEt.getText().toString().replaceAll(" ", ""));
        String text = "";
        if (type.equals("sber")){

            //1000/0,976*0,024
//xx
                String sFeeVal = (getString(R.string.bankFee, String.valueOf(SberBankKomisiya)));
                binding.get().fee.setText(Html.fromHtml(sFeeVal));
                double toPayWithFee = AmountSberBankTotal;
                fee = AmountSberBankTotal;
                text = ("Оплатить " + formatPrice(formatter, toPayWithFee) + " \u20B8");
            }


        if (type.equals("other")){
            double feeVal = (toPay / AmountAnotherBankTotal) * AnotherBankKomisiya;
            fee = AmountAnotherBankTotal;
            String sFeeValr = (getString(R.string.bankFee, String.valueOf(AnotherBankKomisiya)));
            binding.get().fee.setText(Html.fromHtml(sFeeValr));
            double toPayWithFee = AmountAnotherBankTotal;
            text = ("Оплатить " + formatPrice(formatter, toPayWithFee) + " \u20B8");
        }

        if (binding.get().autoPaycCheck.isChecked())
            text = text + " и создать ежемесячный платеж";
            binding.get().button10.setText(text);
    }


    private void openDatePicker(){
        DatePickerFragment mDatePicker = new DatePickerFragment();
        mDatePicker.setCallback(selectDateCallback);
        mDatePicker.show(getActivity().getSupportFragmentManager(), "select date");
    }

    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        private IClickCallback callback;
        public void setCallback(IClickCallback callback) {
            this.callback = callback;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), R.style.DateTimeDialogTheme, this, year, month, day);
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            return datePickerDialog; //new DatePickerDialog(getActivity(), R.style.DateTimeDialogTheme, this, year, month, day).getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        }
        public void onDateSet(DatePicker view, int year, int month, int day) {
            Calendar c = Calendar.getInstance();
            c.set(Calendar.YEAR, year);
            c.set(Calendar.MONTH, month);
            c.set(Calendar.DAY_OF_MONTH, day);
            callback.onItemClick(c);
        }
    }

    private void initObservables(PaymentScheduleViewModel viewModel) {
        viewModel.getPaymentSchedule().observe(this, resource -> {
            binding.get().setResource(resource);
            binding.get().setPayment(resource == null || resource.data == null ? null : resource.data);
            binding.get().executePendingBindings();
        });

        viewModel.getMessage().observe(this, resId -> {
            if (resId != null) {
                Toast.makeText(getActivity(), resId, Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        PaymentScheduleFragBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.payment_schedule_frag, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        binding = new AutoClearedValue<>(this, dataBinding);

        progressBar = dataBinding.progressBar2;

        button11 = dataBinding.button11;

        button11.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);

        button11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String contr = null;
                Integer numb = 0;

                Bundle args = getArguments();
                if (args != null && args.containsKey(TOKEN_KEY) && args.containsKey(PROPERTY_KEY) && args.containsKey(CONTRACT_KEY)) {
                    String token = args.getString(TOKEN_KEY);
                    String contractId = args.getString(CONTRACT_KEY);
                    int propertyId = args.getInt(PROPERTY_KEY);
                    numb = propertyId;
                    contr = contractId;
                }
                SharedPreferences sharedPreferencess = PreferenceManager.getDefaultSharedPreferences(getContext());
                String inn = sharedPreferencess.getString("iin", null);
               // String ContractNumber = sharedPreferencess.getString("BankingAccountId", null);
                String ContractNumber = contractNumber_string;

                if(ContractNumber.equals("")){

                    Toast.makeText(getContext(), "Не найден номер Контракта", Toast.LENGTH_SHORT).show();
                    return;
                }



                //ContractNumber

                Log.i("d","click" + ContractNumber + "|"+ inn + "|" + contr + "|");

                sendarray.add(ContractNumber);
                sendarray.add(inn);
                sendarray.add(contr);

                progressBar.setVisibility(View.VISIBLE);

                new SendPay_Request().execute();


            }
        });

        return dataBinding.getRoot();
    }

    public class SendPay_Request extends AsyncTask{
        @Override
        protected String doInBackground(Object[] objects) {



            Retrofit client = Library.NewRetrofitclient();

            GetCheckAccount getCheckAccount = client.create(GetCheckAccount.class);

            Call<Object> objectCall2 = getCheckAccount.getRequest(sendarray.get(0),sendarray.get(1),sendarray.get(2),apptoken);
            //zzz
            try{

                Gson gson = new Gson();

                request_response = gson.toJson(objectCall2.execute().body());

                //objectCall2.execute();

            }catch (Exception e){

            }




            return "sending";
        }

        @Override
        protected void onPostExecute(Object o) {
//zz
            JsonElement jelement = new JsonParser().parse(request_response);
            String jobject = jelement.getAsString();

            if(jobject.equals("success")){
                Log.i("response","response" + request_response.toString());

                Toast.makeText(getContext(), "Запрос успешно создан ожидайте...", Toast.LENGTH_SHORT).show();


                if(fix.equals(0)){


                        timer.scheduleAtFixedRate(new TimerTask() {
                            @Override
                            public void run() {
                                time++;

                                fix = 1;
                                Log.i("send","send_update_request");
                                new Download().execute();


                                //timer.cancel();

                            }
                        }, 0, 10000);

                }

                //button11.setVisibility(View.GONE);

                //Intent intent = new Intent(getActivity(), HomeActivity.class);
                //startActivity(intent);


            }else{
                Toast.makeText(getContext(),"...." , Toast.LENGTH_SHORT).show();
            }

            progressBar.setVisibility(View.GONE);
            super.onPostExecute(o);

        }
    }

    private void payPressed() {

        //setAutoPay("abs4516515ddfdfd");

        progressBar.setVisibility(View.VISIBLE);

        String initValue = binding.get().amountEt.getText().toString().trim();
        initValue.replace(" ", "");
        String paymentAmount = initValue.replace(" ", "");//binding.get().amountEt.getText().toString().trim();
        PaymentScheduleResponse payment = binding.get().getPayment();
        if (payment == null || payment.currentPayment == null) {
            progressBar.setVisibility(View.GONE);
            viewModel.setMessage("Не удалось загрузить график оплаты");
            return;
        }
        if (isEmpty(paymentAmount)) {
            progressBar.setVisibility(View.GONE);
            viewModel.setMessage("Введите сумму оплаты");
            return;
        }
        int amountInt = payment.currentPayment.amount;
        double amount = fee;//Double.parseDouble(paymentAmount) + fee;
//        double amount = (double) amountInt;
        String currency = payment.currentPayment.currency;
        String description = payment.currentPayment.description;
        String publicId = payment.currentPayment.publicId;
        //pk_aa2510615fdafde0a2495d4f15f84
        String invoiceId = String.valueOf(payment.currentPayment.id);
        String accountId = payment.payment.iin;

        HashMap<String, Object> map = new HashMap<>();
        map.put("contractId", payment.payment.contractId);
        map.put("saveCard", "saveCard");
        String data = new Gson().toJson(map);

        if (currency == null || description == null || accountId == null) {
            progressBar.setVisibility(View.GONE);
            viewModel.setMessage("Не удается провести оплату");
            return;
        }

        CloudPayRequest request = new CloudPayRequest();
        request.setAmount(amount);
        request.setCurrency(currency);
        request.setDescription(description);
        request.setAccountId(accountId);
        request.setInvoiceId(invoiceId);

        //publicId = "pk_348c635ba69b355d6f4dc75a4a205";
        request.setPublicId(publicId);//("pk_348c635ba69b355d6f4dc75a4a205");
        Library.publicid = publicId;

        request.setEmail(binding.get().emailEt.getText().toString());
        request.setAutoPayDate(selectedDate != null ? selectedDate.getTime() : new Date());
        request.setAutoPay(binding.get().autoPaycCheck.isChecked());

        Intent intent = new Intent(getActivity(), SingleFragmentActivity.class);
        intent.putExtra(SingleFragmentActivity.CLOUD_PAY, true);
        intent.putExtra("req", request);

        Retrofit client = Library.NewRetrofitclient();
        Get_api_key get_api_key = client.create(Get_api_key.class);

        String token = Library.GetPhoneMemoryOne("token");

        Call<ApiKeyParser> call = get_api_key.getData(token,publicId);

        call.enqueue(new Callback<ApiKeyParser>() {
            @Override
            public void onResponse(Call<ApiKeyParser> call, Response<ApiKeyParser> response) {
                Library.api_key = response.body().getPsw();
                startActivity(intent);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ApiKeyParser> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Library.showToast(getContext(),"ошибка сети повторите запрос");

            }
        });

        /*Intent intent = new Intent(getActivity(), PaymentWidget.class);
        intent.putExtra(PaymentWidget.EXTRA_AMOUNT, amount); // Сумма оплаты
        intent.putExtra(PaymentWidget.EXTRA_DESCRIPTION, " " + description); // Описание
        intent.putExtra(PaymentWidget.EXTRA_CURRENCY, currency); // Код валюты
        intent.putExtra(PaymentWidget.EXTRA_PUBLIC_ID, "pk_348c635ba69b355d6f4dc75a4a205"); // Ваш public ID
        intent.putExtra(PaymentWidget.EXTRA_INVOICE_ID, invoiceId); // ID заказа в вашей системе
        intent.putExtra(PaymentWidget.EXTRA_ACCOUNT_ID, accountId); // ID покупателя в вашей системе
        intent.putExtra(PaymentWidget.EXTRA_DATA, data); // Произвольный набор параметров
        intent.putExtra(PaymentWidget.EXTRA_TYPE, PaymentWidget.TYPE_AUTH); // Тип платежа: TYPE_CHARGE (одностадийный) или TYPE_AUTH (двухстадийный)

        PaymentWidget.taskListener = paymentTaskListener;

        startActivity(intent);*/
    }

    private PaymentTaskListener paymentTaskListener = new PaymentTaskListener() {

        @Override
        public void success(BaseResponse response) {
            if (response instanceof CardsAuthConfirmResponse) {
                showResult(getString(R.string.payment_finished) + ((CardsAuthConfirmResponse) response).transaction.cardHolderMessage + "\n");
                if (binding.get().autoPaycCheck.isChecked())
                    setAutoPay(((CardsAuthConfirmResponse) response).transaction.token);
            }
            else if (response instanceof CardsAuthResponse) {
                showResult(getString(R.string.payment_finished) + ((CardsAuthResponse) response).auth.cardHolderMessage + "\n");
                if (binding.get().autoPaycCheck.isChecked())
                    setAutoPay(((CardsAuthResponse) response).auth.token);
            } else {
                showResult(getString(R.string.payment_finished) + response + "\n");
                if (binding.get().autoPaycCheck.isChecked())
                setAutoPay(((CardsAuthConfirmResponse) response).transaction.token);

            }
        }

        @Override
        public void error(BaseResponse response) {
            if (response instanceof CardsAuthConfirmResponse)
                showResult(getString(R.string.payment_not_finished) + ((CardsAuthConfirmResponse) response).transaction.cardHolderMessage + "\n");
            if (response instanceof CardsAuthResponse)
                showResult(getString(R.string.payment_not_finished) + ((CardsAuthResponse) response).auth.cardHolderMessage + "\n");
            else
                showResult(getString(R.string.error) + response.message + "\n");
        }

        @Override
        public void cancel() {
            showResult(getString(R.string.operation_canceled) + "\n");
        }
    };

    public void showResult(String text) {
        viewModel.setMessage(text);
    }
//xx
    private void setAutoPay(String token){
        PaymentScheduleResponse payment = binding.get().getPayment();

        String initValue = binding.get().amountEt.getText().toString().trim();
        initValue.replace(" ", "");
        String paymentAmount = initValue.replace(" ", "");

        AutoPayRequest request = new AutoPayRequest();
        request.setToken(token);
        request.setEmail(binding.get().emailEt.getText().toString());
        request.setAccountId(payment.payment.iin);
        request.setAmount(Double.parseDouble(paymentAmount));
        request.setCurrency(payment.currentPayment.currency);
        request.setDescription(payment.currentPayment.description);
        request.setRequireConfirmation(true);
        request.setInterval("Month");
        request.setPeriod(1);
        request.setStartDate(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(selectedDate.getTime()));

        //request.setStartDate(selectedDate.getTime());

        String auth = Credentials.basic(Library.publicid,Library.api_key);

        Retrofit client = Library.NewRetrofitclient_Cloud();
        Set_podpicka set_podpicka = client.create(Set_podpicka.class);
        Call<ResponseBody> call = set_podpicka.createAutoPay(auth,request);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.i("ddd","ddd" + response.body().toString());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private IClickCallback selectDateCallback = new IClickCallback() {
        @Override
        public void onItemClick(Object object) {
            selectedDate = (Calendar) object;
            binding.get().dateEt.setText(new SimpleDateFormat("dd MM yyyy", Locale.getDefault()).format(selectedDate.getTime()));
        }

        @Override
        public void onClick() {}
    };


    public class Download extends AsyncTask {

        @Override
        protected String doInBackground(Object[] objects) {


            Retrofit client = Library.NewRetrofitclient();

            GetPaymentsMainPage_int getPaymentsMainPage = client.create(GetPaymentsMainPage_int.class);

            Bundle args = getArguments();

            String token = null;
            String contractId = null;
            Integer propertyId = 0;


            Integer newint = 0;


            if (args != null && args.containsKey(TOKEN_KEY) && args.containsKey(PROPERTY_KEY) && args.containsKey(CONTRACT_KEY)) {
                token = args.getString(TOKEN_KEY);
                apptoken = token;
                contractId = args.getString(CONTRACT_KEY);
                propertyId = args.getInt(PROPERTY_KEY);

                newint = Integer.parseInt(contractId);

            }

            Call<Object> objectCall2 = getPaymentsMainPage.getRequest(propertyId,newint,token);

            try {

                //objectCall2.execute().body();

                Gson gson = new Gson();

                request_value = gson.toJson(objectCall2.execute().body());

                objectCall2.execute();



            } catch (Exception e) {

            }



            //@Query("property_id") Integer integer,@Query("contractId")


            return "null";
        }

        @Override
        protected void onPostExecute(Object o) {
//zz
            //request_value

            try{


                Check_status = Library.getjsonobject_one_bool(request_value,"CheckStatus");

                TotalValue = Library.getjsonobject_one_string(request_value,"TotalValue");
                Amount = Library.getjsonobject_one_2_param(request_value,"CurrentPayment","Amount");

                Double Amountint = Double.parseDouble(Amount);

                contractNumber_string = Library.getjsonobject_one_getFlat(request_value,"flat","ContractNumber");


                //Iin_string = Library.getjsonobject_one_2_param(request_value,"payment","Iin");
                //Iin_string = Library.getjsonobject_one_2_param(request_value,"payment","Iin");
                PublicId_int = Library.getjsonobject_one_2_param(request_value,"payment","PublicId");
                IsSberbank_bool = Library.getjsonobject_one_2_param_bool(request_value,"payment","IsSberbank");
                //constract_int = Library.getjsonobject_one_2_param_int(request_value,"payment","ContractId");

                ///Toast.makeText(Library.context,"check",Toast.LENGTH_LONG).show();

                Log.i("d","d" + IsSberbank_bool + "|" + PublicId_int + " | " + constract_int);

                // PublicId_int = "";
                //IsSberbank_bool = false;


                if(IsSberbank_bool == true && !PublicId_int.equals("") && Check_status == true){
                    // if(checkpar == false){
                    Log.i("d","_________________++++++" + "ddddd");

                    binding.get().button10.setVisibility(View.VISIBLE);
                    button11.setVisibility(View.GONE);

                    //pokazat

                    if(fix.equals(1)){

                        timer.cancel();

                    }

                }

                if(PublicId_int.equals("") && Check_status == false && IsSberbank_bool == false && Amountint < 1){

                    binding.get().button10.setVisibility(View.GONE);
                    button11.setVisibility(View.GONE);

                    //scrit vse

                }
//zz
                //zayavka

                if(IsSberbank_bool == true && Check_status == false && PublicId_int.equals("")){

                    button11.setVisibility(View.VISIBLE);
                    binding.get().button10.setVisibility(View.GONE);

                }

                if(IsSberbank_bool == true && Check_status == false && !PublicId_int.equals("")){

                    button11.setVisibility(View.VISIBLE);
                    binding.get().button10.setVisibility(View.GONE);

                }

                if(IsSberbank_bool == true && Check_status == true && PublicId_int.equals("")){

                    button11.setVisibility(View.VISIBLE);
                    binding.get().button10.setVisibility(View.GONE);

                }

                if(IsSberbank_bool == false && Check_status == false && Amountint > 0){

                    button11.setVisibility(View.VISIBLE);
                    binding.get().button10.setVisibility(View.GONE);

                }


               // binding.get().saveCardSection.setVisibility(View.GONE);
               // binding.get().autoPaySection.setVisibility(View.GONE);


            }catch (Exception e){

            }






            super.onPostExecute(o);
        }
    }
}

//public_id     pk_aa2510615fdafde0a2495d4f15f84
//api_secret    4913b02a4f0b2e0d91ba1849764f1571
