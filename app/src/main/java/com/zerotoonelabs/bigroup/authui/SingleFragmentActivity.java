package com.zerotoonelabs.bigroup.authui;

import android.arch.lifecycle.ViewModelProvider;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.api.PaymentScheduleResponse;
import com.zerotoonelabs.bigroup.authui.authcommon.AuthNavController;
import com.zerotoonelabs.bigroup.entity.CloudPayRequest;
import com.zerotoonelabs.bigroup.entity.RequrentPayment;
import com.zerotoonelabs.bigroup.entity.User;
import com.zerotoonelabs.bigroup.entity.club.Partner;
import com.zerotoonelabs.bigroup.fragments.club.AllPartnersFragment;
import com.zerotoonelabs.bigroup.fragments.club.PartnerDetailsFragment;

import javax.inject.Inject;

import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

import static android.text.TextUtils.isEmpty;
import static com.zerotoonelabs.bigroup.fragments.club.AllPartnersFragment.TYPE;

public class SingleFragmentActivity extends AppCompatActivity implements
        HasSupportFragmentInjector, FragmentManager.OnBackStackChangedListener {

    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Inject
    AuthNavController navigationController;

    private ActionBar mActionBar;

    @Override
    public DispatchingAndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }

    public static final String STOCKS_EXTRA = "stocks";
    public static final String SEND_REQUEST_EXTRA = "send_request";
    public static final String PAYMENT_EXTRA = "payment";
    public static final String BISERVICE_EXTRA = "bi_service";
    public static final String SALES_EXTRA = "sales";
    public static final String APARTMENT_EXTRA = "apartments";
    public static final String REALESTATE_EXTRA = "realestate";
    public static final String PROFILE_EXTRA = "profile";
    public static final String FAVORITES_EXTRA = "favorites";
    public static final String ALL_PARTNERS = "ALL_PARTNERS";
    public static final String PARTNERS_DETAIL = "PARTNERS_DETAIL";
    public static final String REQ_PAY_DETAIL = "REQ_PAY_DETAIL";
    public static final String CLOUD_PAY = "CLOUD_PAY";

    String LOG = "SingleFragmentActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(LOG, "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_fragment_act);
//        getCurrentUser();

        Toolbar toolbar = findViewById(R.id.toolbar);
//        mDrawerLayout = findViewById(R.id.drawer_layout);
//        NavigationView navigationView = findViewById(R.id.nav_view);
//        View headerView = navigationView.getHeaderView(0);
//        initHeaderView(headerView);
        setSupportActionBar(toolbar);
        mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayHomeAsUpEnabled(true);
            mActionBar.setDisplayShowHomeEnabled(true);

//            mActionBar.setHomeButtonEnabled(true);
//            mActionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
        }
        getSupportFragmentManager().addOnBackStackChangedListener(this);

        Bundle extras = getIntent().getExtras();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String contractId = sharedPreferences.getString("contractId", null);
        int propertyId = sharedPreferences.getInt("currentFlat", -1);
        String token = sharedPreferences.getString("token", null);

        if (extras != null && token != null && contractId != null && propertyId != -1 && mActionBar != null) {
            if (extras.containsKey(PAYMENT_EXTRA)) {
                navigationController.navigateToPublicUtilities(token, propertyId, contractId);
                mActionBar.setTitle(R.string.payment_title);
            } else if (extras.containsKey(BISERVICE_EXTRA)) {
                mActionBar.setTitle(R.string.bi_service);
                String serviceId = sharedPreferences.getString("serviceId", null);
                if (isEmpty(serviceId)) {
                    showMessageDialog(R.string.cant_open_bi_service);
                    return;
                }
                navigationController.navigateToBiService();
            } else if (extras.containsKey(SALES_EXTRA)) {
                navigationController.navigateToSales(token);
                mActionBar.setTitle(R.string.sales_request_title);
            } else if (extras.containsKey(STOCKS_EXTRA)) {
                navigationController.navigateToSales();
                mActionBar.setTitle("Акции");
            } else if (extras.containsKey(SEND_REQUEST_EXTRA)) {
                navigationController.navigateToCall();
                mActionBar.setTitle(R.string.request_call_title);
            } else if (extras.containsKey(APARTMENT_EXTRA)) {
                navigationController.navigateToApartments();
                mActionBar.setTitle(R.string.find_flat);
            } else if (extras.containsKey(REALESTATE_EXTRA)) {
                navigationController.navigateToRealEstate();
                mActionBar.setTitle(R.string.building);
            } else if (extras.containsKey(PROFILE_EXTRA)) {
                User user = extras.getParcelable(PROFILE_EXTRA);
                navigationController.navigateToProfile(user);
                mActionBar.setTitle(R.string.user_profile);
            } else if (extras.containsKey(FAVORITES_EXTRA)) {
                navigationController.navigateToFavorites(token);
                mActionBar.setTitle(R.string.favourites);
            } else if (extras.containsKey(ALL_PARTNERS)){
                navigationController.navigateToAllPartners(extras.getString(AllPartnersFragment.TYPE));
                mActionBar.setTitle("Все партнеры");
            } else if (extras.containsKey(PARTNERS_DETAIL)){
                Partner partner = (Partner) extras.getSerializable(PartnerDetailsFragment.PARTNER);
                navigationController.navigateToPartnerDetails(partner);
                mActionBar.setTitle(partner.getName());
            } else if (extras.containsKey(REQ_PAY_DETAIL)){
                RequrentPayment payment = (RequrentPayment) extras.getSerializable("REQ_PAY");
                navigationController.navigateToReqPaymentDetails(payment);
                mActionBar.setTitle("Подписка");
            }
            else if (extras.containsKey(CLOUD_PAY)){
                CloudPayRequest payment = (CloudPayRequest) extras.getSerializable("req");
                navigationController.navigateToCloudPayment(payment);
                mActionBar.setTitle("Платеж");
            }
        }
    }

    @Override
    public void onBackStackChanged() {
        shouldDisplayHomeUp();
    }

    public void shouldDisplayHomeUp() {
        FragmentManager fragmentManager = getSupportFragmentManager();

        Fragment fr = fragmentManager.findFragmentById(R.id.flContent);
        String tag = fr.getTag();

        if (mActionBar != null) {
            mActionBar.setTitle(tag);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void showMessageDialog(int resId) {
        new AlertDialog.Builder(this)
                .setMessage(resId)
                .setCancelable(false)
                .setPositiveButton("Назад", (dialog, which) -> onBackPressed())
                .create()
                .show();
    }
}
