package com.zerotoonelabs.bigroup.authui.authcommon;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.zerotoonelabs.bigroup.authui.payment.PaymentHistoryFragment;
import com.zerotoonelabs.bigroup.authui.payment.QuickPayFragment;

public class SalesPagerAdapter
        extends FragmentPagerAdapter {
    private String contractId;
    private String token;
    private int propertyId;

    public SalesPagerAdapter(FragmentManager fm, String token, String contractId, int propertyId) {
        super(fm);
        this.contractId = contractId;
        this.token = token;
        this.propertyId = propertyId;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return QuickPayFragment.newInstance(token, propertyId, contractId);
        } else {
            return PaymentHistoryFragment.newInstance(token, propertyId, contractId);
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title;
        if (position == 0) {
            title = "Быстрая оплата";
        } else {
            title = "История";
        }
        return title;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
