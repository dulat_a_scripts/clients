package com.zerotoonelabs.bigroup.authui.biserviceauthoruzed;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;


public class NewApplicationFragment extends Fragment {

    public NewApplicationFragment() {
        // Required empty public constructor
    }

    public static NewApplicationFragment newInstance(String param1, String param2) {
        NewApplicationFragment fragment = new NewApplicationFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_application, container, false);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

}
