package com.zerotoonelabs.bigroup.authui.biserviceauthoruzed;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class ReportsFragment extends Fragment {

    public ReportsFragment() {
        // Required empty public constructor
    }


    public static ReportsFragment newInstance(String param1, String param2) {
        ReportsFragment fragment = new ReportsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reports, container, false);

        TextView yearTV = (TextView)view.findViewById(R.id.textView39);
        yearTV.setText(new SimpleDateFormat("yyyy", Locale.getDefault()).format(new Date()) + " год");

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ListView reports = view.findViewById(R.id.reports_by_quarter);
        ArrayList<String> reportsList = new ArrayList<>();
        reportsList.add("1-ый квартал");
        reportsList.add("2-ой квартал");
        reportsList.add("3-ий квартал");
        reportsList.add("4-ый квартал");
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(),
                R.layout.report_item, R.id.report_name, reportsList);

        reports.setAdapter(adapter);

        reports.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Library.showToast(getContext(),"Отчет за " + reportsList.get(position) + " в скором времени будет доступен для просмотра.");
                showAlertDialog(getActivity(), "Отчет за " + reportsList.get(position) + " в скором времени будет доступен для просмотра.");
            }
        });
    }

    public static void showAlertDialog(Context context, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Предупреждение");
        builder.setMessage(message);
        builder.setPositiveButton(android.R.string.yes, (dialog, id) -> {});
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
