package com.zerotoonelabs.bigroup.authui.biserviceauthoruzed;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.os.PersistableBundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.Library.ServiceAdapter;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.authui.HomeActivity;

import com.zerotoonelabs.bigroup.entity.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
import static android.provider.Settings.System.DATE_FORMAT;
import static android.text.TextUtils.isEmpty;

public class NewBiServiceApplicationActivity extends AppCompatActivity implements ServiceAdapter.CallbackInterface {

    private static final int SELECT_IMG_RC = 1;
    public static final String COMPLAINT_EXTRA = "complaint";
    public static final String POSITION_EXTRA = "position";
    private static final int THUMBNAIL_SIZE = 400;
    Activity activity;
    //private AddservicenoteActBinding binding;

    String token;
    AlertDialog b;
    AlertDialog.Builder dialogBuilder;
    SharedPreferences sharedPreferences;
    String contractorIIN, contractID, propertyUID, realPropertyUID;

    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    ServiceAdapter serviceAdapter;
    ArrayList<String> countarray = new ArrayList<>();
    ImageView imageView16;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.addservicenote_act);

        imageView16 = findViewById(R.id.imageView16);
        setupActionBar();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        token = PreferenceManager.getDefaultSharedPreferences(this).getString("token", "cbc821cf-9730-43ee-9477-62de9bf9f1bb");

        //add adapter
        recyclerView = findViewById(R.id.descriptionrecycle);
        recyclerView.setHasFixedSize(false);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        countarray.add("1");
        serviceAdapter = new ServiceAdapter(this,countarray,bitmaps);
        recyclerView.setAdapter(serviceAdapter);
        recyclerView.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
        //add adapter
        activity = this;

        Library.edittextar = new ArrayList<>();

        //dobavlenie photo

//        findViewById(R.id.image_layout).setOnClickListener(v -> {
//            Intent pickPhoto = new Intent(Intent.ACTION_PICK,
//                    EXTERNAL_CONTENT_URI);
//            startActivityForResult(pickPhoto, SELECT_IMG_RC);
//        });



    }

    @Override
    protected void onResume() {

        Bitmap no_image = BitmapFactory.decodeResource(getResources(),R.drawable.no_image);
        for(int i = 0;i < 20;i++){
            bitmaps.add(no_image);
        }

        super.onResume();
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }




//zz
    //zz
    public void addNote(View view) {

        ArrayList<String> desr_array = new ArrayList<>(serviceAdapter.getArrayList());

        Integer emptyfix = 0;

        if(desr_array.size() > 0){
            if(desr_array.get(0).equals("")){
                emptyfix = 1;
            }
        }else{
            emptyfix = 1;
        }



        if(desr_array.size() > 0){
            for(int i = 0;i < desr_array.size();i++){

                if(i > 0){
                    if(desr_array.get(i).equals("")){
                        desr_array.remove(i);
                    }
                }
            }
        }

        if(desr_array.size() < 1){
            emptyfix = 1;
        }

        if(emptyfix == 1){
            Library.showToast(this,"Введите описание жалобы");
            return;
        }



//        Uri imageUri = binding.getImageUri();

        ArrayList<String> encoded_to_base_64_ArrayImage = new ArrayList<>();

        for(int k = 0;k < bitmaps.size();k++){
            encoded_to_base_64_ArrayImage.add(Convert_bitmap_to_string_base64(bitmaps.get(k)));
        }
//zz
        //encodedImage = encodeImage(imageUri);

        String serviceID = sharedPreferences.getString("serviceId", null);
        System.out.println(serviceID);

        if (serviceID == null || serviceID.length() < 1) {
            Library.showToast(this,"Данный раздел будет доступен после сдачи объекта в эксплуатацию. По всем вопросам обращаться к вашему менеджеру объекта.");
        } else {
            //zz
            sendNewBiServiceApplic(encoded_to_base_64_ArrayImage, desr_array);
        }


//        Complaint update = new Complaint(Complaint.UNKNOWN_TYPE_ID, noteDescription, encodedImage, imageUri);
//
//        Intent intent = new Intent();
//        intent.putExtra(COMPLAINT_EXTRA, update);
//        intent.putExtra(POSITION_EXTRA, position);
//        setResult(RESULT_OK, intent);
//
//        onBackPressed();
    }

    private void sendNewBiServiceApplic(ArrayList<String> encoded_string_array, ArrayList<String> arrayList) {
        //encodedImage
        ShowProgressDialog();
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        try{
            StringRequest biServApplicRequest = new StringRequest(Request.Method.POST, "http://webapi.bi-group.org/api/v1/MobileClient/NewBiServiceStatement",
                    response -> {
                        goBack(response);
                    },
                    error -> {
                        showError(error);
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map headers = new HashMap();
                    headers.put("Token", token);
                    System.out.println("token " + token);
                    return headers;
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    String body = "";

                    JSONArray comments = new JSONArray();
                    try {

                        for(int y = 0;y < arrayList.size();y++){
                            JSONObject remark = new JSONObject();
                            remark.put("RemarkComment", arrayList.get(y));
                            try{
                                remark.put("SendFile", encoded_string_array.get(y));
                            }catch (Exception e){
                                remark.put("SendFile", "");
                            }

                            remark.put("SendExtension", "jpg");
                            comments.put(remark);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }



                    Map map = new HashMap();
                    realPropertyUID = sharedPreferences.getString("serviceId", "");
                    propertyUID = sharedPreferences.getString("propertyGuid", "");
                    String user = sharedPreferences.getString(HomeActivity.CURRENT_USER, "");
                    User profileID = new Gson().fromJson(user, User.class);
                    contractID = profileID.getUserName();
                    contractorIIN = profileID.getIin();

                    map.put("SendContractorIIN", contractorIIN);
                    map.put("SendRealPropertyUID", realPropertyUID);
                    map.put("SendContractorUID", contractID);
                    map.put("SendRemarkComment", comments);
                    map.put("SendPropertyUID", propertyUID);
                    String fullBody = new Gson().toJson(map);
                    JSONObject fullJson = new JSONObject(map);
                    System.out.println("fullJson " + fullJson);
                    Library.edittextar = new ArrayList<>();
                    return fullJson.toString().getBytes();
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }


            };

            biServApplicRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestQueue.add(biServApplicRequest);
        }catch (Exception e){

        }



    }

    private void goBack(String otvet) {
        HideProgressDialog();
        if (otvet.indexOf("успешно") > 0) {
            Toast.makeText(this, "Заявка создана успешно", Toast.LENGTH_LONG).show();
            onActivityResult(10, 1, null);
        } else {
            Toast.makeText(this, "Ошибка сети, временные проблемы на сервере", Toast.LENGTH_LONG).show();
        }
        onBackPressed();
    }

    public void addZamech(int position){

        try{
            countarray.add("1");
            serviceAdapter.notifyItemInserted(countarray.size() - 1);
        }catch (Exception e){

        }

       //serviceAdapter.notifyDataSetChanged();
    }

    public void remZamech(int position){

        if(countarray.size() > 1){
           // countarray.remove(countarray.size() - 1);
            try{
                countarray.remove(position);
                //Library.edittextar.remove(position);
            }catch (Exception e){
                try{
                    countarray.remove(countarray.size() - 1);
                    //Library.edittextar.remove(Library.edittextar.size() - 1);
                }catch (Exception e1){

                }

            }


        }

        try{
            if(bitmaps.size() > 0){
                Bitmap no_image = BitmapFactory.decodeResource(getResources(),R.drawable.no_image);
                bitmaps.set(position,no_image);
            }

            if(position > 0){
                serviceAdapter.notifyItemRemoved(position);
            }
        }catch (Exception e){

        }



        //serviceAdapter.notifyDataSetChanged();
    }
//zz
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_CAMERA_CAPTURE = 2;

    int positionto_insert_image = 0;

    private String selectedImagePath = "";
    final private int PICK_IMAGE = 1;
    final private int CAPTURE_IMAGE = 2;



    @Override
    public void onHandleSelection(int position, String text) {

        if(text.equals("add_photo")){

            positionto_insert_image = position;



            //dialog
            android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(this);
            builder1.setMessage("Камера или Галерея?");
            builder1.setCancelable(true);

            builder1.setPositiveButton(
                    "Галерея",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                            try{

                                if (ContextCompat.checkSelfPermission(activity,
                                        Manifest.permission.READ_EXTERNAL_STORAGE)
                                        != PackageManager.PERMISSION_GRANTED) {

                                    //  Разрешение не предоставляется
                                    // Должны ли мы показать объяснение?
                                    if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                                        // Показывать объяснение пользователю * асинхронно * - не блокировать
                                        Library.showToast(getApplicationContext(),"Необходимо разрешение на чтение галереи");
                                        // этот поток ожидает ответа пользователя! После пользователя
                                        // видит объяснение, попробуйте еще раз запросить разрешение.
                                    } else {
                                        // No explanation needed; request the permission
                                        ActivityCompat.requestPermissions(activity,
                                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                                REQUEST_IMAGE_CAPTURE);

                                        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                                        // app-defined int constant. The callback method gets the
                                        // result of the request.
                                    }
                                } else {
                                    // Permission has already been granted
                                    Intent takePictureIntent = new Intent(Intent.ACTION_GET_CONTENT);       //from gallery
                                    takePictureIntent.setType("image/*");
                                    takePictureIntent.putExtra("crop", "true");
                                    takePictureIntent.putExtra("scale", true);
                                    takePictureIntent.putExtra("outputX", 256);
                                    takePictureIntent.putExtra("outputY", 256);
                                    takePictureIntent.putExtra("aspectX", 1);
                                    takePictureIntent.putExtra("aspectY", 1);
                                    //takePictureIntent.putExtra("return-data", true);
                                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                                    }
                                }

                            }catch (Exception e){
                                Library.showToast(getApplicationContext(),"не удалось открыть галерею необходимо дать приложению доступ к галерее в настройках телефона");
                            }



                        }
                    });

            builder1.setNegativeButton(
                    "Камера",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();

                            try{

                                if (ContextCompat.checkSelfPermission(activity,
                                        Manifest.permission.CAMERA)
                                        != PackageManager.PERMISSION_GRANTED) {

                                    //  Разрешение не предоставляется
                                    // Должны ли мы показать объяснение?
                                    if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                                            Manifest.permission.CAMERA)) {
                                        // Показывать объяснение пользователю * асинхронно * - не блокировать
                                        Library.showToast(getApplicationContext(),"Необходимо разрешение на использование камеры");
                                        // этот поток ожидает ответа пользователя! После пользователя
                                        // видит объяснение, попробуйте еще раз запросить разрешение.
                                    } else {
                                        // No explanation needed; request the permission
                                        ActivityCompat.requestPermissions(activity,
                                                new String[]{Manifest.permission.CAMERA},
                                                REQUEST_CAMERA_CAPTURE);

                                        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                                        // app-defined int constant. The callback method gets the
                                        // result of the request.
                                    }
                                } else {

                                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

//                                    intent.putExtra("scale", "true");
//                                    intent.putExtra("outputX", 400);
//                                    intent.putExtra("outputY", 400);
//                                    intent.putExtra("aspectX", 1);
//                                    intent.putExtra("aspectY", 1);

                                    startActivityForResult(intent, REQUEST_CAMERA_CAPTURE);

                                }



                            }catch(Exception e){
                                Library.showToast(getApplicationContext(),"не удалось получить доступ к камере, необходимо дать доступ приложению к камере");
                            }



                        }
                    });

            android.app.AlertDialog alert11 = builder1.create();
            alert11.show();
            //dialog


//            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
//                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
//            }
        }else if(text.equals("del_photo")){

            try{
                //bitmaps.remove(position);
                Bitmap no_image = BitmapFactory.decodeResource(getResources(),R.drawable.no_image);
                bitmaps.set(position,no_image);

            }catch (Exception e){

            }
            //serviceAdapter.notifyDataSetChanged();
            //serviceAdapter.notifyDataSetChanged();

        }else if(text.equals("addzamech")){
            addZamech(position);
        }else if(text.equals("removezamech")){
            remZamech(position);
        }



    }


    ArrayList<Bitmap> bitmaps = new ArrayList<>();

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//xx
        try{

            if (resultCode == RESULT_OK) {

                if(requestCode == REQUEST_IMAGE_CAPTURE){
                    //gallery

                    try{

                        if (data == null) {
                            //Display an error
                            return;
                        }


                        InputStream inputStream = getContentResolver().openInputStream(data.getData());

                        Bitmap bitmapconvert = BitmapFactory.decodeStream(inputStream);
                        Bitmap bitmap = Bitmap.createScaledBitmap(bitmapconvert, 600, 600, false);


                        //Uri selectedImage = data.getData();
                       // Bitmap bitmap = Bitmap.createScaledBitmap(MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage), 50, 50, false);
                       // Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                        UpdateBitmapImage(bitmap);

                    }catch (IOException e){
                        Library.showToast(this,"Не удалось загрузить изображение вероятно ваше устройство не из поддерживаемых моделей");
                    }

                }else if(requestCode == REQUEST_CAMERA_CAPTURE){
                    //camera
                   // try{
                    if (data == null) {
                        //Display an error
                        return;
                    }

                    try{
                        Bundle extras = data.getExtras();
                        Bitmap imageBitmap = (Bitmap) extras.get("data");

                        Bitmap newcompressbitmap = Bitmap.createScaledBitmap(imageBitmap, 600, 600, false);

                        UpdateBitmapImage(newcompressbitmap);
                    }catch (Exception e){
                        Library.showToast(getApplicationContext(),"изображение должно быть не менее 600 px в щирину и в высоту");
                    }



                }

            }

        }catch (Exception e){
            Library.showToast(this,"не удалось обработать изображение добавьте его из галереи");
        }



    }
    //zz




//bit
    public void UpdateBitmapImage(Bitmap bitmap){

        //positionto_insert_image


        try{
            bitmaps.set(positionto_insert_image,bitmap);


            if(Library.StorageTextfix == 1){
                countarray.set(positionto_insert_image,Library.StorageText);
                Library.StorageTextfix = 0;
            }

                serviceAdapter.notifyItemChanged(positionto_insert_image);


        }catch (Exception e){

        }



    }




    public void deleteComplaint(View view) {

        onBackPressed();
    }

    public static String Convert_bitmap_to_string_base64(Bitmap bitmap)
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

        byte[] imageBytes = baos.toByteArray();

        String base64String = Base64.encodeToString(imageBytes, Base64.NO_WRAP);

        return base64String;
    }

    private String encodeImage(@NonNull Uri uri) throws FileNotFoundException {
        Bitmap bm = null;
        try {
            bm = decodeSampledInputStream(uri, 320, 240);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.WEBP, 70, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();
        return Base64.encodeToString(b, Base64.NO_WRAP);
    }

    public Bitmap decodeSampledInputStream(@NonNull Uri uri,
                                           int reqWidth, int reqHeight) throws IOException {
        ContentResolver contentResolver = getContentResolver();
        InputStream inputStream = contentResolver.openInputStream(uri);
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(inputStream, null, options);
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        inputStream = contentResolver.openInputStream(uri);
        return BitmapFactory.decodeStream(inputStream, null, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    private void showError(VolleyError error) {
        error.printStackTrace();
        HideProgressDialog();

        if (error.networkResponse != null) {
            System.out.println("error: " + new String(error.networkResponse.data));
        }
        if (error.networkResponse != null && error.networkResponse.statusCode / 100 == 4) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(new String(error.networkResponse.data));
                Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();


            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(this, "Ошибка сети, временные проблемы на сервере", Toast.LENGTH_LONG).show();
        }
    }


    public void ShowProgressDialog() {
        dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.progress_dialog_layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);
        b = dialogBuilder.create();
        b.show();
    }

    public void HideProgressDialog() {
        b.dismiss();
    }
}