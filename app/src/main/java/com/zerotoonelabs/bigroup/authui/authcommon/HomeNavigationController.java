package com.zerotoonelabs.bigroup.authui.authcommon;


import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.zerotoonelabs.bigroup.Library.Fragments.Pdf_fragment;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.authui.SingleHomeFragmentActivity;
import com.zerotoonelabs.bigroup.authui.paymentschedule.PaymentScheduleFragment;
import com.zerotoonelabs.bigroup.authui.paymentscheduledetails.PaymentScheduleDetailsFragment;
import com.zerotoonelabs.bigroup.authui.pdfviewer.PdfViewerFragment;
import com.zerotoonelabs.bigroup.fragments.peny.PayPenyFragment;
import com.zerotoonelabs.bigroup.ui.visitobject.VisitObjectFragment;

import javax.inject.Inject;

public class HomeNavigationController {
    private final FragmentManager fragmentManager;
    private final int containerId;

    @Inject
    public HomeNavigationController(SingleHomeFragmentActivity activity) {
        this.containerId = R.id.flContent;
        fragmentManager = activity.getSupportFragmentManager();
    }

    public void navigateToVisitObject(String token) {
        VisitObjectFragment fragment = VisitObjectFragment.newInstance(token);
        String tag = "Посещение объекта";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToPdfViewer(String token, int pdfId) {
        PdfViewerFragment fragment = PdfViewerFragment.newInstance(token, pdfId);
        String tag = "Просмотр Pdf";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToPdfViewer2(String token, int propertyId, String contractId) {
        PdfViewerFragment fragment = PdfViewerFragment.newInstance3(token, contractId, propertyId);
        String tag = "Просмотр Pdf";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToPdfViewerStatus(String token, int propertyId, String contractId, int type) {
        PdfViewerFragment fragment = PdfViewerFragment.newInstanceStatus(token, propertyId, contractId, type);
        String tag = "Просмотр Pdf";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToPdfViewer3(String token, int propertyId) {
        Pdf_fragment fragment = new Pdf_fragment();
        String tag = "Просмотр Pdf";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToPaymentSchedule(String token, String contractId, int propertyId) {
        PaymentScheduleFragment fragment = PaymentScheduleFragment.newInstance(token, contractId, propertyId);
        String tag = "График платежей";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToPaymentScheduleDetails(String token, String contractId, String propertyGuid) {
        PaymentScheduleDetailsFragment fragment = PaymentScheduleDetailsFragment.newInstance(token, contractId, propertyGuid);
        String tag = "График платежей";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToPeny(String token, String contractId, String propertyGuid) {
        PayPenyFragment fragment = PayPenyFragment.newInstance(token, contractId, propertyGuid);
        String tag = "График платежей";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateBack() {
        fragmentManager.popBackStack();
    }
}
