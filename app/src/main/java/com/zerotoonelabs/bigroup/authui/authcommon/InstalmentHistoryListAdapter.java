package com.zerotoonelabs.bigroup.authui.authcommon;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.InstalmentHistoryItemBinding;
import com.zerotoonelabs.bigroup.ui.common.DataBoundListAdapter;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.InstalmentHistory;

public class InstalmentHistoryListAdapter extends DataBoundListAdapter<InstalmentHistory, InstalmentHistoryItemBinding> {

    private final InstalmentHistoryClickCallback instalmentClickCallback;

    public InstalmentHistoryListAdapter(InstalmentHistoryClickCallback instalmentClickCallback) {
        this.instalmentClickCallback = instalmentClickCallback;
    }

    @Override
    protected InstalmentHistoryItemBinding createBinding(ViewGroup parent) {
        InstalmentHistoryItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.instalment_history_item,
                        parent, false);
        binding.getRoot().setOnClickListener(v -> {
            InstalmentHistory repo = binding.getInstalmentHistory();
            if (repo != null && instalmentClickCallback != null) {
                instalmentClickCallback.onClick(repo);
            }
        });

        return binding;
    }

    @Override
    protected void bind(InstalmentHistoryItemBinding binding, InstalmentHistory item, int position) {
        binding.setInstalmentHistory(item);
    }

    @Override
    protected boolean areItemsTheSame(InstalmentHistory oldItem, InstalmentHistory newItem) {
        return Objects.equals(oldItem.date, newItem.date);
    }

    @Override
    protected boolean areContentsTheSame(InstalmentHistory oldItem, InstalmentHistory newItem) {
        return Objects.equals(oldItem.details, newItem.details);
    }

    public interface InstalmentHistoryClickCallback {
        void onClick(InstalmentHistory utility);
    }
}