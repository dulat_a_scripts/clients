package com.zerotoonelabs.bigroup.authui.statuses;

import android.annotation.SuppressLint;
import android.arch.lifecycle.LiveData;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.zerotoonelabs.bigroup.BiApplication;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.api.ApiResponse;
import com.zerotoonelabs.bigroup.authui.SingleHomeFragmentActivity;
import com.zerotoonelabs.bigroup.repository.BiRepository;
import com.zerotoonelabs.bigroup.entity.StatusDoc;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

public class StatusGetDocumentsActivity extends AppCompatActivity {

    @Inject
    BiRepository repository;

    LiveData<ApiResponse<List<StatusDoc>>> liveData;
    TextView textView1, textView2, textView3, textView4, textView5, textView6, textView10, textView11, textView12;

    String contractId, propertyId, token;


    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_get_documents);
        ((BiApplication) getApplicationContext()).dispatchingAndroidInjector.inject(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        contractId = intent.getStringExtra("contractId");
        propertyId = intent.getStringExtra("propertyId");
        textView1 = (TextView) findViewById(R.id.textView1);
        textView2 = (TextView) findViewById(R.id.textView2);
        textView3 = (TextView) findViewById(R.id.textView3);
        textView4 = (TextView) findViewById(R.id.textView4);
        textView5 = (TextView) findViewById(R.id.textView5);
        textView6 = (TextView) findViewById(R.id.textView6);

        textView10 = (TextView) findViewById(R.id.textView10);
        textView11 = (TextView) findViewById(R.id.textView11);
        textView12 = (TextView) findViewById(R.id.textView12);

        token = PreferenceManager.getDefaultSharedPreferences(this).getString("token", null);
        liveData = repository.loadStatusDoc(token, propertyId);
        liveData.observe(this, result -> {
            if (result != null && result.body != null) {
                textView1.setText(formatDate(result.body.get(0).getDataPodpisaniyaDogovora().substring(0, 10)));
                String price = formatPrice(result.body.get(0).getStoimostPomesheniya()) + " тг.";
                textView2.setText(price);
                textView3.setText(result.body.get(0).getNomerPoTehpasportu());
                textView4.setText(formatPrice(result.body.get(0).getPoleznayaPloshadBTI()));
                textView5.setText(formatPrice(result.body.get(0).getPloshadBalkonovBTI()));
                textView6.setText(formatPrice(result.body.get(0).getZhilayaPloshadBTI()));
            }
        });

        textView10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(StatusGetDocumentsActivity.this, SingleHomeFragmentActivity.class);
                intent.putExtra(SingleHomeFragmentActivity.STATUS_PDF, 3);
                startActivity(intent);
            }
        });

        textView11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(StatusGetDocumentsActivity.this, SingleHomeFragmentActivity.class);
                intent.putExtra(SingleHomeFragmentActivity.STATUS_PDF, 4);
                startActivity(intent);
            }
        });

        textView12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(StatusGetDocumentsActivity.this, SingleHomeFragmentActivity.class);
                intent.putExtra(SingleHomeFragmentActivity.STATUS_PDF, 6);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private String formatDate(String input) {
        if (input == null) return null;
        SimpleDateFormat outputSdf = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
        SimpleDateFormat inputSdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

        try {
            Date inputDate = inputSdf.parse(input);
            return outputSdf.format(inputDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String formatPrice(Float price){
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
        symbols.setGroupingSeparator(' ');
        formatter.setDecimalFormatSymbols(symbols);
        return formatter.format(price);
    }
}
