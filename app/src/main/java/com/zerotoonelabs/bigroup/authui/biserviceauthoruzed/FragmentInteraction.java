package com.zerotoonelabs.bigroup.authui.biserviceauthoruzed;

import android.support.v4.app.Fragment;

/**
 * Created by temirlan on 17.11.17.
 */
public interface FragmentInteraction{
    void changeFragment(Fragment fragment);
}
