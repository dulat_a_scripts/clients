package com.zerotoonelabs.bigroup.binding;

import android.databinding.BindingAdapter;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.NavigationView;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.request.RequestOptions;
import com.zerotoonelabs.bigroup.R;

import java.io.File;

/**
 * Data Binding adapters specific to the app.
 */
public class BindingAdapters {
    @BindingAdapter("expandedMenu")
    public static void inflateMenu(NavigationView navigationView, boolean expanded) {
        navigationView.getMenu().clear();
        navigationView.inflateMenu(expanded ? R.menu.apartment_filter : R.menu.activity_authorized_main_drawer);
    }

    @BindingAdapter("htmlText")
    public static void setHtmlText(TextView view, String text) {
        if (text == null) return;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            view.setText(Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT));
        } else {
            view.setText(Html.fromHtml(text));
        }
    }

    @BindingAdapter("phoneMask")
    public static void setPhoneMask(EditText view, boolean enable) {
        if (!enable) return;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            view.addTextChangedListener(new PhoneNumberFormattingTextWatcher("KZ"));
        } else {
            view.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        }
    }

    @BindingAdapter("blockStatus")
    public static void setBlockStatus(TextView view, int status) {
        String text = "";
        switch (status) {
            case 0:
                text = "Старый объект";
                break;
            case 1:
                text = "Строящийся";
                break;
            default:
                text = "Перспективный";
                break;
        }
        view.setText(text);
    }


    @BindingAdapter("setEtazh")
    public static void setEtazh(TextView view, int etazh) {

        view.setText(String.valueOf(etazh));
    }

    @BindingAdapter("setNumber_of_parking")
    public static void setNumber_of_parking(TextView view,int number){
        view.setText(String.valueOf(number));
    }


    @BindingAdapter("flatStatus")
    public static void setFlatStatus(ImageView view, int status) {
        int resId;
        switch (status) {
            case 2:
                resId = R.drawable.ic_grey_circle;
                break;
            case 3:
                resId = R.drawable.ic_grey_circle;
                break;
            default:
                resId = R.drawable.ic_success;
                break;
        }
        view.setImageResource(resId);
    }

    @BindingAdapter("visibleGone")
    public static void showHide(View view, boolean show) {
        view.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @BindingAdapter("currentHeaderLayout")
    public static void setHeaderLayout(NavigationView view, boolean signedIn) {
        view.inflateHeaderView(signedIn ? R.layout.nav_header_auth : R.layout.nav_header_main);
    }

    @BindingAdapter("imageUri")
    public static void setImageUri(ImageView imageView, Uri uri) {
        if (uri == null) return;
        Glide.with(imageView).load(uri).into(imageView);
    }

    @BindingAdapter("filepath")
    public static void loadImage(ImageView imageView, String filepath) {
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder);

        Glide.with(imageView)
                .asBitmap()
                .apply(options)
                .load(filepath == null ? null : new File(filepath))
                .into(imageView);
    }

    @BindingAdapter("imageUrl")
    public static void loadImageUrl(ImageView imageView, String url) {
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder);

        Glide.with(imageView)
                .asBitmap()
                .apply(options)
                .load(url)
                .into(imageView);
    }

    @BindingAdapter({"headerImageUrl", "token"})
    public static void loadHeaderImageUrl(ImageView imageView, String url, String token) {

        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder);

        if (url == null || token == null) {
            Glide.with(imageView)
                    .asBitmap()
                    .apply(options)
                    .load(url)
                    .into(imageView);

            return;
        }
        GlideUrl glideUrl = new GlideUrl(url, new LazyHeaders.Builder()
                .addHeader("Token", token)
                .build());

        Glide.with(imageView)
                .asBitmap()
                .apply(options)
                .load(glideUrl)
                .into(imageView);
    }
}
