package com.zerotoonelabs.bigroup;

/**
 * Created by Admin on 13.09.2017.
 */

public class Notification {
    String type ="", text = "", process = "", date, sum, step;
    Integer number;
    Notification(String type, String text, Integer number){
        this.type = type;
        this.text = text;
        this.number = number;
    }

    Notification(Integer number){
        this.number = number;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public void setSum(String sum) {
        this.sum = sum;
    }

    public String getDate() {
        return date;
    }

    public String getStep() {
        return step;
    }

    public String getSum() {
        return sum;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public String getProcess() {
        return process;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
