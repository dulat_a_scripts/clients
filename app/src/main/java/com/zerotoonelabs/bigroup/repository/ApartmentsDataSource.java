package com.zerotoonelabs.bigroup.repository;

import android.arch.paging.PositionalDataSource;
import android.support.annotation.NonNull;

import com.zerotoonelabs.bigroup.api.Webservice;
import com.zerotoonelabs.bigroup.entity.Apartments.Apartment;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import retrofit2.Response;

import static android.text.TextUtils.isEmpty;
import static java.util.Collections.emptyList;

public class ApartmentsDataSource extends PositionalDataSource<Apartment> {

    private Webservice webservice;
    private String token;
    private HashMap<String, String> filter;

    private int count = 0;

    ApartmentsDataSource(Webservice webservice, String token, HashMap<String, String> filter) {
        this.webservice = webservice;
        this.token = token;
        this.filter = filter;
    }

    private int computeCount() {
        return count;
    }

    private int computePageNumber(int startPosition, int loadCount) {
        if (startPosition < 1) return 1;

        return startPosition / 10;
    }

    private List<Apartment> loadRangeInternal(int startPosition, int loadCount) throws IOException {
        int page = computePageNumber(startPosition, loadCount);
//zz
        Response<List<Apartment>> response = webservice.getApartments2(token, filter, page).execute();
        if (response.isSuccessful()) {
            List<Apartment> apartments = response.body();

            if (apartments != null && !apartments.isEmpty()) {
                count += apartments.size();

                DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
                symbols.setGroupingSeparator(' ');
                formatter.setDecimalFormatSymbols(symbols);

                String urlPrefix = "http://webapi.bi-group.org/api/v1/SalesBIGroup/GetImage?ifImg=1&path=";

                for (Apartment apartment : apartments) {
                    apartment.setFormattedPrice(formatPrice(formatter, apartment.getPriceF()));
                    apartment.setFormattedPriceM(formatPrice(formatter, apartment.getPriceMF()));
                    apartment.setFormattedTitle(apartment.getTitle());

                    apartment.setListImageUrl(!isEmpty(apartment.getListImageUrl()) ? urlPrefix + apartment.getListImageUrl() : null);
                    apartment.setImageUrl(!isEmpty(apartment.getImageUrl()) ? urlPrefix + apartment.getImageUrl() : null);
                    apartment.setDeadline(apartment.getDeadline() != null ? formatDate(apartment.getDeadline()) : null);
                }

                return apartments;
            }
        }

        return emptyList();
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams params, @NonNull LoadInitialCallback<Apartment> callback) {
        int totalCount = computeCount();
        int position = computeInitialLoadPosition(params, totalCount);
        int loadSize = computeInitialLoadSize(params, position, totalCount);
        try {
            callback.onResult(loadRangeInternal(position, loadSize), position);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void loadRange(@NonNull LoadRangeParams params, @NonNull LoadRangeCallback<Apartment> callback) {
        try {
            callback.onResult(loadRangeInternal(params.startPosition, params.loadSize));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String formatPrice(DecimalFormat formatter, double price) {
        return formatter.format(price);
    }

    private String formatDate(String input) {
//        2019-03-26T00:00:00
        SimpleDateFormat outputSdf = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
        SimpleDateFormat inputSdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());

        try {
            Date inputDate = inputSdf.parse(input);
            return outputSdf.format(inputDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
