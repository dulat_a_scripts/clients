package com.zerotoonelabs.bigroup.repository;

import android.arch.paging.DataSource;

import com.zerotoonelabs.bigroup.api.Webservice;
import com.zerotoonelabs.bigroup.entity.Apartments.Apartment;

import java.util.HashMap;

class ApartmentDataSourceFactory implements DataSource.Factory<Integer, Apartment> {

    private Webservice webservice;
    private String token;
    private HashMap<String, String> filter;

    ApartmentDataSourceFactory(Webservice webservice, String token, HashMap<String, String> filter) {
        this.webservice = webservice;
        this.token = token;
        this.filter = filter;
    }

    @Override
    public DataSource<Integer, Apartment> create() {
        return new ApartmentsDataSource(webservice, token, filter);
    }
}
