package com.zerotoonelabs.bigroup.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.paging.DataSource;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.zerotoonelabs.bigroup.AppExecutors;
import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.api.ApiResponse;
import com.zerotoonelabs.bigroup.api.ServicePropertyResponse;
import com.zerotoonelabs.bigroup.api.Webservice;
import com.zerotoonelabs.bigroup.db.ApartmentDao;
import com.zerotoonelabs.bigroup.db.BiDb;
import com.zerotoonelabs.bigroup.db.BlockVisitDao;
import com.zerotoonelabs.bigroup.db.RealEstateDao;
import com.zerotoonelabs.bigroup.db.ServiceRequestDao;
import com.zerotoonelabs.bigroup.util.AbsentLiveData;
import com.zerotoonelabs.bigroup.util.RateLimiter;
import com.zerotoonelabs.bigroup.entity.Apartments.Apartment;
import com.zerotoonelabs.bigroup.entity.BsDate;
import com.zerotoonelabs.bigroup.entity.BsTime;
import com.zerotoonelabs.bigroup.entity.BuildingSite;
import com.zerotoonelabs.bigroup.entity.KeyTransfer;
import com.zerotoonelabs.bigroup.entity.ReBlock;
import com.zerotoonelabs.bigroup.entity.RealEstate;
import com.zerotoonelabs.bigroup.entity.Resource;
import com.zerotoonelabs.bigroup.entity.ServiceProperty;
import com.zerotoonelabs.bigroup.entity.ServiceRequest;
import com.zerotoonelabs.bigroup.entity.StatusAuth.StatusAuth;
import com.zerotoonelabs.bigroup.entity.StatusDoc;
import com.zerotoonelabs.bigroup.entity.VisitNumber;
import com.zerotoonelabs.bigroup.entity.blockvisit.BlockVisit;
import com.zerotoonelabs.bigroup.entity.statusesPred.StatusPred;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.text.TextUtils.isEmpty;

@Singleton
public class BiRepository {
    private final Webservice webservice;
    private final ServiceRequestDao dao;
    private final RealEstateDao realEstateDao;
    private final ApartmentDao apartmentDao;
    private final BlockVisitDao blockVisitDao;
    private final AppExecutors appExecutors;
    public final BiDb db;
    private final Gson gson;

    private RateLimiter<String> rateLimiter = new RateLimiter<>(30, TimeUnit.MINUTES);

    @Inject
    public BiRepository(Webservice webservice, ServiceRequestDao dao, RealEstateDao realEstateDao, ApartmentDao apartmentDao, BlockVisitDao blockVizitDao, AppExecutors appExecutors, BiDb db, Gson gson) {
        this.webservice = webservice;
        this.dao = dao;
        this.realEstateDao = realEstateDao;
        this.apartmentDao = apartmentDao;
        this.blockVisitDao = blockVizitDao;
        this.appExecutors = appExecutors;
        this.db = db;
        this.gson = gson;
    }

    public LiveData<ApiResponse<StatusAuth>> loadFlatStatus(String token, String contractId, String propertyId) {
        return webservice.getFlatStatus(token, contractId, propertyId);
    }

    public LiveData<List<Apartment>> getApartments() {
        return db.apartmentDao().getApartments();
    }

    public LiveData<Resource<List<VisitNumber>>> loadVisitNumbers(String token, String blockId) {
        return new NetworkBoundResource<List<VisitNumber>, List<VisitNumber>>(appExecutors) {

            @Override
            protected String createRequestBody() {
                return "";
            }

            @Override
            protected void saveCallResult(@NonNull List<VisitNumber> item) {
                for (VisitNumber visitNumber : item) {
                    visitNumber.setBlockId(blockId);
                }
                blockVisitDao.insertVisitNumbers(item);
            }

            @Override
            protected boolean shouldFetch(@Nullable List<VisitNumber> data) {
                return data == null || data.isEmpty();
            }

            @NonNull
            @Override
            protected LiveData<List<VisitNumber>> loadFromDb() {
                return blockVisitDao.getVisitNumbers(blockId);
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<VisitNumber>>> createCall(RequestBody requestBody) {
                return webservice.getVisitNumbers(token, blockId);
            }
        }.asLiveData();
    }

    public LiveData<Resource<List<ServiceProperty>>> loadServiceProperties(@NonNull String token) {
        return new NetworkBoundResource<List<ServiceProperty>, ServicePropertyResponse>(appExecutors) {

            @Override
            protected String createRequestBody() {
                return "";
            }

            @Override
            protected void saveCallResult(@NonNull ServicePropertyResponse item) {
                List<ServiceProperty> properties = item.serviceProperties;
                if (item.serviceProperties != null) {
                    dao.insertProperties(properties);
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable List<ServiceProperty> data) {
                return data == null || data.isEmpty();
            }

            @NonNull
            @Override
            protected LiveData<List<ServiceProperty>> loadFromDb() {
                return dao.getServiceProperties();
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<ServicePropertyResponse>> createCall(RequestBody requestBody) {
                return webservice.getServiceProperties(token);
            }
        }.asLiveData();
    }

    public LiveData<Resource<List<RealEstate>>> loadRealEstates(@NonNull String token,
                                                                @Nullable String city, int status) {
        return new NetworkBoundResource<List<RealEstate>, List<RealEstate>>(appExecutors) {

            private String formatPrice(DecimalFormat formatter, int price) {
                return formatter.format(price);
            }

            @Override
            protected String createRequestBody() {
                return "";
            }

            @Override
            protected void saveCallResult(@NonNull List<RealEstate> item) {
                DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
                symbols.setGroupingSeparator(' ');
                formatter.setDecimalFormatSymbols(symbols);

                String urlPrefix = "http://webapi.bi-group.org/api/v1/SalesBIGroup/GetImage?ifImg=1&path=";

                for (RealEstate realEstate : item) {
                    realEstate.setFormattedPrice(realEstate.getPriceM() == 0 ? null : formatPrice(formatter, realEstate.getPriceM()));
                    realEstate.setLogoWebUrl(!isEmpty(realEstate.getLogoWebUrl()) ? urlPrefix + realEstate.getLogoWebUrl() : null);
                    realEstate.setLogoUrl(!isEmpty(realEstate.getLogoUrl()) ? urlPrefix + realEstate.getLogoUrl() : null);
                    realEstate.setPhotoUrl(!isEmpty(realEstate.getPhotoUrl()) ? urlPrefix + realEstate.getPhotoUrl() : null);
                    realEstate.setPhotoWebUrl(!isEmpty(realEstate.getPhotoWebUrl()) ? urlPrefix + realEstate.getPhotoWebUrl() : null);
                    String latLong = realEstate.getLatlong();
                    realEstate.setLatitude(formatLatitude(latLong));
                    realEstate.setLongitude(formatLongitude(latLong));
                }
                db.beginTransaction();
                try {
                    realEstateDao.insert(item);
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable List<RealEstate> data) {
                return data == null || data.isEmpty() || rateLimiter.shouldFetch(token);
            }

            @NonNull
            @Override
            protected LiveData<List<RealEstate>> loadFromDb() {
                if (city != null && status != -1) {
                    return realEstateDao.getData(city, status + "");
                }
                return realEstateDao.getData();
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<RealEstate>>> createCall(RequestBody requestBody) {
                return webservice.getRealEstates(token);
            }

            @Override
            protected void onFetchFailed() {
                rateLimiter.reset(token);
            }
        }.asLiveData();
    }

    public LiveData<Resource<List<ServiceRequest>>> loadServiceRequests(String token) {
        return new NetworkBoundResource<List<ServiceRequest>, List<ServiceRequest>>(appExecutors) {

            @Override
            protected String createRequestBody() {
                return null;
            }

            @Override
            protected void saveCallResult(@NonNull List<ServiceRequest> item) {

            }

            @Override
            protected boolean shouldFetch(@Nullable List<ServiceRequest> data) {
                return false;
            }

            @NonNull
            @Override
            protected LiveData<List<ServiceRequest>> loadFromDb() {
                return dao.getServiceRequests();
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<ServiceRequest>>> createCall(RequestBody requestBody) {
                return null;
            }
        }.asLiveData();
    }


    public LiveData<Resource<Boolean>> shouldUpdateToken(String token) {
        MutableLiveData<Resource<Boolean>> result = new MutableLiveData<>();
        result.setValue(Resource.loading(null));
        webservice.getCities(token).enqueue(new Callback<List<String>>() {
            @Override
            public void onResponse(@NonNull Call<List<String>> call, @NonNull Response<List<String>> response) {
                List<String> cityList = response.body();
                if (cityList == null || cityList.isEmpty()) {
                    result.setValue(Resource.error("Список городов пуст", null));
                    return;
                }
                result.setValue(Resource.success(false));
            }

            @Override
            public void onFailure(@NonNull Call<List<String>> call, @NonNull Throwable t) {
                result.setValue(Resource.error(t.getMessage(), null));
            }
        });

        return result;
    }

//zz getcities



    public LiveData<Resource<Boolean>> checkToken(String token) {
        final MutableLiveData<Resource<Boolean>> result = new MutableLiveData<>();
        result.setValue(Resource.loading(null));
        webservice.checkToken(token).enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                int code = response.code();
                result.setValue(code >= 200 && code < 300 ? Resource.success(true) : Resource.error(response.message(), null));
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                result.setValue(Resource.error(t.getMessage(), null));
            }
        });
        return result;
    }

    public LiveData<Resource<String>> loadTokenTemp(final Context applicationContext, String auth) {
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(applicationContext);
        final MutableLiveData<Resource<String>> result = new MutableLiveData<>();
        result.setValue(Resource.loading(null));
        webservice.getToken2(auth).enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                Headers headers = response.headers();
                if (headers == null || headers.get("Token") == null) {
                    sharedPreferences
                            .edit()
                            .remove("token")
                            .apply();

                    result.setValue(Resource.error(response.message(), null));
                } else {
                    String token = headers.get("Token");
                    Library.SavePhoneMemoryOne("token",token);
                    sharedPreferences
                            .edit()
                            .putString("token", token)
                            .apply();

                    result.setValue(Resource.success(token));
                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                sharedPreferences
                        .edit()
                        .remove("token")
                        .apply();

                result.setValue(Resource.error(t.getMessage(), null));
            }
        });
        return result;
    }

    public LiveData<Resource<String>> loadToken(final Context applicationContext, String tokenId, boolean shouldFetch) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(applicationContext);

        return new NetworkBoundResource<String, String>(appExecutors) {
            @Override
            protected String createRequestBody() {
                return "";
            }

            @Override
            protected boolean shouldProcessHeader() {
                return true;
            }

            @Override
            protected void saveCallHeader(@NonNull String item) {
                saveCallResult(item);
            }

            @Override
            protected void saveCallResult(@NonNull String item) {
                db.beginTransaction();
                try {
                    PreferenceManager.getDefaultSharedPreferences(applicationContext)
                            .edit()
                            .putString("token", item)
                            .apply();
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable String data) {
//                c3110ede-4b06-4cc5-90d1-ef53c7a75d27
                return data == null || shouldFetch;
            }

            @NonNull
            @Override
            protected LiveData<String> loadFromDb() {
                String token = PreferenceManager.getDefaultSharedPreferences(applicationContext)
                        .getString("token", null);
                MutableLiveData<String> tokenValue = new MutableLiveData<>();
                tokenValue.postValue(token);
                return tokenValue;
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<String>> createCall(RequestBody requestBody) {
                Library.SavePhoneMemoryOne("token",tokenId);
                return webservice.getToken(tokenId);
            }

            @Override
            protected void onFetchFailed() {
                sharedPreferences.edit().remove("tokenExpiry").apply();
            }
        }.asLiveData();
    }

    public LiveData<Resource<ServiceRequest>> loadServiceRequest(final String token, final ServiceRequest serviceRequest) {
        return new NetworkBoundResource<ServiceRequest, ServiceRequest>(appExecutors) {

            @Override
            protected String createRequestBody() {

                Map<String, Object> map = new HashMap<>();
                map.put("fio", serviceRequest.getFullName());
                map.put("phone", serviceRequest.getPhone());
                map.put("E-mail", serviceRequest.getEmail());
                map.put("iin", serviceRequest.getUid());
                // FIXME: 11/15/17 send proper statement_type_id
                map.put("statement_type_id", "8373f2e3-d0b1-11e5-b9a5-b4b52f5555e7");
//                map.put("statement_type_id", "");
                map.put("service_property_id", serviceRequest.getObject());
                map.put("number_property", serviceRequest.getApartmentNo());
                map.put("complaint_list", serviceRequest.getComplaints());

                return gson.toJson(map);
            }

            @Override
            protected void saveCallResult(@NonNull ServiceRequest item) {
                db.beginTransaction();
                serviceRequest.setId(item.getId());
                serviceRequest.setStatus(item.getStatus());
                serviceRequest.setDateTime(item.getDateTime());
                try {
                    dao.insert(serviceRequest);
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable ServiceRequest data) {
                return data == null;
            }

            @NonNull
            @Override
            protected LiveData<ServiceRequest> loadFromDb() {
                return dao.getServiceRequest(serviceRequest.getId());
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<ServiceRequest>> createCall(RequestBody requestBody) {
                return webservice.getServiceRequest(token, requestBody);
            }
        }.asLiveData();
    }


    public LiveData<Resource<List<ReBlock>>> loadReBlocks(String id, String token) {
        return new NetworkBoundResource<List<ReBlock>, List<ReBlock>>(appExecutors) {

            @Override
            protected String createRequestBody() {
                return "";
            }

            @Override
            protected void saveCallResult(@NonNull List<ReBlock> item) {

                for (ReBlock block : item) {
                    block.deadline = formatDate(block.deadline);
                }

                realEstateDao.insertBlocks(item);
            }

            @Override
            protected boolean shouldFetch(@Nullable List<ReBlock> data) {
                return data == null || data.isEmpty();
            }

            @NonNull
            @Override
            protected LiveData<List<ReBlock>> loadFromDb() {
                return realEstateDao.getBlocks(id);
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<ReBlock>>> createCall(RequestBody requestBody) {
                return webservice.getBlocks(token, id);
            }

            private String formatDate(String input) {
                if (input == null) return null;
//                2019-03-03T00:00:00
                SimpleDateFormat outputSdf = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
                SimpleDateFormat inputSdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());

                try {
                    Date inputDate = inputSdf.parse(input);
                    return outputSdf.format(inputDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.asLiveData();
    }

    public LiveData<Resource<List<BuildingSite>>> loadBuildingSites(String token) {
        return new NetworkBoundResource<List<BuildingSite>, List<BuildingSite>>(appExecutors) {

            @Override
            protected String createRequestBody() {
                return "";
            }

            @Override
            protected void saveCallResult(@NonNull List<BuildingSite> item) {
                realEstateDao.insertBuildingSites(item);
            }

            @Override
            protected boolean shouldFetch(@Nullable List<BuildingSite> data) {
                return data == null || data.isEmpty();
            }

            @NonNull
            @Override
            protected LiveData<List<BuildingSite>> loadFromDb() {
                return realEstateDao.getBuildingSites();
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<BuildingSite>>> createCall(RequestBody requestBody) {
                return webservice.getBuildingSites(token);
            }
        }.asLiveData();
    }

    public LiveData<Resource<BsDate>> loadBsDates(String id, String token) {
        MutableLiveData<Resource<BsDate>> result = new MutableLiveData<>();
        result.setValue(Resource.loading(null));
        webservice.getBuildingSiteDates(token, id).enqueue(new Callback<BsDate>() {
            @Override
            public void onResponse(@NonNull Call<BsDate> call, @NonNull Response<BsDate> response) {
                BsDate bsDate = response.body();
                if (bsDate == null || bsDate.dates == null) {
                    result.setValue(Resource.error("Посещение объекта закрыто", null));
                    return;
                }
                result.setValue(Resource.success(bsDate));
            }

            @Override
            public void onFailure(@NonNull Call<BsDate> call, @NonNull Throwable t) {
                result.setValue(Resource.error(t.getLocalizedMessage(), null));
            }
        });
        return result;
    }

    /*public LiveData<Resource<BsDate>> loadBsDates(String id, String token) {
        return new NetworkBoundResource<BsDate, BsDate>(appExecutors) {

            @Override
            protected String createRequestBody() {
                return "";
            }

            @Override
            protected void saveCallResult(@NonNull BsDate item) {
                BsDate bsDate = new BsDate(id, item.dates);
                realEstateDao.insertBsDate(bsDate);
            }

            @Override
            protected boolean shouldFetch(@Nullable BsDate data) {
                return data == null || data.dates == null;
            }

            @NonNull
            @Override
            protected LiveData<BsDate> loadFromDb() {
                return realEstateDao.getBuildingSiteDate(id);
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<BsDate>> createCall(RequestBody requestBody) {
                return webservice.getBuildingSiteDates(token, id);
            }
        }.asLiveData();
    }*/

    public LiveData<Resource<BsTime>> loadBsTimes(String id, String date, String token) {
        MutableLiveData<Resource<BsTime>> result = new MutableLiveData<>();
        result.setValue(Resource.loading(null));
        webservice.getBuildingSiteTimes(token, id, date).enqueue(new Callback<BsTime>() {
            @Override
            public void onResponse(@NonNull Call<BsTime> call, @NonNull Response<BsTime> response) {
                BsTime bsTime = response.body();
                result.setValue(bsTime == null || bsTime.times == null
                        ? Resource.error("Посещение объекта закрыто", null)
                        : Resource.success(bsTime));
            }

            @Override
            public void onFailure(@NonNull Call<BsTime> call, @NonNull Throwable t) {
                result.setValue(Resource.error(t.getLocalizedMessage(), null));
            }
        });
        return result;
    }
    /*public LiveData<Resource<BsTime>> loadBsTimes(String id, String date, String token) {
        return new NetworkBoundResource<BsTime, BsTime>(appExecutors) {

            @Override
            protected String createRequestBody() {
                return "";
            }

            @Override
            protected void saveCallResult(@NonNull BsTime item) {
                BsTime bsTime = new BsTime(id, date, item.times);
                realEstateDao.insertBsTime(bsTime);
            }

            @Override
            protected boolean shouldFetch(@Nullable BsTime data) {
                return data == null || data.times == null;
            }

            @NonNull
            @Override
            protected LiveData<BsTime> loadFromDb() {
                return realEstateDao.getBuildingSiteTimes(id, date);
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<BsTime>> createCall(RequestBody requestBody) {
                return webservice.getBuildingSiteTimes(token, id, date);
            }

        }.asLiveData();
    }*/


    public LiveData<Resource<String>> loadVisitObj(String fullName, String phone,
                                                   String objId, String date, String time, String token, String visitNumber) {
        final MutableLiveData<Resource<String>> message = new MutableLiveData<>();
        message.setValue(Resource.loading(null));
        Map<String, String> map = new HashMap<>();
        map.put("block", objId);
        map.put("date", date);
        map.put("time", time);
        map.put("fio", fullName);
        map.put("tel", phone);
        map.put("number", visitNumber);
        String stringBody = gson.toJson(map);
        RequestBody body = RequestBody.create(MediaType.parse("application/json"), stringBody);
        webservice.getVisitObj(token, body).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                JsonObject jsonObject = response.body();
                String messageResp = jsonObject != null ? jsonObject.get("message").getAsString() : null;
                message.setValue(messageResp == null ?
                        Resource.error("Не удалось подать заявку", null) :
                        Resource.success(messageResp));
            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                message.setValue(Resource.error(t.getMessage(), null));
            }
        });

        return message;
    }

    public LiveData<PagedList<Apartment>> loadApartments5(String token, HashMap<String, String> filter) {
        PagedList.Config builder = new PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setInitialLoadSizeHint(10)
                .setPageSize(10)
                .build();

        DataSource.Factory<Integer, Apartment> sourceFactory = new ApartmentDataSourceFactory(webservice, token, filter);

        return new LivePagedListBuilder<>(sourceFactory, builder)
                .setBackgroundThreadExecutor(appExecutors.networkIO())
                .build();
    }

    public LiveData<Resource<String>> sendRequest(String fullName, String phone, String site, String token) {
        final MutableLiveData<Resource<String>> response = new MutableLiveData<>();
        response.setValue(Resource.loading(null));
        Map<String, String> params = new HashMap<String, String>();
        params.put("number", phone);
        params.put("name", fullName);
        params.put("site", site);
        String stringBody = gson.toJson(params);
        RequestBody body = RequestBody.create(MediaType.parse("application/json"), stringBody);
        webservice.sendRequest(token, body).enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> resp) {
                String jsonObject = resp.body();
                response.setValue(jsonObject == null ?
                        Resource.error("Response is empty", null) :
                        Resource.success(jsonObject));
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                response.setValue(Resource.error(t.getMessage(), null));
            }
        });
        return response;
    }

    public LiveData<Resource<List<BlockVisit>>> loadBlockVisits(String token) {
        return new NetworkBoundResource<List<BlockVisit>, List<BlockVisit>>(appExecutors) {

            @Override
            protected String createRequestBody() {
                return "";
            }

            @Override
            protected void saveCallResult(@NonNull List<BlockVisit> item) {
                db.beginTransaction();
                try {
                    blockVisitDao.insert(item);
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }

            }

            @Override
            protected boolean shouldFetch(@Nullable List<BlockVisit> data) {
                //return data == null || data.size() == 0;
                return true;
            }

            @NonNull
            @Override
            protected LiveData<List<BlockVisit>> loadFromDb() {
                return blockVisitDao.getData();
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<BlockVisit>>> createCall(RequestBody requestBody) {
                return webservice.getBlockVisits(token);
            }
        }.asLiveData();
    }

    public LiveData<Resource<List<String>>> loadCities(String token) {
        MutableLiveData<Resource<List<String>>> result = new MutableLiveData<>();
        result.setValue(Resource.loading(null));
        webservice.getCities(token).enqueue(new Callback<List<String>>() {
            @Override
            public void onResponse(@NonNull Call<List<String>> call, @NonNull Response<List<String>> response) {
                List<String> cityList = response.body();
                if (cityList == null || cityList.isEmpty()) {
                    result.setValue(Resource.error("Список городов пуст", null));
                    return;
                }
                cityList.add("* Все города");
                Collections.sort(cityList);
                result.setValue(Resource.success(cityList));
            }

            @Override
            public void onFailure(@NonNull Call<List<String>> call, @NonNull Throwable t) {
                result.setValue(Resource.error(t.getMessage(), null));
            }
        });

        return result;
    }

    public LiveData<Resource<List<BuildingSite>>> loadJks(String token, String city) {
        MutableLiveData<Resource<List<BuildingSite>>> result = new MutableLiveData<>();
        result.setValue(Resource.loading(null));
        webservice.getBuildingSites(token, city).enqueue(new Callback<List<BuildingSite>>() {
            @Override
            public void onResponse(@NonNull Call<List<BuildingSite>> call, @NonNull Response<List<BuildingSite>> response) {
                List<BuildingSite> buildingSites = response.body();
                if (buildingSites == null || buildingSites.isEmpty()) {
                    result.setValue(Resource.error("Список объектов пуст", null));
                    return;
                }
                buildingSites.add(new BuildingSite(null, "* Все ЖК"));
                Collections.sort(buildingSites);
                result.setValue(Resource.success(buildingSites));
            }

            @Override
            public void onFailure(@NonNull Call<List<BuildingSite>> call, @NonNull Throwable t) {
                result.setValue(Resource.error(t.getMessage(), null));
            }
        });
        return result;
    }

    public LiveData<Resource<List<BuildingSite>>> loadBlocks(String token, String id) {
        MutableLiveData<Resource<List<BuildingSite>>> result = new MutableLiveData<>();
        result.setValue(Resource.loading(null));
        webservice.getBlocksFilter(token, id).enqueue(new Callback<List<BuildingSite>>() {
            @Override
            public void onResponse(@NonNull Call<List<BuildingSite>> call, @NonNull Response<List<BuildingSite>> response) {
                List<BuildingSite> buildingSites = response.body();
                if (buildingSites == null || buildingSites.isEmpty()) {
                    result.setValue(Resource.error("Список объектов пуст", null));
                    return;
                }
                buildingSites.add(new BuildingSite(null, "* Все блоки"));
                Collections.sort(buildingSites);
                result.setValue(Resource.success(buildingSites));
            }

            @Override
            public void onFailure(@NonNull Call<List<BuildingSite>> call, @NonNull Throwable t) {
                result.setValue(Resource.error(t.getMessage(), null));
            }
        });
        return result;
    }

    public LiveData<RealEstate> loadRealEstate(String id) {
        MutableLiveData<RealEstate> result = new MutableLiveData<>();
        appExecutors.diskIO().execute(() -> {
            result.postValue(realEstateDao.getRealEstate(id));
        });

        return result;
    }


    public Call<List<KeyTransfer>> loadKeyTransfer(String token, String propertyId) {
        return webservice.getKeyTransfer(token, propertyId);
    }

    public LiveData<ApiResponse<List<StatusDoc>>> loadStatusDoc(String token, String propertyId) {
        return webservice.getStatusDoc(token, propertyId);
    }

    public LiveData<ApiResponse<List<StatusPred>>> loadStatusPred(String token, String contractId, String propertyId) {
        return webservice.getStatusPred(token, contractId, propertyId);
    }

    public LiveData<Apartment> loadApartment(String apartmentId) {
        MutableLiveData<Apartment> result = new MutableLiveData<>();
        appExecutors.diskIO().execute(() -> result.postValue(apartmentDao.getApartmentById(apartmentId)));
        return result;
    }

    private double formatLongitude(String input) {
        if (isEmpty(input)) return 71.44598;
        return Double.parseDouble(input.substring(input.indexOf(',') + 1, input.length()));
    }

    private double formatLatitude(String input) {
        if (isEmpty(input)) return 51.1801;
        return Double.parseDouble(input.substring(0, input.indexOf(',')));
    }

    public LiveData<List<Apartment>> loadApartments() {
        return Transformations.switchMap(apartmentDao.getApartments(), input -> {
            if (input == null || input.isEmpty()) return AbsentLiveData.create();
            return apartmentDao.getApartments();
        });
    }

    public LiveData<Resource<Integer>> removeApartment(Apartment apartment, Integer position) {
        MutableLiveData<Resource<Integer>> result = new MutableLiveData<>();
        result.setValue(Resource.loading(null));
        appExecutors.diskIO().execute(() -> {
            apartmentDao.removeApartment(apartment);
            result.postValue(Resource.success(position));
        });
        return result;
    }

    public LiveData<Resource<Long>> insertApartment(Apartment apartment) {
        MutableLiveData<Resource<Long>> result = new MutableLiveData<>();
        result.setValue(Resource.loading(null));
        appExecutors.diskIO().execute(() -> {
            long insert = apartmentDao.insertApartment(apartment);
            result.postValue(Resource.success(insert));
        });
        return result;
    }
}
