package com.zerotoonelabs.bigroup.repository;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.zerotoonelabs.bigroup.AppExecutors;
import com.zerotoonelabs.bigroup.api.ApiResponse;
import com.zerotoonelabs.bigroup.api.SalesWebservice;
import com.zerotoonelabs.bigroup.db.BiDb;
import com.zerotoonelabs.bigroup.db.NewsDao;
import com.zerotoonelabs.bigroup.db.SalesDao;
import com.zerotoonelabs.bigroup.util.RateLimiter;
import com.zerotoonelabs.bigroup.entity.News;
import com.zerotoonelabs.bigroup.entity.Resource;
import com.zerotoonelabs.bigroup.entity.Stock;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.RequestBody;

@Singleton
public class SalesRepository {
    private final SalesWebservice webservice;
    private final SalesDao salesDao;
    private final NewsDao newsDao;
    private final AppExecutors appExecutors;
    private final BiDb db;

    private RateLimiter<String> rateLimiter = new RateLimiter<>(30, TimeUnit.MINUTES);

    @Inject
    public SalesRepository(SalesWebservice webservice, SalesDao salesDao, NewsDao newsDao, AppExecutors appExecutors, BiDb db) {
        this.webservice = webservice;
        this.salesDao = salesDao;
        this.newsDao = newsDao;
        this.appExecutors = appExecutors;
        this.db = db;
    }

    public LiveData<Resource<List<Stock>>> loadStocks(String lang, int tab) {
        return new NetworkBoundResource<List<Stock>, List<Stock>>(appExecutors) {

            @Override
            protected String createRequestBody() {
                return "";
            }

            @Override
            protected void saveCallResult(@NonNull List<Stock> item) {
                for (Stock stock : item) {
                    Stock.Section[] sections = stock.section;
                    if (sections != null && sections.length > 0) {
                        String result = "";
                        List<String> imageUrls = new ArrayList<>();
                        for (int i = 0; i < sections.length; i++) {
                            if (sections[i] != null && sections[i].sectionName != null) {
                                result += sections[i].sectionName;
                                if (i != sections.length - 1) {
                                    result += '\n';
                                }

                                if (sections[i].imageUrl != null) {
                                    imageUrls.add("http://sales.bi-group.org" + sections[i].imageUrl);
                                }
                            }
                        }

                        if (imageUrls.size() > 0) {
                            stock.sectionImageUrls = imageUrls;
                        }
                        stock.sections = result;
                    }

                    stock.start = formatDate(stock.start);
                    stock.end = formatDate(stock.end);
                    if (stock.imageUrl != null) {
                        stock.imageUrl = "http://sales.bi-group.org" + stock.imageUrl;
                    }
                }

                db.beginTransaction();
                try {
                    salesDao.insert(item);
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable List<Stock> data) {
                return data == null || data.size() == 0 || rateLimiter.shouldFetch("stocks");
            }

            @NonNull
            @Override
            protected LiveData<List<Stock>> loadFromDb() {
                if (tab == 1) {
                    return salesDao.getTab1Data();
                } else {
                    return salesDao.getTab2Data();
                }
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<Stock>>> createCall(RequestBody requestBody) {
                return webservice.getStocks(lang);
            }

            @Override
            protected void onFetchFailed() {
                rateLimiter.reset("stocks");
            }
        }.asLiveData();
    }

    private String formatDate(String input) {
        if (input == null) return null;
//                2019-03-03T00:00:00
        SimpleDateFormat outputSdf = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
        SimpleDateFormat inputSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        try {
            Date inputDate = inputSdf.parse(input);
            return outputSdf.format(inputDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    private long formatToTimestamp(String input) {
        if (input == null) return 0;
        SimpleDateFormat inputSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        try {
            Date inputDate = inputSdf.parse(input);
            return inputDate.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public LiveData<Resource<List<News>>> loadNews(String lang) {
        return new NetworkBoundResource<List<News>, List<News>>(appExecutors) {

            @Override
            protected String createRequestBody() {
                return "";
            }

            @Override
            protected void saveCallResult(@NonNull List<News> item) {
                String urlPrefix = "http://sales.bi-group.org";
                for (News news : item) {
                    news.setAnoncePhotoPath(news.getAnoncePhotoPath() != null ? urlPrefix + news.getAnoncePhotoPath() : null);
                    news.setTimestamp(formatToTimestamp(news.getStart()));
                    news.setStart(formatDate(news.getStart()));
                }
                db.beginTransaction();
                try {
                    newsDao.insert(item);
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }

            }

            @Override
            protected boolean shouldFetch(@Nullable List<News> data) {
                return data == null || data.size() == 0 || rateLimiter.shouldFetch("news");
            }

            @NonNull
            @Override
            protected LiveData<List<News>> loadFromDb() {
                return newsDao.getData();
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<News>>> createCall(RequestBody requestBody) {
                return webservice.getNews(lang);
            }

            @Override
            protected void onFetchFailed() {
                rateLimiter.reset("news");
            }
        }.asLiveData();
    }
}
