package com.zerotoonelabs.bigroup.repository;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;

import com.zerotoonelabs.bigroup.AppExecutors;
import com.zerotoonelabs.bigroup.api.ApiResponse;
import com.zerotoonelabs.bigroup.entity.Resource;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * A generic class that can provide a resource backed by both the sqlite database and the network.
 * <p>
 * You can read more about it in the <a href="https://developer.android.com/arch">Architecture
 * Guide</a>.
 *
 * @param <ResultType>
 * @param <RequestType>
 */
public abstract class NonLiveNetworkBoundResource<ResultType, RequestType> {
    private final AppExecutors appExecutors;

    private Resource<ResultType> result = Resource.loading(null);

    @WorkerThread
    NonLiveNetworkBoundResource(AppExecutors appExecutors) {
        this.appExecutors = appExecutors;
        final ResultType dbSource = loadFromDb();
        if (shouldFetch(dbSource)) {
            fetchFromNetwork(dbSource);
        } else {
            result = Resource.success(dbSource);
        }
    }

    private void fetchFromNetwork(final ResultType dbSource) {
        RequestBody body = RequestBody.create(MediaType.parse("application/xml"), createRequestBody());

        try {
            final Response<RequestType> response = createCall(body).execute();
            final ApiResponse<RequestType> apiResponse = new ApiResponse<RequestType>(response);
            // we re-attach dbSource as a new source, it will dispatch its latest value quickly
            result = Resource.loading(null);

            if (apiResponse.isSuccessful()) {
                appExecutors.diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        saveCallResult(processResponse(apiResponse));
                        appExecutors.mainThread().execute(new Runnable() {
                            @Override
                            public void run() {
                                // we specially request a new live data,
                                // otherwise we will get immediately last cached value,
                                // which may not be updated with latest results received from network.
                                result = Resource.success(loadFromDb());
                            }
                        });
                    }
                });
            } else {
                onFetchFailed();
                result = Resource.error(apiResponse.errorMessage, dbSource);
            }
        } catch (IOException e) {
            onFetchFailed();
            e.printStackTrace();
        }
    }

    protected void onFetchFailed() {
    }

    public Resource<ResultType> asNonLiveData() {
        return result;
    }

    @WorkerThread
    protected RequestType processResponse(ApiResponse<RequestType> response) {
        return response.body;
    }

    @WorkerThread
    protected abstract void saveCallResult(@NonNull RequestType item);

    @WorkerThread
    protected abstract String createRequestBody();

    @WorkerThread
    protected abstract boolean shouldFetch(@Nullable ResultType data);

    @NonNull
    @WorkerThread
    protected abstract ResultType loadFromDb();

    @NonNull
    @WorkerThread
    protected abstract Call<RequestType> createCall(RequestBody requestBody);
}
