package com.zerotoonelabs.bigroup.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.util.Log;

import com.zerotoonelabs.bigroup.AppExecutors;
import com.zerotoonelabs.bigroup.BiApplication;
import com.zerotoonelabs.bigroup.api.Kassa24Service;
import com.zerotoonelabs.bigroup.api.PaymentScheduleResponse;
import com.zerotoonelabs.bigroup.api.Webservice;
import com.zerotoonelabs.bigroup.entity.BankAccount;
import com.zerotoonelabs.bigroup.entity.Instalment;
import com.zerotoonelabs.bigroup.entity.InstalmentHistory;
import com.zerotoonelabs.bigroup.entity.PaymentHistory;
import com.zerotoonelabs.bigroup.entity.PaymentResponse;
import com.zerotoonelabs.bigroup.entity.Resource;
import com.zerotoonelabs.bigroup.entity.ServiceId;
import com.zerotoonelabs.bigroup.entity.Utility;
import com.zerotoonelabs.bigroup.entity.kassa24.CheckUserResponse;
import com.zerotoonelabs.bigroup.entity.kassa24.Invoices;
import com.zerotoonelabs.bigroup.entity.kassa24.Service;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Singleton
public class PaymentRepository {
    private final Webservice webservice;
    private final Kassa24Service paymentService;
    private final AppExecutors appExecutors;

    @Inject
    public PaymentRepository(Webservice webservice, Kassa24Service service, AppExecutors appExecutors) {
        this.webservice = webservice;
        this.paymentService = service;
        this.appExecutors = appExecutors;
    }

    public LiveData<Resource<List<InstalmentHistory>>> loadPaymentScheduleHistory(String token, String contractId, String propertyGuid) {
        MutableLiveData<Resource<List<InstalmentHistory>>> result = new MutableLiveData<>();
        result.setValue(Resource.loading(null));
        webservice.loadPaymentScheduleHistory(token, contractId, propertyGuid).enqueue(new Callback<List<InstalmentHistory>>() {
            @Override
            public void onResponse(@NonNull Call<List<InstalmentHistory>> call, @NonNull Response<List<InstalmentHistory>> response) {
                List<InstalmentHistory> instalments = response.body();
                if (instalments == null) {
                    result.setValue(Resource.error("Не удалось загрузить Историю графика платежей", null));
                    return;
                }

                DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
                symbols.setGroupingSeparator(' ');
                formatter.setDecimalFormatSymbols(symbols);

                for (InstalmentHistory instalment : instalments) {
                    instalment.timestamp = formatToTimestamp(instalment.date);
                    instalment.date = formatDate(instalment.date);
                    instalment.formattedAmount = formatPrice(formatter, instalment.amount);
                }
                Collections.sort(instalments);
                result.setValue(Resource.success(instalments));
            }

            @Override
            public void onFailure(@NonNull Call<List<InstalmentHistory>> call, @NonNull Throwable t) {
                result.setValue(Resource.error(t.getMessage(), null));
            }
        });
        return result;
    }

    public LiveData<Resource<List<Instalment>>> loadPaymentSchedules(String token, String contractId, String propertyGuid) {
        MutableLiveData<Resource<List<Instalment>>> result = new MutableLiveData<>();
        result.setValue(Resource.loading(null));
        webservice.loadPaymentSchedules(token, contractId, propertyGuid).enqueue(new Callback<List<Instalment>>() {
            @Override
            public void onResponse(@NonNull Call<List<Instalment>> call, @NonNull Response<List<Instalment>> response) {
                List<Instalment> instalments = response.body();
                if (instalments == null) {
                    result.setValue(Resource.error("Не удалось загрузить график платежей", null));
                    return;
                }

                DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
                symbols.setGroupingSeparator(' ');
                formatter.setDecimalFormatSymbols(symbols);

                for (Instalment instalment : instalments) {
                    instalment.timestamp = formatToTimestamp(instalment.date);
                    instalment.date = formatDate(instalment.date);
                    instalment.formattedAmount = formatPrice(formatter, instalment.amount);
                }

                Collections.sort(instalments);
                result.setValue(Resource.success(instalments));
            }

            @Override
            public void onFailure(@NonNull Call<List<Instalment>> call, @NonNull Throwable t) {
                result.setValue(Resource.error(t.getMessage(), null));
            }
        });
        return result;
    }


    public LiveData<Resource<PaymentScheduleResponse>> loadPaymentSchedule(String token, String contractId, int propertyId) {
        final MutableLiveData<Resource<PaymentScheduleResponse>> result = new MutableLiveData<>();
        result.setValue(Resource.loading(null));
        webservice.loadPaymentSchedule(token, contractId, propertyId).enqueue(new Callback<PaymentScheduleResponse>() {
            @Override
            public void onResponse(Call<PaymentScheduleResponse> call, Response<PaymentScheduleResponse> response) {

                if(response.isSuccessful()){
                    Log.d("TEST", "___*****");
                    PaymentScheduleResponse scheduleResponse = response.body();
                    if (scheduleResponse == null || scheduleResponse.payment == null || scheduleResponse.currentPayment == null) {
                        result.setValue(Resource.error("Не удалось загрузить график платежей", null));
                        return;
                    }

                    PreferenceManager.getDefaultSharedPreferences(BiApplication.getAppContext()).edit()
                            .putString("publicId", scheduleResponse.currentPayment.publicId)
                            .putInt("invoiceId", scheduleResponse.currentPayment.id)
                            .putString("accountId", scheduleResponse.currentPayment.personIin)
                            .apply();

                    scheduleResponse.payment.dueDate = formatDate(scheduleResponse.payment.dueDate);
                    scheduleResponse.currentPayment.dueDate = formatDate(scheduleResponse.currentPayment.dueDate);

                    DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                    DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
                    symbols.setGroupingSeparator(' ');
                    formatter.setDecimalFormatSymbols(symbols);

                    if (scheduleResponse.totalValue != null) {
                        double dTotalValue = Double.parseDouble(scheduleResponse.totalValue);
                        int totalValue = (int) dTotalValue;
                        scheduleResponse.formattedTotalValue = formatPrice(formatter, totalValue);
                    }

                    scheduleResponse.payment.formattedLeftover = formatPrice(formatter, scheduleResponse.payment.leftover);

                    scheduleResponse.formattedPaidSum = formatPrice(formatter, scheduleResponse.paidSum);
                    scheduleResponse.payment.formattedValue = formatPrice(formatter, scheduleResponse.payment.value);
                    scheduleResponse.formattedDebt = formatPrice(formatter, scheduleResponse.debt);

                    float toPay = scheduleResponse.payment.value;
                    double toPayWithFee = 0;
                    if (toPay > 10000)
                        toPayWithFee = toPay + 300;
                    else
                        toPayWithFee = toPay + toPay/100;

                    scheduleResponse.buttonText = "Оплатить " + formatPrice(formatter, toPayWithFee) + " \u20B8";

                    result.setValue(Resource.success(scheduleResponse));
                }

            }

            @Override
            public void onFailure(@NonNull Call<PaymentScheduleResponse> call, @NonNull Throwable t) {
                result.setValue(Resource.error(t.getMessage(), null));
            }
        });
        return result;
    }

    public LiveData<Resource<PaymentResponse>> loadTransaction(String serviceId, int amount, String userId, String invoiceId, List<Service> services) {
        final MutableLiveData<Resource<PaymentResponse>> result = new MutableLiveData<>();
        result.setValue(Resource.loading(null));
        Map<String, Object> map = new HashMap<>();
        map.put("serviceId", serviceId);
//        map.put("serviceId", "138");
        map.put("userId", userId);
//        map.put("userId", "16009549");
        map.put("summ", amount);
        map.put("successUrl", "https://www.google.com");
        for (int i = 1; i <= services.size(); i++) {
            Service service = services.get(i - 1);
            map.put("subservices[" + i + "][service]", service.getServiceId());
            map.put("subservices[" + i + "][constraint]", invoiceId);
            map.put("subservices[" + i + "][amount" + (i - 1) + "]", service.getTotalSum());
        }

        String usernamePassword = "670:bi2017gr02ou1p5";
        String authHeader = Base64.encodeToString(usernamePassword.getBytes(), Base64.NO_WRAP);

        paymentService.transaction(/*"Basic " + authHeader,*/ map).enqueue(new Callback<PaymentResponse>() {
            @Override
            public void onResponse(@NonNull Call<PaymentResponse> call, @NonNull Response<PaymentResponse> response) {
                PaymentResponse paymentResponse = response.body();
                if (paymentResponse != null && paymentResponse.errors != null) {
                    result.setValue(Resource.error(paymentResponse.errors.toString(), null));
                    return;
                }
                result.setValue(paymentResponse == null ? Resource.error("Не удалось оплатить", null) : Resource.success(paymentResponse));
            }

            @Override
            public void onFailure(@NonNull Call<PaymentResponse> call, @NonNull Throwable t) {
                result.setValue(Resource.error(t.getMessage(), null));
            }
        });
        return result;
    }

    public LiveData<Resource<Invoices.Invoice>> loadCheckUser(String serviceId, String userId) {
        final MutableLiveData<Resource<Invoices.Invoice>> result = new MutableLiveData<>();
        result.setValue(Resource.loading(null));

        paymentService.checkUser(serviceId, userId).enqueue(new Callback<CheckUserResponse>() {
            @Override
            public void onResponse(@NonNull Call<CheckUserResponse> call, @NonNull Response<CheckUserResponse> response) {
                CheckUserResponse checkUserResponse = response.body();

                if (checkUserResponse == null
                        || checkUserResponse.invoices == null
                        || checkUserResponse.invoices.invoice == null
                        || checkUserResponse.invoices.invoice.services == null
                        || checkUserResponse.invoices.invoice.invoiceId == null
                        || checkUserResponse.invoices.invoice.services.service == null) {
                    result.setValue(Resource.error("Не удалось загрузить быструю оплату", null));
                    return;
                }

//                List<Service> services = checkUserResponse.invoices.invoice.services.service;
                result.setValue(Resource.success(checkUserResponse.invoices.invoice));
            }

            @Override
            public void onFailure(@NonNull Call<CheckUserResponse> call, @NonNull Throwable t) {
                result.setValue(Resource.error("Не удалось загрузить быструю оплату", null));
            }
        });

        return result;
    }

    public LiveData<Resource<List<Utility>>> loadUtilities(String token, int invoiceId) {
        final MutableLiveData<Resource<List<Utility>>> result = new MutableLiveData<>();
        result.setValue(Resource.loading(null));
        webservice.loadUtilities(token, invoiceId).enqueue(new Callback<List<Utility>>() {
            @Override
            public void onResponse(@NonNull Call<List<Utility>> call, @NonNull Response<List<Utility>> response) {
                List<Utility> history = response.body();
                result.setValue(history == null
                        ? Resource.error("Не удалось загрузить коммунальные платежи", null)
                        : Resource.success(history));
            }

            @Override
            public void onFailure(@NonNull Call<List<Utility>> call, @NonNull Throwable t) {
                result.setValue(Resource.error(t.getMessage(), null));
            }
        });
        return result;
    }

    public LiveData<Resource<List<PaymentHistory>>> loadPaymentHistory(String token, int bankAccountId) {
        final MutableLiveData<Resource<List<PaymentHistory>>> result = new MutableLiveData<>();
        result.setValue(Resource.loading(null));
        webservice.loadPaymentHistory(token, bankAccountId).enqueue(new Callback<List<PaymentHistory>>() {
            @Override
            public void onResponse(@NonNull Call<List<PaymentHistory>> call, @NonNull Response<List<PaymentHistory>> response) {
                List<PaymentHistory> history = response.body();
                result.setValue(history == null
                        ? Resource.error("Не удалось загрузить историю оплат", null)
                        : Resource.success(history));
            }

            @Override
            public void onFailure(@NonNull Call<List<PaymentHistory>> call, @NonNull Throwable t) {
                result.setValue(Resource.error(t.getMessage(), null));
            }
        });
        return result;
    }

    public LiveData<Resource<List<ServiceId>>> loadServiceIds(String token) {
        final MutableLiveData<Resource<List<ServiceId>>> result = new MutableLiveData<>();
        result.setValue(Resource.loading(null));
        webservice.getServiceIds(token).enqueue(new Callback<List<ServiceId>>() {
            @Override
            public void onResponse(Call<List<ServiceId>> call, Response<List<ServiceId>> response) {
                List<ServiceId> serviceIds = response.body();
                result.setValue(serviceIds == null || serviceIds.isEmpty()
                        ? Resource.error("Не удалось загрузить Service Ids", null)
                        : Resource.success(serviceIds));
            }

            @Override
            public void onFailure(Call<List<ServiceId>> call, Throwable t) {
                result.setValue(Resource.error(t.getMessage(), null));
            }
        });
        return result;
    }

    public LiveData<Resource<BankAccount>> loadBankAccount(String token, int propertyId, String contractId) {
        final MutableLiveData<Resource<BankAccount>> result = new MutableLiveData<>();
        result.setValue(Resource.loading(null));
        webservice.loadBankAccount(token, propertyId, contractId).enqueue(new Callback<BankAccount[]>() {
            @Override
            public void onResponse(@NonNull Call<BankAccount[]> call, @NonNull Response<BankAccount[]> response) {
                BankAccount[] accounts = response.body();
                result.setValue(accounts == null || accounts.length < 1
                        ? Resource.error("Не удалось загрузить банковский аккаунт", null)
                        : Resource.success(accounts[0]));
            }

            @Override
            public void onFailure(@NonNull Call<BankAccount[]> call, @NonNull Throwable t) {
                result.setValue(Resource.error(t.getMessage(), null));
            }
        });
        return result;
    }

    private String formatDate(String input) {
        if (input == null) return null;
//                2019-03-03T00:00:00
        SimpleDateFormat outputSdf = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
        SimpleDateFormat inputSdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());

        try {
            Date inputDate = inputSdf.parse(input);
            return outputSdf.format(inputDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    private long formatToTimestamp(String input) {
        if (input == null) return 0;
        SimpleDateFormat inputSdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());

        try {
            Date inputDate = inputSdf.parse(input);
            return inputDate.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private String formatPrice(DecimalFormat formatter, double price) {
        return formatter.format(price);
    }
}
