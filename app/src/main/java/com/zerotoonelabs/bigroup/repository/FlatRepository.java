package com.zerotoonelabs.bigroup.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.zerotoonelabs.bigroup.AppExecutors;
import com.zerotoonelabs.bigroup.api.ApiResponse;
import com.zerotoonelabs.bigroup.api.SalesRequestResponse;
import com.zerotoonelabs.bigroup.api.Webservice;
import com.zerotoonelabs.bigroup.db.BiDb;
import com.zerotoonelabs.bigroup.db.FlatDao;
import com.zerotoonelabs.bigroup.util.AbsentLiveData;
import com.zerotoonelabs.bigroup.util.DownloadCallback;
import com.zerotoonelabs.bigroup.entity.Flat;
import com.zerotoonelabs.bigroup.entity.FlatScheme;
import com.zerotoonelabs.bigroup.entity.Manager;
import com.zerotoonelabs.bigroup.entity.Resource;
import com.zerotoonelabs.bigroup.entity.SalesRequestHistory;
import com.zerotoonelabs.bigroup.entity.SalesRequestType;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Singleton
public class FlatRepository {
    private final Webservice webservice;
    private final FlatDao flatDao;
    private final AppExecutors appExecutors;
    public final BiDb db;

    @Inject
    public FlatRepository(Webservice webservice, FlatDao flatDao, AppExecutors appExecutors, BiDb db) {
        this.webservice = webservice;
        this.flatDao = flatDao;
        this.appExecutors = appExecutors;
        this.db = db;
    }

    public LiveData<Resource<String>> sendSalesRequest(String token, String iin, String documentId, String contractId, String propertyId) {
        final MutableLiveData<Resource<String>> result = new MutableLiveData<>();
        result.setValue(Resource.loading(null));

        HashMap<String, String> map = new HashMap<>();
        map.put("SendContractorIIN", iin);
        map.put("SendDocumentUID", documentId);
        map.put("SendContractorUID", contractId);
        map.put("SendPropertyUID", propertyId);
        String stringBody = new Gson().toJson(map);
        RequestBody body = RequestBody.create(MediaType.parse("application/json"), stringBody);
        webservice.sendSalesRequest(token, body).enqueue(new Callback<SalesRequestResponse>() {
            @Override
            public void onResponse(@NonNull Call<SalesRequestResponse> call, @NonNull Response<SalesRequestResponse> response) {
                SalesRequestResponse salesRequestResponse = response.body();
                if (salesRequestResponse == null || salesRequestResponse.number == null || salesRequestResponse.error != null) {
                    result.setValue(Resource.error("Не удалось отправить заявку.", null));
                    return;
                }

                result.setValue(Resource.success(salesRequestResponse.number));
            }

            @Override
            public void onFailure(@NonNull Call<SalesRequestResponse> call, @NonNull Throwable t) {
                result.setValue(Resource.error(t.getMessage(), null));
            }
        });
        return result;
    }

    public LiveData<Resource<List<SalesRequestHistory>>> loadSalesRequestHistory(String token, String applicationUserId, String propertyGuid) {
        final MutableLiveData<Resource<List<SalesRequestHistory>>> result = new MutableLiveData<>();
        result.setValue(Resource.loading(null));
//        applicationUserId = "fb791b7b-bd33-4bda-ab32-4f3041c64913";
//        propertyGuid = "8936ea4d-d3c8-11e5-b9a5-b4b52f5405e7";
        webservice.loadSalesRequestsHistory(token, applicationUserId, propertyGuid).enqueue(new Callback<List<SalesRequestHistory>>() {
            @Override
            public void onResponse(@NonNull Call<List<SalesRequestHistory>> call, @NonNull Response<List<SalesRequestHistory>> response) {
                List<SalesRequestHistory> salesRequestTypes = response.body();
                if (salesRequestTypes == null) {
                    result.setValue(Resource.error("Не удалось загрузить историю заявок в отдел продаж", null));
                    return;
                }

                for (SalesRequestHistory history : salesRequestTypes) {
                    history.createdAt = formatDate(history.createdAt);
                }
                result.setValue(Resource.success(salesRequestTypes));
            }

            @Override
            public void onFailure(@NonNull Call<List<SalesRequestHistory>> call, @NonNull Throwable t) {
                result.setValue(Resource.error(t.getMessage(), null));
            }
        });
        return result;
    }

    public LiveData<Resource<List<SalesRequestType>>> loadSalesReqTypes(String token) {
        MutableLiveData<Resource<List<SalesRequestType>>> result = new MutableLiveData<>();
        result.setValue(Resource.loading(null));
        webservice.loadSalesRequestTypes(token).enqueue(new Callback<List<SalesRequestType>>() {
            @Override
            public void onResponse(@NonNull Call<List<SalesRequestType>> call, @NonNull Response<List<SalesRequestType>> response) {
                List<SalesRequestType> salesRequestTypes = response.body();
                if (salesRequestTypes == null) {
                    result.setValue(Resource.error("Не удалось загрузить каталог", null));
                    return;
                }
                result.setValue(Resource.success(salesRequestTypes));
            }

            @Override
            public void onFailure(@NonNull Call<List<SalesRequestType>> call, @NonNull Throwable t) {
                result.setValue(Resource.error(t.getMessage(), null));
            }
        });
        return result;
    }
//xx
    public LiveData<Resource<File>> loadTechPassportPdf(Context applicationContext, String token, int propertyId, String contractId) {
        MutableLiveData<Resource<File>> result = new MutableLiveData<>();
        result.setValue(Resource.loading(null));
        webservice.loadTechPassportPdf(token, contractId, propertyId).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                ResponseBody body = response.body();
                if (body != null) {
                    appExecutors.diskIO().execute(() -> {
                        boolean writtenToDisk = writeResponseBodyToDisk(applicationContext, Integer.parseInt(contractId), body, ".pdf");
                        if (writtenToDisk) {
                            File file = new File(applicationContext.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
                                    + File.separator + contractId + ".pdf");
                            result.postValue(Resource.success(file));
                        } else {
                            result.postValue(Resource.error("Запрошенный файл не найден.", null));
                            //result.postValue(loadApartmentSheetPdf(applicationContext, token, propertyId).getValue());
                        }
                    });
                } else {
                    result.setValue(Resource.error("Запрошенный файл не найден.", null));
                    //result.postValue(loadApartmentSheetPdf(applicationContext, token, propertyId).getValue());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                result.setValue(Resource.error(t.getMessage(), null));
                //result.postValue(loadApartmentSheetPdf(applicationContext, token, propertyId).getValue());
            }
        });
        return result;
    }
//xx
    public LiveData<Resource<File>> loadApartmentSheetPdf(Context applicationContext, String token, int propertyId) {
        MutableLiveData<Resource<File>> result = new MutableLiveData<>();
        result.setValue(Resource.loading(null));
        webservice.loadApartmentSheetPdf(token, propertyId).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                ResponseBody body = response.body();
                if (body != null) {
                    appExecutors.diskIO().execute(() -> {
                        boolean writtenToDisk = writeResponseBodyToDisk(applicationContext, propertyId, body, ".pdf");
                        if (writtenToDisk) {
                            File file = new File(applicationContext.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
                                    + File.separator + propertyId + ".pdf");
                            result.postValue(Resource.success(file));
                        } else {
                            result.postValue(Resource.error("Запрошенный файл не найден.", null));
                            //result.postValue(loadApartmentSheetPdf(applicationContext, token, propertyId).getValue());
                        }
                    });
                } else {
                    result.setValue(Resource.error("Запрошенный файл не найден.", null));
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                result.setValue(Resource.error(t.getMessage(), null));
            }
        });
        return result;
    }

    public LiveData<Resource<File>> loadPdf(Context applicationContext, String token, int pdfId) {
        MutableLiveData<Resource<File>> result = new MutableLiveData<>();
        result.setValue(Resource.loading(null));
        webservice.loadPdf(token, pdfId).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                ResponseBody body = response.body();
                if (body != null) {
                    appExecutors.diskIO().execute(() -> {
                        boolean writtenToDisk = writeResponseBodyToDisk(applicationContext, pdfId, body, ".pdf");
                        if (writtenToDisk) {
                            File file = new File(applicationContext.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
                                    + File.separator + pdfId + ".pdf");
                            result.postValue(Resource.success(file));
                        } else {
                            result.postValue(Resource.error("Запрошенный файл не найден.", null));
                        }
                    });
                } else {
                    result.setValue(Resource.error("Запрошенный файл не найден.", null));
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                result.setValue(Resource.error(t.getMessage(), null));
            }
        });
        return result;
    }


    public LiveData<Resource<File>> loadStatusPdf(Context applicationContext, String token, String contractId, int propertyId, int type) {
        MutableLiveData<Resource<File>> result = new MutableLiveData<>();
        result.setValue(Resource.loading(null));
        webservice.loadStatusPdf(token, contractId, propertyId, type).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                ResponseBody body = response.body();
                if (body != null) {
                    appExecutors.diskIO().execute(() -> {
                        boolean writtenToDisk = writeResponseBodyToDisk(applicationContext, (type + propertyId), body, ".pdf");
                        if (writtenToDisk) {
                            File file = new File(applicationContext.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
                                    + File.separator + (type + propertyId) + ".pdf");
                            result.postValue(Resource.success(file));
                        } else {
                            result.postValue(Resource.error("Запрошенный файл не найден.", null));
                        }
                    });
                } else {
                    result.setValue(Resource.error("Запрошенный файл не найден.", null));
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                result.setValue(Resource.error(t.getMessage(), null));
            }
        });
        return result;
    }

    public LiveData<Resource<File>> loadPriseListPdf(Context applicationContext, String token) {
        MutableLiveData<Resource<File>> result = new MutableLiveData<>();
        result.setValue(Resource.loading(null));
        webservice.loadPriseListPdf(token).enqueue(new DownloadCallback(result, appExecutors, applicationContext, "Prise list"));
        return result;
    }

    private boolean writeResponseBodyToDisk(Context context, int pdfId, @NonNull ResponseBody body, String fileType) {
        try {
            File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS) + File.separator + pdfId + fileType);

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(file);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;
                }

                outputStream.flush();

                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }


    public LiveData<Resource<List<FlatScheme>>> loadFlatSchemes(String token, int propertyId, String contractId) {
        MutableLiveData<Resource<List<FlatScheme>>> result = new MutableLiveData<>();
        result.setValue(Resource.loading(null));
        webservice.loadFlatSchemes(token, propertyId, contractId).enqueue(new Callback<List<FlatScheme>>() {
            @Override
            public void onResponse(@NonNull Call<List<FlatScheme>> call, @NonNull Response<List<FlatScheme>> response) {
                List<FlatScheme> flatSchemes = response.body();
                result.setValue(flatSchemes == null ? Resource.error("Список пуст", null) : Resource.success(flatSchemes));
            }

            @Override
            public void onFailure(@NonNull Call<List<FlatScheme>> call, @NonNull Throwable t) {
                result.setValue(Resource.error(t.getMessage(), null));
            }
        });
        return result;
    }

    public LiveData<Resource<List<Flat>>> loadFlats2(String token, String contractId) {
        return new NetworkBoundResource<List<Flat>, List<Flat>>(appExecutors) {

            @Override
            protected String createRequestBody() {
                return "";
            }

            @Override
            protected void saveCallResult(@NonNull List<Flat> flats) {
                for (Flat flat : flats) {
                    flat.setContractId(contractId);
                }
                if (flats.size() > 0) {
                    flats.get(0).setSelected(true);
                }
                flatDao.insertFlats(flats);
            }

            @Override
            protected boolean shouldFetch(@Nullable List<Flat> data) {
                return data == null || data.isEmpty();
            }

            @NonNull
            @Override
            protected LiveData<List<Flat>> loadFromDb() {
                return Transformations.switchMap(flatDao.getFlats(contractId), input -> {
                    if (input == null) {
                        return AbsentLiveData.create();
                    }
                    return flatDao.getFlats(contractId);
                });
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<Flat>>> createCall(RequestBody requestBody) {
                return webservice.loadFlats2(token, contractId);
            }
        }.asLiveData();
    }

    public void updateFlats(List<Flat> flats) {
        appExecutors.diskIO().execute(() -> {
            db.beginTransaction();
            flatDao.updateFlats(flats);
            db.setTransactionSuccessful();
            db.endTransaction();
//            flatDao.updateFlats(flats);
        });
    }

    public LiveData<Resource<List<Flat>>> loadFlats(String token, String contractId) {
        MutableLiveData<Resource<List<Flat>>> result = new MutableLiveData<>();
        result.setValue(Resource.loading(null));
        webservice.loadFlats(token, contractId).enqueue(new Callback<List<Flat>>() {
            @Override
            public void onResponse(@NonNull Call<List<Flat>> call, @NonNull Response<List<Flat>> response) {

//xx
                try{
                    List<Flat> flats = response.body();
                    if (flats != null && flats.size() > 0) {
                        for (Flat flat : flats) {
                            flat.setContractId(contractId);
                        }
                        flats.get(0).setSelected(true);
                    }
                    result.setValue(flats == null ? Resource.error("Список пуст", null) : Resource.success(flats));
                }catch (Exception e){

                }


            }

            @Override
            public void onFailure(@NonNull Call<List<Flat>> call, @NonNull Throwable t) {
                result.setValue(Resource.error(t.getMessage(), null));
            }
        });
        return result;
    }

    public LiveData<Resource<Manager>> loadManager2(String token, String resManagerId, String contractId) {
        MutableLiveData<Resource<Manager>> result = new MutableLiveData<>();
        result.setValue(Resource.loading(null));
        webservice.getManager2(token, resManagerId, contractId).enqueue(new Callback<Manager[]>() {
            @Override
            public void onResponse(@NonNull Call<Manager[]> call, @NonNull Response<Manager[]> response) {
                Manager[] managers = response.body();
                result.setValue(managers == null || managers.length < 1
                        ? Resource.error("Не удалось загрузиить менеджера счастья", null)
                        : Resource.success(managers[0]));
            }

            @Override
            public void onFailure(@NonNull Call<Manager[]> call, @NonNull Throwable t) {
                result.setValue(Resource.error("Не удалось загрузиить менеджера счастья", null));
            }
        });
        return result;
    }

    public LiveData<Resource<Manager>> loadManager(String token, String realEstateId, String conractId) {
        MutableLiveData<Resource<Manager>> result = new MutableLiveData<>();
        result.setValue(Resource.loading(null));
        webservice.getManager(token, realEstateId, conractId).enqueue(new Callback<Manager[]>() {
            @Override
            public void onResponse(@NonNull Call<Manager[]> call, @NonNull Response<Manager[]> response) {
                Manager[] managers = response.body();
                result.setValue(managers == null || managers.length < 1
                        ? Resource.error("Не удалось загрузиить менеджера счастья", null)
                        : Resource.success(managers[0]));
            }

            @Override
            public void onFailure(@NonNull Call<Manager[]> call, @NonNull Throwable t) {
                result.setValue(Resource.error("Не удалось загрузиить менеджера счастья", null));
            }
        });
        return result;
    }

    public LiveData<Resource<Flat>> loadFlat(Context applicationContext, String token, int propertyId, String contractId) {
        MutableLiveData<Resource<Flat>> result = new MutableLiveData<>();
        result.setValue(Resource.loading(null));
        webservice.getFlat(token, contractId, propertyId).enqueue(new Callback<Flat[]>() {
            @Override
            public void onResponse(@NonNull Call<Flat[]> call, @NonNull Response<Flat[]> response) {
                Flat[] flats = response.body();
                if (flats == null || flats.length < 1) {
                    result.setValue(Resource.error("Не удалось загрузить недвижимость", null));
                    return;
                }
                //xx
                Flat flat = flats[0];
                PreferenceManager.getDefaultSharedPreferences(applicationContext)
                        .edit()
                        .putString("realEstateGuid", flat.getRealEstateGuid())
                        .putString("propertyGuid", flat.getPropertyGuid())
                        .putInt("currentFlat", propertyId)
                        .putString("serviceId", flat.getServiceId())
                        .putString("contracterGUID", flat.getContracterGuid())
                        .putString("contractNumber", flat.getContractNumber())
                        .putBoolean("checkStatus", flat.isCheckStatus())
                        .commit();
                flat.setDateKeyDelivery(formatDate(flat.getDateKeyDelivery()));
                flat.setWarranty(formatDate(flat.getWarranty()));

                result.setValue(Resource.success(flat));
            }

            @Override
            public void onFailure(@NonNull Call<Flat[]> call, @NonNull Throwable t) {
                result.setValue(Resource.error("Не удалось загрузить недвижимость", null));
            }
        });
        return result;
    }



    private String formatDate(String input) {
        if (input == null) return null;
//                2019-03-03T00:00:00
        SimpleDateFormat outputSdf = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
        SimpleDateFormat inputSdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());

        try {
            Date inputDate = inputSdf.parse(input);
            return outputSdf.format(inputDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
