package com.zerotoonelabs.bigroup;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.zerotoonelabs.bigroup.Library.Adapters.ThreeLevelListAdapter;
import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.Library.Request_parsers.SerializeCalcData;
import com.zerotoonelabs.bigroup.Library.Retrofit_request.Get_calculate_data_int;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class CalculationActivity extends AppCompatActivity {

    String LOG = "CalculationActivity";

    ExpandableListAdapter expandableListAdapter;
    ExpandableListView expListView;
    ArrayList<String> listDataHeader;
    List<String> listDataHeadertwo;
    HashMap<String, List<String>> listDataChild;
    HashMap<String, List<String>> listDataChildtwo;
    Context context;
    ExpandableListView expandableListView;
    String[] parent = new String[]{};
    ProgressBar progressBar;
    TextView textView;
    TextView textView2;


    //zz


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(LOG, "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculation);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(R.string.title_calculation);

        progressBar = findViewById(R.id.progressBar11);
        textView = findViewById(R.id.textView151);
        textView2 = findViewById(R.id.textView152);

        expandableListView = findViewById(R.id.lvExp);

        expListView = findViewById(R.id.lvExp);
        context = this;

    }

    @Override
    protected void onResume() {
        Download_calculator();
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i(LOG, "onOptionsItemSelected()");
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }



    public void Download_calculator(){

        try{
            progressBar.setVisibility(View.VISIBLE);

            Retrofit client = Library.NewRetrofitclient();

            String token = Library.GetPhoneMemoryOne("token");
            String contract_id  = Library.GetPhoneMemoryOne("contractId");
            String property_id = Library.GetPhoneMemoryOne("propertyid");

            Get_calculate_data_int get_calculate_data_int = client.create(Get_calculate_data_int.class);
            Call<SerializeCalcData> request = get_calculate_data_int.getData(token,property_id);

            request.enqueue(new Callback<SerializeCalcData>() {
                @Override
                public void onResponse(Call<SerializeCalcData> call, Response<SerializeCalcData> response) {

                    listDataHeader = new ArrayList<String>();
                    listDataChild = new HashMap<String, List<String>>();
                    listDataChildtwo = new HashMap<String, List<String>>();

                    float countsumtarification = 0.0f;



                    ArrayList<Integer> category2_indicators = new ArrayList<>();
                    ArrayList<String> category2_names_for_indecator = new ArrayList<>();

                    //3 uroven
                    ArrayList<Integer> categoryindicator_one = new ArrayList<>();
                    ArrayList<Integer> categoryindicator_two = new ArrayList<>();
                    ArrayList<Integer> categoryindicator_three = new ArrayList<>();
                    ArrayList<String> categoryarray_for_indicator = new ArrayList<>();
                    //3 uroven

                    int size = response.body().getList().size();
                    for(int i = 0;i < size;i++){

                        String number = response.body().getList().get(i).getNumber();
                        int category1 = response.body().getList().get(i).getCat1();
                        int category2 = response.body().getList().get(i).getCat2();
                        int category3 = response.body().getList().get(i).getCat3();
                        String name = response.body().getList().get(i).getName();
                        String sum = String.valueOf(response.body().getList().get(i).getSum());

                        //1 uroven

                        //vivodim zagolovki
                        for(int o = 0;o < size;o++){
                            if(category1 == o && category2 == 0 && category3 == 0){
                                if(category1 != 0){
                                    listDataHeader.add(category1 + "--" + name + "--" + sum + " тг.");
                                    countsumtarification += Float.parseFloat(sum);
                                }

                            }
                        }

                        //1 uroven
                        //2 uroven
                        // vivodim

                        for(int f = 0;f < size;f++){
                            for(int g = 0;g < size;g++){
                                if(category1 == f && category2 == g && category3 == 0){
                                    if(category1 != 0){
                                        if(category2 != 0){
                                            category2_indicators.add(category1);        //kladem categoryiyu k kotoroi otnositsya
                                            category2_names_for_indecator.add(category1 + "." + category2 + "--" + name + "--" + sum + " тг.");    //kladem string k kategorii kotoroi on privyazan
                                        }

                                        //по сути мы клвдем индикатор массива к которому он относится
                                        // и кладем само значение к индикатору парралельно
                                    }
                                }
                            }
                        }

                        //2 uroven
                        //2.3
                        //2.4

                        //3 oroven


                        for(int d = 0;d < size;d++){
                            for(int s = 0;s < size;s++){
                                for(int a = 0;a < size;a++){
                                    if(category1 == d && category2 == s && category3 == a){
                                        if(category1 != 0){
                                            if(category2 != 0){
                                                if(category3 != 0){
                                                    categoryindicator_one.add(category1);
                                                    categoryindicator_two.add(category2);
                                                    categoryindicator_three.add(category3);
                                                    categoryarray_for_indicator.add(category1 + "." + category2 + "." + category3 + "--" + name + "--" + sum + " тг.");
                                                    // мф прибавили name (имя далее на следует прибавить и остальные значения суммы и валюты)
                                                }
                                            }

                                        }
                                    }
                                }

                            }
                        }
                        //3 oroven


                    }

                    ArrayList<ArrayList<String>> twocategories_sort = new ArrayList<>();

                    for(int p = 0;p < category2_indicators.size();p++){
                        ArrayList<String> arr_count = new ArrayList<>();
                        //0 iterator sortirovki
                        for(int l = 0;l < category2_indicators.size();l++){
                            if(p == category2_indicators.get(l)){   //esli indicator categorii raven massivu po iterasii zapisivaem massiv
                                arr_count.add(category2_names_for_indecator.get(l));
                            }
                        }

                        if(arr_count.size() > 0){
                            twocategories_sort.add(arr_count);
                        }

                    }


                    ArrayList<HashMap> three_level_sort_one_category = new ArrayList<>();
                    ArrayList<ArrayList> newthreelevel = new ArrayList<>();
                    for(int t = 0;t < categoryindicator_one.size();t++){

                        ArrayList<HashMap> ar3 = new ArrayList<>(); //zdes vse massivi po categoriyam,1.2.3.
                        ArrayList ar4 = new ArrayList();
                        //nachinaya s nulya

                        for(int r = 0;r < categoryindicator_one.size();r++){

                            if(t == categoryindicator_one.get(r)){
                                HashMap<Integer,String> stringhash = new HashMap<>();
                                stringhash.put(categoryindicator_two.get(r),categoryarray_for_indicator.get(r));
                                ar3.add(stringhash);
                                //categoryindicator_three
                            }

                            if(t == categoryindicator_two.get(r)){
                                ar4.add(categoryarray_for_indicator.get(r));
                            }

                        }

                        if(ar3.size() > 0){
                            HashMap<Integer,ArrayList> category_hash = new HashMap<>();
                            category_hash.put(categoryindicator_one.get(t),ar3);
                            three_level_sort_one_category.add(category_hash);
                        }

                        if(ar4.size() > 0){
                            newthreelevel.add(ar4);
                        }


                    }





                    ArrayList<ArrayList> central_hashmap = new ArrayList<>();

                    for(int e = 0;e < twocategories_sort.size();e++){
                        listDataChild.put(listDataHeader.get(e),twocategories_sort.get(e));
                    }



                    Log.i("arsort","arsort" + token + "| " +  property_id  + "| "+ twocategories_sort.toString() + listDataHeader.toString());

                    parent = new String[]{"MOVIES", "GAMES"};//1 level

                    //2 level
                    String[] movies = new String[]{"Horror", "Action", "Thriller/Drama"};
                    String[] games = new String[]{"Fps", "Moba", "Rpg", "Racing"};

                    //3 level
                    String[] horror = new String[]{"Conjuring", "Insidious", "The Ring"};
                    String[] action = new String[]{"Jon Wick", "Die Hard", "Fast 7", "Avengers"};
                    String[] thriller = new String[]{"Imitation Game", "Tinker, Tailer, Soldier, Spy", "Inception", "Manchester by the Sea"};
                    //games
                    String[] fps = new String[]{"CS: GO", "Team Fortress 2", "Overwatch", "Battlefield 1", "Halo II", "Warframe"};
                    String[] moba = new String[]{"Dota 2", "League of Legends", "Smite", "Strife", "Heroes of the Storm"};
                    String[] rpg = new String[]{"Witcher III", "Skyrim", "Warcraft", "Mass Effect II", "Diablo", "Dark Souls", "Last of Us"};
                    String[] racing = new String[]{"NFS: Most Wanted", "Forza Motorsport 3", "EA: F1 2016", "Project Cars"};


                    //---------------------add

                    //add to 2 level
                    List<String[]> secondLevel = new ArrayList<>();
                    ArrayList<ArrayList> secondLeveltwo = new ArrayList<>();
                    secondLevel.add(movies);
                    secondLevel.add(games);
                    //add to 2 level

                    //2 -> 3
                    // movies category all data
                    LinkedHashMap<String, String[]> thirdLevelMovies = new LinkedHashMap<>();
                    LinkedHashMap<String, ArrayList> thirdLevelMoviestwo = new LinkedHashMap<>();
                    LinkedHashMap<String, String[]> thirdLevelGames = new LinkedHashMap<>();
                    LinkedHashMap<String, ArrayList> thirdLevelGamestwo = new LinkedHashMap<>();

                    //newthreelevel
                    for(int kk = 0;kk < newthreelevel.size();kk++){
                        thirdLevelMoviestwo.put(twocategories_sort.get(0).get(kk),newthreelevel.get(kk));
                    }

                    for(int hh = 0;hh < twocategories_sort.size();hh++){

                        ArrayList<ArrayList> empty = new ArrayList<>();

                        ArrayList newar = new ArrayList();
//
                        empty.add(newar);
                        empty.add(newar);
                        empty.add(newar);
                        empty.add(newar);
                        empty.add(newar);
                        empty.add(newar);
                        empty.add(newar);
                        empty.add(newar);
                        empty.add(newar);

                        if(twocategories_sort.size() > 1){


                            if(hh != 0){
                                thirdLevelGamestwo.put(twocategories_sort.get(1).get(hh),empty.get(hh));
                            }
                        }

                    }

                    //thirdLevelGamestwo
                    //_____________
                    thirdLevelMovies.put(movies[0], horror);        //2 Strgin -> array
                    thirdLevelMovies.put(movies[1], action);
                    thirdLevelMovies.put(movies[2], thriller);
                    //2 -> 3
                    //2 level to 3 leve
                    // //2 -> 3
                    // games category all data
                    thirdLevelGames.put(games[0], fps);
                    thirdLevelGames.put(games[1], moba);
                    thirdLevelGames.put(games[2], rpg);
                    thirdLevelGames.put(games[3], racing);
                    //2 -> 3
                    //2 level to 3 level

                    List<LinkedHashMap<String, ArrayList>> data = new ArrayList<>();
                    data.add(thirdLevelMoviestwo);
                    data.add(thirdLevelGamestwo);
                    //data.add(thirdLevelGames);
                    //2 String => 2 array ->
                    ThreeLevelListAdapter threeLevelListAdapterAdapter = new ThreeLevelListAdapter(context, listDataHeader, twocategories_sort, data);
                    expandableListView.setAdapter(threeLevelListAdapterAdapter);

                    expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                        int previousGroup = -1;

                        @Override
                        public void onGroupExpand(int groupPosition) {
                            if (groupPosition != previousGroup)
                                expandableListView.collapseGroup(previousGroup);
                            previousGroup = groupPosition;
                        }
                    });

                    textView2.setText(Math.floor(countsumtarification) + " тг. за кв.м ");

                    progressBar.setVisibility(View.GONE);

                    //zz

                }

                @Override
                public void onFailure(Call<SerializeCalcData> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                }
            });
        }catch (Exception e){

        }


    }





}
