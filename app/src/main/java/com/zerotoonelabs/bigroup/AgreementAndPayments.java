package com.zerotoonelabs.bigroup;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zerotoonelabs.bigroup.adapters.MyNotificationsAdapter;

import java.util.ArrayList;
import java.util.List;

public class AgreementAndPayments extends AppCompatActivity implements MyNotificationsAdapter.ListItemClickListener {

    private RelativeLayout schedule, archive, relativeNotification;
    private RecyclerView recyclerView, secondRecycle;
    private TextView scheduleText, archiveText, overall, notifText;
    private Integer overallSum = 0, overallSumArchive = 0;
    private List<Notification> notificationList = new ArrayList<>(), scheduleList = new ArrayList<>(), archiveList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agreement_and_payments);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        overall = (TextView) findViewById(R.id.overall);
        notifText = (TextView) findViewById(R.id.noificationText);
        schedule = (RelativeLayout) findViewById(R.id.schedule);
        archive = (RelativeLayout) findViewById(R.id.archive);
        relativeNotification = (RelativeLayout) findViewById(R.id.relativeNotifications);
        recyclerView = (RecyclerView) findViewById(R.id.recycleView);
        secondRecycle = (RecyclerView) findViewById(R.id.secondRecycle);
        scheduleText = (TextView) findViewById(R.id.receiptText);
        archiveText = (TextView) findViewById(R.id.archiveText);

        initializeNotificationList();
        initializeScheduleList();
        initializeArchiveList();



        if(notificationList.isEmpty()){
            relativeNotification.setVisibility(View.GONE);
        }
        notifText.setText("Уведомлений :" + notificationList.size());

        int recyclerViewOrientation = LinearLayoutManager.VERTICAL;
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, recyclerViewOrientation, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        MyNotificationsAdapter ad = new MyNotificationsAdapter(this, notificationList, 1);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(ad);

        overall.setText(overallSum +" тг");

        LinearLayoutManager layoutManager2 = new LinearLayoutManager(this, recyclerViewOrientation, false);
        secondRecycle.setLayoutManager(layoutManager2);
        secondRecycle.setHasFixedSize(true);
        secondRecycle.setNestedScrollingEnabled(false);
        MyNotificationsAdapter ad2 = new MyNotificationsAdapter(this, scheduleList, 4);
        secondRecycle.setAdapter(ad2);


        schedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                schedule.setBackgroundColor(Color.parseColor("#004a92"));
                archive.setBackgroundColor(Color.parseColor("#ffffff"));
                scheduleText.setTextColor(Color.parseColor("#ffffff"));
                archiveText.setTextColor(Color.parseColor("#004a92"));
                MyNotificationsAdapter ad2 = new MyNotificationsAdapter(AgreementAndPayments.this, scheduleList, 4);
                secondRecycle.setAdapter(ad2);
                overall.setText(overallSum +" тг");
            }
        });

        archive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                archive.setBackgroundColor(Color.parseColor("#004a92"));
                schedule.setBackgroundColor(Color.parseColor("#ffffff"));
                archiveText.setTextColor(Color.parseColor("#ffffff"));
                scheduleText.setTextColor(Color.parseColor("#004a92"));
                MyNotificationsAdapter ad2 = new MyNotificationsAdapter(AgreementAndPayments.this, archiveList, 4);
                secondRecycle.setAdapter(ad2);
                overall.setText(overallSumArchive +" тг");
            }
        });


    }

    private void initializeArchiveList() {

        Notification notification = new Notification(4);
        notification.setDate("08.06.2017");
        notification.setSum("2 000 000 тг");
        overallSumArchive += 2000000;
        notification.setStep("Платежное поручение входящее BIY0002578 от 08.06.2017 18:01:10");
        archiveList.add(notification);

        notification = new Notification(4);
        notification.setDate("15.04.2017");
        notification.setSum("400 000 тг");
        overallSumArchive += 400000;
        notification.setStep("Платежное поручение входящее BIY0002578 от 08.06.2017 18:01:10");
        archiveList.add(notification);
    }

    private void initializeScheduleList() {
        Notification notification = new Notification(4);
        notification.setDate("15.07.2016");
        notification.setSum("5 000 000 тг");
        overallSum += 5000000;
        notification.setStep("Первоначальный");
        scheduleList.add(notification);

        notification = new Notification(4);
        notification.setDate("30.10.2017");
        notification.setSum("400 000 тг");
        overallSum += 400000;
        notification.setStep("Взнос 1 из 6");
        scheduleList.add(notification);

        notification = new Notification(4);
        notification.setDate("30.01.2017");
        notification.setSum("400 000 тг");
        overallSum += 400000;
        notification.setStep("Взнос 2 из 6");
        scheduleList.add(notification);

        notification = new Notification(4);
        notification.setDate("30.04.2017");
        notification.setSum("400 000 тг");
        overallSum += 400000;
        notification.setStep("Взнос 3 из 6");
        scheduleList.add(notification);

        notification = new Notification(4);
        notification.setDate("30.07.2017");
        notification.setSum("400 000 тг");
        overallSum += 400000;
        notification.setStep("Взнос 4 из 6");
        scheduleList.add(notification);

        notification = new Notification(4);
        notification.setDate("30.01.2018");
        notification.setSum("7 000 000 тг");
        overallSum += 7000000;
        notification.setStep("Взнос 5 из 6");
        scheduleList.add(notification);

        notification = new Notification(4);
        notification.setDate("30.02.2018");
        notification.setSum("10 649 000 тг");
        overallSum += 10649000;
        notification.setStep("Взнос 6 из 6");
        scheduleList.add(notification);


    }

    private void initializeNotificationList() {
        Notification notification = new Notification("Договор и оплата", "Вам нужно внести 400 000 тг по графику платежей до 08.07.2017", 1);
        notificationList.add(notification);
    }

    @Override
    public void onListItemClick(Notification notification) {
        if(notification.getNumber() != 4){
            Intent intent = new Intent(AgreementAndPayments.this, TechnicalNotifActivity.class);
            startActivity(intent);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
}
