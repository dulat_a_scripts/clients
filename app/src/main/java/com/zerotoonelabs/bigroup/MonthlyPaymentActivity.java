package com.zerotoonelabs.bigroup;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MonthlyPaymentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monthly_payment);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
