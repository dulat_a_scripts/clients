package com.zerotoonelabs.bigroup;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.authui.HomeActivity;
import com.zerotoonelabs.bigroup.entity.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import br.com.sapereaude.maskedEditText.MaskedEditText;

public class LoginActivity extends BaseActivity {
    EditText passwordET;
    MaskedEditText phoneET;
    String token;
    AlertDialog b;
    AlertDialog.Builder dialogBuilder;
    private boolean firstLogin = false;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_act);
        phoneET = findViewById(R.id.phone_et);
        passwordET = findViewById(R.id.password_et);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        token = PreferenceManager.getDefaultSharedPreferences(this).getString("token", "84eb0165-9628-4ead-b61e-550c365e3cd4");
    }

    public void openLogin(View view) {
        String phone = phoneET.getText().toString();
        String password = passwordET.getText().toString();
        if (phone.isEmpty() || phone.contains("—")) {
            showToast(R.string.invalid_phone_message);
            return;
        }
        if (TextUtils.isEmpty(password)) {
            showToast(R.string.invalid_password_message);
            return;
        }
        phone = getFormattedPhone(phone);
        ShowProgressDialog();
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        String finalPhone = phone;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://webapi.bi-group.org/api/v1/MobileClient/Authorization",
                this::getUser,
                this::showError) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map headers = new HashMap();
                headers.put("Token", token);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                String body = "{\"phone\":\"8-701-773-04-32\",\n" +
                        "  \"password\":\"Family89!\"}";

                Map map = new HashMap();
                map.put("phone", finalPhone);
                map.put("password", password);
                sharedPreferences.edit().putString("login", finalPhone).commit();
                Library.SavePhoneMemoryOne("password",password);

                //sharedPreferences.edit().putString("password", finalPhone).commit();
                body = new Gson().toJson(map);
//                if (!BuildConfig.DEBUG) {
//                    Map map = new HashMap();
//                    map.put("phone", phoneET.getText().toString());
//                    map.put("password", passwordET.getText().toString());
//                    body = new Gson().toJson(map);
//                }
                return body.getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        requestQueue.add(stringRequest);

    }

    private void showError(VolleyError error) {
        Log.d("TEST", error.getMessage() + "_" + error.getLocalizedMessage());
        HideProgressDialog();
        if (error == null || error.networkResponse == null) {
            Toast.makeText(this, "Ошибка сети, проверьте подключение к интернету", Toast.LENGTH_LONG).show();
            return;
        }
        if (error.networkResponse.statusCode / 100 == 4) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(new String(error.networkResponse.data));
                Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(this, "Ошибка сети, проверьте подключение к интернету", Toast.LENGTH_LONG).show();
        }
    }

    private void getUser(String contractID) {
        JSONObject jsonObject = null;

        try {
            jsonObject = new JSONObject(contractID);
            contractID = jsonObject.getJSONObject("data").getString("contractId");
            firstLogin = jsonObject.getJSONObject("data").getBoolean("firstLogin");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String finalContractID = contractID;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://webapi.bi-group.org/api/v1/MobileClient/GetProfile?contractId=" + contractID,
                response -> doLogin(response, finalContractID),
                this::showError) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map headers = new HashMap();
                headers.put("Token", token);
                return headers;
            }
        };

//        {
//            "ApplicationUserId": "3b0a7071-c494-4fa5-bf21-98303c6da830",
//                "Guid1C": "dfb0b168-41ab-11e1-9236-505054503030",
//                "UserName": "4811892",
//                "LastName": "Сиазбаева Айжан Сатвалдыевна",
//                "FirstName": null,
//                "MiddleName": null,
//                "Phone": "8-747-068-40-10",
//                "Email": "siazbaeva_aijan@mail.ru",
//                "Iin": "820628400041",
//                "SecretCode": "Айжан",
//                "Ava": null,
//                "BiCardType": "BI Silver",
//                "Discount": 5.00,
//                "DiscountOffice": 6.00
//        }

        requestQueue.add(stringRequest);
    }

    private void doLogin(String profileJson, String contractID) {
        HideProgressDialog();
        Log.d("TEST", profileJson +"_");
        User user = new Gson().fromJson(profileJson, User.class);

        PreferenceManager.getDefaultSharedPreferences(this)
                .edit()
                .putString("contractId", contractID)
                .putString("applicationUserId", user.getApplicationUserId())
                .putString("fullName", user.getFullName())
                .putString("phone", user.getPhone())
                .putString("iin", user.getIin())
                .putString(HomeActivity.CURRENT_USER, profileJson)
                .putString("email", user.getEmail())
                .putString("parentLogin", "BiGroupSub")
                .commit();

        Library.SavePhoneMemoryOne("phone",user.getPhone());
        Library.SavePhoneMemoryOne("email",user.getEmail());
        Library.SavePhoneMemoryOne("iin",user.getIin());
        Library.SavePhoneMemoryOne("bitmap","not");
        Library.SavePhoneMemoryOne("bitmaptwo","not");


        openHomeActivity();
    }
//BiGroupSub
    private void openHomeActivity() {
        Intent intent = new Intent(this, HomeActivity.class);
//        firstLogin = PreferenceManager.getDefaultSharedPreferences(this).getBoolean("firstLogin", true);
        intent.putExtra("firstLogin", firstLogin);

        startActivity(intent);
    }

    public void openForgotPass(View view) {
        startActivity(new Intent(this, ForgotPasswordActivity.class));
    }

    public void openRegistration(View view) {
        startActivity(new Intent(this, RegistrationActivity.class));
    }

    public void ShowProgressDialog() {
        dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.progress_dialog_layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);
        b = dialogBuilder.create();
        b.show();
    }

    public void HideProgressDialog() {
        b.dismiss();
    }

}
