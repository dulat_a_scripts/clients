package com.zerotoonelabs.bigroup;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class ConfirmSMSActivity extends AppCompatActivity {
    boolean fromReg;
    String data;
    String url = " http://webapi.bi-group.org/api/v1/MobileClient/ConfirmRegistration";
    TextView timer;
    EditText smsET, password, confirmPassword;
    String token;
    AlertDialog b;
    AlertDialog.Builder dialogBuilder;
    int countdown;
    Timer counter;
    LinearLayout registerLayout;


    String contractID, messageId, iin, phone, mail, smsCode, passwordStr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_sms);
        getSupportActionBar().hide();
        token = PreferenceManager.getDefaultSharedPreferences(this).getString("token", "");
        timer = findViewById(R.id.timer_tv);
        smsET = findViewById(R.id.sms_code_et);
        registerLayout = findViewById(R.id.register_layout);
        password = findViewById(R.id.password_et);
        confirmPassword = findViewById(R.id.confirm_password_et);
        fromReg = getIntent().hasExtra("reg");
        if (fromReg) {
            registerLayout.setVisibility(View.VISIBLE);
        }
        if (getIntent().hasExtra("data")) {
            data = getIntent().getStringExtra("data");
        }
        if (getIntent().hasExtra("otvet")) {
            password.setHint("Введите новый пароль");
            data = getIntent().getStringExtra("otvet");
        }
        startCountDown();

    }

    public void confirmSMS(View view) {
        if (fromReg)
            confirmREG();
        else
            confirmForgot();
    }

    private void confirmForgot() {
        if (!password.getText().toString().equals(confirmPassword.getText().toString())) {
            Toast.makeText(this, "Ваши пароли не совпадают", Toast.LENGTH_LONG).show();
            return;
        }
        ShowProgressDialog();

        RequestQueue requestQueue = Volley.newRequestQueue(this);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, " http://webapi.bi-group.org/api/v1/MobileClient/ResetPassword",
                response -> confirmPassAndSMS(response),
                error -> showError(error)) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map headers = new HashMap();
                headers.put("Token", token);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                String body = "{\"phone\":\"8-700-777-77-77\",\n" +
                        "  \"password\":\"AlexandrT1979@\"}";
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    JSONObject dataJson = jsonObject.getJSONObject("data");
                    phone = dataJson.getString("phone");
                    messageId = dataJson.getString("messageId");
                    passwordStr = password.getText().toString();
                    smsCode = smsET.getText().toString();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Map map = new HashMap();
                map.put("phone", phone);
                map.put("messageId", messageId == null ? "" : messageId);
                map.put("password", passwordStr);
                map.put("smsCode", smsCode);
                body = new Gson().toJson(map);
                return body.getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        requestQueue.add(stringRequest);
    }

    private void confirmREG() {
        if (!password.getText().toString().equals(confirmPassword.getText().toString())) {
            Toast.makeText(this, "Ваши пароли не совпадают", Toast.LENGTH_LONG).show();
            return;
        }
        ShowProgressDialog();

        RequestQueue requestQueue = Volley.newRequestQueue(this);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, " http://webapi.bi-group.org/api/v1/MobileClient/ConfirmRegistration",
                response -> confirmPassAndSMS(response),
                error -> showError(error)) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map headers = new HashMap();
                headers.put("Token", token);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                String body = "{\"phone\":\"8-700-777-77-77\",\n" +
                        "  \"password\":\"AlexandrT1979@\"}";
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    JSONObject dataJson = jsonObject.getJSONObject("data");
                    contractID = dataJson.getString("contractId");
                    phone = dataJson.getString("phone");
                    mail = dataJson.getString("email");
                    iin = dataJson.getString("iin");
                    messageId = dataJson.getString("messageId");
                    passwordStr = password.getText().toString();
                    smsCode = smsET.getText().toString();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Map map = new HashMap();
                map.put("contractId", contractID);
                map.put("phone", phone);
                map.put("iin", iin);
                map.put("email", mail);
                map.put("messageId", messageId);
                map.put("password", passwordStr);
                map.put("smsCode", smsCode);
                body = new Gson().toJson(map);
                return body.getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        requestQueue.add(stringRequest);

    }

    private void confirmPassAndSMS(String response) {
        HideProgressDialog();
        if (response.indexOf("true") > 0) {
            Toast.makeText(this, "Ваш запрос успешно выполнен", Toast.LENGTH_LONG).show();
            startActivity(new Intent(this, LoginActivity.class));
        }

    }


    public void ShowProgressDialog() {
        dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.progress_dialog_layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);
        b = dialogBuilder.create();
        b.show();
    }

    public void HideProgressDialog() {
        b.dismiss();
    }


    private void startCountDown() {
        counter = new Timer();
        countdown = 31;
        //timer.setEnabled(false);
        timer.setTypeface(Typeface.DEFAULT);
        counter.schedule(new TimerTask() {
            @Override
            public void run() {
                TimerMethod();
            }
        }, 0, 1000);

    }

    private void TimerMethod() {
        countdown--;
        this.runOnUiThread(Timer_Tick);
    }


    private Runnable Timer_Tick = new Runnable() {
        public void run() {
            if (countdown == 0) {
                counter.cancel();
                timer.setText("Отправить смс");
                timer.setTypeface(Typeface.DEFAULT_BOLD);
                timer.setEnabled(true);
            } else {
                timer.setText("Отправить смс заново через " + countdown + " секунд");
            }

        }
    };

    private void showError(VolleyError error) {
        HideProgressDialog();
        if (error.networkResponse != null && error.networkResponse.statusCode / 100 == 4) {
            JSONObject jsonObject = null;
            System.out.println();
            try {
                jsonObject = new JSONObject(new String(error.networkResponse.data));
                Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();


            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(this, "Ошибка сети, временные неполадки на сервере", Toast.LENGTH_LONG).show();
        }
    }


}
