package com.zerotoonelabs.bigroup.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.di.Injectable;
import com.zerotoonelabs.bigroup.ui.common.NavigationController;

import javax.inject.Inject;

public class PartPaymentFragment extends Fragment implements Injectable {

    @Inject
    NavigationController navigationController;

    public PartPaymentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_part_payment, container, false);
        view.findViewById(R.id.button).setOnClickListener(v -> navigationController.navigateToCallBackstack());
        return view;
    }

}
