package com.zerotoonelabs.bigroup.fragments.peny;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.api.PaymentScheduleResponse;
import com.zerotoonelabs.bigroup.api.RestClient;
import com.zerotoonelabs.bigroup.authui.SingleFragmentActivity;
import com.zerotoonelabs.bigroup.entity.Agreement;
import com.zerotoonelabs.bigroup.entity.CloudPayRequest;
import com.zerotoonelabs.bigroup.entity.PenaltyRequest;
import com.zerotoonelabs.bigroup.entity.PenaltyResponse;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.cloudpayments.sdk.PaymentWidget;
import ru.cloudpayments.sdk.business.domain.model.BaseResponse;
import ru.cloudpayments.sdk.business.domain.model.billing.CardsAuthConfirmResponse;
import ru.cloudpayments.sdk.business.domain.model.billing.CardsAuthResponse;
import ru.cloudpayments.sdk.view.PaymentTaskListener;

import static android.text.TextUtils.isEmpty;
import static com.zerotoonelabs.bigroup.fragments.peny.PayPenyFragment.CONTRACT_KEY;
import static com.zerotoonelabs.bigroup.fragments.peny.PayPenyFragment.PROPERTY_GUID_KEY;
import static com.zerotoonelabs.bigroup.fragments.peny.PayPenyFragment.TOKEN_KEY;

/**
 * A simple {@link Fragment} subclass.
 */
public class PenyFragment extends Fragment {

    private String token;
    private String contract;
    private String contractNumber;
    private String contractGUID;
    private String propertyGUID;

    private View view;
    private TextView penyTV;
    private EditText penyET;
    private Button buttonPay;
    private LinearLayout sberSection;
    private LinearLayout otherSection;
    private CheckBox sberCheck;
    private CheckBox otherCheck;
    private TextView fee;

    private double fee1;

    private LinearLayout sectionShow;

    public PenyFragment() {}

    public static PenyFragment newInstance(String token, String contractId, String param3) {
        PenyFragment fragment = new PenyFragment();
        Bundle args = new Bundle();
        args.putString(TOKEN_KEY, token);
        args.putString(CONTRACT_KEY, contractId);
        args.putString(PROPERTY_GUID_KEY, param3);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_peny, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        token = getArguments().getString(TOKEN_KEY);
        contract = getArguments().getString(CONTRACT_KEY);
        propertyGUID = getArguments().getString(PROPERTY_GUID_KEY);
        contractGUID = PreferenceManager.getDefaultSharedPreferences(getContext()).getString("contracterGUID", "null");
        contractNumber = PreferenceManager.getDefaultSharedPreferences(getContext()).getString("contractNumber", "null");

        penyTV = view.findViewById(R.id.penyTV);
        penyET = view.findViewById(R.id.amount_et);
        buttonPay = view.findViewById(R.id.buttonPay);
        sectionShow = view.findViewById(R.id.sectionShow);
        sberSection = view.findViewById(R.id.sberSection);
        otherSection = view.findViewById(R.id.otherSection);
        sberCheck = view.findViewById(R.id.sberCheck);
        otherCheck = view.findViewById(R.id.otherCheck);
        fee = view.findViewById(R.id.fee);

       sberSection.setOnClickListener(v -> {
            sberCheck.setChecked(true);
            otherCheck.setChecked(false);
            countFee("sber");

        });
        otherSection.setOnClickListener(v -> {
            sberCheck.setChecked(false);
            otherCheck.setChecked(true);
            countFee("other");
        });
        penyET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    countFee(sberCheck.isChecked() ? "sber" : "other");
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        loadData();
        return view;
    }

    private String formatPrice(double price) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
        symbols.setGroupingSeparator(' ');
        formatter.setDecimalFormatSymbols(symbols);
        return formatter.format(price);
    }

    private void countFee(String type){
        double toPay = Double.parseDouble(penyET.getText().toString().replaceAll(" ", ""));
        String text = "";
        if (type.equals("sber")){
            if (toPay > 10000) {
                //String feeVal = "<font color=#ff0000>" + "+300 тг" +"</font>";
                //fee.setText("Технический сбор " + Html.fromHtml(feeVal));
                fee1 = 300;
                String sFeeVal = (getString(R.string.bankFee, String.valueOf(300)));
                fee.setText(Html.fromHtml(sFeeVal));
                double toPayWithFee = toPay + 300;
                text = ("Оплатить " + formatPrice(toPayWithFee) + " \u20B8");
            } else {
                double feeVal = toPay/100;
                fee1 = feeVal;
                String sFeeVal = (getString(R.string.bankFee, String.valueOf(feeVal)));
                fee.setText(Html.fromHtml(sFeeVal));

                //fee.setText("Технический сбор " + Html.fromHtml(sFeeVal));
                double toPayWithFee = toPay + (toPay / 100);
                text = ("Оплатить " + formatPrice(toPayWithFee) + " \u20B8");
            }
        }
        if (type.equals("other")){
            double feeVal = (toPay * 2.4) / 100;
            fee1 = feeVal;
            //String sFeeVal = "<font color=#ff0000>" + feeVal + " тг" +"</font>";
            //fee.setText("Технический сбор " + Html.fromHtml(sFeeVal));
            String sFeeVal = (getString(R.string.bankFee, String.valueOf(feeVal)));
            fee.setText(Html.fromHtml(sFeeVal));

            double toPayWithFee = toPay + ((toPay * 2.4) / 100);
            text = ("Оплатить " + formatPrice(toPayWithFee) + " \u20B8");
        }

        buttonPay.setText(text);
    }

    private void updateUI(PenaltyResponse response){

        penyTV.setText(formatPrice(response.getSummfine()) + " \u20B8");
        Log.d("TEST", PreferenceManager.getDefaultSharedPreferences(getContext()).getBoolean("checkStatus", false) + "_");
        if (response.getSummfine() > 0){
            if (!PreferenceManager.getDefaultSharedPreferences(getContext()).getBoolean("checkStatus", false)){
                penyET.setText("Запрос на онлайн оплату");
                buttonPay.setEnabled(true);

                buttonPay.setOnClickListener(v -> {
                    Toast.makeText(getContext(), "Ваш запрос отправлен", Toast.LENGTH_LONG).show();
                });

            } else {
                //xx
                penyET.setText(String.valueOf(response.getSummfine()));
                buttonPay.setEnabled(true);
                countFee("sber");
                sectionShow.setVisibility(View.VISIBLE);

                buttonPay.setOnClickListener(v -> loadAgreement());
            }
        }
    }

    private void payPressed() {
        String initValue = penyET.getText().toString().trim();
        initValue.replace(" ", "");
        String paymentAmount = initValue.replace(" ", "");//binding.get().amountEt.getText().toString().trim();


        double amount = Double.parseDouble(paymentAmount) + fee1;
        //String currency = payment.currentPayment.currency;
        //String description = payment.currentPayment.description;
        String publicId = PreferenceManager.getDefaultSharedPreferences(getContext()).getString("publicId", "null");
        String invoiceId = String.valueOf(PreferenceManager.getDefaultSharedPreferences(getContext()).getInt("invoiceId", 1));
        String accountId = PreferenceManager.getDefaultSharedPreferences(getContext()).getString("accountId", "null");

        //HashMap<String, Object> map = new HashMap<>();
        //map.put("saveCard", "saveCard");
        //String data = new Gson().toJson(map);

        //Intent intent = new Intent(getActivity(), PaymentWidget.class);
        //intent.putExtra(PaymentWidget.EXTRA_AMOUNT, amount); // Сумма оплаты
        //intent.putExtra(PaymentWidget.EXTRA_DESCRIPTION, " опалата пени"); // Описание
        //intent.putExtra(PaymentWidget.EXTRA_CURRENCY, "KZT"); // Код валюты
        //intent.putExtra(PaymentWidget.EXTRA_PUBLIC_ID, "pk_348c635ba69b355d6f4dc75a4a205"); // Ваш public ID
        //intent.putExtra(PaymentWidget.EXTRA_INVOICE_ID, invoiceId); // ID заказа в вашей системе
        //intent.putExtra(PaymentWidget.EXTRA_ACCOUNT_ID, accountId); // ID покупателя в вашей системе
        //intent.putExtra(PaymentWidget.EXTRA_DATA, data); // Произвольный набор параметров
        //intent.putExtra(PaymentWidget.EXTRA_TYPE, PaymentWidget.TYPE_AUTH); // Тип платежа: TYPE_CHARGE (одностадийный) или TYPE_AUTH (двухстадийный)
        //PaymentWidget.taskListener = paymentTaskListener;
        //startActivity(intent);

        CloudPayRequest request = new CloudPayRequest();
        request.setAmount(amount);
        request.setCurrency("KZT");
        request.setDescription("опалата пени");
        request.setAccountId(accountId);
        request.setInvoiceId(invoiceId);
        request.setPublicId(publicId);//("pk_348c635ba69b355d6f4dc75a4a205");
        //request.setEmail(binding.get().emailEt.getText().toString());
        //request.setAutoPayDate(selectedDate != null ? selectedDate.getTime() : new Date());
        //request.setAutoPay(binding.get().autoPaycCheck.isChecked());

        Intent intent = new Intent(getActivity(), SingleFragmentActivity.class);
        intent.putExtra(SingleFragmentActivity.CLOUD_PAY, true);
        intent.putExtra("req", request);
        startActivity(intent);
    }

    private PaymentTaskListener paymentTaskListener = new PaymentTaskListener() {

        @Override
        public void success(BaseResponse response) {
            if (response instanceof CardsAuthConfirmResponse) {
                showResult(getString(R.string.payment_finished) + ((CardsAuthConfirmResponse) response).transaction.cardHolderMessage + "\n");
            }
            else if (response instanceof CardsAuthResponse) {
                showResult(getString(R.string.payment_finished) + ((CardsAuthResponse) response).auth.cardHolderMessage + "\n");
            } else {
                showResult(getString(R.string.payment_finished) + response + "\n");
            }
        }

        @Override
        public void error(BaseResponse response) {
            if (response instanceof CardsAuthConfirmResponse)
                showResult(getString(R.string.payment_not_finished) + ((CardsAuthConfirmResponse) response).transaction.cardHolderMessage + "\n");
            if (response instanceof CardsAuthResponse)
                showResult(getString(R.string.payment_not_finished) + ((CardsAuthResponse) response).auth.cardHolderMessage + "\n");
            else
                showResult(getString(R.string.error) + response.message + "\n");
        }

        @Override
        public void cancel() {
            showResult(getString(R.string.operation_canceled) + "\n");
        }
    };

    public void showResult(String text) {
        Toast.makeText(getContext(), text, Toast.LENGTH_SHORT).show();
    }
//zz
    private void loadAgreement(){

        String address = PreferenceManager.getDefaultSharedPreferences(getContext()).getString("ADD", "");
        String docs = PreferenceManager.getDefaultSharedPreferences(getContext()).getString("DOG", "");
        String newdocs = docs.replace(" ","%20");
        String newaddress = address.replace(" ","%20");

        String penya = penyET.getText().toString().trim();
        long timestamp = System.currentTimeMillis() / 1000L;
        String datestring = String.valueOf(timestamp);

        RestClient
                .getRestProvider()
                .getPenaltyAgreementText(token,datestring,newdocs,penya,address)
                .enqueue(new Callback<Agreement>() {
                    @Override
                    public void onResponse(Call<Agreement> call, Response<Agreement> response) {

                        try {
                            final Dialog dialog = new Dialog(getContext());
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setContentView(R.layout.dialog_layout);
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));


                            TextView title = dialog.findViewById(R.id.title);
                            TextView body = dialog.findViewById(R.id.body);
                            TextView ok = dialog.findViewById(R.id.ok);
                            title.setText(Html.fromHtml(response.body().title));

                            ok.setText(Html.fromHtml(response.body().agree));


                            //long timestamp = Calendar.getInstance().getTime() / 1000;
                            String add = PreferenceManager.getDefaultSharedPreferences(getContext()).getString("ADD", "");
                            String dog = PreferenceManager.getDefaultSharedPreferences(getContext()).getString("DOG", "");

                            String str = response.body().body;
//                            String str2 = str.replace("сумма в размере ", "сумма в размере " + penyET.getText().toString().trim())
//                                    .replace("по адресу: ", "по адресу: " + add)
//                                    .replace("с условиями ", "с условиями " + dog);

                            body.setText(Html.fromHtml(str));

                            ok.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                   dialog.dismiss();
                                   payPressed();
                                }
                            });
                            dialog.show();
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<Agreement> call, Throwable t) {

                    }
                });
    }

    private void loadData(){
        PenaltyRequest request = new PenaltyRequest();
        request.setContracterGuid(contractGUID);
        request.setContractNumber(contractNumber);
        request.setPropertyId(propertyGUID);
        RestClient
                .getRestProvider()
                .loadPenalty(token, request)
                .enqueue(new Callback<PenaltyResponse>() {
            @Override
            public void onResponse(Call<PenaltyResponse> call, Response<PenaltyResponse> response) {

                if(response.isSuccessful()){

                    if(response.body() != null){
                        try {
                            updateUI(response.body());
                        } catch (NullPointerException e){
                            e.printStackTrace();
                            Library.showToast(getContext(),"ошибка на стороне сервера");
                        }
                    }else {
                        Library.showToast(getContext(),"Нет данных от сервера");
                    }

                }else{
                    Library.showToast(getContext(),"ошибка на стороне сервера..");
                }

            }

            @Override
            public void onFailure(Call<PenaltyResponse> call, Throwable t) {
                Library.showToast(getContext(),"ошибка сети проверьте данные");
            }
        });
    }
}
