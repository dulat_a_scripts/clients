package com.zerotoonelabs.bigroup.fragments.club;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.api.PicasoClient;
import com.zerotoonelabs.bigroup.api.RestClient;
import com.zerotoonelabs.bigroup.entity.club.Partner;

public class PartnerDetailsFragment extends Fragment {

    public static final String PARTNER = "PARTNER";

    private View view;
    private ImageView imageView;
    private TextView orgName;
    private TextView field;
    private TextView discount;
    private TextView city;
    private LinearLayout webSection;
    private LinearLayout orgNameSection;

    private Partner partner;

    public PartnerDetailsFragment() {}

    public static PartnerDetailsFragment newInstance(Partner partner) {
        PartnerDetailsFragment fragment = new PartnerDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(PARTNER, partner);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_partner_details, container, false);
        partner = (Partner) getArguments().getSerializable(PARTNER);

        imageView = view.findViewById(R.id.partnerLogo);
        orgName = view.findViewById(R.id.orgName);
        field = view.findViewById(R.id.field);
        discount = view.findViewById(R.id.discount);
        city = view.findViewById(R.id.city);
        webSection = view.findViewById(R.id.webSection);
        orgNameSection = view.findViewById(R.id.orgNameSection);

        webSection.setOnClickListener(v -> goToWeb(partner.getWeb()));
        loadImg(partner);
        updateUI(partner);
        return view;
    }

    private void goToWeb(String url){
        String finalUrl = url;
        if (!url.startsWith("http://") && !url.startsWith("https://"))
            finalUrl = "http://" + url;

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(finalUrl));
        startActivity(browserIntent);
    }

    private void loadImg(Partner partner){

        Picasso.get()
                .load(partner.getAva())
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError(Exception e) {
                        imageView.setImageDrawable(getContext().getResources().getDrawable(R.drawable.logo_bi_club));
                    }
                });
    }

    private void updateUI(Partner partner){
        if (partner.getOrganization().isEmpty())
            orgNameSection.setVisibility(View.GONE);

        orgName.setText(partner.getOrganization());
        field.setText(partner.getActivityField());
        discount.setText(partner.getDiscountFrom() + " %");
        city.setText(partner.getCity());
    }
}
