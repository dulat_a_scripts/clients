package com.zerotoonelabs.bigroup.fragments;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.zerotoonelabs.bigroup.R;

public class PaymentGraphActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_graph);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
