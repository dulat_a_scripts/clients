package com.zerotoonelabs.bigroup.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zerotoonelabs.bigroup.R;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SpravkiFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SpravkiFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    TextView status;


    public SpravkiFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SpravkiFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SpravkiFragment newInstance(String param1, String param2) {
        SpravkiFragment fragment = new SpravkiFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_spravki, container, false);
        RecyclerView myQueries = (RecyclerView) view.findViewById(R.id.my_spravkas);
        myQueries.setLayoutManager(new LinearLayoutManager(getContext()));
        myQueries.setHasFixedSize(true);
        myQueries.setAdapter(new RecyclerView.Adapter() {
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_query_list_item, parent, false);
                status = (TextView) view.findViewById(R.id.query_status_tv);
                RecyclerView.ViewHolder viewHolder = new RecyclerView.ViewHolder(view) {
                };
                return viewHolder;
            }

            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
                if (position == 0) {
                    status.setText("Заявка в обработке");
                    status.setTextColor(ContextCompat.getColor(getContext(), R.color.orangeAccent));
                }

            }

            @Override
            public int getItemCount() {
                return 2;
            }
        });
        return view;
    }

}
