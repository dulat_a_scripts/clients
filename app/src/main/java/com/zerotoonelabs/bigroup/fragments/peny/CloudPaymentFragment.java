package com.zerotoonelabs.bigroup.fragments.peny;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.zerotoonelabs.bigroup.Library.Dialogmessage;
import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.Library.Retrofit_request.Set_podpicka;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.api.PaymentScheduleResponse;
import com.zerotoonelabs.bigroup.api.RestClient;
import com.zerotoonelabs.bigroup.authui.HomeActivity;
import com.zerotoonelabs.bigroup.authui.paymentschedule.PaymentScheduleFragment;
import com.zerotoonelabs.bigroup.entity.AutoPayRequest;
import com.zerotoonelabs.bigroup.entity.CloudPayRequest;
import com.zerotoonelabs.bigroup.views.masked_edit_text.MaskedEditText;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Set;

import okhttp3.Credentials;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import ru.cloudpayments.sdk.CardFactory;
import ru.cloudpayments.sdk.ICard;
import ru.cloudpayments.sdk.IPayment;
import ru.cloudpayments.sdk.PaymentFactory;
import ru.cloudpayments.sdk.business.domain.model.BaseResponse;
import ru.cloudpayments.sdk.business.domain.model.billing.CardsAuthConfirmResponse;
import ru.cloudpayments.sdk.business.domain.model.billing.CardsAuthResponse;
import ru.cloudpayments.sdk.view.PaymentTaskListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class CloudPaymentFragment extends Fragment {

    private View view;
    private TextView amountView;
    private TextView descriptionView;
    private EditText cardHolderNameView;
    private MaskedEditText cardNumberView;
    private MaskedEditText cardExpView;
    private EditText cvvView;
    private Button payButton;
    private CloudPayRequest request;
    ProgressBar progressBar;

    public CloudPaymentFragment() {}

    public static CloudPaymentFragment newInstance(CloudPayRequest request) {
        Bundle args = new Bundle();
        args.putSerializable("req", request);
        CloudPaymentFragment fragment = new CloudPaymentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_cloud_payment, container, false);
        amountView = view.findViewById(R.id.amountView);
        descriptionView = view.findViewById(R.id.descriptionView);
        cardHolderNameView = view.findViewById(R.id.cardHolderNameView);
        cardNumberView = view.findViewById(R.id.cardNumberView);
        cardExpView = view.findViewById(R.id.cardExpView);
        cvvView = view.findViewById(R.id.cvvView);
        payButton = view.findViewById(R.id.payButton);

        progressBar = view.findViewById(R.id.progressBar4);

        cardNumberView.setInputType(InputType.TYPE_CLASS_NUMBER);
        cardNumberView.setSelection(cardNumberView.getText().length());
        cardNumberView.setOnClickListener(view -> cardNumberView.setSelection(cardNumberView.getText().length()));

        cardExpView.setInputType(InputType.TYPE_CLASS_NUMBER);
        cardExpView.setSelection(cardExpView.getText().length());
        cardExpView.setOnClickListener(view -> cardNumberView.setSelection(cardNumberView.getText().length()));

        request = (CloudPayRequest) getArguments().getSerializable("req");

        cardHolderNameView.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        descriptionView.setText(request.getDescription());
        displayAmount();
        payButton.setOnClickListener(v -> payAction());
        return view;
    }

    private void payAction(){



        //Dialog("Оплата успешно произведена! ","закрыть","перейти на главную");
        progressBar.setVisibility(View.VISIBLE);
        payButton.setEnabled(false);
        String cardNumber = cardNumberView.getInputText();
        String expDate = cardExpView.getInputText();
        String cvv = cvvView.getText().toString();
        String holderName = cardHolderNameView.getText().toString();
        // TODO validate form and go from page after payment
        //if (cardNumber)

        ICard card = CardFactory.create(cardNumber, expDate, cvv);
        try {
            String cardCryptogram = card.cardCryptogram(request.getPublicId());
            if (card.isValidNumber()) {
                IPayment paymentAuth = PaymentFactory.auth(getActivity(),
                        request.getPublicId(),
                        request.getAccountId(),
                        request.getInvoiceId(),
                        cardCryptogram,
                        holderName,
                        request.getAmount(),
                        request.getCurrency(),
                        request.getDescription(),

                        "{}");
                paymentAuth.run(paymentTaskListener);
                //xx
            } else {
                payButton.setEnabled(true);
                showResult("CardNumber is not valid");
                progressBar.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
            payButton.setEnabled(true);
            showResult("Error get card cryptogram");
            progressBar.setVisibility(View.GONE);
        }
    }

    public void Dialog(String message,String button1,String button2){

        //new dialog

        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setMessage(message);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                button2,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Intent intents = new Intent(getActivity(), HomeActivity.class);
                        intents.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intents);

                    }
                });

        builder1.setNegativeButton(
                button1,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();

    }

    private void displayAmount() {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
        symbols.setGroupingSeparator(' ');
        formatter.setDecimalFormatSymbols(symbols);

        payButton.setText("Оплатить " + formatter.format(request.getAmount()) + " \u20B8");
        amountView.setText(formatter.format(request.getAmount()) + " \u20B8");
    }

    private void showResult(String resultString){
        Toast.makeText(getContext(), resultString, Toast.LENGTH_SHORT).show();
    }

    private PaymentTaskListener paymentTaskListener = new PaymentTaskListener() {

        @Override
        public void success(BaseResponse response) {
            payButton.setEnabled(true);
            if (response instanceof CardsAuthConfirmResponse) {
                showResult(getString(R.string.payment_finished) + ((CardsAuthConfirmResponse) response).transaction.cardHolderMessage + "\n");
                progressBar.setVisibility(View.GONE);
                if (request.isAutoPay()){
                    setAutoPay(((CardsAuthConfirmResponse) response).transaction.token);
                }
                progressBar.setVisibility(View.GONE);

                Dialog("Оплата успешно произведена! ","закрыть","перейти на главную");

            }
            else if (response instanceof CardsAuthResponse) {
                showResult(getString(R.string.payment_finished) + ((CardsAuthResponse) response).auth.cardHolderMessage + "\n");
                progressBar.setVisibility(View.GONE);
                if (request.isAutoPay()){
                    setAutoPay(((CardsAuthConfirmResponse) response).transaction.token);
                }

                progressBar.setVisibility(View.GONE);

                Dialog("Оплата успешно произведена! ","закрыть","перейти на главную");

            } else {
                showResult(getString(R.string.payment_finished) + response + "\n");
                if (request.isAutoPay()){
                    setAutoPay(((CardsAuthConfirmResponse) response).transaction.token);
                }

                progressBar.setVisibility(View.GONE);

                Dialog("Оплата успешно произведена! ","закрыть","перейти на главную");
            }
        }

        @Override
        public void error(BaseResponse response) {
            payButton.setEnabled(true);
            if (response instanceof CardsAuthConfirmResponse){
                progressBar.setVisibility(View.GONE);
                showResult(getString(R.string.payment_not_finished) + ((CardsAuthConfirmResponse) response).transaction.cardHolderMessage + "\n");
            }

            if (response instanceof CardsAuthResponse){
                progressBar.setVisibility(View.GONE);
                showResult(getString(R.string.payment_not_finished) + ((CardsAuthResponse) response).auth.cardHolderMessage + "\n");
            }

            else{
                showResult(getString(R.string.error) + response.message + "\n");
                progressBar.setVisibility(View.GONE);
            }

        }

        @Override
        public void cancel() {
            showResult(getString(R.string.operation_canceled) + "\n");
            progressBar.setVisibility(View.GONE);

        }
    };





    private void setAutoPay(String token){
        AutoPayRequest autoPayRequest = new AutoPayRequest();
        autoPayRequest.setToken(token);
        autoPayRequest.setEmail(request.getEmail());
        autoPayRequest.setAccountId(request.getAccountId());
        autoPayRequest.setAmount(request.getAmount());
        autoPayRequest.setCurrency(request.getCurrency());
        autoPayRequest.setDescription(request.getDescription());
        autoPayRequest.setRequireConfirmation(true);
        autoPayRequest.setInterval("Month");
        autoPayRequest.setPeriod(1);
       // autoPayRequest.setStartDate(new SimpleDateFormat("yyyy-dd-MMMM'T'HH:mm:ss").format(request.getAutoPayDate()));
        autoPayRequest.setStartDate(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(request.getAutoPayDate()));
       // autoPayRequest.setStartDate(request.getAutoPayDate());


        String auth = Credentials.basic(Library.publicid,Library.api_key);

        Retrofit client = Library.NewRetrofitclient_Cloud();
        Set_podpicka set_podpicka = client.create(Set_podpicka.class);
        Call<ResponseBody> call = set_podpicka.createAutoPay(auth,autoPayRequest);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.i("ddd","ddd" + response.body().toString());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });


    }
}
