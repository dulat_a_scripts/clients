package com.zerotoonelabs.bigroup.fragments.peny;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.entity.PaymentsFineElement;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MyItemRecyclerViewAdapter extends RecyclerView.Adapter<MyItemRecyclerViewAdapter.ViewHolder> {

    private ArrayList<PaymentsFineElement> elements;

    public MyItemRecyclerViewAdapter(ArrayList<PaymentsFineElement> elements) {
        this.elements = elements;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        PaymentsFineElement element = elements.get(position);
        try {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
            Date date = format.parse((String)element.getCell()[1]);
            holder.date.setText(new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault()).format(date));
        } catch (Exception e){
            e.printStackTrace();
            holder.date.setText((String)element.getCell()[1]);
        }
        holder.amount.setText((double)element.getCell()[2] + " \u0009");
    }

    @Override
    public int getItemCount() {
        return elements.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView date;
        TextView amount;

        public ViewHolder(View view) {
            super(view);
            date = (TextView) view.findViewById(R.id.date);
            amount = (TextView) view.findViewById(R.id.amount);
        }
    }
}
