package com.zerotoonelabs.bigroup.fragments.peny;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PayPenyFragment extends Fragment {

    public static final String TOKEN_KEY = "token";
    public static final String CONTRACT_KEY = "contract";
    public static final String PROPERTY_GUID_KEY = "property";

    private String token;
    private String contract;
    private String property;

    private TabLayout tabLayout;
    private ViewPager viewPager;

    public PayPenyFragment() {}

    public static PayPenyFragment newInstance(String token, String contractId, String param3) {
        PayPenyFragment fragment = new PayPenyFragment();
        Bundle args = new Bundle();
        args.putString(TOKEN_KEY, token);
        args.putString(CONTRACT_KEY, contractId);
        args.putString(PROPERTY_GUID_KEY, param3);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.public_utilities_frag, container, false);
        tabLayout = view.findViewById(R.id.tab_layout);
        viewPager = view.findViewById(R.id.container);

        token = getArguments().getString(TOKEN_KEY);
        contract = getArguments().getString(CONTRACT_KEY);
        property = getArguments().getString(PROPERTY_GUID_KEY);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupAdapter(new SectionsPagerAdapter(getChildFragmentManager()));
    }

    private void setupAdapter(SectionsPagerAdapter adapter) {
        viewPager.setAdapter(adapter);
        viewPager.getAdapter().notifyDataSetChanged();
        tabLayout.setupWithViewPager(viewPager, true);
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        SectionsPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {

            switch (position){
                case 0:
                    return PenyFragment.newInstance(token, contract, property);
                case 1:
                    return PenyItemFragment.newInstance(token, contract, property);
                default:
                    return null;
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title;
            if (position == 0) {
                title = "Оплата";
            } else {
                title = "История";
            }
            return title;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

}
