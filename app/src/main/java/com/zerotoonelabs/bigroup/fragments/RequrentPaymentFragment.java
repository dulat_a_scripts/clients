package com.zerotoonelabs.bigroup.fragments;


import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.api.RestClient;
import com.zerotoonelabs.bigroup.entity.RequrentPayment;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class RequrentPaymentFragment extends Fragment {

    private View view;
    private TextView amount;
    private TextView desc;
    private TextView period;
    private TextView succesCount;
    private TextView nextDate;
    private RequrentPayment payment;

    private Dialog dialog;

    public RequrentPaymentFragment() {}

    public static RequrentPaymentFragment newInstance(RequrentPayment payment) {
        Bundle args = new Bundle();
        args.putSerializable("PAYMENT", payment);
        RequrentPaymentFragment fragment = new RequrentPaymentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.edit_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.action_edit)
            showEditDialog();
        if(id == R.id.action_delete)
            showDeleteDialog();

        return super.onOptionsItemSelected(item);
    }

    private void cancelAction(){
        RestClient.getRestProvider2().cancelAutoPay(payment.getId()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    getActivity().onBackPressed();
                } catch (NullPointerException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                try {
                    getActivity().onBackPressed();
                } catch (NullPointerException e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void showDeleteDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Вы действительно хотите отменить подписку?");
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                cancelAction();
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showEditDialog(){
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.edit_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        LinearLayout mainView = dialog.findViewById(R.id.mainView);
        EditText amount = dialog.findViewById(R.id.amountInput);
        TextView back = dialog.findViewById(R.id.backButton);
        TextView ok = dialog.findViewById(R.id.okButton);

        amount.setText(payment.getAmount() +"");
        amount.setSelection(amount.getText().length());

        back.setOnClickListener(view -> dialog.dismiss());
        ok.setOnClickListener(v-> {
            try {
                back.setEnabled(false);
                ok.setEnabled(false);
                mainView.setAlpha(0.5f);

                updateAmount(Double.parseDouble(amount.getText().toString()));
            } catch (Exception e){
                e.printStackTrace();
            }

        });
        dialog.show();
    }

    private void updateAmount(double amount2){
        RestClient.getRestProvider2().updateAutoPay(payment.getId(), amount2).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    dialog.dismiss();
                    amount.setText(String.valueOf(amount2));
                } catch (NullPointerException e){
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                try {
                    dialog.dismiss();
                    Toast.makeText(getActivity(), "Операция не выполнена", Toast.LENGTH_SHORT).show();
                } catch (NullPointerException e){
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_requrent_payment, container, false);
        setHasOptionsMenu(true);
        payment = (RequrentPayment) getArguments().getSerializable("PAYMENT");

        amount = view.findViewById(R.id.amount);
        desc = view.findViewById(R.id.desc);
        period = view.findViewById(R.id.period);
        succesCount = view.findViewById(R.id.succesCount);
        nextDate = view.findViewById(R.id.nextDate);

        amount.setText(formatPrice(payment.getAmount()) + " \u0009");
        desc.setText(payment.getDescription());
        period.setText("Ежемесячно");
        succesCount.setText(payment.getSuccessfulTransactionsNumber() +"");

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
        try {
            Date date = format.parse(payment.getNextTransactionDate());
            nextDate.setText(new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault()).format(date));
        } catch (Exception e){
            e.printStackTrace();
            nextDate.setText(payment.getNextTransactionDate());
        }

        return view;
    }

    private String formatPrice(double price) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
        symbols.setGroupingSeparator(' ');
        formatter.setDecimalFormatSymbols(symbols);
        return formatter.format(price);
    }

}
