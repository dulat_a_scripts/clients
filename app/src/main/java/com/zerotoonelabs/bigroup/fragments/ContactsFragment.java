package com.zerotoonelabs.bigroup.fragments;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.FragmentCallBinding;
import com.zerotoonelabs.bigroup.databinding.FragmentContactsBinding;
import com.zerotoonelabs.bigroup.util.AutoClearedValue;


/**
 * A simple {@link Fragment} subclass.
 */
public class ContactsFragment extends Fragment {

    AutoClearedValue<FragmentContactsBinding> binding;


    public ContactsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentContactsBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_contacts, container, false);
        binding = new AutoClearedValue<>(this,dataBinding);
        return binding.get().getRoot();
    }
}
