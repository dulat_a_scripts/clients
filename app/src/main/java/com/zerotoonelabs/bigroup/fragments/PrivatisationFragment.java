package com.zerotoonelabs.bigroup.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class PrivatisationFragment extends Fragment {


    public PrivatisationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_privatisation, container, false);

        return v;
    }

}
