package com.zerotoonelabs.bigroup.fragments.club;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.adapters.TicketAdapter;
import com.zerotoonelabs.bigroup.api.PicasoClient;
import com.zerotoonelabs.bigroup.api.RestClient;
import com.zerotoonelabs.bigroup.entity.User;
import com.zerotoonelabs.bigroup.entity.club.ClientProfile;
import com.zerotoonelabs.bigroup.entity.club.ClubTicket;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.zerotoonelabs.bigroup.authui.HomeActivity.CURRENT_USER;

/**
 * A simple {@link Fragment} subclass.
 */
public class WalletFragment extends Fragment {

    private View view;
    private TextView flatDiscountView;
    private TextView officeDiscountView;
    private TextView userName;
    private ImageView cardImage;
    private ImageView qrCodeImg;
    private RecyclerView ticketsList;

    private String token;
    private String contractId;

    public WalletFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_wallet, container, false);
        flatDiscountView = view.findViewById(R.id.flatDiscountText);
        officeDiscountView = view.findViewById(R.id.officeDiscountText);
        userName = view.findViewById(R.id.userName);
        cardImage = view.findViewById(R.id.clubCardImage);
        qrCodeImg = view.findViewById(R.id.qrCodeImg);
        ticketsList = view.findViewById(R.id.ticketsList);
        ticketsList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        token = sharedPreferences.getString("token", null);
        contractId = sharedPreferences.getString("contractId", null);

        User user = getCurrentUser(sharedPreferences);
        userName.setText(user.getFullName());

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request newRequest = chain.request().newBuilder()
                                .addHeader("token", token)
                                .build();
                        return chain.proceed(newRequest);
                    }
                })
                .build();

        Picasso picasso = new Picasso.Builder(getContext())
                .downloader(new OkHttp3Downloader(client))
                .build();

        picasso.load("http://webapi.bi-group.org/api/v1/MobileClient/GetBIClubQRCode?contractId=" + contractId).into(qrCodeImg);

        loadData();
        return view;
    }

    public User getCurrentUser(SharedPreferences sharedPreferences) {
        String userString = sharedPreferences.getString(CURRENT_USER, null);
        if (userString != null) {
            return new Gson().fromJson(userString, User.class);
        }
        return null;
    }

    private void loadData(){
        RestClient.getRestProvider().getProfile(token, contractId).enqueue(new Callback<ClientProfile>() {
            @Override
            public void onResponse(Call<ClientProfile> call, Response<ClientProfile> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        updateUI(response.body());
                    }
                }
            }

            @Override
            public void onFailure(Call<ClientProfile> call, Throwable t) {
                Log.d("TEST", t.getMessage() + "_");
            }
        });
    }

    private void updateUI(ClientProfile profile){
        flatDiscountView.setText(profile.getDiscount() + " %");
        officeDiscountView.setText(profile.getDiscountOffice() + " %");
        setClubImage(profile.getBiCardType());

        // test ui
        ArrayList<ClubTicket> tickets = new ArrayList<>();
        tickets.add(new ClubTicket("Безлимитный абонемент", "от 120 000 тг"));
        tickets.add(new ClubTicket("Безлимитный абонемент 2", "от 150 000 тг"));
        ticketsList.setAdapter(new TicketAdapter(tickets));
    }

    private void setClubImage(String cardType){
        if (cardType.toLowerCase().contains("platinum"))
            cardImage.setImageDrawable(getResources().getDrawable(R.drawable.platinum_card));
        if (cardType.toLowerCase().contains("gold"))
            cardImage.setImageDrawable(getResources().getDrawable(R.drawable.gold_card));
        if (cardType.toLowerCase().contains("silver"))
            cardImage.setImageDrawable(getResources().getDrawable(R.drawable.silver_card));
        if (cardType.toLowerCase().contains("bronze"))
            cardImage.setImageDrawable(getResources().getDrawable(R.drawable.bronze_card));
    }

}
