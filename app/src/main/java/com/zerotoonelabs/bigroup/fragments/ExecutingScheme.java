package com.zerotoonelabs.bigroup.fragments;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.adapters.ExecutiveSchemeAdapter;

public class ExecutingScheme extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_executing_scheme);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        RecyclerView execRv = (RecyclerView) findViewById(R.id.exec_rv);
        execRv.setAdapter(new ExecutiveSchemeAdapter());
    }
}
