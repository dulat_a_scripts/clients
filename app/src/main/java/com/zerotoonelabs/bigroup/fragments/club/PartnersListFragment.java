package com.zerotoonelabs.bigroup.fragments.club;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.adapters.PartnersAdapter;
import com.zerotoonelabs.bigroup.adapters.TicketAdapter;
import com.zerotoonelabs.bigroup.entity.club.ClientProfile;
import com.zerotoonelabs.bigroup.entity.club.ClubTicket;
import com.zerotoonelabs.bigroup.entity.club.Partner;

import java.util.ArrayList;

import static com.zerotoonelabs.bigroup.fragments.club.AllPartnersFragment.PARTNER_LIST;

/**
 * A simple {@link Fragment} subclass.
 */
public class PartnersListFragment extends Fragment {

    private View view;
    private RecyclerView partnersList;

    public PartnersListFragment() {}

    public static PartnersListFragment newInstance(ArrayList<Partner> partners){
        Bundle bundle = new Bundle();
        bundle.putSerializable(PARTNER_LIST, partners);
        PartnersListFragment fragment = new PartnersListFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_partners_list, container, false);
        partnersList = view.findViewById(R.id.partnersList);
        partnersList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        ArrayList<Partner> partners = (ArrayList<Partner>) getArguments().getSerializable(PARTNER_LIST);
        updateUI(partners);
        return view;
    }

    private void updateUI(ArrayList<Partner> partners){
        partnersList.setAdapter(new PartnersAdapter(partners));
    }
}
