package com.zerotoonelabs.bigroup.fragments.club;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.api.RestClient;
import com.zerotoonelabs.bigroup.entity.club.Partner;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class AllPartnersFragment extends Fragment {

    public static final String PARTNER_LIST = "PARTNER_LIST";
    public static final String TYPE = "TYPE";
    public static final String ALL = "ALL";

    public static final String FOOD = "еда";
    public static final String HEALTH = "здоровье";
    public static final String FURNITURE = "мебель";
    public static final String REST = "отдых";
    public static final String GOODS = "товары";
    public static final String SERVICES = "услгуи";

    private TabLayout tabLayout;
    private ViewPager viewPager;

    public AllPartnersFragment() {}

    public static AllPartnersFragment newInstance(String type){
        Bundle bundle = new Bundle();
        bundle.putString(TYPE, type);
        AllPartnersFragment fragment = new AllPartnersFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.public_utilities_frag, container, false);
        tabLayout = view.findViewById(R.id.tab_layout);
        viewPager = view.findViewById(R.id.container);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);

        loadData(getArguments().getString(TYPE));
    }

    private void loadData(String type){

        if (type.equals(ALL)) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
            String token = sharedPreferences.getString("token", null);
            String contractId = sharedPreferences.getString("contractId", null);
            RestClient.getRestProvider().getClubPartners(token, contractId).enqueue(new Callback<Partner[]>() {
                @Override
                public void onResponse(Call<Partner[]> call, Response<Partner[]> response) {
                    if (response.isSuccessful()){
                        if (response.body() != null){
                            ArrayList<Partner> partners = new ArrayList<>();
                            partners.addAll(Arrays.asList(response.body()));
                            setupAdapter(new AllPartnersFragment.SectionsPagerAdapter(getChildFragmentManager(), partners));
                        }
                    }
                }

                @Override
                public void onFailure(Call<Partner[]> call, Throwable t) {
                    Log.d("TEST", t.getMessage() + "_");
                }
            });
        } else {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
            String token = sharedPreferences.getString("token", null);
            String contractId = sharedPreferences.getString("contractId", null);
            RestClient.getRestProvider().getClubPartnersByType(token, contractId, type).enqueue(new Callback<Partner[]>() {
                @Override
                public void onResponse(Call<Partner[]> call, Response<Partner[]> response) {
                    if (response.isSuccessful()){
                        if (response.body() != null){
                            ArrayList<Partner> partners = new ArrayList<>();
                            partners.addAll(Arrays.asList(response.body()));
                            setupAdapter(new AllPartnersFragment.SectionsPagerAdapter(getChildFragmentManager(), partners));
                        }
                    }
                }

                @Override
                public void onFailure(Call<Partner[]> call, Throwable t) {
                    Log.d("TEST", t.getMessage() + "_");
                }
            });
        }


    }

    private void setupAdapter(AllPartnersFragment.SectionsPagerAdapter adapter) {
        viewPager.setAdapter(adapter);
        viewPager.getAdapter().notifyDataSetChanged();
        tabLayout.setupWithViewPager(viewPager, true);
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private ArrayList<Partner> partners;

        SectionsPagerAdapter(FragmentManager fragmentManager, ArrayList<Partner> partners) {
            super(fragmentManager);
            this.partners = partners;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position){
                case 0:
                    return PartnersListFragment.newInstance(partners);

                case 1:
                    return new PartnersMapFragment();
                default:
                    return null;
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title;
            if (position == 0) {
                title = "Рядом";
            } else {
                title = "На карте";
            }
            return title;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

}
