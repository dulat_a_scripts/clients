package com.zerotoonelabs.bigroup.fragments.club;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.authui.SingleFragmentActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class PartnersFragment extends Fragment {

    private View view;
    private LinearLayout allPartnersView;
    private LinearLayout foodCategory;
    private LinearLayout restCategory;
    private LinearLayout furnitureCategory;
    private LinearLayout productsCategory;
    private LinearLayout servicesCategory;
    private LinearLayout healthCategory;

    public PartnersFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_partners, container, false);
        allPartnersView = view.findViewById(R.id.goToAllPartners);
        foodCategory = view.findViewById(R.id.foodCategory);
        restCategory = view.findViewById(R.id.restCategory);
        furnitureCategory = view.findViewById(R.id.furnitureCategory);
        productsCategory = view.findViewById(R.id.productsCategory);
        servicesCategory = view.findViewById(R.id.servicesCategory);
        healthCategory = view.findViewById(R.id.healthCategory);

        allPartnersView.setOnClickListener(v -> openAllCategories());
        foodCategory.setOnClickListener(v -> openCategory(AllPartnersFragment.FOOD));
        restCategory.setOnClickListener(v -> openCategory(AllPartnersFragment.REST));
        servicesCategory.setOnClickListener(v -> openCategory(AllPartnersFragment.SERVICES));
        productsCategory.setOnClickListener(v -> openCategory(AllPartnersFragment.GOODS));
        healthCategory.setOnClickListener(v -> openCategory(AllPartnersFragment.HEALTH));
        furnitureCategory.setOnClickListener(v -> openCategory(AllPartnersFragment.FURNITURE));

        updateUI();
        return view;
    }

    private void openAllCategories(){
        Intent intent = new Intent(getContext(), SingleFragmentActivity.class);
        intent.putExtra(SingleFragmentActivity.ALL_PARTNERS, true);
        intent.putExtra(AllPartnersFragment.TYPE, AllPartnersFragment.ALL);
        startActivity(intent);
    }

    private void openCategory(String type){
        Intent intent = new Intent(getContext(), SingleFragmentActivity.class);
        intent.putExtra(SingleFragmentActivity.ALL_PARTNERS, true);
        intent.putExtra(AllPartnersFragment.TYPE, type);
        startActivity(intent);
    }

    private void updateUI(){}
}
