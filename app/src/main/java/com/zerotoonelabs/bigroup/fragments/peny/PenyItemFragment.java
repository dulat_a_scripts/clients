package com.zerotoonelabs.bigroup.fragments.peny;

import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.adapters.RequrentPaymentsAdapter;
import com.zerotoonelabs.bigroup.api.RestClient;
import com.zerotoonelabs.bigroup.entity.CPModel;
import com.zerotoonelabs.bigroup.entity.PaymentsFineElement;
import com.zerotoonelabs.bigroup.entity.PenaltyRequest;
import com.zerotoonelabs.bigroup.entity.PenaltyResponse;
import com.zerotoonelabs.bigroup.entity.RequrentPayment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.zerotoonelabs.bigroup.fragments.peny.PayPenyFragment.CONTRACT_KEY;
import static com.zerotoonelabs.bigroup.fragments.peny.PayPenyFragment.PROPERTY_GUID_KEY;
import static com.zerotoonelabs.bigroup.fragments.peny.PayPenyFragment.TOKEN_KEY;


public class PenyItemFragment extends Fragment {

    private String token;
    private String contract;
    private String contractNumber;
    private String contractGUID;
    private String propertyGUID;
    private boolean isPodpiski;

    private RecyclerView list;
    private TextView noData;

    public PenyItemFragment() {}

    public static PenyItemFragment newInstance2(String token, String contractId, String param3) {
        PenyItemFragment fragment = new PenyItemFragment();
        Bundle args = new Bundle();
        args.putString(TOKEN_KEY, token);
        args.putString(CONTRACT_KEY, contractId);
        args.putString(PROPERTY_GUID_KEY, param3);
        args.putBoolean("PODPISKI", true);

        fragment.setArguments(args);
        return fragment;
    }

    public static PenyItemFragment newInstance(String token, String contractId, String param3) {
        PenyItemFragment fragment = new PenyItemFragment();
        Bundle args = new Bundle();
        args.putString(TOKEN_KEY, token);
        args.putString(CONTRACT_KEY, contractId);
        args.putString(PROPERTY_GUID_KEY, param3);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        token = getArguments().getString(TOKEN_KEY);
        contract = getArguments().getString(CONTRACT_KEY);
        propertyGUID = getArguments().getString(PROPERTY_GUID_KEY);
        contractGUID = PreferenceManager.getDefaultSharedPreferences(getContext()).getString("contracterGUID", "null");
        contractNumber = PreferenceManager.getDefaultSharedPreferences(getContext()).getString("contractNumber", "null");
        isPodpiski = getArguments().getBoolean("PODPISKI", false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_list, container, false);



        noData = view.findViewById(R.id.noData);
        list = view.findViewById(R.id.list);
        list.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!isPodpiski)
            loadData();
        else
            getRequrentPayments();
    }

    private void getRequrentPayments(){
        String accountId = PreferenceManager.getDefaultSharedPreferences(getContext()).getString("accountId", "null");

        RestClient.getRestProvider2().getAutoPays(accountId).enqueue(new Callback<CPModel>() {
            @Override
            public void onResponse(Call<CPModel> call, Response<CPModel> response) {
                try {
                    ArrayList<RequrentPayment> payments = new ArrayList<>();
                    for (int i = 0; i < response.body().getModel().length; i++){
                        if (response.body().getModel()[i].getStatus().toLowerCase().contains("active"))
                            payments.add(response.body().getModel()[i]);
                    }
                    list.setAdapter(new RequrentPaymentsAdapter(payments));
                    if(!(payments.size() >0)){
                        list.setVisibility(View.GONE);
                        noData.setVisibility(View.VISIBLE);
                        noData.setText("Нет действующих подписок");
                    }
                } catch (NullPointerException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<CPModel> call, Throwable t) {}
        });

    }

    private void loadData(){
        PenaltyRequest request = new PenaltyRequest();
        request.setContracterGuid(contractGUID);
        request.setContractNumber(contractNumber);
        request.setPropertyId(propertyGUID);
        RestClient
                .getRestProvider()
                .loadPenalty(token, request)
                .enqueue(new Callback<PenaltyResponse>() {
                    @Override
                    public void onResponse(Call<PenaltyResponse> call, Response<PenaltyResponse> response) {
                        try {
                            ArrayList<PaymentsFineElement> elements = new ArrayList<>();
                            elements.addAll(Arrays.asList(response.body().getPaymentsfine()));
                            list.setAdapter(new MyItemRecyclerViewAdapter(elements));

                            if(!(elements.size() >0)){
                                list.setVisibility(View.GONE);
                                noData.setVisibility(View.VISIBLE);
                                noData.setText("Список пуст");
                            }

                        } catch (NullPointerException e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<PenaltyResponse> call, Throwable t) {}
                });
    }
}
