package com.zerotoonelabs.bigroup;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ApartmentSchemeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apartment_scheme);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
