package com.zerotoonelabs.bigroup.di;

import com.zerotoonelabs.bigroup.authui.home.FlatInfoFragment;
import com.zerotoonelabs.bigroup.authui.home.FlatStatusFragment;
import com.zerotoonelabs.bigroup.ui.visitobject.VisitObjectFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class HomeFragmentBuildersModule {

    @ContributesAndroidInjector
    abstract FlatInfoFragment contributeFlatInfoFragment();

    @ContributesAndroidInjector
    abstract FlatStatusFragment contributeFlatStatusFragment();
}
