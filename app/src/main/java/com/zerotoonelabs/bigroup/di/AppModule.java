/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zerotoonelabs.bigroup.di;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.google.gson.Gson;
import com.zerotoonelabs.bigroup.api.Kassa24Service;
import com.zerotoonelabs.bigroup.api.SalesWebservice;
import com.zerotoonelabs.bigroup.api.Webservice;
import com.zerotoonelabs.bigroup.db.ApartmentDao;
import com.zerotoonelabs.bigroup.db.BiDb;
import com.zerotoonelabs.bigroup.db.BlockVisitDao;
import com.zerotoonelabs.bigroup.db.FlatDao;
import com.zerotoonelabs.bigroup.db.NewsDao;
import com.zerotoonelabs.bigroup.db.RealEstateDao;
import com.zerotoonelabs.bigroup.db.SalesDao;
import com.zerotoonelabs.bigroup.db.ServiceRequestDao;
import com.zerotoonelabs.bigroup.util.LiveDataCallAdapterFactory;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

@Module(includes = ViewModelModule.class)
class AppModule {

    @Singleton
    @Provides
    Kassa24Service provideKassa24Service() {
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        /*clientBuilder.addNetworkInterceptor(chain -> {
            Request.Builder requestBuilder = chain.request().newBuilder();
            requestBuilder.header("Content-Type", "application/json");
            return chain.proceed(requestBuilder.build());
        });*/
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        clientBuilder.addInterceptor(loggingInterceptor);

        // increase timeout for slow services
        clientBuilder.connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS);

        return new Retrofit.Builder()
                .baseUrl("https://onepay.kassa24.kz/")
                .client(clientBuilder.build())
                .addConverterFactory(GsonConverterFactory.create())
//                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(new LiveDataCallAdapterFactory())
                .build()
                .create(Kassa24Service.class);
    }

    @Singleton
    @Provides
    Webservice provideWebservice() {
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        /*clientBuilder.addNetworkInterceptor(chain -> {
            Request.Builder requestBuilder = chain.request().newBuilder();
            requestBuilder.header("Content-Type", "application/json");
            return chain.proceed(requestBuilder.build());
        });*/
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        clientBuilder.addInterceptor(loggingInterceptor);

        // increase timeout for slow services
        clientBuilder.connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS);

        return new Retrofit.Builder()
                .baseUrl("http://webapi.bi-group.org/")
                .client(clientBuilder.build())
                .addConverterFactory(GsonConverterFactory.create())
//                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(new LiveDataCallAdapterFactory())
                .build()
                .create(Webservice.class);
    }


    @Singleton
    @Provides
    SalesWebservice provideSalesWebservice() {
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        clientBuilder.addInterceptor(loggingInterceptor);

        // increase timeout for slow services
        clientBuilder.connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS);

        return new Retrofit.Builder()
                .baseUrl("http://sales.bi-group.org/api/v1/mobile/")
                .client(clientBuilder.build())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(new LiveDataCallAdapterFactory())
                .build()
                .create(SalesWebservice.class);
    }

    @Singleton
    @Provides
    BiDb provideDb(Application app) {
        return Room.databaseBuilder(app, BiDb.class, "bigroup.db")
                .fallbackToDestructiveMigration()
                .build();
    }

    @Singleton
    @Provides
    ServiceRequestDao provideUserDao(BiDb db) {
        return db.applicationDao();
    }

    @Singleton
    @Provides
    SalesDao provideSalesDao(BiDb db) {
        return db.salesDao();
    }

    @Singleton
    @Provides
    RealEstateDao provideRealEstateDao(BiDb db) {
        return db.realEstateDao();
    }

    @Singleton
    @Provides
    NewsDao provideNewsDao(BiDb db) {
        return db.newsDao();
    }

    @Singleton
    @Provides
    ApartmentDao provideApartmentDao(BiDb db) {
        return db.apartmentDao();
    }

    @Singleton
    @Provides
    BlockVisitDao provideBlockVisitDao(BiDb db) {
        return db.blockVisitDao();
    }

    @Singleton
    @Provides
    FlatDao provideFlatDao(BiDb db) {
        return db.flatDao();
    }

    @Singleton
    @Provides
    Gson provideGson() {
        return new Gson();
    }
}