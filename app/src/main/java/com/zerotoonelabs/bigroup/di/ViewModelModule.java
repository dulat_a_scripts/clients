package com.zerotoonelabs.bigroup.di;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.zerotoonelabs.bigroup.authui.HomeViewModel;
import com.zerotoonelabs.bigroup.authui.home.FlatViewModel;
import com.zerotoonelabs.bigroup.authui.payment.PaymentViewModel;
import com.zerotoonelabs.bigroup.authui.payment.utilities.UtilitiesViewModel;
import com.zerotoonelabs.bigroup.authui.paymentschedule.PaymentScheduleViewModel;
import com.zerotoonelabs.bigroup.authui.paymentscheduledetails.PaymentSchedulesViewModel;
import com.zerotoonelabs.bigroup.authui.pdfviewer.PdfViewerViewModel;
import com.zerotoonelabs.bigroup.authui.salesdepartment.SalesViewModel;
import com.zerotoonelabs.bigroup.ui.MainViewModel;
import com.zerotoonelabs.bigroup.ui.apartments.ApartmentViewModel;
import com.zerotoonelabs.bigroup.ui.biservice.BiServiceViewModel;
import com.zerotoonelabs.bigroup.ui.favourites.FavoritesViewModel;
import com.zerotoonelabs.bigroup.ui.news.NewsViewModel;
import com.zerotoonelabs.bigroup.ui.opendoor.OpenDoorViewModel;
import com.zerotoonelabs.bigroup.ui.realestate.RealEstateViewModel;
import com.zerotoonelabs.bigroup.ui.realestatedetail.RealEstateDetailViewModel;
import com.zerotoonelabs.bigroup.ui.sales.StockViewModel;
import com.zerotoonelabs.bigroup.ui.sendapplication.SendServiceRequestViewModel;
import com.zerotoonelabs.bigroup.ui.sendrequest.SendRequestViewModel;
import com.zerotoonelabs.bigroup.ui.visitobject.VisitObjViewModel;
import com.zerotoonelabs.bigroup.viewmodel.BiViewModelFactory;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel.class)
    abstract ViewModel bindMainViewModel(MainViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(StockViewModel.class)
    abstract ViewModel bindStockViewModel(StockViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SendServiceRequestViewModel.class)
    abstract ViewModel bindSendApplicationViewModel(SendServiceRequestViewModel userViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(NewsViewModel.class)
    abstract ViewModel bindNewsViewModel(NewsViewModel userViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(RealEstateViewModel.class)
    abstract ViewModel bindObjectViewModel(RealEstateViewModel userViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(BiServiceViewModel.class)
    abstract ViewModel bindBiServiceViewModel(BiServiceViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(RealEstateDetailViewModel.class)
    abstract ViewModel bindRealEstateDetailViewModel(RealEstateDetailViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(VisitObjViewModel.class)
    abstract ViewModel bindVisitObjViewModel(VisitObjViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ApartmentViewModel.class)
    abstract ViewModel bindApartmentViewModel(ApartmentViewModel viewModel);


    @Binds
    @IntoMap
    @ViewModelKey(OpenDoorViewModel.class)
    abstract ViewModel bindOpenDoorViewModel(OpenDoorViewModel userViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SendRequestViewModel.class)
    abstract ViewModel bindSendRequestViewModel(SendRequestViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(FlatViewModel.class)
    abstract ViewModel bindFlatViewModel(FlatViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel.class)
    abstract ViewModel bindHomeViewModel(HomeViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PaymentViewModel.class)
    abstract ViewModel bindPaymentViewModel(PaymentViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(UtilitiesViewModel.class)
    abstract ViewModel bindUtilitiesViewModel(UtilitiesViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PdfViewerViewModel.class)
    abstract ViewModel bindPdfViewerViewModel(PdfViewerViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PaymentScheduleViewModel.class)
    abstract ViewModel bindPaymentScheduleViewModel(PaymentScheduleViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PaymentSchedulesViewModel.class)
    abstract ViewModel bindPaymentSchedulesViewModel(PaymentSchedulesViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SalesViewModel.class)
    abstract ViewModel bindSalesViewModel(SalesViewModel viewModel);


    @Binds
    @IntoMap
    @ViewModelKey(FavoritesViewModel.class)
    abstract ViewModel bindFavoritesViewModel(FavoritesViewModel viewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(BiViewModelFactory factory);
}
