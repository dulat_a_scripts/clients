package com.zerotoonelabs.bigroup.di;

import com.zerotoonelabs.bigroup.authui.biserviceauthoruzed.ActiveApplicationsFragment;
import com.zerotoonelabs.bigroup.authui.biserviceauthoruzed.ApplicationsHistoryFragment;
import com.zerotoonelabs.bigroup.authui.biserviceauthoruzed.AuthorizedBiServiceFragment;
import com.zerotoonelabs.bigroup.authui.biserviceauthoruzed.CallServiceFragment;
import com.zerotoonelabs.bigroup.authui.payment.PaymentFragment;
import com.zerotoonelabs.bigroup.authui.payment.PaymentHistoryFragment;
import com.zerotoonelabs.bigroup.authui.payment.PublicUtilitiesFragment;
import com.zerotoonelabs.bigroup.authui.payment.QuickPayFragment;
import com.zerotoonelabs.bigroup.authui.payment.utilities.UtilitiesFragment;
import com.zerotoonelabs.bigroup.authui.salesdepartment.SalesRequestsHistory;
import com.zerotoonelabs.bigroup.authui.salesdepartment.SendSalesRequestFragment;
import com.zerotoonelabs.bigroup.ui.apartments.ApartmentDetailFragment;
import com.zerotoonelabs.bigroup.ui.apartments.ApartmentFilterFragment;
import com.zerotoonelabs.bigroup.ui.apartments.ApartmentFragment;
import com.zerotoonelabs.bigroup.ui.favourites.FavoriteApartmentsFragment;
import com.zerotoonelabs.bigroup.ui.profile.ProfileFragment;
import com.zerotoonelabs.bigroup.ui.realestate.RealEstateFilterFragment;
import com.zerotoonelabs.bigroup.ui.realestate.RealEstateFragment;
import com.zerotoonelabs.bigroup.ui.realestatedetail.RealEstateDetailFragment;
import com.zerotoonelabs.bigroup.ui.sales.StocksFragment;
import com.zerotoonelabs.bigroup.ui.sendrequest.SendRequestFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class SingleFragmentBuildersModule {

    @ContributesAndroidInjector
    abstract PaymentFragment contributePaymentFragment();

    @ContributesAndroidInjector
    abstract PublicUtilitiesFragment contributePublicUtilitiesFragment();

    @ContributesAndroidInjector
    abstract QuickPayFragment contributeQuickPayFragment();

    @ContributesAndroidInjector
    abstract PaymentHistoryFragment contributePaymentHistoryFragment();

    @ContributesAndroidInjector
    abstract UtilitiesFragment contributeUtilitiesFragment();

    @ContributesAndroidInjector
    abstract ActiveApplicationsFragment contributeActiveApplicationsFragment();

    @ContributesAndroidInjector
    abstract ApplicationsHistoryFragment contributeApplicationsHistoryFragment();

    @ContributesAndroidInjector
    abstract AuthorizedBiServiceFragment contributeAuthorizedBiServiceFragment();

    @ContributesAndroidInjector
    abstract CallServiceFragment contributeCallServiceFragment();

    @ContributesAndroidInjector
    abstract SendSalesRequestFragment contributeSendSalesRequestFragment();

    @ContributesAndroidInjector
    abstract SendRequestFragment contributeSendRequestFragment();

    @ContributesAndroidInjector
    abstract StocksFragment contributeStocksFragment();

    @ContributesAndroidInjector
    abstract SalesRequestsHistory contributeSalesDepartmentHistoryFragment();

    @ContributesAndroidInjector
    abstract ApartmentFragment contributeApartmentFragment();

    @ContributesAndroidInjector
    abstract ApartmentDetailFragment contributeApartmentDetailFragment();

    @ContributesAndroidInjector
    abstract ApartmentFilterFragment contributeApartmentFilterFragment();

    @ContributesAndroidInjector
    abstract RealEstateFragment contributeRealEstateFragment();

    @ContributesAndroidInjector
    abstract RealEstateDetailFragment contributeRealEstateDetailFragment();

    @ContributesAndroidInjector
    abstract RealEstateFilterFragment contributeRealEstateFilterFragment();

    @ContributesAndroidInjector
    abstract ProfileFragment contributeProfileFragment();

    @ContributesAndroidInjector
    abstract FavoriteApartmentsFragment contributeFavoriteApartmentsFragment();
}
