package com.zerotoonelabs.bigroup.di;

import com.zerotoonelabs.bigroup.authui.paymentschedule.PaymentScheduleFragment;
import com.zerotoonelabs.bigroup.authui.paymentscheduledetails.PaymentSchedule1Fragment;
import com.zerotoonelabs.bigroup.authui.paymentscheduledetails.PaymentSchedule2Fragment;
import com.zerotoonelabs.bigroup.authui.pdfviewer.PdfViewerFragment;
import com.zerotoonelabs.bigroup.ui.visitobject.VisitObjectFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class SingleHomeFragmentBuildersModule {

    @ContributesAndroidInjector
    abstract PdfViewerFragment contributePdfViewerFragment();

    @ContributesAndroidInjector
    abstract VisitObjectFragment contributeVisitObjectFragment();

    @ContributesAndroidInjector
    abstract PaymentScheduleFragment contributePaymentScheduleFragment();

    @ContributesAndroidInjector
    abstract PaymentSchedule1Fragment contributePaymentSchedule1Fragment();

    @ContributesAndroidInjector
    abstract PaymentSchedule2Fragment contributePaymentSchedule2Fragment();
}
