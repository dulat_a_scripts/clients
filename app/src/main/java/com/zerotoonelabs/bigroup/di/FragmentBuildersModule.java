/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zerotoonelabs.bigroup.di;

import com.zerotoonelabs.bigroup.fragments.PartPaymentFragment;
import com.zerotoonelabs.bigroup.ui.apartments.ApartmentDetailFragment;
import com.zerotoonelabs.bigroup.ui.apartments.ApartmentFilterFragment;
import com.zerotoonelabs.bigroup.ui.apartments.ApartmentFragment;
import com.zerotoonelabs.bigroup.ui.biservice.BiServiceFragment;
import com.zerotoonelabs.bigroup.ui.favourites.FavoriteApartmentsFragment;
import com.zerotoonelabs.bigroup.ui.news.NewsFragment;
import com.zerotoonelabs.bigroup.ui.opendoor.OpenDoorFragment;
import com.zerotoonelabs.bigroup.ui.realestate.RealEstateFilterFragment;
import com.zerotoonelabs.bigroup.ui.realestate.RealEstateFragment;
import com.zerotoonelabs.bigroup.ui.realestatedetail.RealEstateDetailFragment;
import com.zerotoonelabs.bigroup.ui.sales.StocksFragment;
import com.zerotoonelabs.bigroup.ui.sendapplication.SendServiceRequestFragment;
import com.zerotoonelabs.bigroup.ui.sendrequest.SendRequestFragment;
import com.zerotoonelabs.bigroup.ui.visitobject.VisitObjectFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;


@Module
public abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract SendServiceRequestFragment contributeSendApplicationFragment();

    @ContributesAndroidInjector
    abstract StocksFragment contributeStocksFragment();

    @ContributesAndroidInjector
    abstract RealEstateFragment contributeRealEstateFragment();

    @ContributesAndroidInjector
    abstract BiServiceFragment contributeBiServiceFragment();

    @ContributesAndroidInjector
    abstract RealEstateDetailFragment contributeRealEstateDetailFragment();

    @ContributesAndroidInjector
    abstract RealEstateFilterFragment contributeRealEstateFilterFragment();

    @ContributesAndroidInjector
    abstract VisitObjectFragment contributeVisitObjectFragment();

    @ContributesAndroidInjector
    abstract SendRequestFragment contributeSendRequestFragment();

    @ContributesAndroidInjector
    abstract NewsFragment contributeNewsFragment();

    @ContributesAndroidInjector
    abstract ApartmentFragment contributeApartmentFragment();

    @ContributesAndroidInjector
    abstract OpenDoorFragment contributeOpenDoorFragmentt();

    @ContributesAndroidInjector
    abstract PartPaymentFragment contributePartPaymentFragment();

    @ContributesAndroidInjector
    abstract ApartmentFilterFragment contributeApartmentFilterFragment();

    @ContributesAndroidInjector
    abstract ApartmentDetailFragment contributeApartmentDetailFragment();

    @ContributesAndroidInjector
    abstract FavoriteApartmentsFragment contributeFavoriteApartmentsFragment();
}
