/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zerotoonelabs.bigroup.di;

import com.zerotoonelabs.bigroup.authui.AuthorizedActivity;
import com.zerotoonelabs.bigroup.authui.HomeActivity;
import com.zerotoonelabs.bigroup.authui.SingleFragmentActivity;
import com.zerotoonelabs.bigroup.authui.SingleHomeFragmentActivity;
import com.zerotoonelabs.bigroup.authui.statuses.IntroductoryStatusActivity;
import com.zerotoonelabs.bigroup.authui.statuses.KeyTransferActivity;
import com.zerotoonelabs.bigroup.authui.statuses.StatusGetDocumentsActivity;
import com.zerotoonelabs.bigroup.ui.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {
    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract MainActivity contributeMainActivity();

    @ContributesAndroidInjector(modules = {SingleFragmentBuildersModule.class})
    abstract SingleFragmentActivity contributeSingleFragmentActivity();

    @ContributesAndroidInjector(modules = HomeFragmentBuildersModule.class)
    abstract HomeActivity contributeHomeActivity();

    @ContributesAndroidInjector(modules = SingleHomeFragmentBuildersModule.class)
    abstract SingleHomeFragmentActivity contributeSingleHomeFragmentActivity();

    @ContributesAndroidInjector
    abstract KeyTransferActivity contributeKeyTransferActivity();

    @ContributesAndroidInjector
    abstract StatusGetDocumentsActivity contributeStatusGetDocumentsActivity();


    @ContributesAndroidInjector
    abstract IntroductoryStatusActivity contributeIntroductoryStatusActivity();

    @ContributesAndroidInjector(modules = HomeFragmentBuildersModule.class)
    abstract AuthorizedActivity contributeAuthorizedActivity();
}
