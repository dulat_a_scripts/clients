package com.zerotoonelabs.bigroup.ui.common;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.RealestateItemBinding;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.RealEstate;

/**
 * A RecyclerView adapter for {@link RealEstate} class.
 */
public class RealEstateListAdapter extends DataBoundListAdapter<RealEstate, RealestateItemBinding> {
    private final RealEstateClickCallback serviceRequestClickCallback;
    private final String token;

    public RealEstateListAdapter(RealEstateClickCallback serviceRequestClickCallback, String token) {
        this.serviceRequestClickCallback = serviceRequestClickCallback;
        this.token = token;
    }

    @Override
    protected RealestateItemBinding createBinding(ViewGroup parent) {
        RealestateItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.realestate_item,
                        parent, false);
        binding.textView9.setOnClickListener(view -> {
            RealEstate repo = binding.getRealEstate();
            if (repo != null && serviceRequestClickCallback != null) {
                serviceRequestClickCallback.onAddressClick(repo.getLatitude(), repo.getLongitude(), repo.getName());
            }
        });
        binding.getRoot().setOnClickListener(v -> {
            RealEstate repo = binding.getRealEstate();
            if (repo != null && serviceRequestClickCallback != null) {
                serviceRequestClickCallback.onClick(repo);
            }
        });

        binding.textView11.setOnClickListener(v -> {
            RealEstate repo = binding.getRealEstate();
            if (repo != null && serviceRequestClickCallback != null) {
                serviceRequestClickCallback.onApartmentClick(repo);
            }
        });

        binding.textView12.setOnClickListener(v -> {
            RealEstate repo = binding.getRealEstate();
            if (repo != null && serviceRequestClickCallback != null) {
                serviceRequestClickCallback.onApartmentClick(repo);
            }
        });

        binding.apartmentCount.setOnClickListener(v -> {
            RealEstate repo = binding.getRealEstate();
            if (repo != null && serviceRequestClickCallback != null) {
                String id = repo.getId();
                serviceRequestClickCallback.onApartmentCountClick(id);
            }
        });

        return binding;
    }

    @Override
    protected void bind(RealestateItemBinding binding, RealEstate item, int position) {
        binding.setToken(token);
        binding.setRealEstate(item);
    }

    @Override
    protected boolean areItemsTheSame(RealEstate oldItem, RealEstate newItem) {
        return Objects.equals(oldItem.getId(), newItem.getId());
    }

    @Override
    protected boolean areContentsTheSame(RealEstate oldItem, RealEstate newItem) {
        return Objects.equals(oldItem.getName(), newItem.getName());
    }

    public interface RealEstateClickCallback {
        void onClick(RealEstate complaint);
        void onApartmentClick(RealEstate realEstate);
        void onApartmentCountClick(String id);
        void onAddressClick(double lat, double lon, String label);
    }
}