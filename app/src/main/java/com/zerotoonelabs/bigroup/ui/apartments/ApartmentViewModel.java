package com.zerotoonelabs.bigroup.ui.apartments;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.arch.paging.PagedList;
import android.text.TextUtils;

import com.zerotoonelabs.bigroup.repository.BiRepository;
import com.zerotoonelabs.bigroup.util.AbsentLiveData;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.ApartmentFilter;
import com.zerotoonelabs.bigroup.entity.Apartments.Apartment;
import com.zerotoonelabs.bigroup.entity.BuildingSite;
import com.zerotoonelabs.bigroup.entity.Resource;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

public class ApartmentViewModel extends ViewModel {
    private final MutableLiveData<ApartmentId> apartmentId = new MutableLiveData<>();
    private final MutableLiveData<ApartmentId> detailedApartmentId = new MutableLiveData<>();
    private final MutableLiveData<String> tokenId = new MutableLiveData<>();
    private final MutableLiveData<FilterId> filterId = new MutableLiveData<>();
    private final MutableLiveData<FilterId2> filterId2 = new MutableLiveData<>();
    private final MutableLiveData<String> apartId = new MutableLiveData<>();
    private final MutableLiveData<Apartment> apartmentToInsert = new MutableLiveData<>();
    private final MutableLiveData<Apartment> apartmentToRemove = new MutableLiveData<>();

    private final LiveData<PagedList<Apartment>> apartments;
    private final LiveData<PagedList<Apartment>> detailedApartments;
    private final LiveData<Resource<List<String>>> cities;
    private final LiveData<Resource<List<BuildingSite>>> list1;
    private final LiveData<Resource<List<BuildingSite>>> list2;
    private final LiveData<Apartment> apartment;
    private final LiveData<Resource<Long>> insertedApartment;
    private final LiveData<Resource<Integer>> removedApartment;
    private boolean isFavorite;
    private ApartmentFilter apartmentFilter;

    private String token;

    @Inject
    ApartmentViewModel(BiRepository repository) {
        detailedApartments = Transformations.switchMap(detailedApartmentId, input -> {
            if (input.isEmpty()) {
                return AbsentLiveData.create();
            }
            return repository.loadApartments5(input.token, input.queries);
        });

        apartments = Transformations.switchMap(apartmentId, input -> {
            if (input.isEmpty()) {
                return AbsentLiveData.create();
            }
            return repository.loadApartments5(input.token, input.queries);
        });

        cities = Transformations.switchMap(tokenId, input -> {
            if (TextUtils.isEmpty(input)) {
                return AbsentLiveData.create();
            }
            return repository.loadCities(input);
        });

        list1 = Transformations.switchMap(filterId, input -> {
            if (input.isEmpty()) {
                return AbsentLiveData.create();
            }
            return repository.loadJks(input.token, input.city);
        });

        list2 = Transformations.switchMap(filterId2, input -> {
            if (input.isEmpty()) {
                return AbsentLiveData.create();
            }
            return repository.loadBlocks(input.token, input.blockId);
        });

        apartment = Transformations.switchMap(apartId, input -> {
            if (TextUtils.isEmpty(input)) return AbsentLiveData.create();
            return repository.loadApartment(input);
        });

        insertedApartment = Transformations.switchMap(apartmentToInsert, input -> {
            if (input == null || input.getId() == null) return AbsentLiveData.create();
            return repository.insertApartment(input);
        });

        removedApartment = Transformations.switchMap(apartmentToRemove, input -> {
            if (input == null || input.getId() == null) return AbsentLiveData.create();
            return repository.removeApartment(input, -1);
        });
    }

    void setApartmentToInsert(Apartment update) {
        if (apartment == null || Objects.equals(apartmentToInsert.getValue(), update)) return;
        apartmentToInsert.setValue(update);
    }

    void setApartmentToRemove(Apartment update) {
        if (apartment == null || Objects.equals(apartmentToRemove.getValue(), update)) return;
        apartmentToRemove.setValue(update);
    }

    boolean isFavorite() {
        return isFavorite;
    }

    void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    void setApartId(String input) {
        apartId.setValue(input);
    }

    void setTokenId() {
        String token = this.token;
        if (Objects.equals(tokenId.getValue(), token)) {
            return;
        }
        tokenId.setValue(token);
    }

    void setFilterId(String city) {
        String token = this.token;
        FilterId update = new FilterId(token, city);
        if (Objects.equals(filterId.getValue(), update)) {
            return;
        }
        filterId.setValue(update);
    }

    void setFilterId2(String jkId) {
        String token = this.token;
        FilterId2 update = new FilterId2(token, jkId);
        if (Objects.equals(filterId2.getValue(), update)) {
            return;
        }
        filterId2.setValue(update);
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public LiveData<PagedList<Apartment>> getApartments() {//apartmentFragment
        return apartments;
    }

    public void retry() {
        ApartmentId current = apartmentId.getValue();
        if (current != null && !current.isEmpty()) {
            apartmentId.setValue(current);
        }
    }

    void setApartmentId(HashMap<String, String> filter) {
        String token = this.token;
        ApartmentId update = new ApartmentId(token, filter);
        if (Objects.equals(apartmentId.getValue(), update)) {
            return;
        }
        apartmentId.setValue(update);
    }

    void setDetailedApartmentId(HashMap<String, String> filter) {
        String token = this.token;
        ApartmentId update = new ApartmentId(token, filter);
        if (Objects.equals(detailedApartmentId.getValue(), update)) {
            return;
        }
        detailedApartmentId.setValue(update);
    }

    LiveData<Resource<List<String>>> getCities() {
        return cities;
    }

    LiveData<Resource<List<BuildingSite>>> getList1() {
        return list1;
    }

    LiveData<Resource<List<BuildingSite>>> getList2() {
        return list2;
    }

    LiveData<PagedList<Apartment>> getDetailedApartments() {
        return detailedApartments;
    }

    public LiveData<Apartment> getApartment() {
        return apartment;
    }

    void setApartmentFilter(ApartmentFilter apartmentFilter) {
        this.apartmentFilter = apartmentFilter;
    }

    ApartmentFilter getApartmentFilter() {
        return apartmentFilter;
    }

    public LiveData<Resource<Long>> getInsertedApartment() {
        return insertedApartment;
    }

    public LiveData<Resource<Integer>> getRemovedApartment() {
        return removedApartment;
    }

    static class FilterId {
        String token;
        String city;

        public FilterId(String token, String city) {
            this.token = token;
            this.city = city;
        }

        boolean isEmpty() {
            return token == null || city == null;
        }
    }

    static class FilterId2 {
        String token;
        String blockId;

        public FilterId2(String token, String blockId) {
            this.token = token;
            this.blockId = blockId;
        }

        boolean isEmpty() {
            return token == null || blockId == null;
        }
    }

    static class ApartmentId {
        HashMap<String, String> queries;
        String token;

        ApartmentId(String token, HashMap<String, String> queries) {
            this.token = token;
            this.queries = queries;
        }

        boolean isEmpty() {
            return token == null || token.length() == 0;
        }

    }
}
