package com.zerotoonelabs.bigroup.ui.visitobject;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.VisitObjectFragBinding;
import com.zerotoonelabs.bigroup.di.Injectable;
import com.zerotoonelabs.bigroup.util.AutoClearedValue;
import com.zerotoonelabs.bigroup.entity.BsDate;
import com.zerotoonelabs.bigroup.entity.BsTime;
import com.zerotoonelabs.bigroup.entity.BuildingSite;
import com.zerotoonelabs.bigroup.entity.Resource;
import com.zerotoonelabs.bigroup.entity.VisitNumber;

import javax.inject.Inject;

public class VisitObjectFragment extends Fragment implements Injectable {

    private static final String TOKEN_KEY = "token";
    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private VisitObjViewModel mViewModel;

    AutoClearedValue<VisitObjectFragBinding> binding;
    AutoClearedValue<ArrayAdapter<BuildingSite>> adapter1;
    AutoClearedValue<ArrayAdapter<VisitNumber>> adapter4;
    AutoClearedValue<ArrayAdapter<String>> adapter2;
    AutoClearedValue<ArrayAdapter<String>> adapter3;
    private Spinner spinner1;
    private Spinner spinner4;
    private Spinner spinner2;
    private Spinner spinner3;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(VisitObjViewModel.class);

        Bundle args = getArguments();
        if (args != null && args.containsKey(TOKEN_KEY)) {
            String token = args.getString(TOKEN_KEY);
            mViewModel.setToken(token);
        } else {
            mViewModel.setToken(null);
        }

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String realEstateId = preferences.getString("realEstateGuid", null);
        String propertyId = preferences.getString("propertyGuid", null);
        String fullName = preferences.getString("fullName", null);
        String phone = preferences.getString("phone", null);
        boolean auth = realEstateId != null && propertyId != null;

        mViewModel.setRealEstateId(realEstateId);
        mViewModel.setPropertyId(propertyId);

        mViewModel.setBsId(realEstateId);

        binding.get().setAuth(auth);
        binding.get().setFullName(fullName);
        binding.get().setPhone(phone);

        setupAdapters();
        initObservables(mViewModel);
        initClickListeners(mViewModel);
    }

    private void setupAdapters() {
        ArrayAdapter<VisitNumber> adapter4 = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item);
        adapter4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.adapter4 = new AutoClearedValue<>(this, adapter4);
        spinner4.setAdapter(adapter4);

        ArrayAdapter<BuildingSite> adapter1 = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.adapter1 = new AutoClearedValue<>(this, adapter1);
        spinner1.setAdapter(adapter1);

        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.adapter2 = new AutoClearedValue<>(this, adapter2);
        spinner2.setAdapter(adapter2);

        ArrayAdapter<String> adapter3 = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item);
        adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.adapter3 = new AutoClearedValue<>(this, adapter3);
        spinner3.setAdapter(adapter3);
    }

    private void initClickListeners(VisitObjViewModel viewModel) {
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                BuildingSite item = (BuildingSite) parent.getItemAtPosition(position);
                viewModel.setVisitNumberId(item.id);
                viewModel.setBsId(item.id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                viewModel.setBsId(null);
            }
        });

        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String date = (String) parent.getItemAtPosition(position);
                viewModel.setBsId2(date);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
//                mViewModel.setMessage(R.string.cant_visit_object_message);
                viewModel.setBsId2(null);
            }
        });
    }

    private void initObservables(VisitObjViewModel viewModel) {
        viewModel.getVisitNumbers().observe(this, listResource -> {
            if (listResource != null && listResource.data != null) {
                adapter4.get().addAll(listResource.data);
            } else {
                adapter4.get().clear();
            }
            adapter4.get().notifyDataSetChanged();
        });

        viewModel.getBuildingSites().observe(this, listResource -> {
//            binding.get().setResource(listResource);
            if (listResource != null && listResource.data != null) {
                adapter1.get().addAll(listResource.data);
                adapter1.get().notifyDataSetChanged();
            }
        });

        viewModel.getBsDate().observe(this, resource -> {
//            binding.get().setResource(resource);
            handleBsDate(resource, viewModel);
        });

        viewModel.getBsTime().observe(this, resource -> {
//            binding.get().setResource(resource);
            handleBsTime(resource, viewModel);
        });

        viewModel.getMessage().observe(this, integer -> {
            if (integer != null) {
                Toast.makeText(getActivity(), integer, Toast.LENGTH_SHORT).show();
            }
        });

        viewModel.getVisitObj().observe(this, resource -> {
            binding.get().setResource(resource);
            if (resource != null && resource.data != null) {
                showDialog(resource.data);
            }
        });
    }

    private void handleBsTime(Resource<BsTime> resource, VisitObjViewModel viewModel) {
        if (resource == null) return;

        switch (resource.status) {
            case ERROR:
                viewModel.setMessage(R.string.cant_visit_object_message);
                adapter3.get().clear();
                adapter3.get().notifyDataSetChanged();
                break;
            case SUCCESS:
                BsTime bsTime = resource.data;
                adapter3.get().clear();
                adapter3.get().addAll(bsTime.times);
                adapter3.get().notifyDataSetChanged();
                break;
        }
    }

    private void handleBsDate(Resource<BsDate> resource, VisitObjViewModel viewModel) {
        if (resource == null) return;

        switch (resource.status) {
            case ERROR:
                viewModel.setMessage(R.string.cant_visit_object_message);
                adapter2.get().clear();
                adapter2.get().notifyDataSetChanged();
                break;
            case SUCCESS:
                BsDate bsDate = resource.data;
                adapter2.get().clear();
                adapter2.get().addAll(bsDate.dates);
                adapter2.get().notifyDataSetChanged();
                break;
        }
    }

    private void sendButtonPressed(String fullName, String phone) {
        BuildingSite obj = (BuildingSite) spinner1.getSelectedItem();
        String date = (String) spinner2.getSelectedItem();
        String time = (String) spinner3.getSelectedItem();
        VisitNumber visitNumber = (VisitNumber) spinner4.getSelectedItem();
        mViewModel.setVisitId(obj, fullName, phone, date, time, visitNumber);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        VisitObjectFragBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.visit_object_frag, container, false);
        binding = new AutoClearedValue<>(this, dataBinding);
        // Inflate the layout for this fragment

        spinner1 = dataBinding.jkSpinner;
        spinner2 = dataBinding.dateSpinner;
        spinner3 = dataBinding.timeSpinner;
        spinner4 = dataBinding.visitNumberSpinner;

//        dataBinding.phoneTil.setError("8 701 234 5678");

        TextInputEditText fullNameEt = dataBinding.fullName;
        TextInputEditText phoneEt = dataBinding.phone;
        Button button = dataBinding.visitobjButton;

        button.setOnClickListener(view -> {
            dismissKeyboard(view.getWindowToken());
            sendButtonPressed(fullNameEt.getText().toString(), phoneEt.getText().toString());
        });

        dataBinding.setRetryCallback(() -> mViewModel.retry());
        return dataBinding.getRoot();
    }

    private void showDialog(String message) {
        if (!isAdded()) return;

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.visit_obj_success_dialog, null, false);
        new AlertDialog.Builder(getActivity())
                .setView(view)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> cleanViews())
                .create()
                .show();
    }

    private void cleanViews() {
        boolean auth = binding.get().getAuth();
        if (!auth) {
            binding.get().setAuth(false);
            binding.get().setFullName(null);
            binding.get().setPhone(null);
        }
    }

    public static VisitObjectFragment newInstance(String token) {
        VisitObjectFragment fragment = new VisitObjectFragment();
        Bundle args = new Bundle();
        args.putString(TOKEN_KEY, token);
        fragment.setArguments(args);
        return fragment;
    }

    private void dismissKeyboard(IBinder windowToken) {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(windowToken, 0);
        }
    }
}
