package com.zerotoonelabs.bigroup.ui.sales;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.zerotoonelabs.bigroup.repository.SalesRepository;
import com.zerotoonelabs.bigroup.util.AbsentLiveData;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.Resource;
import com.zerotoonelabs.bigroup.entity.Stock;

import java.util.List;

import javax.inject.Inject;

public class StockViewModel extends ViewModel {

    private final MutableLiveData<TabId> tabId = new MutableLiveData<>();
    private final LiveData<Resource<List<Stock>>> stocks;

    @Inject
    public StockViewModel(SalesRepository repository) {

        stocks = Transformations.switchMap(tabId, input -> {
            if (input.isEmpty()) {
                return AbsentLiveData.create();
            }
            return repository.loadStocks(input.lang, input.tab);
        });
    }

    public LiveData<Resource<List<Stock>>> getStocks() {
        return stocks;
    }

    public void retry() {
        TabId current = tabId.getValue();
        if (current != null && !current.isEmpty()) {
            tabId.setValue(current);
        }
    }

    void setTabId(String input, int tab) {
        TabId update = new TabId(input, tab);
        if (Objects.equals(tabId.getValue(), update)) {
            return;
        }
        tabId.setValue(update);
    }

    static class TabId {
        public String lang;
        public int tab;

        public TabId(String lang, int tab) {
            this.lang = lang;
            this.tab = tab;
        }

        boolean isEmpty() {
            return lang == null || lang.length() == 0;
        }
    }
}
