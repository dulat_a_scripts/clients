package com.zerotoonelabs.bigroup.ui.realestatedetail;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.text.TextUtils;

import com.zerotoonelabs.bigroup.repository.BiRepository;
import com.zerotoonelabs.bigroup.util.AbsentLiveData;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.ReBlock;
import com.zerotoonelabs.bigroup.entity.RealEstate;
import com.zerotoonelabs.bigroup.entity.Resource;

import java.util.List;

import javax.inject.Inject;

public class RealEstateDetailViewModel extends ViewModel {
    private final MutableLiveData<ReId> reId = new MutableLiveData<>();
    private final LiveData<Resource<List<ReBlock>>> reBlocks;
    private final MutableLiveData<String> realEstateId = new MutableLiveData<>();
    private final LiveData<RealEstate> realEstate;

    @Inject
    public RealEstateDetailViewModel(BiRepository repository) {
        reBlocks = Transformations.switchMap(reId, input -> {
            if (input.isEmpty()) {
                return AbsentLiveData.create();
            }
            return repository.loadReBlocks(input.id, input.token);
        });

        realEstate = Transformations.switchMap(realEstateId, input -> {
            if (TextUtils.isEmpty(input)) {
                return AbsentLiveData.create();
            }
            return repository.loadRealEstate(input);
        });
    }

    void setRealEstateId(String id) {
        realEstateId.setValue(id);
    }

    void setId(String id, String token) {
        ReId update = new ReId(token, id);
        if (Objects.equals(reId.getValue(), update)) {
            return;
        }
        reId.setValue(update);

    }

    public LiveData<Resource<List<ReBlock>>> getBlocks() {
        return reBlocks;
    }

    public LiveData<RealEstate> getRealEstate() {
        return realEstate;
    }

    static class ReId {
        String token;
        String id;

        public ReId(String token, String id) {
            this.token = token;
            this.id = id;
        }

        boolean isEmpty() {
            return token == null || id == null;
        }
    }
}
