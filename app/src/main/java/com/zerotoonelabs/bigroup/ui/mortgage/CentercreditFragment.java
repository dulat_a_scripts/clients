package com.zerotoonelabs.bigroup.ui.mortgage;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.ui.sendrequest.SendRequestFragment;

public class CentercreditFragment extends Fragment {

    private FragmentManager fragmentManager;

    public CentercreditFragment() {
        // Required empty public constructor
    }

    public static CentercreditFragment newInstance(String param1, String param2) {
        CentercreditFragment fragment = new CentercreditFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentManager = getFragmentManager();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.centercreditbank_frag, container, false);
        ImageView imageView = view.findViewById(R.id.imageView);
        Glide.with(getActivity())
                .load(R.drawable.centercredit_logo)
                .into(imageView);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.button).setOnClickListener(v -> navigateToSendRequest());
    }

    public void navigateToSendRequest() {
        if (fragmentManager == null) return;
        SendRequestFragment fragment = new SendRequestFragment();
        String tag = "Оставить заявку";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(R.id.flContent, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }
}
