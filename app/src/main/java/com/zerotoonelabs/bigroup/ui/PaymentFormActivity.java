package com.zerotoonelabs.bigroup.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.entity.Constants;

import ru.cloudpayments.sdk.PaymentWidget;
import ru.cloudpayments.sdk.business.domain.model.BaseResponse;
import ru.cloudpayments.sdk.business.domain.model.billing.CardsAuthConfirmResponse;
import ru.cloudpayments.sdk.business.domain.model.billing.CardsAuthResponse;
import ru.cloudpayments.sdk.view.PaymentTaskListener;

public class PaymentFormActivity extends AppCompatActivity {

    public static final String AMOUNT_KEY = "amount";
    private TextView resultText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_form_act);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        resultText = findViewById(R.id.textView46);

        TextInputEditText edtAmount = findViewById(R.id.edtAmount);
        TextInputEditText edtCurrency = findViewById(R.id.edtCurrency);
        TextInputEditText edtDescr = findViewById(R.id.edtDescr);
        Button button = findViewById(R.id.payButton);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            int amount = extras.getInt(AMOUNT_KEY);
            edtAmount.setText(String.valueOf(amount));
            button.setOnClickListener(v -> {
                payPressed(String.valueOf(amount), edtCurrency.getText().toString(), edtDescr.getText().toString());
            });
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void payPressed(String amountStr, String currency, String description) {
        double amount = Double.parseDouble(amountStr);

        Intent intent = new Intent(this, PaymentWidget.class);
        intent.putExtra(PaymentWidget.EXTRA_AMOUNT, amount); // Сумма оплаты
        intent.putExtra(PaymentWidget.EXTRA_DESCRIPTION, description); // Описание
        intent.putExtra(PaymentWidget.EXTRA_CURRENCY, currency); // Код валюты
        intent.putExtra(PaymentWidget.EXTRA_PUBLIC_ID, Constants.publicId); // Ваш public ID
        intent.putExtra(PaymentWidget.EXTRA_INVOICE_ID, Constants.invoiceId); // ID заказа в вашей системе
        intent.putExtra(PaymentWidget.EXTRA_ACCOUNT_ID, Constants.accountId); // ID покупателя в вашей системе
        intent.putExtra(PaymentWidget.EXTRA_DATA, "{\"age\":27,\"name\":\"Madi\",\"phone\":\"+77029834123\"}"); // Произвольный набор параметров
        intent.putExtra(PaymentWidget.EXTRA_TYPE, PaymentWidget.TYPE_AUTH); // Тип платежа: TYPE_CHARGE (одностадийный) или TYPE_AUTH (двухстадийный)

        PaymentWidget.taskListener = paymentTaskListener;

        startActivity(intent);
    }

    private PaymentTaskListener paymentTaskListener = new PaymentTaskListener() {

        @Override
        public void success(BaseResponse response) {
            if (response instanceof CardsAuthConfirmResponse)
                showResult(getString(R.string.payment_finished) + ((CardsAuthConfirmResponse) response).transaction.cardHolderMessage + "\n");
            else if (response instanceof CardsAuthResponse) {
                showResult(getString(R.string.payment_finished) + ((CardsAuthResponse) response).auth.cardHolderMessage + "\n");
            } else {
                showResult(getString(R.string.payment_finished) + response + "\n");
            }
        }

        @Override
        public void error(BaseResponse response) {
            if (response instanceof CardsAuthConfirmResponse)
                showResult(getString(R.string.payment_not_finished) + ((CardsAuthConfirmResponse) response).transaction.cardHolderMessage + "\n");
            if (response instanceof CardsAuthResponse)
                showResult(getString(R.string.payment_not_finished) + ((CardsAuthResponse) response).auth.cardHolderMessage + "\n");
            else
                showResult(getString(R.string.error) + response.message + "\n");
        }

        @Override
        public void cancel() {
            showResult(getString(R.string.operation_canceled) + "\n");
        }
    };

    public void showResult(String text) {
        resultText.setText(text);
    }
}
