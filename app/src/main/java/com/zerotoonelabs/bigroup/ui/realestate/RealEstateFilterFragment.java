package com.zerotoonelabs.bigroup.ui.realestate;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.di.Injectable;
import com.zerotoonelabs.bigroup.entity.RealEstateFilter;

import javax.inject.Inject;

public class RealEstateFilterFragment extends Fragment implements Injectable {

    private static final String FILTER_KEY = "filter";
    @Inject
    ViewModelProvider.Factory viewModelFactory;

    RealEstateViewModel viewModel;

    private OnFilterAppliedListener mListener;

    private Spinner spinner1;
    private Spinner spinner2;

    public RealEstateFilterFragment() {
        // Required empty public constructor
    }

    public static RealEstateFilterFragment newInstance(RealEstateFilter filter) {
        RealEstateFilterFragment fragment = new RealEstateFilterFragment();
        Bundle args = new Bundle();
        args.putParcelable(FILTER_KEY, filter);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(RealEstateViewModel.class);

        Bundle args = getArguments();
        if (args != null && args.containsKey(FILTER_KEY)) {
            RealEstateFilter filter = args.getParcelable(FILTER_KEY);
            if (filter != null) {
                setSpinnerValue1(filter);
                setSpinnerValue2(filter);
            }
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.real_estate_filter_frag, container, false);
        spinner1 = view.findViewById(R.id.city_spinner);
        spinner2 = view.findViewById(R.id.status_spinner);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Spinner citySpinner = view.findViewById(R.id.city_spinner);
        Spinner statusSpinner = view.findViewById(R.id.status_spinner);
        Button button = view.findViewById(R.id.apply_button);
        button.setOnClickListener(v -> {
            String city = citySpinner.getSelectedItem().toString();
            int selectedStatus = statusSpinner.getSelectedItemPosition();

            onButtonPressed(city, selectedStatus);
        });
    }

    public void onButtonPressed(String city, int status) {
        if (mListener != null) {
            RealEstateFilter filter = new RealEstateFilter();
            filter.setCity(city);
            filter.setStatus(status);

            mListener.onFilterApplied(filter);
            navigateBack();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (OnFilterAppliedListener) getTargetFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException(getTargetFragment()
                    + " must implement OnFilterAppliedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFilterAppliedListener {
        void onFilterApplied(RealEstateFilter filter);
    }

    private void setSpinnerValue1(RealEstateFilter filter) {
        String city = filter.getCity();
        String[] cities = getResources().getStringArray(R.array.cities);
        if (city == null) return;

        int position = 0;
        for (int i = 0; i < cities.length; i++) {
            if (cities[i].equals(city)) {
                position = i;
                break;
            }
        }
        spinner1.setSelection(position);
    }

    private void setSpinnerValue2(RealEstateFilter filter) {
        int position = filter.getStatus();
        spinner2.setSelection(position);
    }

    private void navigateBack() {
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager != null) {
            fragmentManager.popBackStack();
        }
    }
}
