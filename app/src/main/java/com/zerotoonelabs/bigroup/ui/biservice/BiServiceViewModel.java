package com.zerotoonelabs.bigroup.ui.biservice;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.zerotoonelabs.bigroup.repository.BiRepository;
import com.zerotoonelabs.bigroup.util.AbsentLiveData;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.Resource;
import com.zerotoonelabs.bigroup.entity.ServiceRequest;

import java.util.List;

import javax.inject.Inject;

import static android.text.TextUtils.isEmpty;

public class BiServiceViewModel extends ViewModel {

    private final MutableLiveData<String> token = new MutableLiveData<>();
    private final LiveData<Resource<List<ServiceRequest>>> serviceRequests;

    @Inject
    public BiServiceViewModel(BiRepository repository) {

        serviceRequests = Transformations.switchMap(token, input -> {
            if (isEmpty(input)) {
                return AbsentLiveData.create();
            }
            return repository.loadServiceRequests(input);
        });
    }

    void setToken(String input) {
        if (Objects.equals(input, token.getValue())) {
            return;
        }
        token.setValue(input);
    }

    LiveData<Resource<List<ServiceRequest>>> getServiceRequests() {
        return serviceRequests;
    }
}
