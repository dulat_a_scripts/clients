package com.zerotoonelabs.bigroup.ui.apartments;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.ApartmentDetailFragBinding;
import com.zerotoonelabs.bigroup.di.Injectable;
import com.zerotoonelabs.bigroup.ui.common.ApartmentPagedListAdapter;
import com.zerotoonelabs.bigroup.ui.mortgage.MortgageFragment;
import com.zerotoonelabs.bigroup.ui.realestatedetail.RealEstateDetailFragment;
import com.zerotoonelabs.bigroup.ui.sendrequest.SendRequestFragment;
import com.zerotoonelabs.bigroup.util.AutoClearedValue;
import com.zerotoonelabs.bigroup.entity.Apartments.Apartment;
import com.zerotoonelabs.bigroup.entity.Status;

import java.util.HashMap;

import javax.inject.Inject;

public class ApartmentDetailFragment extends Fragment implements Injectable {
    private static final String TOKEN_KEY = "token";
    private static final String APARTMENT_KEY = "apartment";

    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private FragmentManager fragmentManager;

    public ApartmentViewModel mViewModel;
    AutoClearedValue<ApartmentPagedListAdapter> adapter;
    AutoClearedValue<ApartmentDetailFragBinding> binding;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(ApartmentViewModel.class);

        Bundle args = getArguments();
        if (args != null && args.containsKey(TOKEN_KEY) && args.containsKey(APARTMENT_KEY)) {
            String token = args.getString(TOKEN_KEY);
            Apartment apartment = args.getParcelable(APARTMENT_KEY);
            binding.get().setToken(token);
            binding.get().setApartment(apartment);

            HashMap<String, String> filter = new HashMap<>();
            if (apartment != null && apartment.getBlock() != null) {
                filter.put("real_estate_guids", apartment.getBlock().realEstate.id);
                filter.put("block_guids", apartment.getBlock().id);
            }

            if (apartment != null) {
                mViewModel.setApartId(apartment.getId());
            }

            setupAdapter(token, filter);
        }

        initObervables(mViewModel);
    }

    private void initObervables(ApartmentViewModel viewModel) {
        viewModel.getApartment().observe(this, apartment -> {
            if (isAdded()) {
                viewModel.setFavorite(apartment != null);
                getActivity().invalidateOptionsMenu();
            }
        });

        viewModel.getDetailedApartments().observe(this, result -> {
            adapter.get().setList(result);
            binding.get().executePendingBindings();
        });
    }

    private void setupAdapter(String token, HashMap<String, String> filter) {
        ApartmentPagedListAdapter adapter = new ApartmentPagedListAdapter(apartment -> {
            navigateToApartmentDetail(token, apartment);
        }, token);
        this.adapter = new AutoClearedValue<>(this, adapter);
        binding.get().apartmentsList.setAdapter(adapter);

        binding.get().floorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int floor = position + 1;
                filter.put("floor_min", String.valueOf(floor));
                filter.put("floor_max", String.valueOf(floor));

                mViewModel.setDetailedApartmentId(filter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                filter.put("floor_min", "1");
                filter.put("floor_max", "1");

                mViewModel.setDetailedApartmentId(filter);
            }
        });

        binding.get().button8.setOnClickListener(v -> {
            Apartment apartment = binding.get().getApartment();
            if (apartment != null && apartment.getBlock() != null && apartment.getBlock().realEstate != null) {
                navigateToRealEstateDetails(apartment.getBlock().realEstate.id);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ApartmentDetailFragBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.apartment_detail_frag, container, false);
        binding = new AutoClearedValue<>(this, dataBinding);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setAutoMeasureEnabled(true);
        dataBinding.apartmentsList.setLayoutManager(layoutManager);
        dataBinding.apartmentsList.setNestedScrollingEnabled(false);

        dataBinding.imageView7.setOnClickListener(view -> {
            StaticImage.bitmap = ((BitmapDrawable) dataBinding.imageView7.getDrawable()).getBitmap();
            Intent intent = new Intent(getContext(), ImageFullScreenActivity.class);
            startActivity(intent);
        });
        dataBinding.button7.setOnClickListener(v -> navigateToMortgage());
        dataBinding.button9.setOnClickListener(v -> navigateToSendRequest());
        return dataBinding.getRoot();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        fragmentManager = getFragmentManager();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.apartment_menu, menu);
        MenuItem favItem = menu.findItem(R.id.favorite);
        boolean favorite = mViewModel.isFavorite();
        favItem.setIcon(favorite ? R.drawable.ic_star_filled : R.drawable.ic_star);

        mViewModel.getInsertedApartment().observe(this, resource -> {
            favItem.setEnabled(resource != null && resource.status == Status.SUCCESS);
        });

        mViewModel.getRemovedApartment().observe(this, integerResource -> {
            favItem.setEnabled(integerResource != null && integerResource.status == Status.SUCCESS);
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.favorite:
                if (isAdded()) {
                    setFavorite();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setFavorite() {
        Apartment apartment = binding.get().getApartment();
        boolean favorite = !mViewModel.isFavorite();
        if (favorite) {
            mViewModel.setApartmentToInsert(apartment);
        } else {
            mViewModel.setApartmentToRemove(apartment);
        }
        mViewModel.setFavorite(favorite);
        getActivity().invalidateOptionsMenu();
    }

    public void navigateToRealEstateDetails(String id) {
        if (fragmentManager == null) return;
        RealEstateDetailFragment fragment = RealEstateDetailFragment.newInstance(id);
        String tag = "Просмотр ЖК";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(R.id.flContent, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    public void navigateToSendRequest() {
        if (fragmentManager == null) return;
        SendRequestFragment fragment = new SendRequestFragment();
        String tag = "Оставить заявку";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(R.id.flContent, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    public void navigateToMortgage() {
        if (fragmentManager == null) return;
        MortgageFragment fragment = new MortgageFragment();
        String tag = "Ипотека";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(R.id.flContent, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    public void navigateToApartmentDetail(String token, Apartment apartment) {
        if (fragmentManager == null) return;

        ApartmentDetailFragment fragment = ApartmentDetailFragment.newInstance(token, apartment);
        String tag = apartment.getTitle();
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(R.id.flContent, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    public ApartmentDetailFragment() {
        // Required empty public constructor
    }

    public static ApartmentDetailFragment newInstance(String token, Apartment apartment) {
        ApartmentDetailFragment fragment = new ApartmentDetailFragment();
        Bundle args = new Bundle();
        args.putString(TOKEN_KEY, token);
        args.putParcelable(APARTMENT_KEY, apartment);
        fragment.setArguments(args);
        return fragment;
    }
}
