package com.zerotoonelabs.bigroup.ui.apartments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.authui.HomeActivity;
import com.zerotoonelabs.bigroup.databinding.FragmentMatchApartmentBinding;
import com.zerotoonelabs.bigroup.di.Injectable;
import com.zerotoonelabs.bigroup.ui.common.ApartmentPagedListAdapter;
import com.zerotoonelabs.bigroup.util.AutoClearedValue;
import com.zerotoonelabs.bigroup.entity.ApartmentFilter;
import com.zerotoonelabs.bigroup.entity.Apartments.Apartment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;


public class ApartmentFragment extends Fragment implements Injectable {
    private static final String REAL_ESTATE_ID_KEY = "realestate_key";
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    public ApartmentViewModel mViewModel;

    private FragmentManager fragmentManager;
    String token;


    AutoClearedValue<FragmentMatchApartmentBinding> binding;
    AutoClearedValue<ApartmentPagedListAdapter> adapter;

    public static ApartmentFragment newInstance(String id) {
        ApartmentFragment fragment = new ApartmentFragment();
        Bundle args = new Bundle();
        args.putString(REAL_ESTATE_ID_KEY, id);
        fragment.setArguments(args);
        return fragment;
    }

    public ApartmentFragment() {
        // Required empty public constructor
    }
    ProgressBar progressBar;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentMatchApartmentBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_match_apartment, container, false);
        dataBinding.setCallback(() -> mViewModel.retry());
        dataBinding.clearFilterButton.setOnClickListener(view -> {
            clearFilter();
        });
        binding = new AutoClearedValue<>(this, dataBinding);
       // ButterKnife.bind(this,dataBinding.getRoot());

        //progressBar = dataBinding.


        return dataBinding.getRoot();
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        token = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("token", null);
        mViewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(ApartmentViewModel.class);
        mViewModel.setToken(token);

        Bundle args = getArguments();


        HashMap<String, String> filtert = new HashMap<>();
        HashMap<String, String> filter_obect = new HashMap<>();

        Integer fix_render = 0;

//zz
        if(Library.data != null){

            Integer fixprice = 0;

            for(Map.Entry<String, String> entry : Library.data.entrySet()) {
                String key = entry.getKey();
                //String value = entry.getValue();

                switch (key){

                    case "price_min":
                        fixprice = 1;
                        break;

                }

            }

            filtert = new HashMap<>(Library.data);

            if(fixprice == 0){
                filtert.put("price_min", "1");
            }

            filtert.put("sort_price", "asc");

        }else{
            if (args != null && args.containsKey(REAL_ESTATE_ID_KEY)) {
                String id = args.getString(REAL_ESTATE_ID_KEY);
                filter_obect.put("real_estate_guids", id);
                filter_obect.put("sort_price", "asc");
                filter_obect.put("price_min", "1");
                fix_render = 1;
            }
        }


        if(fix_render == 0){
            mViewModel.setApartmentId(filtert);
            setupAdapter(token);
            initApartmentList(mViewModel);
        }else if(fix_render == 1){
            mViewModel.setApartmentId(filter_obect);
            setupAdapter(token);
            initApartmentList(mViewModel);
        }




        if(Library.Fixfilterpage == 0){
            ApartmentFilterFragment fragment = new ApartmentFilterFragment();
            fragment.setTargetFragment(this, 0);
            String tag = "Применить фильтр";

            fragmentManager.beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .replace(R.id.flContent, fragment, tag)
                    .addToBackStack(null)
                    .commit();

            Library.Fixfilterpage = 1;
        }





        //progressBar.setVisibility(View.VISIBLE);
        //zz
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser) {
            // called here
            Log.i("resumed","resumes");
        }
    }



    private void setupAdapter(String token) {
        ApartmentPagedListAdapter adapter = new ApartmentPagedListAdapter(apartment -> {
            navigateToApartmentDetails(token, apartment);
        }, token);
        this.adapter = new AutoClearedValue<>(this, adapter);
        binding.get().apartmentsList.setAdapter(adapter);
    }

//zz
    private void initApartmentList(ApartmentViewModel viewModel) {
        viewModel.getApartments().observe(this, result -> {
            adapter.get().setList(result);
            binding.get().setResultCount(result == null ? 0 : result.size());
            binding.get().executePendingBindings();
        });

        //progressBar.setVisibility(View.GONE);
    }

    private void clearFilter() {
        mViewModel.setApartmentId(new HashMap<>());
        mViewModel.setApartmentFilter(null);
        binding.get().setFiltered(false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        fragmentManager = getFragmentManager();
    }
    @Override
    public void onStart(){
        super.onStart();
        //ApartmentFilter apartmentFilter = mViewModel.getApartmentFilter();
        //navigateToApartmentsFilter(this, apartmentFilter);
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.apartment_filter, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_filter) {
            ApartmentFilter apartmentFilter = mViewModel.getApartmentFilter();
            navigateToApartmentsFilter(this, apartmentFilter);
        }

        if(id == R.id.action_filterable){
            showactionmessagebar();
        }

        return super.onOptionsItemSelected(item);
    }

    public void showactionmessagebar(){
//zz
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = this.getLayoutInflater();
            View view = inflater.inflate(R.layout.apartment_fragment_action_bar, null);
            builder.setView(view);
            Button button = view.findViewById(R.id.button17);
            Button button2 = view.findViewById(R.id.button18);
            Button button3 = view.findViewById(R.id.button19);
            Button button4 = view.findViewById(R.id.button20);
            Button button5cancel = view.findViewById(R.id.button21);
            Dialog dialog = builder.create();

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(Library.data == null){
                        HashMap<String, String> filter = new HashMap<>();
                        filter.put("sort_square", "asc");
                        filter.put("price_min", "1");
                        mViewModel.setApartmentId(filter);
                        setupAdapter(token);
                        initApartmentList(mViewModel);
                        dialog.dismiss();
                    }else{
                        HashMap<String, String> newfilter = new HashMap<>(Library.data);
                        newfilter.put("sort_square", "asc");
                        mViewModel.setApartmentId(newfilter);
                        setupAdapter(token);
                        initApartmentList(mViewModel);
                        dialog.dismiss();
                    }

                    dialog.dismiss();
                }
            });

            button2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(Library.data == null){
                        HashMap<String, String> filter = new HashMap<>();
                        filter.put("sort_square", "desc");
                        filter.put("price_min", "1");
                        mViewModel.setApartmentId(filter);
                        setupAdapter(token);
                        initApartmentList(mViewModel);
                        dialog.dismiss();
                    }else{

                        HashMap<String, String> newfilter = new HashMap<>(Library.data);
                        newfilter.put("sort_square", "desc");
                        mViewModel.setApartmentId(newfilter);
                        setupAdapter(token);
                        initApartmentList(mViewModel);
                        dialog.dismiss();
                    }
                    dialog.dismiss();
                }
            });

            button3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(Library.data == null){
                        HashMap<String, String> filter = new HashMap<>();
                        filter.put("sort_price", "asc");
                        filter.put("price_min", "1");
                        mViewModel.setApartmentId(filter);
                        setupAdapter(token);
                        initApartmentList(mViewModel);
                        dialog.dismiss();
                    }else{
                        HashMap<String, String> newfilter = new HashMap<>(Library.data);
                        newfilter.put("sort_price", "asc");
                        mViewModel.setApartmentId(newfilter);
                        setupAdapter(token);
                        initApartmentList(mViewModel);
                        dialog.dismiss();
                    }
                    dialog.dismiss();
                }
            });

            button4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if(Library.data == null){
                        dialog.dismiss();
                        HashMap<String, String> filter = new HashMap<>();
                        filter.put("sort_price", "desc");
                        filter.put("price_min", "1");
                        mViewModel.setApartmentId(filter);
                        setupAdapter(token);
                        initApartmentList(mViewModel);

                    }else{
                        dialog.dismiss();
                        HashMap<String, String> newfilter = new HashMap<>(Library.data);
                        newfilter.put("sort_price", "desc");
                        mViewModel.setApartmentId(newfilter);
                        setupAdapter(token);
                        initApartmentList(mViewModel);

                    }

                }
            });

            button5cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();

                }
            });

        // Setting dialogview
        Window window = dialog.getWindow();
        WindowManager.LayoutParams param = window.getAttributes();
        param.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
        window.setAttributes(param);
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.show();
        // Setting dialogview

    }

//    @Override
//    public void applyFilter(HashMap<String, String> filter, ApartmentFilter apartmentFilter) {
//        mViewModel.setApartmentId(filter);
//        mViewModel.setApartmentFilter(apartmentFilter);
//        binding.get().setFiltered(true);
//    }

    private void navigateToApartmentsFilter(ApartmentFragment targetFragment, ApartmentFilter apartmentFilter) {
        if (fragmentManager == null) return;
        ApartmentFilterFragment fragment = new ApartmentFilterFragment();

        fragment.setTargetFragment(targetFragment, 0);
        String tag = "Применить фильтр";


//        fragmentManager.beginTransaction()
//                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
//                .add(R.id.flContent, fragment, tag)
//                .commit();
//zz
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(R.id.flContent, fragment, tag)
                .addToBackStack(null)
                .commit();
    }

    private void navigateToApartmentDetails(String token, Apartment apartment) {
        if (fragmentManager == null) return;
        ApartmentDetailFragment fragment = ApartmentDetailFragment.newInstance(token, apartment);
        String tag = apartment.getTitle();
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(R.id.flContent, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }
}
