package com.zerotoonelabs.bigroup.ui.common;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.ServiceRequestItemBinding;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.ServiceRequest;

/**
 * A RecyclerView adapter for {@link ServiceRequest} class.
 */
public class ServiceRequestListAdapter extends DataBoundListAdapter<ServiceRequest, ServiceRequestItemBinding> {
    private final ServiceRequestClickCallback serviceRequestClickCallback;

    public ServiceRequestListAdapter(ServiceRequestClickCallback serviceRequestClickCallback) {
        this.serviceRequestClickCallback = serviceRequestClickCallback;
    }

    @Override
    protected ServiceRequestItemBinding createBinding(ViewGroup parent) {
        ServiceRequestItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.service_request_item,
                        parent, false);
        binding.getRoot().setOnClickListener(v -> {
            ServiceRequest repo = binding.getRequest();
            if (repo != null && serviceRequestClickCallback != null) {
                serviceRequestClickCallback.onClick(repo);
            }
        });
        return binding;
    }

    @Override
    protected void bind(ServiceRequestItemBinding binding, ServiceRequest item, int position) {
        binding.setRequest(item);
    }

    @Override
    protected boolean areItemsTheSame(ServiceRequest oldItem, ServiceRequest newItem) {
        return Objects.equals(oldItem.getId(), newItem.getId());
    }

    @Override
    protected boolean areContentsTheSame(ServiceRequest oldItem, ServiceRequest newItem) {
        return Objects.equals(oldItem.getDateTime(), newItem.getDateTime());
    }

    public interface ServiceRequestClickCallback {
        void onClick(ServiceRequest serviceRequest);
    }
}