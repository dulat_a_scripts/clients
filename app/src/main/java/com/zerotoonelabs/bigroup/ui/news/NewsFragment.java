package com.zerotoonelabs.bigroup.ui.news;


import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.FragmentNewsBinding;
import com.zerotoonelabs.bigroup.di.Injectable;
import com.zerotoonelabs.bigroup.ui.common.NewsListAdapter;
import com.zerotoonelabs.bigroup.util.AutoClearedValue;

import javax.inject.Inject;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewsFragment extends Fragment implements Injectable {

    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private NewsViewModel mViewModel;

    AutoClearedValue<FragmentNewsBinding> binding;
    AutoClearedValue<NewsListAdapter> adapter;

    public NewsFragment() {
        // Required empty public constructor
    }

    public static NewsFragment newInstance(String param1, String param2) {
        NewsFragment fragment = new NewsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        FragmentNewsBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_news, container, false);
        dataBinding.setCallback(() -> mViewModel.retry());
        binding = new AutoClearedValue<>(this, dataBinding);
        return dataBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(NewsViewModel.class);
        mViewModel.setLang("1");

        NewsListAdapter adapter = new NewsListAdapter(
                news -> {
                    Intent intent = new Intent(getActivity(), NewsActivity.class);
                    intent.putExtra(NewsActivity.NEWS_EXTRA, news);
                    startActivity(intent);
                });

        this.adapter = new AutoClearedValue<>(this, adapter);
        binding.get().newsList.setAdapter(adapter);
        initStockList(mViewModel);
    }

    private void initStockList(NewsViewModel viewModel) {
        viewModel.getStocks().observe(this, result -> {
            binding.get().setSearchResource(result);
            binding.get().setResultCount((result == null || result.data == null)
                    ? 0 : result.data.size());
            adapter.get().replace(result == null ? null : result.data);
            binding.get().executePendingBindings();
        });
    }

}
