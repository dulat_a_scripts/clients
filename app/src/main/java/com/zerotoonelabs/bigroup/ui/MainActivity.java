package com.zerotoonelabs.bigroup.ui;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.SupportMapFragment;
import com.zerotoonelabs.bigroup.AuthorizedSplashScreenActivity;
import com.zerotoonelabs.bigroup.Library.DaggerModules.PreferenceModule;
import com.zerotoonelabs.bigroup.Library.Fragments.Notification_fragments;
import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.RegistrationActivity;
import com.zerotoonelabs.bigroup.authui.HomeActivity;
import com.zerotoonelabs.bigroup.ui.common.NavigationController;
import com.zerotoonelabs.bigroup.entity.Status;

import java.util.Observable;

import javax.inject.Inject;

import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, HasSupportFragmentInjector,
        FragmentManager.OnBackStackChangedListener {

    String LOG = "MainActivity";

    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;
    @Inject
    NavigationController navigationController;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Override
    public DispatchingAndroidInjector<Fragment> supportFragmentInjector()  {
        return dispatchingAndroidInjector;
    }

    private NavigationView mNavView;
    private DrawerLayout mDrawerLayout;
    private ActionBar mActionBar;
    private MainViewModel viewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(LOG, "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initDummyMap();


        //DaggerInjectComponent.create.poke(app:this)

        setupToolbar(R.drawable.ic_menu);
        setupNavigationDrawer();

        getSupportFragmentManager().addOnBackStackChangedListener(this);

        mNavView.setNavigationItemSelectedListener(this);
        mNavView.setItemIconTintList(null);

        //DaggerInjectComponent.inject(this);


        Library.Phone_memory = PreferenceManager.getDefaultSharedPreferences(this);

        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(MainViewModel.class);
        initObservables(viewModel);
        checkTokenValidity(viewModel);

        //setting dialog do not touch
        Library.viewdialog = getLayoutInflater().inflate(R.layout.universal_dialog,null);
        Library.newcontext = this;
        //setting dialog

        io.reactivex.Observable<Integer> observable = io.reactivex.Observable.create(new ObservableOnSubscribe<Integer>() {
            @Override
            public void subscribe(ObservableEmitter<Integer> emitter) throws Exception {

                emitter.onNext(1);
                emitter.onNext(2);
                emitter.onNext(3);
                emitter.onNext(4);

                emitter.onComplete();

            }
        });


        Observer<Integer> observer = new Observer<Integer>() {
            @Override
            public void onSubscribe(Disposable d) {
                Log.i("onSubscribe ","sabscribe ");
            }

            @Override
            public void onNext(Integer integer) {
                Log.i("onNext ","onNext " + integer);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };

        //Create our subscription//
        observable.subscribe(observer);

        //Вы можете использовать observable.just()оператор для преобразования любого объекта в Observable.
        //Observable.from() -- array
        //Observable.range(0.5) -- array
        //Observable<Long> observable = Observable.interval(1, TimeUnit.SECONDS)
        //Observable<String> observable = Observable.empty(); - for testing observable

//        Observable.just("one", "two", "three", "four", "five")
//                .subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(/* an Observer */);

//        Looper backgroundLooper = // ...
//                Observable.just("one", "two", "three", "four", "five")
//                        .observeOn(AndroidSchedulers.from(backgroundLooper))
//                        .subscribe(/* an Observer */)


        //finish method

    }

    private void initObservables(MainViewModel viewModel) {
        viewModel.getToken().observe(this, resource -> {
            if (resource != null && resource.data != null) {
                viewModel.setRawToken(resource.data);
                checkCurrentUser();
                viewModel.getToken().removeObservers(this);
            }
            if (resource != null && resource.status == Status.ERROR) {
                Toast.makeText(this, resource.message, Toast.LENGTH_SHORT).show();
            }
        });

        viewModel.getValidToken().observe(this, booleanResource -> {
            if (booleanResource == null) return;
            switch (booleanResource.status) {
                case SUCCESS:
                    checkCurrentUser();
                    viewModel.getValidToken().removeObservers(this);
                    break;
                case ERROR:
                    viewModel.setTokenId("Basic WmVyb1RvT25lTGFiOjEyMzQ1Ng==");
                    break;
            }
        });
    }

    private void checkTokenValidity(MainViewModel viewModel) {
        String token = PreferenceManager.getDefaultSharedPreferences(this)
                .getString("token", null);
        if (token == null) {
            viewModel.setTokenId("Basic WmVyb1RvT25lTGFiOjEyMzQ1Ng==");
        } else {
            viewModel.setCheckTokenId(token);
        }
    }

    /*
    * First launch of map in RealEstateDetails is slow
    * https://stackoverflow.com/q/26178212/5222156
    */
    private void initDummyMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(googleMap -> Timber.d("init dummy map"));
    }

    private void checkCurrentUser() {
        String user = PreferenceManager.getDefaultSharedPreferences(this)
                .getString(HomeActivity.CURRENT_USER, null);
        if (user == null) {
            navigationController.navigateToRealEstate();
        } else {
            takeUserToHomePage();
        }
    }

    private void takeUserToHomePage() {
        Intent i = new Intent(this, HomeActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        finish();
    }

    private void closeDrawer() {
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    private void openDrawer() {
        if (mDrawerLayout != null) {
            mDrawerLayout.openDrawer(GravityCompat.START);
        }
    }

    private void openProfile() {
//        fragment = new ProfileFragment();
//        mFragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
//        mToolbar.setTitle("Профиль");
//        invalidateOptionsMenu();
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }

    }

    @Override
    public void onBackStackChanged() {
        shouldDisplayHomeUp();
    }

    public void shouldDisplayHomeUp() {
        //Enable Up button only  if there are entries in the back stack
        FragmentManager fragmentManager = getSupportFragmentManager();
        int count = fragmentManager.getBackStackEntryCount();

        Fragment fr = fragmentManager.findFragmentById(R.id.flContent);
        String tag = fr.getTag();

        boolean canGoBack = count > 0;
        if (mActionBar != null) {
            mActionBar.setHomeAsUpIndicator(canGoBack ? R.drawable.ic_arrow_back : R.drawable.ic_menu);
            mActionBar.setTitle(tag);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        boolean canGoBack = getSupportFragmentManager().getBackStackEntryCount() > 0;

        if (canGoBack) {
            navigationController.navigateBack();
        } else {
            mDrawerLayout.openDrawer(Gravity.START);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    public void setupToolbar(int iconId) {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mActionBar = getSupportActionBar();
        mActionBar.setHomeAsUpIndicator(iconId);
        mActionBar.setDisplayHomeAsUpEnabled(true);
    }

    private void setupNavigationDrawer() {
        mDrawerLayout = findViewById(R.id.drawer_layout);
        mNavView = findViewById(R.id.nav_view);

        setupDrawer(mNavView);

        mDrawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
            }

            @Override
            public void onDrawerOpened(View drawerView) {
            }

            @Override
            public void onDrawerClosed(View drawerView) {
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                dismissKeyboard(mDrawerLayout.getWindowToken());
            }
        });
    }

    private void setupDrawer(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Log.i(LOG, "onNavigationItemSelected(" + item.getItemId() + ")");
        int id = item.getItemId();
        closeDrawer();
//        showFilterIcon = false;

        new Handler().postDelayed(() -> {
            setActionBarTitle(item.getTitle());
            navigateTo(id);
        }, 250);

//        invalidateOptionsMenu();
        return true;
    }

    private void setActionBarTitle(CharSequence title) {
        if (mActionBar != null) {
            mActionBar.setTitle(title);
        }
    }

    private void navigateTo(int id) {
        switch (id) {
            case R.id.nav_objects:
                navigationController.navigateToRealEstate();
//                showFilterIcon = true;
//                objectFilter = true;
                break;
            case R.id.nav_match_apartments:

                Library.Fixfilterpage = 1;
                navigationController.navigateToMatchApartment();
//                showFilterIcon = true;
//                objectFilter = false;
                break;
            case R.id.nav_promos:
                navigationController.navigateToSales();
                break;
            case R.id.nav_call:
                navigationController.navigateToCall();
                break;
            case R.id.nav_favourite:
                navigationController.navigateToFavorites(viewModel.getRawToken());
                break;
            case R.id.nav_mortgage:
                navigationController.navigateToMortgage();
//                mToolbar.setTitle(getString(R.string.buy_conditions));
                break;
            case R.id.nav_part_payments:
                navigationController.navigateToPayment();
//                mToolbar.setTitle(getString(R.string.buy_conditions));
                break;
            case R.id.nav_instant_payment:
                navigationController.navigateToInstantPayment();
//                mToolbar.setTitle(getString(R.string.buy_conditions));
                break;
            case R.id.nav_about:
                navigationController.navigateToAbout();
                break;
            case R.id.nav_news:
                navigationController.navigateToNews();
                break;
            case R.id.nav_contacts:
                navigationController.navigateToContacts();
                break;
            case R.id.nav_info:
                navigationController.navigateToInfo();
                break;
            case R.id.nav_send_application:
                setActionBarTitle("Заявка в BI Service");
                navigationController.navigateToSendApplication();
                break;
            case R.id.nav_visit_object:
                navigationController.navigateToVisitObject(viewModel.getRawToken());
                break;
            case R.id.nav_opent_door:
                navigationController.navigateToOpenDoor();
                break;
            case R.id.nav_keys:
                navigationController.navigateToKeys();
                break;
            case R.id.nav_privatisation:
                navigationController.navigateToPrivatization();
                break;
            case R.id.nav_resale:
                navigationController.navigateToResale();
                break;
            case R.id.nav_bi_service:
                navigationController.navigateToBiService();
                break;
            case R.id.push_notification:
                //zz
//                FragmentManager manager = getSupportFragmentManager();
//                FragmentTransaction transaction = manager.beginTransaction();
//                Notification_fragments notification_fragments = new Notification_fragments();
//                String tag = "Уведомления";
//                transaction.replace(R.id.flContent , notification_fragments , tag);
//                transaction.commit();
                //navigationController.navigateToNotification();

                break;
//            case R.id.nav_ref:
//                navigationController.navigateToSprakvi();
//                break;
        }
    }

    public void openCallFragment(View view) {
        navigationController.navigateToCall();
    }

    public void onProfilePressed(View view) {
        closeDrawer();
        new Handler().postDelayed(() -> {
            navigationController.navigateToProfile();
        }, 250);
    }

    private void dismissKeyboard(IBinder windowToken) {
        FragmentActivity activity = this;
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(windowToken, 0);
    }

    public void onLoginPressed(View view) {
        closeDrawer();
        new Handler().postDelayed(this::login, 250);
    }

    public void onSignUpPressed(View view) {
        closeDrawer();
        new Handler().postDelayed(this::signup, 250);
    }

    private void signup() {
        startActivity(new Intent(MainActivity.this, RegistrationActivity.class));
    }

    private void login() {
        startActivity(new Intent(MainActivity.this, AuthorizedSplashScreenActivity.class));
    }


    public void makeCall(View view) {
        String phoneNumber = ((TextView) view).getText().toString();
        if (!phoneNumber.isEmpty()) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + phoneNumber));
            startActivity(intent);
        }
    }
}

