package com.zerotoonelabs.bigroup.ui;

import android.support.v4.app.Fragment;
import android.widget.Toast;

public class BaseFragment extends Fragment {

    /**
     * Format phone according to specification
     */
    public String getFormattedPhone(String phone) {
        phone = phone
                .replace("(", "")
                .replace(")", "")
                .replace(" ", "-");
        phone = phone.substring(0, 12) + "-" + phone.substring(12, phone.length());

        return phone;
    }

    public void showToast(int messageId) {
        if (!isAdded()) return;
        Toast.makeText(getActivity(), messageId, Toast.LENGTH_SHORT).show();
    }
}
