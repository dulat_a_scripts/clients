package com.zerotoonelabs.bigroup.ui.visitobject;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
/**
 * A fragment with a Google +1 button.
 */
public class VisitBookingSuccessFragment extends Fragment {
    private String calledFrom;


    public VisitBookingSuccessFragment() {
        // Required empty public constructor
    }

    public static VisitBookingSuccessFragment newInstance(){
        return new VisitBookingSuccessFragment();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_visit_booking_success, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();
    }


}
