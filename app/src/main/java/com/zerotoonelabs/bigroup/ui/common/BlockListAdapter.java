package com.zerotoonelabs.bigroup.ui.common;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.ReBlockItemBinding;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.ReBlock;

/**
 * A RecyclerView adapter for {@link com.zerotoonelabs.bigroup.entity.ReBlock} class.
 */
public class BlockListAdapter extends DataBoundListAdapter<ReBlock, ReBlockItemBinding> {

    public BlockListAdapter() {}

    @Override
    protected ReBlockItemBinding createBinding(ViewGroup parent) {
        return DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.re_block_item,
                        parent, false);
    }

    @Override
    protected void bind(ReBlockItemBinding binding, ReBlock item, int position) {
        binding.setBlock(item);
    }

    @Override
    protected boolean areItemsTheSame(ReBlock oldItem, ReBlock newItem) {
        return Objects.equals(oldItem.blockId, newItem.blockId);
    }

    @Override
    protected boolean areContentsTheSame(ReBlock oldItem, ReBlock newItem) {
        return Objects.equals(newItem.address, newItem.address);
    }
}