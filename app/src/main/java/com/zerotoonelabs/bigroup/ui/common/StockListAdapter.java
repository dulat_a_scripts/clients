/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zerotoonelabs.bigroup.ui.common;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.StockItemBinding;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.Stock;

/**
 * A RecyclerView adapter for {@link Stock} class.
 */
public class StockListAdapter extends DataBoundListAdapter<Stock, StockItemBinding> {
    private final StockClickCallback stockClickCallback;

    public StockListAdapter(StockClickCallback stockClickCallback) {
        this.stockClickCallback = stockClickCallback;
    }

    @Override
    protected StockItemBinding createBinding(ViewGroup parent) {
        StockItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.stock_item,
                        parent, false);
        binding.getRoot().setOnClickListener(v -> {
            Stock repo = binding.getStock();
            if (repo != null && stockClickCallback != null) {
                stockClickCallback.onClick(repo);
            }
        });
        return binding;
    }

    @Override
    protected void bind(StockItemBinding binding, Stock item, int position) {
        String imageUrl;
        if (item.imageUrl == null && item.sectionImageUrls != null && item.sectionImageUrls.size() > 0) {
            imageUrl = item.sectionImageUrls.get(0);
        } else {
            imageUrl = item.imageUrl;
        }
        binding.setImageUrl(imageUrl);
        binding.setStock(item);
    }

    @Override
    protected boolean areItemsTheSame(Stock oldItem, Stock newItem) {
        return Objects.equals(oldItem.id, newItem.id) &&
                Objects.equals(oldItem.title, newItem.title);
    }

    @Override
    protected boolean areContentsTheSame(Stock oldItem, Stock newItem) {
        return Objects.equals(oldItem.description, newItem.description);
    }

    public interface StockClickCallback {
        void onClick(Stock stock);
    }
}
