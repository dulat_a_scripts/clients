package com.zerotoonelabs.bigroup.ui.realestate;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.RealEstateFragBinding;
import com.zerotoonelabs.bigroup.di.Injectable;
import com.zerotoonelabs.bigroup.ui.apartments.ApartmentFragment;
import com.zerotoonelabs.bigroup.ui.common.RealEstateListAdapter;
import com.zerotoonelabs.bigroup.ui.realestatedetail.RealEstateDetailFragment;
import com.zerotoonelabs.bigroup.util.AutoClearedValue;
import com.zerotoonelabs.bigroup.entity.RealEstate;
import com.zerotoonelabs.bigroup.entity.RealEstateFilter;

import javax.inject.Inject;

public class RealEstateFragment extends Fragment implements Injectable, RealEstateFilterFragment.OnFilterAppliedListener {

    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private FragmentManager fragmentManager;

    AutoClearedValue<RealEstateFragBinding> binding;
    AutoClearedValue<RealEstateListAdapter> adapter;
    private RealEstateViewModel mViewModel;

    public RealEstateFragment() {
        // Required empty public constructor
    }

    public static RealEstateFragment newInstance(String param1, String param2) {
        RealEstateFragment fragment = new RealEstateFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        fragmentManager = getFragmentManager();
        Library.Fixfilterpage = 1;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.real_estate_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.horizontal_vertical_item:
                changeListOrientation();
                item.setIcon(binding.get().getOrientation() == LinearLayoutManager.HORIZONTAL ? R.drawable.ic_horizontal_list : R.drawable.ic_list_view);
                return true;
            case R.id.filter_item:
                navigateToRealEstateFilter(this, mViewModel.getFilter());
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void changeListOrientation() {
        LinearLayoutManager layoutManager = (LinearLayoutManager) binding.get().realEstateList.getLayoutManager();
        if (layoutManager != null) {
            switch (layoutManager.getOrientation()) {
                case LinearLayoutManager.HORIZONTAL:
                    layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                    setupSnapHelper(false);
                    break;
                case LinearLayoutManager.VERTICAL:
                    layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                    setupSnapHelper(true);
                    break;
            }
            binding.get().setOrientation(layoutManager.getOrientation());
            binding.get().realEstateList.setLayoutManager(layoutManager);
            binding.get().realEstateList.setAdapter(adapter.get());
            adapter.get().notifyDataSetChanged();
            if (layoutManager.getOrientation() == LinearLayoutManager.HORIZONTAL) {
                binding.get().setCurrent(1);
                setupScrollListener();
            }
        }
    }

    private void setupSnapHelper(boolean attach) {
        if (attach) {
            PagerSnapHelper pagerSnapHelper = new PagerSnapHelper();
            pagerSnapHelper.attachToRecyclerView(binding.get().realEstateList);
        } else {
            binding.get().realEstateList.clearOnScrollListeners();
            binding.get().realEstateList.setOnFlingListener(null);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String token = PreferenceManager.getDefaultSharedPreferences(getActivity())
                .getString("token", null);

        setupViewModel(token);

        RealEstateListAdapter adapter = new RealEstateListAdapter(new RealEstateListAdapter.RealEstateClickCallback() {
            @Override
            public void onClick(RealEstate realEstate) {
                navigateToRealEstateDetails(realEstate.getId());
            }

            @Override
            public void onApartmentClick(RealEstate realEstate) {
                navigateToApartments(realEstate.getId());
            }

            @Override
            public void onApartmentCountClick(String id) {
                navigateToApartments(id);
            }

            @Override
            public void onAddressClick(double lat, double lon, String label) {
                openMap(lat, lon, label);
            }
        }, token);

        setupSnapHelper(true);
        this.adapter = new AutoClearedValue<>(this, adapter);
        binding.get().realEstateList.setHasFixedSize(true);
        binding.get().realEstateList.setAdapter(adapter);
        binding.get().setOrientation(LinearLayoutManager.HORIZONTAL);
        binding.get().setCurrent(1);
        initObservables(mViewModel);
        setupScrollListener();
    }


    private void setupViewModel(String token) {
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(RealEstateViewModel.class);
        if (token == null) {
            getActivity().recreate();
        } else {
            mViewModel.setToken(token);
            mViewModel.applyFilter(null);
        }
    }

    private void setupScrollListener() {
        LinearLayoutManager layoutManager = (LinearLayoutManager) binding.get().realEstateList.getLayoutManager();
        if (layoutManager != null) {
            binding.get().realEstateList.addOnScrollListener(
                    new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                            super.onScrollStateChanged(recyclerView, newState);
                            if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                                int position = layoutManager.findFirstVisibleItemPosition();
                                if (position != -1 && binding != null && binding.get() != null) {
                                    binding.get().setCurrent(position + 1);
                                }
                            }
                        }

                    }
            );
        }
    }

    private void initObservables(RealEstateViewModel viewModel) {
        viewModel.getRealEstates().observe(this, listResource -> {
            binding.get().setResource(listResource);
            adapter.get().replace(listResource == null ? null : listResource.data);
            binding.get().setTotal(listResource == null || listResource.data == null ? 0 : listResource.data.size());
            binding.get().executePendingBindings();
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        RealEstateFragBinding dataBinding = DataBindingUtil
                .inflate(inflater, R.layout.real_estate_frag, container, false);
        binding = new AutoClearedValue<>(this, dataBinding);
        Library.data = null;
        return dataBinding.getRoot();
    }

    @Override
    public void onFilterApplied(RealEstateFilter filter) {
        mViewModel.applyFilter(filter);
        mViewModel.setFilter(filter);
    }

    public void navigateToRealEstateFilter(RealEstateFragment targetFragment, RealEstateFilter filter) {
        if (fragmentManager == null) return;
        RealEstateFilterFragment fragment = RealEstateFilterFragment.newInstance(filter);
        fragment.setTargetFragment(targetFragment, 0);
        String tag = "Применить фильтр";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(R.id.flContent, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    public void navigateToRealEstateDetails(String id) {
        if (fragmentManager == null) return;
        RealEstateDetailFragment fragment = RealEstateDetailFragment.newInstance(id);
        String tag = "Просмотр ЖК";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(R.id.flContent, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    public void navigateToApartments(String id) {



        if (fragmentManager == null) return;
        ApartmentFragment fragment = ApartmentFragment.newInstance(id);
        String tag = "Подбор квартиры";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(R.id.flContent, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    private void openMap(double lat, double lon, String label) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:" + lat + "," + lon + " ?q= " + lat + "," + lon + "(" + label + ")"));
        startActivity(intent);
    }
}
