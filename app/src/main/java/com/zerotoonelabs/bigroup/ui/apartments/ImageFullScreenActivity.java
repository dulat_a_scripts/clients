package com.zerotoonelabs.bigroup.ui.apartments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.zerotoonelabs.bigroup.R;

public class ImageFullScreenActivity extends AppCompatActivity {

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_full_screen);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Просмотр схемы квартиры");
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        ZoomableImageView imageView = findViewById(R.id.imageview);
        Bitmap bmp = StaticImage.bitmap;
        imageView.setImageBitmap(bmp);

    }

    @Override
    public boolean onNavigateUp() {
        onBackPressed();
        return false;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
