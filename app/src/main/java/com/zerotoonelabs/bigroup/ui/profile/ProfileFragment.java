package com.zerotoonelabs.bigroup.ui.profile;

import android.app.AlertDialog;
import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.Library.Request_parsers.ChangePasswordParser;
import com.zerotoonelabs.bigroup.Library.Request_parsers.Get_flat_parser;
import com.zerotoonelabs.bigroup.Library.Request_parsers.SendChangeEmail;
import com.zerotoonelabs.bigroup.Library.Request_parsers.SendChangePassword;
import com.zerotoonelabs.bigroup.Library.Request_parsers.SendChangePhone;
import com.zerotoonelabs.bigroup.Library.Retrofit_request.ChangeEmail_int;
import com.zerotoonelabs.bigroup.Library.Retrofit_request.Change_password_int;
import com.zerotoonelabs.bigroup.Library.Retrofit_request.Change_phone_int;
import com.zerotoonelabs.bigroup.Library.Retrofit_request.GetFlat_int;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.ProfileFragBinding;
import com.zerotoonelabs.bigroup.di.Injectable;
import com.zerotoonelabs.bigroup.util.AutoClearedValue;
import com.zerotoonelabs.bigroup.entity.User;

import java.util.List;

import javax.security.auth.callback.CallbackHandler;

import io.fabric.sdk.android.services.concurrency.AsyncTask;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ProfileFragment extends Fragment implements Injectable {
    private static final String USER_KEY = "user";

    private AutoClearedValue<ProfileFragBinding> binding;
    String token;
    public ProfileFragment() {
        // Required empty public constructor
    }

    public static ProfileFragment newInstance(User user) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putParcelable(USER_KEY, user);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle args = getArguments();
        if (args != null && args.containsKey(USER_KEY)) {
            User user = args.getParcelable(USER_KEY);
            binding.get().setUser(user);
            loadProfileImage(user);
        }
    }

    TextView textView;
    TextView textView2;
    TextView textView3;
    Button button;
    ProgressBar progressBar;
    ProgressBar progressBar10;
    String contractId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ProfileFragBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.profile_frag, container, false);
        binding = new AutoClearedValue<>(this, dataBinding);

        textView = binding.get().textView132;
        textView2 = binding.get().textView133;
        textView3 = binding.get().textView114;
        button = binding.get().button25;
        progressBar10 = binding.get().progressBar10;

        contractId = Library.GetPhoneMemoryOne("contractId");

        binding.get().imageView18.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    String userid = Library.GetPhoneMemoryOne("userid");
                    EditText editText = binding.get().editText7;

                    String email = editText.getText().toString();

                     progressBar10.setVisibility(View.VISIBLE);

                    Retrofit client = Library.NewRetrofitclient();
                    ChangeEmail_int changeEmail_int = client.create(ChangeEmail_int.class);
                    SendChangeEmail sendChangeEmail = new SendChangeEmail();
                    sendChangeEmail.setEmail(email);
                    sendChangeEmail.setUserId(contractId);
                    Call<Object> call = changeEmail_int.sendRequest(sendChangeEmail,token);

                    call.enqueue(new Callback<Object>() {
                        @Override
                        public void onResponse(Call<Object> call, Response<Object> response) {

                            if(response.isSuccessful()){
                                Library.showToast(getContext(),"ваш email обновлен");
                                progressBar10.setVisibility(View.GONE);
                            }else{
                                Library.showToast(getContext(),"не удалось сменить email попробуйте позднее");
                                progressBar10.setVisibility(View.GONE);
                            }

                        }

                        @Override
                        public void onFailure(Call<Object> call, Throwable t) {
                            Library.showToast(getContext(),"ошибка сети");
                            progressBar10.setVisibility(View.GONE);
                        }
                    });



            }
        });

        binding.get().imageView19.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//xx
                String userid = Library.GetPhoneMemoryOne("userid");
                EditText editText = binding.get().editText8;

                String phone = editText.getText().toString();
                progressBar10.setVisibility(View.VISIBLE);
                Retrofit client = Library.NewRetrofitclient();
                Change_phone_int change_phone_int = client.create(Change_phone_int.class);
                SendChangePhone sendChangePhone = new SendChangePhone();
                sendChangePhone.setPhoneNumber(phone);
                sendChangePhone.setUserId(contractId);
                Call<Object> call = change_phone_int.sendRequest(sendChangePhone,token);
                call.enqueue(new Callback<Object>() {
                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {

                        if(response.isSuccessful()){
                            Library.showToast(getContext(),"номер вашего телефона обновлен");
                            progressBar10.setVisibility(View.GONE);
                        }else{
                            Library.showToast(getContext(),"не удалось сменить телефон попробуйте позднее");
                            progressBar10.setVisibility(View.GONE);
                        }

                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable t) {
                        Library.showToast(getContext(),"ошибка сети");
                        progressBar10.setVisibility(View.GONE);
                    }
                });

            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String userid = Library.GetPhoneMemoryOne("userid");



                Log.i("dd","d" + contractId + " | " + token);

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                 LayoutInflater inflater = getActivity().getLayoutInflater();
                 View views = inflater.inflate(R.layout.profile_fragment_dialog, null);
                    builder.setView(views);
                    ImageView buttonclose = views.findViewById(R.id.imageView20);
                    EditText oldpassword_edit = views.findViewById(R.id.editText15);
                    EditText newpassword = views.findViewById(R.id.editText16);
                    EditText repeat_password = views.findViewById(R.id.editText17);
                    Button sendchangepassword = views.findViewById(R.id.button21);
                    progressBar = views.findViewById(R.id.progressBar9);

                Dialog dialog = builder.create();

                sendchangepassword.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String oldpassword = Library.GetPhoneMemoryOne("password");
                        String oldpassword_edit_text = oldpassword_edit.getText().toString();
                        if(!oldpassword_edit_text.equals(oldpassword)){
                            Library.showToast(getContext(),"текущий пароль не совпадает с введенным");
                            return;
                        }

                        String newpassword_text = newpassword.getText().toString();
                        String repeat_password_text = repeat_password.getText().toString();

                        if(!newpassword_text.equals(repeat_password_text)){
                            Library.showToast(getContext(),"новые введенные пароли не совпадают");
                            return;
                        }

                        if(newpassword_text.equals(oldpassword) || repeat_password_text.equals(oldpassword)){
                            Library.showToast(getContext(),"старый пароль не должен совпадать с новым");
                            return;
                        }

                        if(newpassword_text.length() < 7 || repeat_password_text.length() < 7){
                            Library.showToast(getContext(),"Длина пароля должна быть от 7 символов");
                            return;
                        }
                        Integer fixalphabet = 0;
                        for(int i = 0;i < newpassword_text.length();i++){
                            char n = newpassword_text.charAt(i);
                            String g = String.valueOf(n);

                            if(g.matches("A-Z")){
                                fixalphabet = 1;
                            }

                            //zz
                        }

                        if(!Library.containsUppercase(newpassword_text)){
                            Library.showToast(getContext(),"В строке должна быть хотя бы одна заглавная буква латинского алфавита");
                            return;
                        }


                        boolean onlyLatinAlphabet = newpassword_text.matches("^[a-zA-Z0-9]+$");
                        if(onlyLatinAlphabet == false){
                            Library.showToast(getContext(),"Текст должен быть написан латинскими буквами и иметь хотя бы одну цифру");
                            return;
                        }

                        progressBar.setVisibility(View.VISIBLE);
                        Retrofit client = Library.NewRetrofitclient();
                        Change_password_int change_password_int = client.create(Change_password_int.class);

                        SendChangePassword sendChangePassword = new SendChangePassword();
                        sendChangePassword.setUserId(userid);
                        sendChangePassword.setCurrentPassword(oldpassword);
                        sendChangePassword.setNewPassword(newpassword_text);

                        Call<ChangePasswordParser> changePasswordParserCall = change_password_int.sendRequest(sendChangePassword,token);

                        changePasswordParserCall.enqueue(new Callback<ChangePasswordParser>() {
                            @Override
                            public void onResponse(Call<ChangePasswordParser> call, Response<ChangePasswordParser> response) {

                                if(response.isSuccessful()){
                                    if(response.body().getIsSuccess() == true){
                                        Library.SavePhoneMemoryOne("password",newpassword_text);
                                        Library.showToast(getContext(),"Ваш пароль изменен!");
                                        progressBar.setVisibility(View.GONE);
                                        dialog.dismiss();

                                    }else{
                                        progressBar.setVisibility(View.GONE);
                                        Library.showToast(getContext(),"Текущий пароль неверен! проверьте вводимые данные");
                                    }
                                }else{
                                    progressBar.setVisibility(View.GONE);
                                    Library.showToast(getContext(),"Текущий пароль неверен! проверьте вводимые данные");
                                }



                            }

                            @Override
                            public void onFailure(Call<ChangePasswordParser> call, Throwable t) {
                                progressBar.setVisibility(View.GONE);
                            }
                        });

                    }
                });



                    buttonclose.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });


                // Setting dialogview
                Window window = dialog.getWindow();
                WindowManager.LayoutParams param = window.getAttributes();
                param.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
                window.setAttributes(param);
                window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                dialog.show();

            }
        });

        return dataBinding.getRoot();
    }



    public String getFormattedPhone(String phone) {
        phone = phone
                .replace("(", "")
                .replace(")", "")
                .replace(" ", "-");
        phone = phone.substring(0, 12) + "-" + phone.substring(12, phone.length());

        return phone;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        token = Library.GetPhoneMemoryOne("token");
        Download_flat();
        super.onViewCreated(view, savedInstanceState);
    }

    private void loadProfileImage(User user) {
        if (!isAdded()) return;

        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.ic_account_circle)
                .error(R.drawable.ic_account_circle);

        Glide.with(getActivity())
                .asBitmap()
                .apply(options)
                .load(user.getBitmapAva())
                .into(binding.get().profileImage);
    }





    public void Download_flat(){
        Retrofit client = Library.NewRetrofitclient();

        String contractid = Library.GetPhoneMemoryOne("contractId");

        GetFlat_int getFlat_int = client.create(GetFlat_int.class);
        Call<List<Get_flat_parser>> call = getFlat_int.getRequest(token,contractid);

        call.enqueue(new Callback<List<Get_flat_parser>>() {
            @Override
            public void onResponse(Call<List<Get_flat_parser>> call, Response<List<Get_flat_parser>> response) {

                Log.i("d","d" + response.body().toString());
                if(response.body().size() == 1){
                    textView.setText("ЖК  " + response.body().get(0).getRealEstate() +" " + response.body().get(0).getType() + " " +  response.body().get(0).getNumber());
                }

                if(response.body().size() == 2){
                    textView.setText("ЖК  " + response.body().get(1).getRealEstate()+" " +  response.body().get(1).getType() + "  " +  response.body().get(1).getNumber());
                    textView2.setText("ЖК  " + response.body().get(0).getRealEstate() +" " +  response.body().get(0).getType() + "  " +  response.body().get(0).getNumber());
                }

                if(response.body().size() == 3){
                    textView.setText("ЖК  " + response.body().get(1).getRealEstate() + " " +  response.body().get(1).getType() + " "+  response.body().get(1).getNumber());
                    textView2.setText("ЖК  " + response.body().get(0).getRealEstate() +" " +  response.body().get(0).getType() + "  " +  response.body().get(0).getNumber());
                    textView3.setText("ЖК  " + response.body().get(3).getRealEstate() + " " + response.body().get(3).getType() + "  " +  response.body().get(3).getNumber());
                }



            }

            @Override
            public void onFailure(Call<List<Get_flat_parser>> call, Throwable t) {

            }
        });


    }

}
