package com.zerotoonelabs.bigroup.ui.common;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.zerotoonelabs.bigroup.authui.biserviceauthoruzed.ActiveApplicationsFragment;
import com.zerotoonelabs.bigroup.authui.biserviceauthoruzed.ApplicationsHistoryFragment;
import com.zerotoonelabs.bigroup.authui.biserviceauthoruzed.ReportsFragment;

/**
 * Created by mac on 15.11.2017.
 */

public class AuthorizedBiServicePagerAdapter extends FragmentStatePagerAdapter {


    public AuthorizedBiServicePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;

        switch (position) {
            case 0:
                fragment = new ActiveApplicationsFragment();
                break;
            case 1:
                fragment = new ApplicationsHistoryFragment();
                break;
            default:
                fragment = new ReportsFragment();
                break;
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String s = null;

        switch (position) {
            case 0:
                s = "Заявки";
                break;
            case 1:
                s = "История";
                break;
            case 2:
                s = "Отчеты";
                break;

        }
        return s;
    }
}