package com.zerotoonelabs.bigroup.ui.biservice;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.BiServiceFragBinding;
import com.zerotoonelabs.bigroup.di.Injectable;
import com.zerotoonelabs.bigroup.ui.common.NavigationController;
import com.zerotoonelabs.bigroup.ui.common.ServiceRequestListAdapter;
import com.zerotoonelabs.bigroup.util.AutoClearedValue;

import javax.inject.Inject;

public class BiServiceFragment extends Fragment implements Injectable {
    TextView status;

    public static final String TOKEN_KEY = "token";
    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Inject
    NavigationController navigationController;

    private AutoClearedValue<ServiceRequestListAdapter> adapter;
    private AutoClearedValue<BiServiceFragBinding> binding;

    public BiServiceFragment() {
        // Required empty public constructor
    }

    public static BiServiceFragment newInstance(String token) {
        BiServiceFragment fragment = new BiServiceFragment();
        Bundle args = new Bundle();
        args.putString(TOKEN_KEY, token);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        BiServiceViewModel mViewModel = ViewModelProviders.of(this, viewModelFactory).get(BiServiceViewModel.class);

        Bundle args = getArguments();
        if (args != null && args.containsKey(TOKEN_KEY)) {
            mViewModel.setToken(args.getString(TOKEN_KEY));
        }

        ServiceRequestListAdapter adapter = new ServiceRequestListAdapter(serviceRequest -> {
            Intent intent = new Intent(getActivity(), BiServiceDetailActivity.class);
            intent.putExtra(BiServiceDetailActivity.REQUEST_EXTRA, serviceRequest);
            startActivity(intent);
        });

        this.adapter = new AutoClearedValue<>(this, adapter);
        binding.get().requestList.setAdapter(adapter);

        initList(mViewModel);

        binding.get().addNewRequest.setOnClickListener(view -> {
            navigationController.navigateToNewServiceRequest();
        });
    }

    private void initList(BiServiceViewModel viewModel) {
        viewModel.getServiceRequests().observe(this, resource -> {
            binding.get().setResultCount(resource == null || resource.data == null
                    ? 0
                    : resource.data.size());
            this.adapter.get().replace(resource == null ? null : resource.data);
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        BiServiceFragBinding dataBinding = DataBindingUtil
                .inflate(inflater, R.layout.bi_service_frag, container, false);
        binding = new AutoClearedValue<>(this, dataBinding);
        return dataBinding.getRoot();
    }

}
