package com.zerotoonelabs.bigroup.ui.sendapplication;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.SendServiceRequestFragBinding;
import com.zerotoonelabs.bigroup.di.Injectable;
import com.zerotoonelabs.bigroup.ui.BaseFragment;
import com.zerotoonelabs.bigroup.ui.common.ComplaintListAdapter;
import com.zerotoonelabs.bigroup.util.AutoClearedValue;
import com.zerotoonelabs.bigroup.entity.Complaint;
import com.zerotoonelabs.bigroup.entity.ServiceProperty;
import com.zerotoonelabs.bigroup.entity.ServiceRequest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import static android.app.Activity.RESULT_OK;

public class SendServiceRequestFragment extends BaseFragment implements Injectable {

    public static final int NOTE_REQUEST_CODE = 12;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private AutoClearedValue<SendServiceRequestFragBinding> binding;
    private AutoClearedValue<ComplaintListAdapter> adapter;
    AutoClearedValue<ArrayAdapter<ServiceProperty>> adapter1;
    private List<Complaint> complaintList;
    private SendServiceRequestViewModel mViewModel;

    public SendServiceRequestFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        SendServiceRequestFragBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.send_service_request_frag, container, false);
        dataBinding.setRetryCallback(() -> mViewModel.retry());
//        dataBinding.phoneTil.setError("8 701 234 5678");
        binding = new AutoClearedValue<>(this, dataBinding);
        return dataBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        String token = PreferenceManager.getDefaultSharedPreferences(getActivity())
                .getString("token", null);

        mViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(SendServiceRequestViewModel.class);

        mViewModel.setToken(token);

        setupAdapters();
        initObservables(mViewModel);

        binding.get().setRetryCallback(() -> mViewModel.retry());
    }

    private void initObservables(SendServiceRequestViewModel viewModel) {
        mViewModel.getServiceProperties().observe(this, listResource -> {
            if (listResource != null && listResource.data != null) {
                adapter1.get().addAll(listResource.data);
                adapter1.get().notifyDataSetChanged();
            }
        });
        mViewModel.getServiceRequest().observe(this, resource -> {
            if (resource != null && resource.data != null) {
                this.adapter.get().replace(Collections.emptyList());
                binding.get().setServiceRequest(null);
                showSuccessMessage();
            }
            binding.get().setResource(resource);
            binding.get().executePendingBindings();
        });

        mViewModel.getMessage().observe(this, resId -> {
            if (resId != null) {
                Toast.makeText(getActivity(), resId, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setupAdapters() {
        ArrayAdapter<ServiceProperty> adapter1 = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.adapter1 = new AutoClearedValue<>(this, adapter1);
        binding.get().jkSpinner.setAdapter(adapter1);

        complaintList = new ArrayList<>(0);
        ComplaintListAdapter adapter = new ComplaintListAdapter((complaint, position) -> {
            Intent intent = new Intent(getContext(), AddEditNoteActivity.class);
            intent.putExtra(AddEditNoteActivity.COMPLAINT_EXTRA, complaint);
            intent.putExtra(AddEditNoteActivity.POSITION_EXTRA, position);
            startActivityForResult(intent, NOTE_REQUEST_CODE);
        });

        this.adapter = new AutoClearedValue<>(this, adapter);
        adapter.replace(complaintList);
        binding.get().complaintList.setAdapter(adapter);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.send_complaint).setOnClickListener(v -> {
            dismissKeyboard(v.getWindowToken());
            sendServiceRequest();
        });
        view.findViewById(R.id.add_comment).setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), AddEditNoteActivity.class);
            int size = complaintList.size();
            intent.putExtra(AddEditNoteActivity.POSITION_EXTRA, size);
            startActivityForResult(intent, NOTE_REQUEST_CODE);
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == NOTE_REQUEST_CODE && resultCode == RESULT_OK) {
            int position = data.getIntExtra(AddEditNoteActivity.POSITION_EXTRA, 0);
            if (data.hasExtra(AddEditNoteActivity.COMPLAINT_EXTRA)) {
                Complaint complaint = data.getParcelableExtra(AddEditNoteActivity.COMPLAINT_EXTRA);
                if (position >= complaintList.size()) {
                    complaintList.add(complaint);
                    adapter.get().notifyItemInserted(position);
                } else {
                    complaintList.set(position, complaint);
                    adapter.get().notifyItemChanged(position);
                }
            } else {
                complaintList.remove(position);
                adapter.get().notifyItemRemoved(position);
            }
        }
    }

    private void sendServiceRequest() {
        String fullName = binding.get().fullName.getText().toString();
        String email = binding.get().email.getText().toString();
        String phone = binding.get().phone.getText().toString();
        String uid = binding.get().uid.getText().toString();
        String apartmentNo = binding.get().apartmentNo.getText().toString();
        ServiceProperty serviceProperty = (ServiceProperty) binding.get().jkSpinner.getSelectedItem();
        String object = serviceProperty.id;

        boolean validFields = mViewModel.checkFields(fullName, email, phone, uid, apartmentNo, object);
        if (!validFields) return;

        phone = getFormattedPhone(phone);

        ServiceRequest serviceRequest = new ServiceRequest(fullName, email, phone, uid, object, apartmentNo);
        serviceRequest.setComplaints(complaintList);

        mViewModel.setServiceRequest(serviceRequest);
    }

    private void showSuccessMessage() {
        if (isAdded()) {
            new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.dialog_visit_success_title)
                    .setMessage(R.string.dialog_visit_success_text)
                    .setPositiveButton(android.R.string.ok, null)
                    .create()
                    .show();
        }
    }

    private void dismissKeyboard(IBinder windowToken) {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(windowToken, 0);
        }
    }
}
