package com.zerotoonelabs.bigroup.ui.mortgage;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.di.Injectable;
import com.zerotoonelabs.bigroup.ui.common.NavigationController;
import com.zerotoonelabs.bigroup.ui.sendrequest.SendRequestFragment;

import javax.inject.Inject;

public class SberbankFragment extends Fragment {

    private FragmentManager fragmentManager;

    public SberbankFragment() {
        // Required empty public constructor
    }

    public static SberbankFragment newInstance(String param1, String param2) {
        SberbankFragment fragment = new SberbankFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentManager = getFragmentManager();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.sberbank_frag, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.button).setOnClickListener(v -> navigateToSendRequest());
    }

    public void navigateToSendRequest() {
        if (fragmentManager == null) return;
        SendRequestFragment fragment = new SendRequestFragment();
        String tag = "Оставить заявку";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(R.id.flContent, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }
}
