package com.zerotoonelabs.bigroup.ui.mortgage;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.di.Injectable;
import com.zerotoonelabs.bigroup.ui.common.NavigationController;
import com.zerotoonelabs.bigroup.ui.sendrequest.SendRequestFragment;

import javax.inject.Inject;

public class MortgageFragment extends Fragment {
    private OnFragmentInteractionListener mListener;

    private FragmentManager fragmentManager;

    public MortgageFragment() {
        // Required empty public constructor
    }

    public static MortgageFragment newInstance(String param1, String param2) {
        MortgageFragment fragment = new MortgageFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentManager = getFragmentManager();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.mortgage_frag, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.bank1).setOnClickListener(v -> navigateToSberbank());
        view.findViewById(R.id.bank2).setOnClickListener(v -> navigateToAtfbank());
        view.findViewById(R.id.bank3).setOnClickListener(v -> navigateToCenterCredit());

        view.findViewById(R.id.button).setOnClickListener(v -> navigateToSendRequest());
        ImageView imageView = view.findViewById(R.id.imageView);
        Glide.with(getActivity())
                .load(R.drawable.centercredit_logo)
                .into(imageView);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    public void navigateToSendRequest() {
        if (fragmentManager == null) return;
        SendRequestFragment fragment = new SendRequestFragment();
        String tag = "Оставить заявку";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(R.id.flContent, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    public void navigateToSberbank() {
        if (fragmentManager == null) return;
        SberbankFragment fragment = new SberbankFragment();
        String tag = "Сбербанк";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(R.id.flContent, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    public void navigateToAtfbank() {
        if (fragmentManager == null) return;
        AtfbankFragment fragment = new AtfbankFragment();
        String tag = "Atf Bank";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(R.id.flContent, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    public void navigateToCenterCredit() {
        if (fragmentManager == null) return;
        CentercreditFragment fragment = new CentercreditFragment();
        String tag = "CenterCredit Bank";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(R.id.flContent, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }
}
