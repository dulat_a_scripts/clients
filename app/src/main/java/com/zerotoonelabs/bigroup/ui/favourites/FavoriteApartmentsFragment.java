package com.zerotoonelabs.bigroup.ui.favourites;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.FavoriteApartmentsFragBinding;
import com.zerotoonelabs.bigroup.di.Injectable;
import com.zerotoonelabs.bigroup.ui.apartments.ApartmentDetailFragment;
import com.zerotoonelabs.bigroup.ui.common.ApartmentListAdapter;
import com.zerotoonelabs.bigroup.util.AutoClearedValue;
import com.zerotoonelabs.bigroup.entity.Apartments.Apartment;

import javax.inject.Inject;

public class FavoriteApartmentsFragment extends Fragment implements Injectable {

    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private FragmentManager fragmentManager;
    private AutoClearedValue<FavoriteApartmentsFragBinding> binding;

    public FavoriteApartmentsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentManager = getFragmentManager();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle args = getArguments();
        String token = null;
        if (args != null && args.containsKey(TOKEN_KEY)) {
            token = args.getString(TOKEN_KEY);
        }
        FavoritesViewModel viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(FavoritesViewModel.class);

        String finalToken = token;
        ApartmentListAdapter adapter = new ApartmentListAdapter(new ApartmentListAdapter.ApartmentClickCallback() {
            @Override
            public void onClick(Apartment apartment) {
                navigateToApartmentDetails(finalToken, apartment);
            }

            @Override
            public void onRemoveClick(Apartment apartment, int position) {
                viewModel.setApartmentId(apartment, position);
            }
        });
        binding.get().favoriteList.setAdapter(adapter);

        viewModel.getApartments().observe(this, apartments -> {
            binding.get().setResultCount(apartments == null ? 0 : apartments.size());
            adapter.replace(apartments);
            binding.get().executePendingBindings();
        });

        viewModel.getRemoveApartment().observe(this, integerResource -> {
//            binding.get().setSearchResource(integerResource);
//            if (integerResource != null && integerResource.data != null) {
//                adapter.notifyItemRemoved(integerResource.data);
//            }
//            binding.get().executePendingBindings();
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FavoriteApartmentsFragBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.favorite_apartments_frag,
                container, false);
        binding = new AutoClearedValue<>(this, dataBinding);
        return dataBinding.getRoot();
    }

    private void navigateToApartmentDetails(String token, Apartment apartment) {
        if (fragmentManager == null) return;
        ApartmentDetailFragment fragment = ApartmentDetailFragment.newInstance(token, apartment);
        String tag = apartment.getTitle();
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(R.id.flContent, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    private static final String TOKEN_KEY = "token";

    public static FavoriteApartmentsFragment newInstance(String token) {
        FavoriteApartmentsFragment fragment = new FavoriteApartmentsFragment();
        Bundle args = new Bundle();
        args.putString(TOKEN_KEY, token);
        fragment.setArguments(args);
        return fragment;
    }
}
