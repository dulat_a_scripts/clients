package com.zerotoonelabs.bigroup.ui.opendoor;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.zerotoonelabs.bigroup.repository.BiRepository;
import com.zerotoonelabs.bigroup.util.AbsentLiveData;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.Resource;
import com.zerotoonelabs.bigroup.entity.blockvisit.BlockVisit;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by mac on 08.11.2017.
 */

public class OpenDoorViewModel extends ViewModel {
    private final MutableLiveData<String> tokenID = new MutableLiveData<>();
    private final LiveData<Resource<List<BlockVisit>>> blockVisits;

    @Inject
    public OpenDoorViewModel(BiRepository repository) {
        blockVisits = Transformations.switchMap(tokenID, input -> {
            if (input.isEmpty()) {
                return AbsentLiveData.create();
            }
            return repository.loadBlockVisits(input);
        });

    }

    public LiveData<Resource<List<BlockVisit>>> getBlockVisits() {
        return blockVisits;
    }

    public void retry() {
        String current = tokenID.getValue();
        if (current != null && !current.isEmpty()) {
            tokenID.setValue(current);
        }
    }


    public void setToken(String token) {
        if (Objects.equals(tokenID.getValue(), token)) {
            return;
        }
        tokenID.setValue(token);
    }

}
