package com.zerotoonelabs.bigroup.ui.opendoor;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.ActivityOpenDoorBinding;
import com.zerotoonelabs.bigroup.entity.blockvisit.BlockVisit;

public class OpenDoorActivity extends AppCompatActivity {

    BlockVisit blockVisit;
    public static final String EXTRA_BLOCK = "blockvisit_extra";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityOpenDoorBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_open_door);


        Intent intent = getIntent();
        blockVisit = intent.getParcelableExtra(EXTRA_BLOCK);
        binding.jk.setText(blockVisit.getName());
        binding.setBlockvisit(blockVisit);
        binding.date.setText(blockVisit.getVisitDate().substring(0, 10));
        binding.description.setText("День открытых дверей " + blockVisit.getVisitDate().substring(0, 10));
        binding.address.setText(blockVisit.getAddress());
        binding.manager.setText(blockVisit.getHappinessManager());
        binding.telephone.setText(blockVisit.getHappinessManagerTel());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }
}
