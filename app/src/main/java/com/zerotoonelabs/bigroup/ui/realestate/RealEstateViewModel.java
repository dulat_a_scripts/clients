package com.zerotoonelabs.bigroup.ui.realestate;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.Nullable;

import com.zerotoonelabs.bigroup.repository.BiRepository;
import com.zerotoonelabs.bigroup.util.AbsentLiveData;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.RealEstate;
import com.zerotoonelabs.bigroup.entity.RealEstateFilter;
import com.zerotoonelabs.bigroup.entity.Resource;

import java.util.List;

import javax.inject.Inject;

public class RealEstateViewModel extends ViewModel {

    private final LiveData<Resource<List<RealEstate>>> realEstates;

    private final MutableLiveData<Filter> filterId = new MutableLiveData<>();

    private String token;

    private RealEstateFilter filter;

    @Inject
    public RealEstateViewModel(final BiRepository repository) {
        realEstates = Transformations.switchMap(filterId, input -> {
            if (input.isEmpty()) {
                return AbsentLiveData.create();
            }
            return repository.loadRealEstates(input.token, input.city, input.status);
        });
    }

    public LiveData<Resource<List<RealEstate>>> getRealEstates() {
        return realEstates;
    }

    void applyFilter(@Nullable RealEstateFilter realEstateFilter) {
        String city = null;
        int status = -1;
        if (realEstateFilter != null) {
            city = realEstateFilter.getCity();
            status = realEstateFilter.getStatus();
        }
        String token = this.token;
        Filter filter = new Filter(token, city, status);
        if (Objects.equals(filterId.getValue(), filter)) {
            return;
        }
        filterId.setValue(filter);
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public RealEstateFilter getFilter() {
        return filter;
    }

    public void setFilter(RealEstateFilter filter) {
        this.filter = filter;
    }

    static class Filter {
        String token;
        String city;
        int status;

        public Filter(String token, String city, int status) {
            this.token = token;
            this.city = city;
            this.status = status;
        }

        boolean isEmpty() {
            return token == null;
        }
    }
}