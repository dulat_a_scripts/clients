package com.zerotoonelabs.bigroup.ui.visitobject;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.repository.BiRepository;
import com.zerotoonelabs.bigroup.ui.common.SingleLiveEvent;
import com.zerotoonelabs.bigroup.util.AbsentLiveData;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.BsDate;
import com.zerotoonelabs.bigroup.entity.BsTime;
import com.zerotoonelabs.bigroup.entity.BuildingSite;
import com.zerotoonelabs.bigroup.entity.Resource;
import com.zerotoonelabs.bigroup.entity.VisitNumber;

import java.util.List;
import java.util.regex.Pattern;

import javax.inject.Inject;

import static android.text.TextUtils.*;

public class VisitObjViewModel extends ViewModel {

    private final MutableLiveData<String> token = new MutableLiveData<>();
    private final MutableLiveData<BsId> bsId;
    private final MutableLiveData<BsId> bsId2;
    private final MutableLiveData<VisitId> visitId;
    private final MutableLiveData<VisitNumberId> visitNumberId;

    private final LiveData<Resource<List<BuildingSite>>> buildingSites;
    private final LiveData<Resource<BsDate>> bsDate;
    private final LiveData<Resource<BsTime>> bsTime;
    private final LiveData<Resource<String>> visitObj;
    private final LiveData<Resource<List<VisitNumber>>> visitNumbers;

    private SingleLiveEvent<Integer> message = new SingleLiveEvent<>();

    private String propertyId;
    private String realEstateId;

    @Inject
    public VisitObjViewModel(BiRepository repository) {
        bsId = new MutableLiveData<>();
        bsId2 = new MutableLiveData<>();
        visitId = new MutableLiveData<>();
        visitNumberId = new MutableLiveData<>();

        buildingSites = Transformations.switchMap(token, input -> {
            if (isEmpty(input)) {
                return AbsentLiveData.create();
            }
            return repository.loadBuildingSites(input);
        });

        bsDate = Transformations.switchMap(bsId, input -> {
            if (input.isEmpty()) {
                return AbsentLiveData.create();
            }
            return repository.loadBsDates(input.bsId, input.token);
        });

        bsTime = Transformations.switchMap(bsId2, input -> {
            if (input.isEmpty()) {
                return AbsentLiveData.create();
            }
            return repository.loadBsTimes(input.bsId, input.date, input.token);
        });

        visitObj = Transformations.switchMap(visitId, input -> {
            if (input.isEmpty()) {
                return AbsentLiveData.create();
            }
            return repository.loadVisitObj(input.fullName, input.phone, input.objId, input.date, input.time, input.token, input.visitNumber);
        });

        visitNumbers = Transformations.switchMap(visitNumberId, input -> {
            if (input.isEmpty()) {
                return AbsentLiveData.create();
            }
            return repository.loadVisitNumbers(input.token, input.blockId);
        });
    }

    void setVisitNumberId(String blockId) {
        String token = this.token.getValue();
        VisitNumberId updae = new VisitNumberId(token, blockId);
        if (Objects.equals(visitNumberId.getValue(), updae)) {
            return;
        }
        visitNumberId.setValue(updae);
    }

    void setToken(String input) {
        if (Objects.equals(token.getValue(), input)) {
            return;
        }
        token.setValue(input);
    }

    void setBsId(String input) {
        String token = this.token.getValue();
        BsId update = new BsId(token, input, null);
//        if (Objects.equals(bsId.getValue(), update)) return;
        bsId.setValue(update);
    }

    void setBsId2(String date) {
        String token = this.token.getValue();
        if (bsId.getValue() != null && bsId.getValue().bsId != null) {
            String id = bsId.getValue().bsId;

            BsId update = new BsId(token, id, date);
            bsId2.setValue(update);
        }
    }

    LiveData<Resource<List<BuildingSite>>> getBuildingSites() {
        return buildingSites;
    }

    LiveData<Resource<BsDate>> getBsDate() {
        return bsDate;
    }

    LiveData<Resource<BsTime>> getBsTime() {
        return bsTime;
    }

    SingleLiveEvent<Integer> getMessage() {
        return message;
    }

    void setMessage(Integer resId) {
        if (resId != null)
            message.setValue(resId);
    }

    void setVisitId(BuildingSite object, String fullName, String phone, String date, String time, VisitNumber visitNumber) {
        String realEstateId = this.realEstateId;
        String propertyId = this.propertyId;

        boolean auth = realEstateId != null && propertyId != null;

        String token = this.token.getValue();
        VisitId update = new VisitId(realEstateId == null && object != null ? object.id : realEstateId,
                date, time, fullName, phone, token, propertyId == null && visitNumber != null ? visitNumber.getId() : propertyId);
//        if (isEmpty(visitNumber)) {
//            setMessage(R.string.invalid_visit_number);
//            return;
//        }
        if (isEmpty(date)) {
            setMessage(R.string.invalid_visit_date);
            return;
        }
        if (update.isEmpty()) {
            setMessage(R.string.visit_id_is_empty);
            return;
        }
        if (!auth && !isValidPhone(phone)) {
            setMessage(R.string.invalid_phone_message);
            return;
        }

        visitId.setValue(update);
    }

    LiveData<Resource<String>> getVisitObj() {
        return visitObj;
    }

    void retry() {
        if (visitId.getValue() != null) {
            visitId.setValue(visitId.getValue());
        }
    }

    LiveData<Resource<List<VisitNumber>>> getVisitNumbers() {
        return visitNumbers;
    }

    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public String getRealEstateId() {
        return realEstateId;
    }

    public void setRealEstateId(String realEstateId) {
        this.realEstateId = realEstateId;
    }

    static class VisitNumberId {
        String token;
        String blockId;

        public VisitNumberId(String token, String blockId) {
            this.token = token;
            this.blockId = blockId;
        }

        boolean isEmpty() {
            return token == null || blockId == null;
        }
    }

    static class VisitId {
        String objId;
        String date;
        String time;
        String fullName;
        String phone;
        String token;
        String visitNumber;

        public VisitId(String objId, String date, String time, String fullName, String phone, String token, String visitNumber) {
            this.objId = objId;
            this.date = date;
            this.time = time;
            this.fullName = fullName;
            this.phone = phone;
            this.token = token;
            this.visitNumber = visitNumber;
        }

        boolean isEmpty() {
            return TextUtils.isEmpty(objId) || TextUtils.isEmpty(date) || TextUtils.isEmpty(time)
                    || TextUtils.isEmpty(fullName) || TextUtils.isEmpty(phone) || TextUtils.isEmpty(token) || TextUtils.isEmpty(visitNumber);
        }
    }

    static class BsId {
        public String token;
        public String bsId;
        public String date;

        public BsId(String token, String bsId, String date) {
            this.token = token;
            this.bsId = bsId;
            this.date = date;
        }

        boolean isEmpty() {
            return token == null || bsId == null;
        }
    }

    private boolean isValidPhone(@NonNull String phone) {
//        return PHONE.matcher(phone).matches();
        return Pattern.matches("^[+]?[0-9]{10,11}$", phone.replaceAll("\\s+", ""));
    }
}
