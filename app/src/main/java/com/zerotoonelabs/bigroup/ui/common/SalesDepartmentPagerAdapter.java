package com.zerotoonelabs.bigroup.ui.common;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.zerotoonelabs.bigroup.authui.salesdepartment.SalesRequestsHistory;
import com.zerotoonelabs.bigroup.authui.salesdepartment.SendSalesRequestFragment;

/**
 * Created by mac on 14.11.2017.
 */

public class SalesDepartmentPagerAdapter extends FragmentStatePagerAdapter {

    private String token;

    public SalesDepartmentPagerAdapter(FragmentManager fm, String token) {
        super(fm);
        this.token = token;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;

        switch (position) {
            case 0:
                fragment = SendSalesRequestFragment.newInstance(token);
                break;
            case 1:
                fragment = SalesRequestsHistory.newInstance(token);
                break;
            default:
                fragment = SendSalesRequestFragment.newInstance(token);
                break;
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String s = null;

        if (position == 0) {
            s = "Заявка";
        } else {
            s = "История";
        }
        return s;
    }
}