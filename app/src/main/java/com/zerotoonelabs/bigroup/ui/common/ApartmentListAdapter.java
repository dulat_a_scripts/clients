package com.zerotoonelabs.bigroup.ui.common;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.FavApartmentItemBinding;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.Apartments.Apartment;

public class ApartmentListAdapter extends DataBoundListAdapter<Apartment, FavApartmentItemBinding> {

    private final ApartmentClickCallback apartmentClickCallback;

    public ApartmentListAdapter(ApartmentClickCallback apartmentClickCallback) {
        this.apartmentClickCallback = apartmentClickCallback;
    }

    @Override
    protected FavApartmentItemBinding createBinding(ViewGroup parent) {
        FavApartmentItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.fav_apartment_item,
                        parent, false);
        binding.getRoot().setOnClickListener(v -> {
            Apartment repo = binding.getApartment();
            if (repo != null && apartmentClickCallback != null) {
                apartmentClickCallback.onClick(repo);
            }
        });

        binding.imageButton.setOnClickListener(v -> {
            Apartment repo = binding.getApartment();
            int position = binding.getPosition();
            if (repo != null && apartmentClickCallback != null) {
                apartmentClickCallback.onRemoveClick(repo, position);
            }
        });
        return binding;
    }


    @Override
    protected void bind(FavApartmentItemBinding binding, Apartment item, int position) {
        binding.setPosition(position);
        binding.setApartment(item);
    }

    @Override
    protected boolean areItemsTheSame(Apartment oldItem, Apartment newItem) {
        return Objects.equals(oldItem.getId(), newItem.getId());
    }

    @Override
    protected boolean areContentsTheSame(Apartment oldItem, Apartment newItem) {
        return Objects.equals(oldItem.getId(), newItem.getId());
    }

    public interface ApartmentClickCallback {
        void onClick(Apartment apartment);
        void onRemoveClick(Apartment apartment, int position);
    }
}

