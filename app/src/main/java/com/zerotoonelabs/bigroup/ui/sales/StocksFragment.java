package com.zerotoonelabs.bigroup.ui.sales;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.StocksFragBinding;
import com.zerotoonelabs.bigroup.di.Injectable;
import com.zerotoonelabs.bigroup.ui.common.StockListAdapter;
import com.zerotoonelabs.bigroup.util.AutoClearedValue;

import javax.inject.Inject;

public class StocksFragment extends Fragment implements Injectable {

    private static final String TAB_KEY = "tab";
    @Inject
    ViewModelProvider.Factory viewModelFactory;

    AutoClearedValue<StocksFragBinding> binding;
    AutoClearedValue<StockListAdapter> adapter;

    private StockViewModel mViewModel;

    public StocksFragment() {
        // Required empty public constructor
    }

    public static StocksFragment newInstance(int tab) {
        StocksFragment fragment = new StocksFragment();
        Bundle args = new Bundle();
        args.putInt(TAB_KEY, tab);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(StockViewModel.class);
        Bundle args = getArguments();
        if (args != null) {
            int tab = args.getInt(TAB_KEY, 1);
            mViewModel.setTabId("1", tab);
        }

        StockListAdapter adapter = new StockListAdapter(
                stock -> {
                    Intent intent = new Intent(getActivity(), StockDetailActivity.class);
                    intent.putExtra(StockDetailActivity.STOCK_EXTRA, stock);
                    startActivity(intent);
                });

        this.adapter = new AutoClearedValue<>(this, adapter);
        binding.get().stockList.setAdapter(adapter);
        initStockList(mViewModel);
    }

    private void initStockList(StockViewModel viewModel) {
        try{
            viewModel.getStocks().observe(this, resource -> {
                try{
                    binding.get().setResource(resource);
                }catch (Exception e){

                }

                adapter.get().replace(resource == null ? null : resource.data);
                binding.get().executePendingBindings();
            });
        }catch (Exception e){

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        StocksFragBinding dataBinding = DataBindingUtil
                .inflate(inflater, R.layout.stocks_frag, container, false);
        dataBinding.setCallback(() -> mViewModel.retry());
        binding = new AutoClearedValue<>(this, dataBinding);
        return dataBinding.getRoot();
    }
}
