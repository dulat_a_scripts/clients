package com.zerotoonelabs.bigroup.ui;

import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.support.annotation.NonNull;

import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.repository.BiRepository;
import com.zerotoonelabs.bigroup.util.AbsentLiveData;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.Resource;

import javax.inject.Inject;

import static android.text.TextUtils.isEmpty;

public class MainViewModel extends AndroidViewModel {

    private final MutableLiveData<String> tokenId = new MutableLiveData<>();
    private final MutableLiveData<String> checkTokenId = new MutableLiveData<>();
    private final LiveData<Resource<String>> token;
    private final LiveData<Resource<Boolean>> validToken;
    private String rawToken;

    @Inject
    public MainViewModel(final android.app.Application application, final BiRepository repository) {
        super(application);
        token = Transformations.switchMap(tokenId, input -> {
            if (input.isEmpty()) {
                return AbsentLiveData.create();
            } else {
                return repository.loadTokenTemp(application, input);
            }
        });

        validToken = Transformations.switchMap(checkTokenId, input -> {
            if (isEmpty(input)) return AbsentLiveData.create();
            return repository.checkToken(input);
        });
    }

    void setTokenId(@NonNull String auth) {
        if (Objects.equals(tokenId.getValue(), auth)) {
            return;
        }

        tokenId.setValue(auth);
    }

    public LiveData<Resource<String>> getToken() {
        //Library.SavePhoneMemoryOne("token",token.toString());
        return token;
    }

    void setCheckTokenId(String token) {
        checkTokenId.setValue(token);
    }

    String getRawToken() {
        return rawToken;
    }

    void setRawToken(String rawToken) {
        this.rawToken = rawToken;
    }

    public LiveData<Resource<Boolean>> getValidToken() {
        return validToken;
    }

}
