package com.zerotoonelabs.bigroup.ui.news;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.ActivityNewsBinding;
import com.zerotoonelabs.bigroup.entity.News;

public class NewsActivity extends AppCompatActivity {

    private ActivityNewsBinding binding;
    private News news;
    private Context context;
    public static final String NEWS_EXTRA = "news";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_news);
        context = this;
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            news = extras.getParcelable(NEWS_EXTRA);
            binding.setNews(news);
        }

        initWebView();


    }

    private void setImageUri(ImageView imageView, String uri) {
        if (uri == null || uri.equals("")) return;
        Glide.with(context).load(uri).into(imageView);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void initWebView() {
        String body = news.getContent();
        for (int i = 0; i < body.length() - 7; i++) {
            String cur = body.substring(i, i + 8);
            if (cur.equals("img src=")) {
                if (body.charAt(i + 9) != 'h') {
                    body = body.substring(0, i + 9) + "http://sales.bi-group.org" + body.substring(i + 9);
                }
            }

        }
        body = "<html>" + body + "</html>";
        binding.webview.getSettings().setJavaScriptEnabled(true);
        binding.webview.loadDataWithBaseURL(null,"<style>img{display: inline;height: auto;max-width: 100%;}</style>" + body, "text/html", "utf-8", null);
    }
}
