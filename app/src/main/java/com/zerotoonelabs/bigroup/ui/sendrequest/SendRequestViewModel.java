package com.zerotoonelabs.bigroup.ui.sendrequest;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.repository.BiRepository;
import com.zerotoonelabs.bigroup.ui.common.SingleLiveEvent;
import com.zerotoonelabs.bigroup.util.AbsentLiveData;
import com.zerotoonelabs.bigroup.entity.Resource;

import java.util.regex.Pattern;

import javax.inject.Inject;

public class SendRequestViewModel extends ViewModel {

    private final MutableLiveData<RequestId> requestId;
    private final LiveData<Resource<String>> request;
    private SingleLiveEvent<Integer> message = new SingleLiveEvent<>();

    private String mToken;

    @Inject
    public SendRequestViewModel(BiRepository repository) {
        requestId = new MutableLiveData<>();
        request = Transformations.switchMap(requestId, input -> {
            if (input.isEmpty()) {
                return AbsentLiveData.create();
            }
            return repository.sendRequest(input.fullName, input.phone, input.site, input.token);
        });
    }

    public String getToken() {
        return mToken;
    }

    public void setToken(String mToken) {
        this.mToken = mToken;
    }

    void setRequestId(String fullName, String phone, String site) {
        String token = mToken;
        RequestId update = new RequestId(fullName, phone, site, token);
        if (update.isEmpty()) {
            setMessage(R.string.empty_fields_error_message);
            return;
        }

        if (!isValidFullName(fullName)) {
            setMessage(R.string.invalid_fullname_message2);
            return;
        }

        if (fullName.length() < 6) {
            setMessage(R.string.invalid_fullname_message);
            return;
        }

        if (phone.isEmpty() || phone.contains("—")) {
            setMessage(R.string.invalid_phone_message);
            return;
        }

        requestId.setValue(update);
    }

    public SingleLiveEvent<Integer> getMessage() {
        return message;
    }

    public void setMessage(Integer resId) {
        if (resId != null)
            message.setValue(resId);
    }

    LiveData<Resource<String>> getRequest() {
        return request;
    }

    static class RequestId {
        public String fullName;
        public String phone;
        public String site;
        public String token;

        public RequestId(String fullName, String phone, String site, String token) {
            this.fullName = fullName;
            this.phone = phone;
            this.site = site;
            this.token = token;
        }

        boolean isEmpty() {
            return TextUtils.isEmpty(fullName) || TextUtils.isEmpty(phone) || TextUtils.isEmpty(site) || TextUtils.isEmpty(token);
        }
    }

    private boolean isValidPhone(@NonNull String phone) {
//        return PHONE.matcher(phone).matches();
        return Pattern.matches("^[+]?[0-9]{10,11}$", phone.replaceAll("\\s+",""));
    }

    private boolean isValidFullName(@NonNull String fullName) {
        String regx = "^[\\p{L} .'-]+$";
        return Pattern.matches(regx, fullName);
    }
}