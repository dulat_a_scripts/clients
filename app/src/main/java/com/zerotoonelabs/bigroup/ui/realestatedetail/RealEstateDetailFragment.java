package com.zerotoonelabs.bigroup.ui.realestatedetail;


import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.RealEstateDetailFragBinding;
import com.zerotoonelabs.bigroup.di.Injectable;
import com.zerotoonelabs.bigroup.fragments.InstantPaymentFragment;
import com.zerotoonelabs.bigroup.ui.apartments.ApartmentFragment;
import com.zerotoonelabs.bigroup.ui.common.BlockListAdapter;
import com.zerotoonelabs.bigroup.ui.sendrequest.SendRequestFragment;
import com.zerotoonelabs.bigroup.util.AutoClearedValue;
import com.zerotoonelabs.bigroup.entity.RealEstate;

import javax.inject.Inject;

import static android.text.TextUtils.isEmpty;

/**
 * A simple {@link Fragment} subclass.
 */
public class RealEstateDetailFragment extends Fragment implements Injectable, OnMapReadyCallback {

    AutoClearedValue<RealEstateDetailFragBinding> binding;
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private FragmentManager fragmentManager;
    private SupportMapFragment mapFragment;
    RealEstateDetailViewModel mViewModel;

    AutoClearedValue<BlockListAdapter> adapter;

    private static final String REALESATE_ID_KEY = "realestate_id";
    private static final String REALESATE_KEY = "real_estate";

    public RealEstateDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        fragmentManager = getFragmentManager();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(RealEstateDetailViewModel.class);

        String token = PreferenceManager.getDefaultSharedPreferences(getActivity())
                .getString("token", null);
        Bundle args = getArguments();
        if (args != null && args.containsKey(REALESATE_ID_KEY))

        {
            String id = args.getString(REALESATE_ID_KEY);
            binding.get().setToken(token);
            mViewModel.setId(id, token);
            mViewModel.setRealEstateId(id);
        } else {
            mViewModel.setId(null, null);
        }

        BlockListAdapter adapter = new BlockListAdapter();

        this.adapter = new AutoClearedValue<>(this, adapter);
        binding.get().blockList.setNestedScrollingEnabled(false);
//        binding.get().blockList.setHasFixedSize(true);
        binding.get().blockList.setAdapter(adapter);

        initClickListeners(mViewModel);

    }


    private void initClickListeners(RealEstateDetailViewModel viewModel) {

        viewModel.getRealEstate().observe(this, realEstate -> {
            binding.get().setRealEstate(realEstate);
            if (realEstate != null) {
                mapFragment.getMapAsync(this);
            }
            binding.get().executePendingBindings();
        });

        viewModel.getBlocks().observe(this, listResource -> {
            adapter.get().replace(listResource == null ? null : listResource.data);
            binding.get().executePendingBindings();
        });

        binding.get().button4.setOnClickListener(v -> {
            openWebsite();
        });

        binding.get().button5.setOnClickListener(v -> navigateToSendRequest());
        binding.get().button3.setOnClickListener(v -> navigateToInstantPayment());
        binding.get().button2.setOnClickListener(v -> navigateToApartments(binding.get().getRealEstate().getId()));
    }

    private void openWebsite() {
        if (binding.get() == null || !isAdded()) {
            return;
        }
        RealEstate realEstate = binding.get().getRealEstate();
        if (realEstate != null && !isEmpty(realEstate.getWebsite())) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(realEstate.getWebsite()));
            if (browserIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(browserIntent);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        RealEstateDetailFragBinding dataBinding = DataBindingUtil
                .inflate(inflater, R.layout.real_estate_detail_frag, container, false);
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.imageView5);
        binding = new AutoClearedValue<>(this, dataBinding);
        return dataBinding.getRoot();
    }

    public static RealEstateDetailFragment newInstance(RealEstate realEstate) {
        RealEstateDetailFragment fragment = new RealEstateDetailFragment();
        Bundle args = new Bundle();
        args.putParcelable(REALESATE_KEY, realEstate);
        fragment.setArguments(args);
        return fragment;
    }

    public static RealEstateDetailFragment newInstance(String id) {
        RealEstateDetailFragment fragment = new RealEstateDetailFragment();
        Bundle args = new Bundle();
        args.putString(REALESATE_ID_KEY, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onMapReady(GoogleMap map) {
        RealEstate realEstate = binding.get().getRealEstate();
        if (realEstate == null) return;
        map.getUiSettings().setMyLocationButtonEnabled(false);
        double lon = realEstate.getLongitude();
        double lat = realEstate.getLatitude();
        map.addMarker(new MarkerOptions()
                .position(new LatLng(lat, lon)).title(realEstate.getName())
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                .draggable(false).visible(true));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 14));
    }

    public void navigateToSendRequest() {
        if (fragmentManager == null) return;
        SendRequestFragment fragment = new SendRequestFragment();
        String tag = "Оставить заявку";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(R.id.flContent, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    public void navigateToInstantPayment() {
        if (fragmentManager == null) return;
        InstantPaymentFragment fragment = new InstantPaymentFragment();
        String tag = "Стандартные условия";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(R.id.flContent, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    public void navigateToApartments(String id) {
        if (fragmentManager == null) return;
        ApartmentFragment fragment = ApartmentFragment.newInstance(id);
        String tag = "Подбор квартиры";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(R.id.flContent, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }
}
