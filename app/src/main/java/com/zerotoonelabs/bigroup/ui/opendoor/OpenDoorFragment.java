package com.zerotoonelabs.bigroup.ui.opendoor;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.FragmentOpenDoorBinding;
import com.zerotoonelabs.bigroup.di.Injectable;
import com.zerotoonelabs.bigroup.ui.common.OpenDoorListAdapter;
import com.zerotoonelabs.bigroup.util.AutoClearedValue;

import javax.inject.Inject;

public class OpenDoorFragment extends Fragment implements Injectable {

    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private OpenDoorViewModel mViewModel;

    AutoClearedValue<FragmentOpenDoorBinding> binding;
    AutoClearedValue<OpenDoorListAdapter> adapter;

    public OpenDoorFragment() {
        // Required empty public constructor
    }

    String token;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentOpenDoorBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_open_door, container, false);
        dataBinding.setCallback(() -> mViewModel.retry());
        binding = new AutoClearedValue<>(this, dataBinding);
        return dataBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(OpenDoorViewModel.class);
        token = PreferenceManager.getDefaultSharedPreferences(getContext()).getString("token", null);
        binding.get().setResultCount(-1);

        mViewModel.setToken(token);

        OpenDoorListAdapter adapter = new OpenDoorListAdapter(
                blockVisit -> {
                    Intent intent = new Intent(getActivity(), OpenDoorActivity.class);
                    intent.putExtra(OpenDoorActivity.EXTRA_BLOCK, blockVisit);
                    startActivity(intent);
                });

        this.adapter = new AutoClearedValue<>(this, adapter);
        binding.get().blockVisitList.setAdapter(adapter);
        initStockList(mViewModel);
    }

    private void initStockList(OpenDoorViewModel viewModel) {
        viewModel.getBlockVisits().observe(this, result -> {
            binding.get().setSearchResource(result);
            binding.get().setResultCount((result == null || result.data == null)
                    ? 0 : result.data.size());
            adapter.get().replace(result == null ? null : result.data);
            binding.get().executePendingBindings();
        });
    }
}