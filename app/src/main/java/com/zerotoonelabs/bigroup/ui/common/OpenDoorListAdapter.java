package com.zerotoonelabs.bigroup.ui.common;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.OpenDoorListItemBinding;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.blockvisit.BlockVisit;

public class OpenDoorListAdapter extends DataBoundListAdapter<BlockVisit, OpenDoorListItemBinding> {
    private final BlockVisitClickCallback blockVisitClickCallback;

    public OpenDoorListAdapter(BlockVisitClickCallback blockVisitClickCallback) {
        this.blockVisitClickCallback = blockVisitClickCallback;
    }

    @Override
    protected OpenDoorListItemBinding createBinding(ViewGroup parent) {
        OpenDoorListItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.open_door_list_item,
                        parent, false);
        binding.getRoot().setOnClickListener(v -> {
            BlockVisit repo = binding.getBlockvisits();
            if (repo != null && blockVisitClickCallback != null) {
                blockVisitClickCallback.onClick(repo);
            }
        });
        return binding;
    }

    @Override
    protected void bind(OpenDoorListItemBinding binding, BlockVisit item, int position) {
        binding.setBlockvisits(item);
        binding.date.setText(item.getVisitDate().substring(0, 10));
        binding.description.setText("День открытых дверей в " + item.getName());
    }


    @Override
    protected boolean areItemsTheSame(BlockVisit oldItem, BlockVisit newItem) {
        return Objects.equals(oldItem.getGuid(), newItem.getGuid());
    }

    @Override
    protected boolean areContentsTheSame(BlockVisit oldItem, BlockVisit newItem) {
        return Objects.equals(oldItem.getGuid(), newItem.getGuid());
    }

    public interface BlockVisitClickCallback {
        void onClick(BlockVisit blockVisit);
    }
}

