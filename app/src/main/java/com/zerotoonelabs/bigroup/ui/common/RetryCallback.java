package com.zerotoonelabs.bigroup.ui.common;

/**
 * Generic interface for retry buttons.
 */
public interface RetryCallback {
    void retry();
}