package com.zerotoonelabs.bigroup.ui.apartments;

import android.annotation.SuppressLint;
import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.Library.Retrofit_request.GetApartment;
import com.zerotoonelabs.bigroup.Library.Retrofit_request.GetBlocks;
import com.zerotoonelabs.bigroup.Library.Retrofit_request.GetCities;
import com.zerotoonelabs.bigroup.Library.Retrofit_request.GetHouse;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.api.ApiResponse;
import com.zerotoonelabs.bigroup.databinding.ApartmentFilterFragBinding;
import com.zerotoonelabs.bigroup.entity.ApartmentFilter;
import com.zerotoonelabs.bigroup.entity.Apartments.Apartment;
import com.zerotoonelabs.bigroup.entity.BuildingSite;
import com.zerotoonelabs.bigroup.util.AutoClearedValue;
import com.zerotoonelabs.bigroup.util.Objects;

import org.florescu.android.rangeseekbar.RangeSeekBar;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ApartmentFilterFragment extends Fragment{


    String token = null;
    ArrayList<String> citiesList = new ArrayList<>();
    ArrayList<String> houses = new ArrayList<>();
    ArrayList<String> houses_guids = new ArrayList<>();
    Spinner spinner;
    Spinner spinner2;
    Spinner spinner3;
    String city = null;
    String house = null;
    String housename = null;
    Button button;
    String kv = "1";
    String dokv = "5";
    RangeSeekBar rangeSeekBar;
    RangeSeekBar rangeSeekBar2;
    RangeSeekBar rangeSeekBaretazh;
    String pricemin = "1";
    String pricemax = "250000000";
    String Areamin = "0";
    String Areamax = "300";
    String etazhmin = "1";
    String etazhmax = "24";
    String srok = "Готов";
    CheckBox checkBoxkv;
    CheckBox checkBoxof;
    String isflat = "1";//ot 2
    String isoffice = "0";//ot 2
    AutoClearedValue<ApartmentFilterFragBinding> binding;
    private OnFilterAppliedListener mListener;
    ArrayList<String> kvlist = new ArrayList<String>();
    String block = null;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (OnFilterAppliedListener) getTargetFragment();
        } catch (Exception e) {

        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @BindViews({R.id.editText9,R.id.editText10,R.id.editText11,R.id.editText12,R.id.editText13,R.id.editText14}) List<EditText> listendittext;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        //inflater.inflate(R.menu.apartment_filter_menu, menu);
    }

    @BindView(R.id.objects)
    LinearLayout layout;

    @BindView(R.id.objects2)
    LinearLayout layout2;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.apartment_filter_frag, container, false);

        ApartmentFilterFragBinding dataBinding =
                DataBindingUtil.inflate(inflater, R.layout.apartment_filter_frag, container, false);
        binding = new AutoClearedValue<>(this, dataBinding);

        spinner = view.findViewById(R.id.cities_spinner);
        spinner2 = view.findViewById(R.id.house_spinner);
        spinner3 = view.findViewById(R.id.block_spinner);
        button = view.findViewById(R.id.button6);
        rangeSeekBar = view.findViewById(R.id.priceSeekBar);
        rangeSeekBar2 = view.findViewById(R.id.areaSeekBar);
        rangeSeekBaretazh = view.findViewById(R.id.floorSeekBar);
        checkBoxkv = view.findViewById(R.id.checkBox);
        checkBoxof = view.findViewById(R.id.checkBox2);




        ButterKnife.bind(this,view);



        rangeSeekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {//cash value
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar bar, Integer minValue, Integer maxValue) {

                String formatstring = SetFormattingString(String.valueOf(minValue));
                String formatstring2 = SetFormattingString(String.valueOf(maxValue));
                listendittext.get(0).setText(formatstring);
                listendittext.get(1).setText(formatstring2);
                pricemin = String.valueOf(minValue);
                pricemax = String.valueOf(maxValue);
            }
        });

        rangeSeekBar2.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {//cash value
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar bar, Integer minValue, Integer maxValue) {
                Areamin = String.valueOf(minValue);
                Areamax = String.valueOf(maxValue);
                listendittext.get(2).setText(String.valueOf(minValue));
                listendittext.get(3).setText(String.valueOf(maxValue));
            }
        });

        rangeSeekBaretazh.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {//cash value
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar bar, Integer minValue, Integer maxValue) {
                etazhmin = String.valueOf(minValue);
                etazhmax = String.valueOf(maxValue);
                listendittext.get(4).setText(String.valueOf(minValue));
                listendittext.get(5).setText(String.valueOf(maxValue));
            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                //if(position != 0){
                    GetHome(position);
                    city = citiesList.get(position);
                    layout.setVisibility(View.VISIBLE);
                    layout2.setVisibility(View.GONE);
              //  }

//                else {
//                    city = null;
//                    layout.setVisibility(View.GONE);
//                    layout2.setVisibility(View.GONE);
//                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position != 0){
                  house = houses_guids.get(position);
                  housename = houses.get(position);
                    layout2.setVisibility(View.GONE);
                    GetBlocks(houses_guids.get(position));
                }else{
                    house = null;
                    layout2.setVisibility(View.GONE);
                }

                Library.housename = housename;
//xx
                //  Log.i("house","house" + houses.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinner3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position != 0){
                    block = blockarray.get(position);
                }else{
                    block = null;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //kv or offise



                if(checkBoxkv.isChecked()){
                    isflat = "1";
                }

                if(checkBoxof.isChecked()){
                    isoffice = "1";
                }else{
                    isoffice = "0";
                }

                Integer checked_kv_of = 0;

                if(checkBoxkv.isChecked() && checkBoxof.isChecked()){
                    isflat = "1";
                    isoffice = "1";
                    checked_kv_of = 1;
                }
                //kv or offise
                //number kv
                //kv
                //number kv
                //area Areamin
                //etazh etazhmin
                //house Zhk
                //price pricemin
                //srok sdachi srok

                Retrofit client = Library.NewRetrofitclient();
                GetApartment getApartment = client.create(GetApartment.class);

                String token = Library.GetPhoneMemoryOne("token");

                ArrayList<String> returnar = MinMax(kvlist);

                String newkvString = "";

                for(int l = 0;l < kvlist.size();l++){

                    if(l == kvlist.size() - 1){
                        newkvString += kvlist.get(l);
                    }else{
                        newkvString += kvlist.get(l) + ",";
                    }

                }

                Library.savekvlist = newkvString;

                kv = returnar.get(0) + "," + returnar.get(1);
//zz
                HashMap<String, String> data = new HashMap<>();
                data.put("floor_min", etazhmin);
                data.put("floor_max", etazhmax);
                data.put("square_min", Areamin);
                data.put("square_max", Areamax);

                if(checked_kv_of == 0){
                    data.put("is_flat", isflat);
                    data.put("is_office", isoffice);
                }

                data.put("price_min", pricemin);
                data.put("price_max", pricemax);
                data.put("rooms_count", kv);

                if(city != null){
                    data.put("gorod",city);
                }

                if(house != null){
                    data.put("real_estate_guids", house);
                }

                if(city != null){
                    data.put("city", city);
                }

                String sroknakopitel = "";
                Integer fixgotov = 0;
                Integer fixdelivery = 0;

                for(int g = 0;g < srokarray.size();g++){
                    if(!srokarray.get(g).equals("Готов")){
                        if(g == 0 && srokarray.size() == 1){
                            sroknakopitel += srokarray.get(g);
                            fixdelivery = 1;
                        }else if(g == srokarray.size() - 1){
                            sroknakopitel += srokarray.get(g);
                            fixdelivery = 1;
                        }else{
                            sroknakopitel += srokarray.get(g) + ",";
                            fixdelivery = 1;
                        }

                    }else{
                        fixgotov = 1;
                    }
                }

                if(fixgotov == 1){
                    data.put("done", "1");
                    Library.fix_gotov = 1;
                }

                if(fixdelivery == 1){
                    data.put("deadline", sroknakopitel);
                    Library.srokarray = srokarray;
                }

                //done 1
                //        filter.put("sort_price", priceSort);
                //        filter.put("sort_square", areaSort);
                //http://webapi.bi-group.org/api/v1/salesbigroup/flats_all?
                // RoomsMoreThan=0&floor_max=24&floor_min=1&is_flat=0&is_office=0&page=1&price_max=250000000&
                // price_min=0&rooms_count=0&sort_price=asc&square_max=300&square_min=10

                //http://webapi.bi-group.org/api/v1/SalesBIGroup/Flats_all/?
                // rooms_count=1,3,2&square_min=50&square_max=60&floor_min=3&floor_max=6&is_flat
                // =true&is_office=true&deadline=4,6,2017,10,12,2017

                //http://webapi.bi-group.org/api/v1/salesbigroup/flats_all?
                // RoomsMoreThan=0&floor_max=24&floor_min=1&is_flat=0&is_office=0&page=1&price_max=250000000&
                // price_min=0&rooms_count=0&sort_price=asc&square_max=300&square_min=10

               // Log.i("d", "d" + returnar.toString());
                Library.data = data;
//                Observable<HashMap<String,String>> observable = Observable.just(data);
//                ApartmentFragment apartmentFragment = new ApartmentFragment();
//                observable.subscribe(apartmentFragment.filterobserver);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.popBackStack();



            }
        });

        return view;
    }




    public String SetFormattingString(String number){
        String newstring = "";
       // StringBuilder builder_string = new StringBuilder(number);
        if(number.length() == 4){

            char n = number.charAt(1);
            String str = String.valueOf(n) + " ";

            for(int i = 0;i < number.length();i++){
                if(i == 1){
                    newstring += str;
                }else{
                    newstring += number.charAt(i);
                }
            }

        }else if(number.length() == 9){
            char n = number.charAt(2);
            char b = number.charAt(5);
            String str = String.valueOf(n) + " ";
            String str2 = String.valueOf(b) + " ";

            for(int i = 0;i < number.length();i++){
                if(i == 2){
                    newstring += str;
                }else if(i == 5){
                    newstring += str2;
                }else{
                    newstring += number.charAt(i);
                }
            }
        }else if(number.length() == 7){
            char n = number.charAt(0);
            char b = number.charAt(3);
            String str = String.valueOf(n) + " ";
            String str2 = String.valueOf(b) + " ";
//xx
            for(int i = 0;i < number.length();i++){
                if(i == 0){
                    newstring += str;
                }else if(i == 3){
                    newstring += str2;
                }else{
                    newstring += number.charAt(i);
                }
            }
        }else if(number.length() == 8){
            char n = number.charAt(1);
            char b = number.charAt(4);
            String str = String.valueOf(n) + " ";
            String str2 = String.valueOf(b) + " ";

            for(int i = 0;i < number.length();i++){
                if(i == 1){
                    newstring += str;
                }else if(i == 4){
                    newstring += str2;
                }else{
                    newstring += number.charAt(i);
                }
            }
        }else{

            try{
                char n = number.charAt(1);
                String str = String.valueOf(n) + " ";

                for(int i = 0;i < number.length();i++){
                    if(i == 1){
                        newstring += str;
                    }else{
                        newstring += number.charAt(i);
                    }
                }
            }catch (Exception e){

            }



        }
        Log.i("length","length" + number.length());
        //newstring = builder_string.toString();
        return newstring;
    }

    ArrayList<String> blockarray = new ArrayList<>();

    public void GetBlocks(String house_guids){
//xx
        Retrofit client = Library.NewRetrofitclient();

        GetBlocks getBlocks = client.create(GetBlocks.class);

        Call<List<BuildingSite>> call = getBlocks.getBlocksFilter(token,house_guids);

        call.enqueue(new Callback<List<BuildingSite>>() {
            @Override
            public void onResponse(Call<List<BuildingSite>> call, Response<List<BuildingSite>> response) {

                blockarray = new ArrayList<>();
                blockarray.add("Все Блоки");

                try{
                    for(int i = 0;i < response.body().size();i++){
                        blockarray.add(response.body().get(i).name);
                    }
                    layout2.setVisibility(View.VISIBLE);
                }catch (Exception e){

                }


                ArrayAdapter<String> blockadapter = new ArrayAdapter<String>(getActivity(),
                        android.R.layout.simple_spinner_dropdown_item, blockarray);
                spinner3.setAdapter(blockadapter);
            }

            @Override
            public void onFailure(Call<List<BuildingSite>> call, Throwable t) {

            }
        });



    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//zz
        HideButtons();
        RestoreButtons();
    }



    public void RestoreButtons(){

        if(Library.data != null){

            Integer fmin = 0;
            Integer fmax = 0;
            Integer fminarea = 0;
            Integer fmaxarea = 0;
            Integer pmin = 0;
            Integer pmax = 0;
            ArrayList<String> roomscount_ar = new ArrayList<>();
            Integer fix_deadline = 0;
            for(Map.Entry<String, String> entry : Library.data.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();


                switch (key){
                    case "floor_min":
                        fmin = Integer.parseInt(value);
                        break;
                    case "gorod":
                        city = value;
                        break;
                    case "floor_max":
                        fmax = Integer.parseInt(value);
                        break;

                    case "square_min":
                        fminarea = Integer.parseInt(value);
                        break;

                    case "square_max":
                        fmaxarea = Integer.parseInt(value);
                        break;

                    case "is_flat":
                        if(value.equals("1")){
                            checkBoxkv.setChecked(true);
                        }else{
                            checkBoxkv.setChecked(false);
                        }
                        break;
//xx
                        case "deadline":
                            fix_deadline = 1;
                        break;

                    case "is_office":
                        if(value.equals("1")){
                            checkBoxof.setChecked(true);
                        }else{
                            checkBoxof.setChecked(false);
                        }
                        break;

                    case "price_min":
                        pmin = Integer.parseInt(value);
                        break;

                    case "price_max":
                        pmax = Integer.parseInt(value);
                        break;

                    case "rooms_count":
                        if(Library.savekvlist != null){
                            if(Library.savekvlist.length() > 1){
                                roomscount_ar = new ArrayList<String>(Arrays.asList(Library.savekvlist.split(",")));
                                for(int j = 0;j < roomscount_ar.size();j++){
                                    kvlist.add(roomscount_ar.get(j));
                                    ChangeColor_Button(buttons.get(Integer.parseInt(roomscount_ar.get(j)) - 1),1);
                                }
                            }
                        }

                        break;
                }

            }

            if(fix_deadline == 1){
                if(Library.srokarray != null){


                    for(int g = 0;g < Library.srokarray.size();g++){

                        //xx
                        switch (Library.srokarray.get(g)){
                            case "Готов":
                                ChangeColor_Buttonsrok(selectssrok.get(0),1);
                                srokarray.add("Готов");
                                break;

                            case "4,6,2018":
                                ChangeColor_Buttonsrok(selectssrok.get(1),1);
                                srokarray.add("4,6,2018");
                                break;

                            case "7,9,2018":
                                ChangeColor_Buttonsrok(selectssrok.get(2),1);
                                srokarray.add("7,9,2018");
                                break;

                            case "8,12,2018":
                                ChangeColor_Buttonsrok(selectssrok.get(3),1);
                                srokarray.add("8,12,2018");
                                break;

                            case "1,3,2019":
                                ChangeColor_Buttonsrok(selectssrok.get(4),1);
                                srokarray.add("1,3,2019");
                                break;

                            case "4,6,2019":
                                ChangeColor_Buttonsrok(selectssrok.get(5),1);
                                srokarray.add("4,6,2019");
                                break;

                        }

                    }


                }
            }

            if(Library.fix_gotov == 1){
                ChangeColor_Buttonsrok(selectssrok.get(0),1);
                srokarray.add("Готов");
            }

                 listendittext.get(0).setText(String.valueOf(pmin));
                 listendittext.get(1).setText(String.valueOf(pmax));
                pricemin = String.valueOf(pmin);
                pricemax = String.valueOf(pmax);

                listendittext.get(2).setText(String.valueOf(fminarea));
                listendittext.get(3).setText(String.valueOf(fmaxarea));
                 Areamin = String.valueOf(fminarea);
                 Areamax = String.valueOf(fmaxarea);
                listendittext.get(4).setText(String.valueOf(fmin));
                listendittext.get(5).setText(String.valueOf(fmax));
                etazhmin = String.valueOf(fmin);
                etazhmax = String.valueOf(fmax);

        }

    }

    @OnClick(R.id.refresh)
    public void Refresh(){
        ResetButtons();
    }

    public void ResetButtons(){



            checkBoxkv.setChecked(false);
            checkBoxof.setChecked(false);

            Library.data = new HashMap<>();


            for(int h = 0;h < buttons.size();h++){
                ChangeColor_Button(buttons.get(h),0);
            }

            kvlist = new ArrayList<>();
            Library.savekvlist = null;


                pricemin = "1";
                pricemax = "250000000";
                Areamin = "0";
                Areamax = "300";
                etazhmin = "1";
                etazhmax = "24";
                srok = "Готов";
                isflat = "1";//ot 2
                isoffice = "0";//ot 2

                listendittext.get(0).setText(String.valueOf(pricemin));
                listendittext.get(1).setText(String.valueOf(pricemax));
                listendittext.get(2).setText(String.valueOf(Areamin));
                listendittext.get(3).setText(String.valueOf(Areamax));
                listendittext.get(4).setText(String.valueOf(etazhmin));
                listendittext.get(5).setText(String.valueOf(etazhmax));

                spinner.setSelection(0);
                spinner2.setSelection(0);
                city = null;
                Library.housename = null;
                house = null;
                block = null;
                houses = new ArrayList<>();
                houses_guids = new ArrayList<>();
                blockarray = new ArrayList<>();
                srokarray = new ArrayList<>();

                for(int g = 0;g < selectssrok.size();g++){
                    ChangeColor_Buttonsrok(selectssrok.get(g),0);
                }

        Library.srokarray = new ArrayList<>();
        srokarray = new ArrayList<>();
        Library.fix_gotov = 0;

        for(int h = 0;h < selectssrok.size();h++){
            ChangeColor_Buttonsrok(selectssrok.get(h),0);
        }


    }

    public ArrayList<String> MinMax(ArrayList<String> arrayList){

        Integer min = 10,max = 1,savenum = 0;
        for(int i = 0;i < arrayList.size();i++){
            if(Integer.parseInt(arrayList.get(i)) > max){
                max = Integer.parseInt(arrayList.get(i));
            }

            if(Integer.parseInt(arrayList.get(i)) < min){
                min = Integer.parseInt(arrayList.get(i));
            }
        }

        if(max == 1){
            min = 1;
        }
        ArrayList<String> newar = new ArrayList<>();
        newar.add(min.toString());
        newar.add(max.toString());
        return newar;

    }

    private String checkDeadlines(boolean d1, boolean d2, boolean d3, boolean d4, boolean d5, boolean d6) {
        StringBuilder result = new StringBuilder();
        List<String> deadlines = new ArrayList<>();
        if (d1) deadlines.add("done");
        if (d2) deadlines.add("4,6,2018");
        if (d3) deadlines.add("7,9,2018");
        if (d4) deadlines.add("10,12,2018");
        if (d5) deadlines.add("1,3,2019");
        if (d6) deadlines.add("4,6,2019");

        for (int i = 0; i < deadlines.size(); i++) {
            if (i == deadlines.size() - 1) {
                result.append(deadlines.get(i));
            } else {
                result.append(deadlines.get(i)).append(",");
            }
        }
        return result.toString();
    }








    //datablock
    @BindViews({R.id.buttonCheck9,R.id.buttonCheck10,R.id.buttonCheck11,R.id.buttonCheck12,R.id.buttonCheck14,R.id.buttonCheck13})
    List<Button> selectssrok;



    public void ChangeColor_Buttonsrok(Button button,Integer number){

                if(number == 0){
                    button.setBackgroundResource(R.drawable.corner_button_white);
                    button.setTextColor(getResources().getColor(R.color.black));
                }else{
                    button.setBackgroundResource(R.drawable.corner_button);
                    button.setTextColor(getResources().getColor(R.color.white));
                }
    }
    Integer fixgotov = 0;
    ArrayList<String> srokarray = new ArrayList<>();

    @OnClick(R.id.buttonCheck9)
    public void one(){

        int fixg = 0;
                String kvartalnameg = "Готов";
                for(int i = 0;i < srokarray.size();i++){
                    if(srokarray.get(i).equals(kvartalnameg)){
                        fixg = 1;
                    }
                }

                if(fixg == 1){
                    ChangeColor_Buttonsrok(selectssrok.get(0),0);
                    Library.fix_gotov = 0;
                    srokarray.remove(kvartalnameg);
                }else{
                    ChangeColor_Buttonsrok(selectssrok.get(0),1);
                    Library.fix_gotov = 1;
                    srokarray.add(kvartalnameg);
                }

    };

    @OnClick(R.id.buttonCheck10)

    public void two(){
        int fix = 0;
                String kvartalname = "4,6,2018";
                for(int i = 0;i < srokarray.size();i++){
                    if(srokarray.get(i).equals(kvartalname)){
                        fix = 1;
                    }
                }

                if(fix == 1){
                    ChangeColor_Buttonsrok(selectssrok.get(1),0);
                    srokarray.remove(kvartalname);
                }else{
                    ChangeColor_Buttonsrok(selectssrok.get(1),1);
                    srokarray.add(kvartalname);
                }
    }

     @OnClick(R.id.buttonCheck11)

     public void three(){
        int fix2 = 0;
                String kvartalname2 = "7,9,2018";
                for(int i = 0;i < srokarray.size();i++){
                    if(srokarray.get(i).equals(kvartalname2)){
                        fix2 = 1;
                    }
                }

                if(fix2 == 1){
                    ChangeColor_Buttonsrok(selectssrok.get(2),0);
                    srokarray.remove(kvartalname2);
                }else{
                    ChangeColor_Buttonsrok(selectssrok.get(2),1);
                    srokarray.add(kvartalname2);
                }
     }

     @OnClick(R.id.buttonCheck12)

     public void four(){
        int fix3 = 0;
                String kvartalname3 = "8,12,2018";
                for(int i = 0;i < srokarray.size();i++){
                    if(srokarray.get(i).equals(kvartalname3)){
                        fix3 = 1;
                    }
                }

                if(fix3 == 1){
                    ChangeColor_Buttonsrok(selectssrok.get(3),0);
                    srokarray.remove(kvartalname3);
                }else{
                    ChangeColor_Buttonsrok(selectssrok.get(3),1);
                    srokarray.add(kvartalname3);
                }
        }

        @OnClick(R.id.buttonCheck14)

        public void five(){
            int fix4 = 0;
                String kvartalname4 = "1,3,2019";
                for(int i = 0;i < srokarray.size();i++){
                    if(srokarray.get(i).equals(kvartalname4)){
                        fix4 = 1;
                    }
                }

                if(fix4 == 1){
                    ChangeColor_Buttonsrok(selectssrok.get(4),0);
                    srokarray.remove(kvartalname4);
                }else{
                    ChangeColor_Buttonsrok(selectssrok.get(4),1);
                    srokarray.add(kvartalname4);
                }
        }

        @OnClick(R.id.buttonCheck13)

        public void six(){

        int fix5 = 0;
                String kvartalname5 = "4,6,2019";
                for(int i = 0;i < srokarray.size();i++){
                    if(srokarray.get(i).equals(kvartalname5)){
                        fix5 = 1;
                    }
                }

                if(fix5 == 1){
                    ChangeColor_Buttonsrok(selectssrok.get(5),0);
                    srokarray.remove(kvartalname5);
                }else{
                    ChangeColor_Buttonsrok(selectssrok.get(5),1);
                    srokarray.add(kvartalname5);
                }

        }


    //select srok sdachi

//select room number


    public void ChangeColor_Button(Button button,Integer number){

        if(number == 1){
            button.setBackgroundResource(R.drawable.corner_button);
            button.setTextColor(getResources().getColor(R.color.white));
        }else{
            button.setBackgroundResource(R.drawable.corner_button_white);
            button.setTextColor(getResources().getColor(R.color.black));
        }
    }

    @BindViews({R.id.buttonCheck3, R.id.buttonCheck4, R.id.buttonCheck5,R.id.buttonCheck6,R.id.buttonCheck8 }) List<Button> buttons;
//buttons

@OnClick(R.id.buttonCheck3)

public void h1(){
    kv = "1";
                Integer fix = 0;
                int position = 0;
                for(int i = 0;i < kvlist.size();i++){
                    if(kvlist.get(i).equals("1")){
                        fix = 1;
                        position = i;
                    }
                }
                if(fix == 1){
                    kvlist.remove(position);
                    ChangeColor_Button(buttons.get(0),0);
                }else{
                    kvlist.add(kv);
                    ChangeColor_Button(buttons.get(0),1);
                }
}

@OnClick(R.id.buttonCheck4)

public void h2(){
    kv = "2";
                Integer fix2 = 0;
                int position2 = 0;
                for(int i = 0;i < kvlist.size();i++){
                    if(kvlist.get(i).equals("2")){
                        fix2 = 1;
                        position2 = i;
                    }
                }
                if(fix2 == 1){
                    kvlist.remove(position2);
                    ChangeColor_Button(buttons.get(1),0);
                }else{
                    kvlist.add(kv);
                    ChangeColor_Button(buttons.get(1),1);
                }
}

@OnClick(R.id.buttonCheck5)

public void h3(){
    kv = "3";
                Integer fix3 = 0;
                int position3 = 0;
                for(int i = 0;i < kvlist.size();i++){
                    if(kvlist.get(i).equals("3")){
                        fix3 = 1;
                        position3 = i;
                    }
                }
                if(fix3 == 1){
                    kvlist.remove(position3);
                    ChangeColor_Button(buttons.get(2),0);
                }else{
                    kvlist.add(kv);
                    ChangeColor_Button(buttons.get(2),1);
                }
}

@OnClick(R.id.buttonCheck6)

public void h4(){
    kv = "4";
                Integer fix4 = 0;
                int position4 = 0;
                for(int i = 0;i < kvlist.size();i++){
                    if(kvlist.get(i).equals("4")){
                        fix4 = 1;
                        position4 = i;
                    }
                }
                if(fix4 == 1){
                    kvlist.remove(position4);
                    ChangeColor_Button(buttons.get(3),0);
                }else{
                    kvlist.add(kv);
                    ChangeColor_Button(buttons.get(3),1);
                }
}

@OnClick(R.id.buttonCheck8)

    public void h5(){
    kv = "5";
                Integer fix5 = 0;
                int position5 = 0;
                for(int i = 0;i < kvlist.size();i++){
                    if(kvlist.get(i).equals("5")){
                        fix5 = 1;
                        position5 = i;
                    }
                }
                if(fix5 == 1){
                    kvlist.remove(position5);
                    ChangeColor_Button(buttons.get(4),0);
                }else{
                    kvlist.add(kv);
                    ChangeColor_Button(buttons.get(4),1);
                }
        }




    @Override
    public void onResume() {
        GetCityes();

        Showmoretextstatus();
        super.onResume();
    }

    public void GetHome(Integer id){

        Retrofit client = Library.NewRetrofitclient();
        GetHouse getHouse = client.create(GetHouse.class);
        token = Library.GetPhoneMemoryOne("token");

        //city = citiesList.get(id);

        Call<List<BuildingSite>> listCall = getHouse.getBuildingSites(token,citiesList.get(id));
//zz
        listCall.enqueue(new Callback<List<BuildingSite>>() {
            @Override
            public void onResponse(Call<List<BuildingSite>> call, Response<List<BuildingSite>> response) {

                if(response.body().size() > 0){

                    houses = new ArrayList<>();
                    houses.add("Все ЖК");
                    houses_guids.add("Все ЖК");

                    Integer positionhouse = 0;
                    Integer fixh = 0;

                    try{
                        for(int k = 0;k < response.body().size();k++){
                            houses.add(response.body().get(k).name);
                            houses_guids.add(response.body().get(k).id);

                            if(Library.housename != null){
                                if(response.body().get(k).name.equals(Library.housename)){
                                    positionhouse = k + 1;
                                    fixh = 1;
                                }
                            }
                        }
                    }catch (Exception e){

                    }


                    ArrayAdapter<String> houseadapter = new ArrayAdapter<String>(getActivity(),
                            android.R.layout.simple_spinner_dropdown_item, houses);

                    spinner2.setAdapter(houseadapter);

                    spinner2.setSelection(positionhouse);

                }


            }

            @Override
            public void onFailure(Call<List<BuildingSite>> call, Throwable t) {

            }
        });

    }

    public void GetCityes(){

        Retrofit client = Library.NewRetrofitclient();
        GetCities getCities = client.create(GetCities.class);
        token = Library.GetPhoneMemoryOne("token");
        Call<List<String>> listCall = getCities.getCitiess(token);

        listCall.enqueue(new Callback<List<String>>() {
            @Override
            public void onResponse(Call<List<String>> call, Response<List<String>> response) {

               // Log.i("resp","resp" + response.body().toString());

                Integer position = 0;
                Integer fix = 0;

                if(response.body().size() > 0){
                   // citiesList.add("Все города");
                    for(int h = 0;h < response.body().size();h++){
                        citiesList.add(response.body().get(h));
                        if(city != null){
                            if(response.body().get(h).equals(city)){
                                position = h;
                                fix = 1;
                            }

                        }
                    }

                    ArrayAdapter<String> citiesadapter = new ArrayAdapter<String>(getActivity(),
                            android.R.layout.simple_spinner_dropdown_item, citiesList);

                    spinner.setAdapter(citiesadapter);

                    if(fix == 1){ //set index city if exist city
                        try{
                            spinner.setSelection(position);
                        }catch (Exception e){

                        }

                    }

                }

//                ArrayAdapter<BuildingSite> adapter2 = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item);
//                adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                this.adapter2 = new AutoClearedValue<>(this, adapter2);
//                binding.get().jkSpinner.setAdapter(adapter2);



            }

            @Override
            public void onFailure(Call<List<String>> call, Throwable t) {

            }
        });

    }

    public interface OnFilterAppliedListener {
        void applyFilter(HashMap<String, String> filter, ApartmentFilter apartmentFilter);
    }

    @BindViews({R.id.textView103,R.id.textView43,R.id.textView42}) List<TextView> textviewar;

    @BindViews({R.id.editText13,R.id.editText14}) List<EditText> editTexts;
    @BindView(R.id.floorSeekBar) RangeSeekBar rangeSeekBarlatest;

    @BindViews({R.id.floorSeekBar,R.id.areaSeekBar,R.id.priceSeekBar}) List<RangeSeekBar> rangearray;

    @BindView(R.id.showMoreText)
    TextView textView;

    Integer fixclick = 0;
    @OnClick(R.id.showMoreText)
    public void Showmoretext(View v){
        if(Library.filter_indicator == 0){
            for(int i = 0;i < selectssrok.size();i++){
                selectssrok.get(i).setVisibility(View.VISIBLE);
            }
            rangeSeekBarlatest.setVisibility(View.VISIBLE);

            for(int y = 0;y < textviewar.size();y++){
                textviewar.get(y).setVisibility(View.VISIBLE);
            }

            for(int x = 0;x < editTexts.size();x++){
                editTexts.get(x).setVisibility(View.VISIBLE);
            }

            textView.setText("Скрыть");


            Library.filter_indicator = 1;
        }else{
            for(int i = 0;i < selectssrok.size();i++){
                selectssrok.get(i).setVisibility(View.GONE);
            }
            rangeSeekBarlatest.setVisibility(View.GONE);

            for(int y = 0;y < textviewar.size();y++){
                textviewar.get(y).setVisibility(View.GONE);
            }

            for(int x = 0;x < editTexts.size();x++){
                editTexts.get(x).setVisibility(View.GONE);
            }

            textView.setText("Больше параметров");

            Library.filter_indicator = 0;
        }

    }



    public void Showmoretextstatus(){
        if(Library.filter_indicator == 1){


            for(int i = 0;i < selectssrok.size();i++){
                selectssrok.get(i).setVisibility(View.VISIBLE);
            }
            rangeSeekBarlatest.setVisibility(View.VISIBLE);

            for(int y = 0;y < textviewar.size();y++){
                textviewar.get(y).setVisibility(View.VISIBLE);
            }

            for(int x = 0;x < editTexts.size();x++){
                editTexts.get(x).setVisibility(View.VISIBLE);
            }

            textView.setText("Скрыть");


        }else{

            for(int i = 0;i < selectssrok.size();i++){
                selectssrok.get(i).setVisibility(View.GONE);
            }
            rangeSeekBarlatest.setVisibility(View.GONE);

            for(int y = 0;y < textviewar.size();y++){
                textviewar.get(y).setVisibility(View.GONE);
            }

            for(int x = 0;x < editTexts.size();x++){
                editTexts.get(x).setVisibility(View.GONE);
            }

            textView.setText("Больше параметров");

        }

    }

    public void HideButtons(){
        for(int i = 0;i < selectssrok.size();i++){
            selectssrok.get(i).setVisibility(View.GONE);
        }
        rangeSeekBarlatest.setVisibility(View.GONE);

        for(int y = 0;y < textviewar.size();y++){
            textviewar.get(y).setVisibility(View.GONE);
        }

        for(int x = 0;x < editTexts.size();x++){
            editTexts.get(x).setVisibility(View.GONE);
        }

        textView.setText("Больше параметров");
    }




    private void navigateBack() {



//        ApartmentFragment apartmentFragment = new ApartmentFragment();
//
//        FragmentManager fragmentManager = getFragmentManager();
//        fragmentManager.beginTransaction()
//                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
//                .add(R.id.flContent, apartmentFragment).commit();
    }
}
