package com.zerotoonelabs.bigroup.ui.common;

import android.arch.paging.PagedListAdapter;
import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.DiffCallback;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.databinding.ApartmentItemBinding;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.Apartments.Apartment;

public class ApartmentPagedListAdapter extends PagedListAdapter<Apartment, ApartmentPagedListAdapter.ApartmentViewHolder> {

    private final ApartmentClickCallback apartmentClickCallback;
    private final String token;

    public ApartmentPagedListAdapter(ApartmentClickCallback apartmentCLickCallback, String token) {
        super(DIFF_CALLBACK);
        this.apartmentClickCallback = apartmentCLickCallback;
        this.token = token;
    }

    @Override
    public ApartmentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(parent.getContext());
        ApartmentItemBinding itemBinding =
                ApartmentItemBinding.inflate(layoutInflater, parent, false);

        itemBinding.getRoot().setOnClickListener(v -> {
            Apartment apartment = itemBinding.getApartment();
            if (apartment != null && apartmentClickCallback != null) {
                apartmentClickCallback.onClick(apartment);
            }
        });


        return new ApartmentViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(ApartmentViewHolder holder, int position) {
        Apartment apartment = getItem(position);
        holder.bind(apartment, token);
    }

    private static final DiffCallback<Apartment> DIFF_CALLBACK = new DiffCallback<Apartment>() {
        @Override
        public boolean areItemsTheSame(@NonNull Apartment oldApartment, @NonNull Apartment newApartment) {
            // Apartment properties may have changed if reloaded from the DB, but ID is fixed
            return Objects.equals(oldApartment.getId(), newApartment.getId());
        }

        @Override
        public boolean areContentsTheSame(@NonNull Apartment oldApartment, @NonNull Apartment newApartment) {
            // NOTE: if you use equals, your object must properly override Object#equals()
            // Incorrectly returning false here will result in too many animations.
            return Objects.equals(oldApartment.getFormattedTitle(), newApartment.getFormattedTitle());
        }
    };

    public interface ApartmentClickCallback {
        void onClick(Apartment apartment);
    }

    static class ApartmentViewHolder extends RecyclerView.ViewHolder {

        private final ApartmentItemBinding binding;

        ApartmentViewHolder(ApartmentItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Apartment apartment, String token) {
            binding.setToken(token);
            binding.setApartment(apartment);
            binding.executePendingBindings();
        }
    }
}
