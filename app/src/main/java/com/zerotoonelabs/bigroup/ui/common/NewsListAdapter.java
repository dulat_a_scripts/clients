package com.zerotoonelabs.bigroup.ui.common;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.NewsItemBinding;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.News;

public class NewsListAdapter extends DataBoundListAdapter<News, NewsItemBinding> {
    private final NewsClickCallback newsClickCallback;

    public NewsListAdapter(NewsClickCallback newsClickCallback) {
        this.newsClickCallback = newsClickCallback;
    }

    @Override
    protected NewsItemBinding createBinding(ViewGroup parent) {
        NewsItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.news_item,
                        parent, false);
        binding.getRoot().setOnClickListener(v -> {
            News repo = binding.getNews();
            if (repo != null && newsClickCallback != null) {
                newsClickCallback.onClick(repo);
            }
        });
        return binding;
    }

    @Override
    protected void bind(NewsItemBinding binding, News item, int position) {
        binding.setNews(item);
    }

    @Override
    protected boolean areItemsTheSame(News oldItem, News newItem) {
        return Objects.equals(oldItem.getId(), newItem.getId());
    }

    @Override
    protected boolean areContentsTheSame(News oldItem, News newItem) {
        return Objects.equals(oldItem.getTitle(), newItem.getTitle());
    }


    public interface NewsClickCallback {
        void onClick(News news);
    }
}
