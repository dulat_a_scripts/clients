package com.zerotoonelabs.bigroup.ui.favourites;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.zerotoonelabs.bigroup.repository.BiRepository;
import com.zerotoonelabs.bigroup.util.AbsentLiveData;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.Apartments.Apartment;
import com.zerotoonelabs.bigroup.entity.Resource;

import java.util.List;

import javax.inject.Inject;

public class FavoritesViewModel extends ViewModel {

    private final LiveData<List<Apartment>> apartments;
    private final LiveData<Resource<Integer>> removeApartment;

    private final MutableLiveData<ApartmentId> apartmentId;

    @Inject
    FavoritesViewModel(BiRepository repository) {
        apartmentId = new MutableLiveData<>();
        apartments = repository.loadApartments();
        removeApartment = Transformations.switchMap(apartmentId, input -> {
            if (input.isEmpty()) return AbsentLiveData.create();
            return repository.removeApartment(input.apartment, input.position);
        });
    }

    LiveData<List<Apartment>> getApartments() {
        return apartments;
    }

    LiveData<Resource<Integer>> getRemoveApartment() {
        return removeApartment;
    }

    void setApartmentId(Apartment apartment, int position) {
        ApartmentId update = new ApartmentId(apartment, position);
        if (Objects.equals(apartmentId.getValue(), update)) return;
        apartmentId.setValue(update);
    }

    static class ApartmentId {
        Apartment apartment;
        Integer position;

        ApartmentId(Apartment apartment, Integer position) {
            this.apartment = apartment;
            this.position = position;
        }

        boolean isEmpty() {
            return apartment == null || position == null;
        }
    }
}
