package com.zerotoonelabs.bigroup.ui.common;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.entity.statusesPred.Contract;
import com.zerotoonelabs.bigroup.entity.statusesPred.DopSoglashenie;
import com.zerotoonelabs.bigroup.entity.statusesPred.Platezhi;
import com.zerotoonelabs.bigroup.entity.statusesPred.Uslugi;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static java.lang.Math.min;

/**
 * Created by mac on 22.11.2017.
 */

public class StatusesAdapter extends RecyclerView.Adapter<StatusesAdapter.ViewHolder> {

    int type;
    List<Contract> contractList = new ArrayList<>();
    List<DopSoglashenie> dopSoglashenieList = new ArrayList<>();
    List<Uslugi> uslugiList = new ArrayList<>();
    List<Platezhi> platezhiList = new ArrayList<>();

    public StatusesAdapter(int type, List<Contract> contractList, List<DopSoglashenie> dopSoglashenieList, List<Uslugi> uslugiList, List<Platezhi> platezhiList) {
        this.type = type;
        this.contractList = contractList;
        this.dopSoglashenieList = dopSoglashenieList;
        this.uslugiList = uslugiList;
        this.platezhiList = platezhiList;
    }


    @Override
    public StatusesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        switch (type) {
            case 1:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_status_1, parent, false);
                break;
            case 2:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_status_2, parent, false);
                break;
            case 3:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_status_3, parent, false);
                break;
            default:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_status_4, parent, false);
                break;
        }
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(StatusesAdapter.ViewHolder holder, int position) {
        String prise;
        switch (type) {
            case 1:
                holder.textView1.setText(contractList.get(position).getNomerDogovora());
                prise = formatPrice(contractList.get(position).getSummaDogovora()) + " тг.";
                holder.textView2.setText(prise);
                break;
            case 2:
                holder.textView1.setText(dopSoglashenieList.get(position).getName());
                prise = formatPrice(dopSoglashenieList.get(position).getSumma()) + " тг.";
                holder.textView2.setText(prise);
                holder.textView3.setText(formatDate(dopSoglashenieList.get(position).getData().substring(0, 10)));
                holder.textView4.setText(dopSoglashenieList.get(position).getVidDokumenta());
                break;
            case 3:
                holder.textView1.setText(uslugiList.get(position).getSostavUslug());
                prise = formatPrice(uslugiList.get(position).getStoimost()) + " тг.";
                holder.textView2.setText(prise);
                break;
            default:
                holder.textView1.setText(formatDate(platezhiList.get(position).getDataPlatezha().substring(0, 10)));
                prise = formatPrice(platezhiList.get(position).getSummaPlatezha()) + " тг.";
                holder.textView2.setText(prise);
                holder.textView3.setText(platezhiList.get(position).getPlatezhDocument());
                break;
        }
    }

    @Override
    public int getItemCount() {
        switch (type) {
            case 1:
                //return contractList.size();

                return min(contractList.size(), 10);
            case 2:
                //return dopSoglashenieList.size();
                return min(dopSoglashenieList.size(), 10);
            case 3:
                //return uslugiList.size();
                return min(uslugiList.size(), 10);
            default:
                return min(platezhiList.size(), 10);
            // return platezhiList.size();
        }
    }

    private String formatPrice(Float price) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
        symbols.setGroupingSeparator(' ');
        formatter.setDecimalFormatSymbols(symbols);
        return formatter.format(price);
    }

    private String formatDate(String input) {
        if (input == null) return null;
        SimpleDateFormat outputSdf = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
        SimpleDateFormat inputSdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

        try {
            Date inputDate = inputSdf.parse(input);
            return outputSdf.format(inputDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView1, textView2, textView3, textView4;

        public ViewHolder(View itemView) {
            super(itemView);
            switch (type) {
                case 1:
                    textView1 = (TextView) itemView.findViewById(R.id.textView1);
                    textView2 = (TextView) itemView.findViewById(R.id.textView2);
                    break;
                case 2:
                    textView1 = (TextView) itemView.findViewById(R.id.textView1);
                    textView2 = (TextView) itemView.findViewById(R.id.textView2);
                    textView3 = (TextView) itemView.findViewById(R.id.textView3);
                    textView4 = (TextView) itemView.findViewById(R.id.textView4);
                    break;
                case 3:
                    textView1 = (TextView) itemView.findViewById(R.id.textView1);
                    textView2 = (TextView) itemView.findViewById(R.id.textView2);
                    break;
                default:
                    textView1 = (TextView) itemView.findViewById(R.id.textView1);
                    textView2 = (TextView) itemView.findViewById(R.id.textView2);
                    textView3 = (TextView) itemView.findViewById(R.id.textView3);
                    break;
            }
        }
    }
}
