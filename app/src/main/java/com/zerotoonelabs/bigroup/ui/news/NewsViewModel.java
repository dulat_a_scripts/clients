package com.zerotoonelabs.bigroup.ui.news;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.zerotoonelabs.bigroup.repository.SalesRepository;
import com.zerotoonelabs.bigroup.util.AbsentLiveData;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.News;
import com.zerotoonelabs.bigroup.entity.Resource;

import java.util.List;

import javax.inject.Inject;

import static android.text.TextUtils.isEmpty;


public class NewsViewModel extends ViewModel {
    private final MutableLiveData<String> lang = new MutableLiveData<>();
    private final LiveData<Resource<List<News>>> news;

    @Inject
    public NewsViewModel(SalesRepository repository) {
        news = Transformations.switchMap(lang, input -> {
            if (isEmpty(input)) {
                return AbsentLiveData.create();
            }
            return repository.loadNews(input);
        });
    }

    public LiveData<Resource<List<News>>> getStocks() {
        return news;
    }

    public void retry() {
        String current = lang.getValue();
        if (current != null && !current.isEmpty()) {
            lang.setValue(current);
        }
    }

    void setLang(String input) {
        if (Objects.equals(lang.getValue(), input)) {
            return;
        }
        lang.setValue(input);
    }


}
