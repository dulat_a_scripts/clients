package com.zerotoonelabs.bigroup.ui.sendapplication;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.repository.BiRepository;
import com.zerotoonelabs.bigroup.ui.common.SingleLiveEvent;
import com.zerotoonelabs.bigroup.util.AbsentLiveData;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.ServiceProperty;
import com.zerotoonelabs.bigroup.entity.ServiceRequest;
import com.zerotoonelabs.bigroup.entity.Resource;

import java.util.List;
import java.util.regex.Pattern;

import javax.inject.Inject;

import static android.text.TextUtils.isEmpty;
import static android.util.Patterns.EMAIL_ADDRESS;

public class SendServiceRequestViewModel extends ViewModel {

    private MutableLiveData<RequestId> requestId;
    private final MutableLiveData<String> token = new MutableLiveData<>();

    private LiveData<Resource<ServiceRequest>> serviceRequest;
    private final LiveData<Resource<List<ServiceProperty>>> serviceProperties;

    private SingleLiveEvent<Integer> message = new SingleLiveEvent<>();

    @Inject
    public SendServiceRequestViewModel(BiRepository repository) {
        requestId = new MutableLiveData<>();
        serviceRequest = Transformations.switchMap(requestId, input -> {
            if (input.isEmpty()) {
                return AbsentLiveData.create();
            }
            return repository.loadServiceRequest(input.token, input.serviceRequest);
        });

        serviceProperties = Transformations.switchMap(token, input -> {
            if (TextUtils.isEmpty(input)) {
                return AbsentLiveData.create();
            }
            return repository.loadServiceProperties(input);
        });
    }

    LiveData<Resource<List<ServiceProperty>>> getServiceProperties() {
        return serviceProperties;
    }

    void setToken(String input) {
        token.setValue(input);
    }

    LiveData<Resource<ServiceRequest>> getServiceRequest() {
        return serviceRequest;
    }


    void setServiceRequest(@NonNull ServiceRequest input) {
        String token = this.token.getValue();
        RequestId update = new RequestId(input, token);
        if (Objects.equals(update, serviceRequest.getValue())) {
            return;
        }
        requestId.setValue(update);
    }

    boolean checkFields(String fullName, String email, String phone, String uid, String apartmentNo, String object) {
        if (isEmpty(fullName) || isEmpty(email) || isEmpty(phone) || isEmpty(uid) || isEmpty(apartmentNo) || isEmpty(object)) {
            setMessage(R.string.empty_fields_error_message);
            return false;
        }
        if (fullName.length() < 6) {
            setMessage(R.string.invalid_fullname_message);
            return false;
        }
        if (uid.length() != 12) {
            setMessage(R.string.invalid_iin_message);
            return false;
        }
        if (!isValidEmail(email)) {
            setMessage(R.string.invalid_email_message);
            return false;
        }
        if (phone.isEmpty() || phone.contains("—")) {
            setMessage(R.string.invalid_phone_message);
            return false;
        }
        return true;
    }

    public SingleLiveEvent<Integer> getMessage() {
        return message;
    }

    public void setMessage(Integer resId) {
        if (resId != null)
            message.setValue(resId);
    }

    public void retry() {
        if (requestId.getValue() != null) {
            requestId.setValue(requestId.getValue());
        }
    }

    static class RequestId {
        public ServiceRequest serviceRequest;
        public String token;

        public RequestId(ServiceRequest serviceRequest, String token) {
            this.serviceRequest = serviceRequest;
            this.token = token;
        }

        boolean isEmpty() {
            return serviceRequest == null || token == null || token.length() == 0;
        }
    }

    private boolean isValidPhone(@NonNull String phone) {
//        return PHONE.matcher(phone).matches();
        return Pattern.matches("^[+]?[0-9]{10,11}$", phone.replaceAll("\\s+",""));
    }

    private boolean isValidEmail(String email) {
        return EMAIL_ADDRESS.matcher(email).matches();
    }
}
