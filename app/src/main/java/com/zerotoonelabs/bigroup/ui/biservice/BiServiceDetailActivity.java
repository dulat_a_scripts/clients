package com.zerotoonelabs.bigroup.ui.biservice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.zerotoonelabs.bigroup.R;

public class BiServiceDetailActivity extends AppCompatActivity {

    public static final String REQUEST_EXTRA = "request";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bi_service_detail_act);
    }
}
