package com.zerotoonelabs.bigroup.ui.sendrequest;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.SendRequestFragBinding;
import com.zerotoonelabs.bigroup.di.Injectable;
import com.zerotoonelabs.bigroup.ui.BaseFragment;
import com.zerotoonelabs.bigroup.ui.common.NavigationController;
import com.zerotoonelabs.bigroup.util.AutoClearedValue;

import javax.inject.Inject;

public class SendRequestFragment extends BaseFragment implements Injectable {
    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private SendRequestViewModel mViewModel;
    private AutoClearedValue<SendRequestFragBinding> binding;

    public SendRequestFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(SendRequestViewModel.class);
        String token = PreferenceManager.getDefaultSharedPreferences(getActivity())
                .getString("token", null);
        if (token != null) {
            mViewModel.setToken(token);
        } else {
            // if token is empty then try to recreate it
            getActivity().finish();
        }

        initObservables(mViewModel);
        initClickListeners(mViewModel);
    }

    private void initClickListeners(SendRequestViewModel viewModel) {
        binding.get().sendQueryButton.setOnClickListener(v -> {

            String phone = binding.get().phoneTv.getText().toString();
            phone = getFormattedPhone(phone);

            dismissKeyboard(v.getWindowToken());
            viewModel.setRequestId(binding.get().nameTv.getText().toString(),
                    phone, "dom");

        });

        binding.get().phone1.setOnClickListener(v -> {
            launchPhone("360-360");
        });

        binding.get().phone2.setOnClickListener(v -> {
            launchPhone("+7(701)7360-360");
        });

        binding.get().phone3.setOnClickListener(v -> {
            launchPhone("+7(7273)315-033");
        });
    }

    private void launchPhone(String tel) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + tel));
        startActivity(intent);
    }

    private void initObservables(SendRequestViewModel viewModel) {
        viewModel.getMessage().observe(this, integer -> {
            if (integer != null) {
                Toast.makeText(getActivity(), integer, Toast.LENGTH_SHORT).show();
            }
        });

        viewModel.getRequest().observe(this, resource -> {
            binding.get().setResource(resource);
            if (resource != null && resource.data != null) {
                showDialog(R.string.dialog_visit_success_title, R.string.dialog_visit_success_text);
                binding.get().nameTv.setText("");
                binding.get().phoneTv.setText("");
            }

        });
    }

    private void showDialog(int titleId, int message) {
        if (!isAdded()) return;
        new AlertDialog.Builder(getActivity())
                .setTitle(titleId)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, null)
                .create()
                .show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        SendRequestFragBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.send_request_frag, container, false);
        binding = new AutoClearedValue<>(this, dataBinding);

        EditText phoneEt = binding.get().phoneTv;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            phoneEt.addTextChangedListener(new PhoneNumberFormattingTextWatcher("KZ"));
        } else {
            phoneEt.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        }
        return dataBinding.getRoot();
    }

    private void dismissKeyboard(IBinder windowToken) {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(windowToken, 0);
        }
    }

}
