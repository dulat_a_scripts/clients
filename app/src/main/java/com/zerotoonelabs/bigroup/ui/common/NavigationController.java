package com.zerotoonelabs.bigroup.ui.common;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.zerotoonelabs.bigroup.Library.Fragments.Notification_fragments;
import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.fragments.AboutFragment;
import com.zerotoonelabs.bigroup.fragments.ContactsFragment;
import com.zerotoonelabs.bigroup.fragments.InfoFragment;
import com.zerotoonelabs.bigroup.fragments.InstantPaymentFragment;
import com.zerotoonelabs.bigroup.fragments.KeysFragment;
import com.zerotoonelabs.bigroup.fragments.PartPaymentFragment;
import com.zerotoonelabs.bigroup.fragments.PrivatisationFragment;
import com.zerotoonelabs.bigroup.fragments.ResaleFragment;
import com.zerotoonelabs.bigroup.fragments.SpravkiFragment;
import com.zerotoonelabs.bigroup.ui.MainActivity;
import com.zerotoonelabs.bigroup.ui.apartments.ApartmentDetailFragment;
import com.zerotoonelabs.bigroup.ui.apartments.ApartmentFilterFragment;
import com.zerotoonelabs.bigroup.ui.apartments.ApartmentFragment;
import com.zerotoonelabs.bigroup.ui.biservice.BiServiceFragment;
import com.zerotoonelabs.bigroup.ui.favourites.FavoriteApartmentsFragment;
import com.zerotoonelabs.bigroup.ui.mortgage.AtfbankFragment;
import com.zerotoonelabs.bigroup.ui.mortgage.CentercreditFragment;
import com.zerotoonelabs.bigroup.ui.mortgage.MortgageFragment;
import com.zerotoonelabs.bigroup.ui.mortgage.SberbankFragment;
import com.zerotoonelabs.bigroup.ui.news.NewsFragment;
import com.zerotoonelabs.bigroup.ui.opendoor.OpenDoorFragment;
import com.zerotoonelabs.bigroup.ui.profile.ProfileFragment;
import com.zerotoonelabs.bigroup.ui.realestate.RealEstateFilterFragment;
import com.zerotoonelabs.bigroup.ui.realestate.RealEstateFragment;
import com.zerotoonelabs.bigroup.ui.realestatedetail.RealEstateDetailFragment;
import com.zerotoonelabs.bigroup.ui.sales.SalesFragment;
import com.zerotoonelabs.bigroup.ui.sendapplication.SendServiceRequestFragment;
import com.zerotoonelabs.bigroup.ui.sendrequest.SendRequestFragment;
import com.zerotoonelabs.bigroup.ui.visitobject.VisitObjectFragment;
import com.zerotoonelabs.bigroup.entity.ApartmentFilter;
import com.zerotoonelabs.bigroup.entity.Apartments.Apartment;
import com.zerotoonelabs.bigroup.entity.RealEstateFilter;

import javax.inject.Inject;

public class NavigationController {
    private final FragmentManager fragmentManager;
    private final int containerId;

    @Inject
    public NavigationController(MainActivity mainActivity) {
        this.containerId = R.id.flContent;
        fragmentManager = mainActivity.getSupportFragmentManager();
    }

    public void navigateToProfile() {

        try{
            ProfileFragment fragment = new ProfileFragment();
            String tag = "Профиль";
            fragmentManager.beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .replace(containerId, fragment, tag)
                    .commitAllowingStateLoss();
        }catch (Exception e){

        }

    }

    public void navigateToSales() {
        SalesFragment fragment = new SalesFragment();
        String tag = SalesFragment.class.getSimpleName();
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToCall() {
        SendRequestFragment fragment = new SendRequestFragment();
        String tag = "Заказать звонок";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToFavorites(String token) {
        FavoriteApartmentsFragment fragment = FavoriteApartmentsFragment.newInstance(token);
        String tag = "Избранное";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();

    }

    public void navigateToMortgage() {
        MortgageFragment fragment = new MortgageFragment();
        String tag = "Ипотека";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToMortgageBackstack() {
        MortgageFragment fragment = new MortgageFragment();
        String tag = "Ипотека";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(containerId, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    public void navigateToPayment() {
        PartPaymentFragment fragment = new PartPaymentFragment();
        String tag = "Рассрочка";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToInstantPayment() {
        InstantPaymentFragment fragment = new InstantPaymentFragment();
        String tag = "Стандартные условия";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToInstantPaymentBackstack() {
        InstantPaymentFragment fragment = new InstantPaymentFragment();
        String tag = "Стандартные условия";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(containerId, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    public void navigateToAbout() {
        AboutFragment fragment = new AboutFragment();
        String tag = AboutFragment.class.getSimpleName();
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToNews() {
        NewsFragment fragment = new NewsFragment();
        String tag = "Новости";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToContacts() {
        ContactsFragment fragment = new ContactsFragment();
        String tag = "Контакты";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToInfo() {
        InfoFragment fragment = new InfoFragment();
        String tag = "Информация";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToSendApplication() {
        SendServiceRequestFragment fragment = new SendServiceRequestFragment();
        String tag = "Заявка в BI Service";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToNewServiceRequest() {
        SendServiceRequestFragment fragment = new SendServiceRequestFragment();
        String tag = "Заявка в BI Service";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    public void navigateToVisitObject(String token) {
        VisitObjectFragment fragment = VisitObjectFragment.newInstance(token);
        String tag = "Посещение объекта";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToOpenDoor() {
        OpenDoorFragment fragment = new OpenDoorFragment();
        String tag = "День открытых дверей";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToKeys() {
        KeysFragment fragment = new KeysFragment();
        String tag = "Получение ключей";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToPrivatization() {
        PrivatisationFragment fragment = new PrivatisationFragment();
        String tag = "Оформление в собственность";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToResale() {
        ResaleFragment fragment = new ResaleFragment();
        String tag = "Перепродажа";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToBiService() {
        BiServiceFragment fragment = new BiServiceFragment();
        String tag = BiServiceFragment.class.getSimpleName();

        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToNotification(){

        Notification_fragments notification_fragments = new Notification_fragments();
        String tag = "Уведомления";

        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, notification_fragments, tag)
                .commitAllowingStateLoss();

    }



    public void navigateToSprakvi() {
        SpravkiFragment fragment = new SpravkiFragment();
        String tag = SpravkiFragment.class.getSimpleName();
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToMatchApartment() {
        Library.Fixfilterpage = 0;

        ApartmentFragment fragment = new ApartmentFragment();
        String tag = "Подбор квартиры";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();

    }

    public void navigateToApartmentBackstack(String id) {

        Library.Fixfilterpage = 0;

        ApartmentFragment fragment = ApartmentFragment.newInstance(id);
        String tag = "Подбор квартиры";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(containerId, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();

    }

    public void navigateToRealEstate() {
        RealEstateFragment fragment = new RealEstateFragment();
        String tag = "Объекты";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    public void navigateToRealEstateDetails(String id) {
        RealEstateDetailFragment fragment = RealEstateDetailFragment.newInstance(id);
        String tag = "Просмотр ЖК";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(containerId, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    public void navigateToCallBackstack() {
        SendRequestFragment fragment = new SendRequestFragment();
        String tag = "Оставить заявку";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(containerId, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    public void navigateBack() {
        fragmentManager.popBackStack();
    }

    public void navigateToRealEstateFilter(RealEstateFragment targetFragment, RealEstateFilter filter) {
        RealEstateFilterFragment fragment = RealEstateFilterFragment.newInstance(filter);
        fragment.setTargetFragment(targetFragment, 0);
        String tag = "Применить фильтр";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(containerId, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    public void navigateToSberbank() {
        SberbankFragment fragment = new SberbankFragment();
        String tag = "Сбербанк";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(containerId, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    public void navigateToCenterCredit() {
        CentercreditFragment fragment = new CentercreditFragment();
        String tag = "CenterCredit Bank";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(containerId, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    public void navigateToAtfbank() {
        AtfbankFragment fragment = new AtfbankFragment();
        String tag = "Atf Bank";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(containerId, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    public void navigateToApartmentsFilter(ApartmentFragment targetFragment, ApartmentFilter apartmentFilter) {
        ApartmentFilterFragment fragment = new ApartmentFilterFragment();
        fragment.setTargetFragment(targetFragment, 0);
        String tag = "Применить фильтр";
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(containerId, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    public void navigateToApartmentDetailFragment(String token, Apartment apartment) {
        ApartmentDetailFragment fragment = ApartmentDetailFragment.newInstance(token, apartment);
        String tag = apartment.getTitle();
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(containerId, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }
}
