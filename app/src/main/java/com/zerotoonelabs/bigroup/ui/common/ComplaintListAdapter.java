package com.zerotoonelabs.bigroup.ui.common;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.ComplaintItemBinding;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.Complaint;

/**
 * A RecyclerView adapter for {@link Complaint} class.
 */
public class ComplaintListAdapter extends DataBoundListAdapter<Complaint, ComplaintItemBinding> {
    private final ComplaintClickCallback serviceRequestClickCallback;

    public ComplaintListAdapter(ComplaintClickCallback serviceRequestClickCallback) {
        this.serviceRequestClickCallback = serviceRequestClickCallback;
    }

    @Override
    protected ComplaintItemBinding createBinding(ViewGroup parent) {
        ComplaintItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.complaint_item,
                        parent, false);
        binding.getRoot().setOnClickListener(v -> {
            Complaint repo = binding.getComplaint();
            int position = binding.getPosition();
            if (repo != null && serviceRequestClickCallback != null) {
                serviceRequestClickCallback.onClick(repo, position);
            }
        });
        return binding;
    }

    @Override
    protected void bind(ComplaintItemBinding binding, Complaint item, int position) {
        binding.setComplaint(item);
        binding.setPosition(position);
    }

    @Override
    protected boolean areItemsTheSame(Complaint oldItem, Complaint newItem) {
        return Objects.equals(oldItem.encodedImage, newItem.encodedImage);
    }

    @Override
    protected boolean areContentsTheSame(Complaint oldItem, Complaint newItem) {
        return Objects.equals(oldItem.message, newItem.message);
    }

    public interface ComplaintClickCallback {
        void onClick(Complaint complaint, int position);
    }
}