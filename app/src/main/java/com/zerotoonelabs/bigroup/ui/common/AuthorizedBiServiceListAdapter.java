package com.zerotoonelabs.bigroup.ui.common;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.Library.Objects.SetTag_obj;
import com.zerotoonelabs.bigroup.Library.Request_parsers.SendReiting_service;
import com.zerotoonelabs.bigroup.Library.Retrofit_request.MobileClientPostRatingBiService_interface;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.authui.HomeActivity;
import com.zerotoonelabs.bigroup.entity.BiServiceApplication;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

/**
 * Created by mac on 15.11.2017.
 */

public class AuthorizedBiServiceListAdapter extends RecyclerView.Adapter<AuthorizedBiServiceListAdapter.ViewHolder> {

    List<BiServiceApplication> applications;
    boolean showRatingBar;

    SendReiting_service sendreiting;
    String num = null;
    String raitingnum = null;
    String response = null;
    ConstraintLayout constraintLayout;
    ProgressBar progressBar;
    String n2 = null;
   // Integer count = 0;



    public AuthorizedBiServiceListAdapter(List<BiServiceApplication> applications, boolean showRatingBar) {
        this.applications = applications;
        this.showRatingBar = showRatingBar;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.authorized_biservice_aplication_item, parent, false);
        return new ViewHolder(view);
    }

    public Integer latestmark = 0;
    public String store_remark = "";

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {



        if (applications != null) {
            BiServiceApplication application = applications.get(position);


            String status = application.getStatus();

            holder.status.setText(status);
            holder.description.setText(application.getOpisanie());
            holder.number.setText(application.getNomer());
            holder.date.setText(getNormalDate(application.getData()));

            if(status == null){
                return;
            }



            if(status.equals("Выполнено")){

                holder.rating2.setIsIndicator(false);
                //holder.rating2.setRating(application.getMark());
            }else{
                holder.rating2.setIsIndicator(true);
            }


            //if(position >= count){
                //count = position;

            holder.number.setText(application.getNomer());

            holder.description.setText(application.getOpisanie());
            holder.date.setText(getNormalDate(application.getData()));

//            if (!showRatingBar) {
//                holder.ratingLayout.setVisibility(View.GONE);
//                holder.successIcon.setVisibility(View.VISIBLE);
//            }

            String num_zayavki = application.getNomer();
            String mark = String.valueOf(application.getMark());

            if(status.equals("Выполнено") || status.equals("Закрыто")){
                holder.rating2.setRating(application.getMark());
                holder.rating2.setVisibility(View.VISIBLE);
                holder.ratingLayout.setVisibility(View.VISIBLE);
            }else{
                holder.rating2.setVisibility(View.GONE);
                holder.ratingLayout.setVisibility(View.GONE);
            }


            if (status.equals("Выполнено")) {

                holder.rating2.setIsIndicator(false);
                SetTag_obj setTag_obj = new SetTag_obj();

                setTag_obj.setParam1(num_zayavki);

                setTag_obj.setIntparam(application.getMark());

                setTag_obj.setParam2(application.getRemark());

                holder.rating2.setTag(setTag_obj);

                holder.rating2.setRating(application.getMark());


                holder.rating2.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                    @Override
                    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {


                        SetTag_obj setTag_obj1 = (SetTag_obj) ratingBar.getTag();



                        try{
                            num = setTag_obj1.getParam1();
                            latestmark = setTag_obj1.getIntparam();
                            store_remark = setTag_obj1.getParam2();

                        }catch (Exception e){

                        }

                        Library.SavePhoneMemoryOne("num", num);

                        Integer integer = Math.round(rating);

                        raitingnum = String.valueOf(integer);

                        //dialog

                        AlertDialog.Builder builder1 = new AlertDialog.Builder(Library.newcontext);
                        builder1.setMessage("Ваша оценка " + raitingnum);
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                "Подтвердить",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        new Download().execute();
                                        dialog.cancel();
                                    }
                                });

                        builder1.setNegativeButton(
                                "отмена",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        holder.rating2.setRating(latestmark);
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();

                        if(!raitingnum.equals("0")){

                            if (status.equals("Выполнено") && !status.equals("Новая") && !status.equals("Закрыто") && fromUser == true) {
                                alert11.show();
                            }

                        }


                        //dialog

                    }
                });
            }else{
                String textstatus = holder.status.getText().toString();
                if(!textstatus.equals("Выполнено")){
                    holder.rating2.setIsIndicator(true);
                }

                if(status.equals("Выполнено") || status.equals("Закрыто")){
                    holder.rating2.setRating(application.getMark());
                    holder.rating2.setVisibility(View.VISIBLE);
                    holder.ratingLayout.setVisibility(View.VISIBLE);
                }else{
                    holder.ratingLayout.setVisibility(View.GONE);
                    holder.rating2.setVisibility(View.GONE);
                }




            }





        }
    }


    public class Download extends AsyncTask{

        @Override
        protected String doInBackground(Object[] objects) {

            Retrofit client = Library.NewRetrofitclient();

            MobileClientPostRatingBiService_interface mobile = client.create(MobileClientPostRatingBiService_interface.class);

            sendreiting = new SendReiting_service();
            sendreiting.setRemark(store_remark);
            sendreiting.setDocumentNumber(num);

            sendreiting.setAssessment(raitingnum);

            String token = Library.token;

            try{

            Call<Object> call =  mobile.getRequest(sendreiting,token);

                Gson gson = new Gson();

                response = gson.toJson(call.execute().body());

                call.execute();

            }catch (Exception e){

            }


            return "not";
        }

        @Override
        protected void onPostExecute(Object o) {

            Log.i("resp","eee" + response);

            Toast.makeText(Library.newcontext, "Ваша заявка оценена..", Toast.LENGTH_SHORT).show();

            super.onPostExecute(o);
        }
    }



//     ((Item)holder).constraintLayout.setTag(id);
//
//            ((Item)holder).constraintLayout.setOnClickListener(new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//
//            Integer m = Integer.parseInt(v.getTag().toString());
//
//            String string = Integer.toString(m);
//
//            Library.SavePhoneMemoryOne("id",string);
//
//            FragmentTransaction fragmentTransaction = Library.fragmentManager.beginTransaction();
//
//            View_request view_request = new View_request();
//
//            fragmentTransaction.replace(R.id.mainContent, view_request);
//            //fragmentTransaction.addToBackStack(null);
//            fragmentTransaction.commit();
//
//        }
//    });

    private String getNormalDate(String data) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS");
        SimpleDateFormat normalDate = new SimpleDateFormat("dd.MM.yyyy");
        try {
            Date date = dateFormat.parse(data);
            String dateString = normalDate.format(date);
            return dateString;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public int getItemCount() {
        if (applications != null) {
            return applications.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView number;
        TextView status;
        TextView description;
        TextView date;
        RatingBar rating;
        RatingBar rating2;
        ConstraintLayout ratingLayout;
        ImageView successIcon;
        TextView textViewos;

        public ViewHolder(View itemView) {
            super(itemView);
            number = itemView.findViewById(R.id.number);
            status = itemView.findViewById(R.id.status);
            description = itemView.findViewById(R.id.description);
            date = itemView.findViewById(R.id.date);
            rating = itemView.findViewById(R.id.rating_bar);
            rating2 = itemView.findViewById(R.id.rating_bar);
            ratingLayout = itemView.findViewById(R.id.raring_layout);
            successIcon = itemView.findViewById(R.id.success);
            textViewos = itemView.findViewById(R.id.osenka);
        }
    }

}

