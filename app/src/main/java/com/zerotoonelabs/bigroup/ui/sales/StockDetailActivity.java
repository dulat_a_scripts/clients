package com.zerotoonelabs.bigroup.ui.sales;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.StockDetailActBinding;
import com.zerotoonelabs.bigroup.entity.Stock;

public class StockDetailActivity extends AppCompatActivity {

    public static final String STOCK_EXTRA = "stock";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StockDetailActBinding binding = DataBindingUtil.setContentView(this, R.layout.stock_detail_act);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            Stock stock = extras.getParcelable(STOCK_EXTRA);
            String imageUrl;
            if (stock != null) {
                if (stock.imageUrl == null && stock.sectionImageUrls != null && stock.sectionImageUrls.size() > 0) {
                    imageUrl = stock.sectionImageUrls.get(0);
                } else {
                    imageUrl = stock.imageUrl;
                }
                binding.setImageUrl(imageUrl);
            }
            binding.setStock(stock);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
