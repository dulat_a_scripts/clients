package com.zerotoonelabs.bigroup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import br.com.sapereaude.maskedEditText.MaskedEditText;

public class ForgotPasswordActivity extends BaseActivity {

    MaskedEditText phoneET;
    String token;
    AlertDialog b;
    AlertDialog.Builder dialogBuilder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        getSupportActionBar().hide();
        phoneET = findViewById(R.id.phone_et);
        token = PreferenceManager.getDefaultSharedPreferences(this).getString("token", "84eb0165-9628-4ead-b61e-550c365e3cd4");

    }

    public void openSMSConfirmation(View view) {
        String phone = phoneET.getText().toString();
        if (phone.isEmpty() || phone.contains("—")) {
            showToast(R.string.invalid_phone_message);
            return;
        }
        phone = getFormattedPhone(phone);

        ShowProgressDialog();
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        String finalPhone = phone;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://webapi.bi-group.org/api/v1/MobileClient/ForgotPassword",
                response -> newPass(response),
                error -> showError(error)) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map headers = new HashMap();
                headers.put("Token", token);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                String body = "";
                Map map = new HashMap();
                map.put("phone", finalPhone);
                body = new Gson().toJson(map);
                return body.getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        requestQueue.add(stringRequest);

        //startActivity(new Intent(this, ConfirmSMSActivity.class));
    }

    private void newPass(String otvet) {
        System.out.println("otv: " + otvet);
        Intent intent = new Intent(this, ConfirmSMSActivity.class);
        intent.putExtra("otvet", otvet);
        startActivity(intent);

    }

    private void showError(VolleyError error) {
        HideProgressDialog();
        if (error == null || error.networkResponse == null) {
            Toast.makeText(this, "Ошибка сети, сервер временно не работает", Toast.LENGTH_LONG).show();
            return;
        }
        if (error.networkResponse.statusCode / 100 == 4) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(new String(error.networkResponse.data));
                Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(this, "Ошибка сети, сервер временно не работает", Toast.LENGTH_LONG).show();
        }
    }

    public void ShowProgressDialog() {
        dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.progress_dialog_layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);
        b = dialogBuilder.create();
        b.show();
    }

    public void HideProgressDialog() {
        b.dismiss();
    }

}
