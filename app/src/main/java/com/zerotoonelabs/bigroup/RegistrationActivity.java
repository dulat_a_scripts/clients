package com.zerotoonelabs.bigroup;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.Library.Request_parsers.CheckPhoneStatus;
import com.zerotoonelabs.bigroup.Library.Retrofit_request.Check_Phone_status_int;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.sapereaude.maskedEditText.MaskedEditText;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RegistrationActivity extends BaseActivity {

    EditText emailET;
    MaskedEditText phoneET;
    String token;
    AlertDialog b;
    AlertDialog.Builder dialogBuilder;
    String contractID = "";
    String inn = null;
    List<String> phonearray;
    TextView inntextview;
    LinearLayout linearLayout;

    EditText inntext;
    Button buttoncheck_inn;
    ProgressBar progressBar;
    Button button;
    TextView textView88;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        getSupportActionBar().hide();
        emailET = findViewById(R.id.email_et);
        phoneET = findViewById(R.id.phone_et);
        inntextview = findViewById(R.id.textView89);
        linearLayout = findViewById(R.id.vertical);
        inntext = findViewById(R.id.editText2);
        buttoncheck_inn = findViewById(R.id.button21);
        progressBar = findViewById(R.id.progressBar8);
        button = findViewById(R.id.button22);
        textView88 = findViewById(R.id.textView88);

        //hide 1
        emailET.setVisibility(View.GONE);
        phoneET.setVisibility(View.GONE);
        button.setVisibility(View.GONE);
        textView88.setVisibility(View.GONE);
        //hide 1

        token = PreferenceManager.getDefaultSharedPreferences(this).getString("token", "cbc821cf-9730-43ee-9477-62de9bf9f1bb");
        Log.i("token",token);

        buttoncheck_inn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(inntext.getText().toString().isEmpty()){
                    showToast(R.string.invalid_iin_message);
                    return;
                }else{
                    String inns = inntext.getText().toString();

                    progressBar.setVisibility(View.VISIBLE);
                    Retrofit client = Library.NewRetrofitclient();
                    Check_Phone_status_int check_phone_status_int = client.create(Check_Phone_status_int.class);

                    Call<CheckPhoneStatus> call = check_phone_status_int.check_inn(token,inns);
//zz
                    call.enqueue(new Callback<CheckPhoneStatus>() {
                        @Override
                        public void onResponse(Call<CheckPhoneStatus> call, Response<CheckPhoneStatus> response) {

                            Log.i("response",response.body().toString());

                            progressBar.setVisibility(View.GONE);
                            Integer status = response.body().getStatus();
                            if(status == 1){

                                inn = inns;
                                phonearray = response.body().getOriginal();
                                String formattednumbers = "";

                                for(int g = 0;g < response.body().getPhone().size();g++){
                                    formattednumbers += response.body().getPhone().get(g) + " | ";
                                }
                                inntextview.setText(formattednumbers);

                                showToast(R.string.next_step_zap);

                                //show 2
                                emailET.setVisibility(View.VISIBLE);
                                phoneET.setVisibility(View.VISIBLE);
                                button.setVisibility(View.VISIBLE);
                                textView88.setVisibility(View.VISIBLE);
                                buttoncheck_inn.setVisibility(View.GONE);
                                inntext.setVisibility(View.GONE);
                                inntextview.setTextSize(TypedValue.COMPLEX_UNIT_DIP,16);
                                //show 2

                            }else{
                                showToast(R.string.not_found_contragent);
                            }


                        }

                        @Override
                        public void onFailure(Call<CheckPhoneStatus> call, Throwable t) {
                            progressBar.setVisibility(View.GONE);
                            showToast(R.string.error_network_request);
                        }
                    });



                }
            }
        });

    }

    @Override
    protected void onResume() {

        //ShowInnDialog();
        super.onResume();
    }

    public void makeRegistration(View view) {
        String phone = phoneET.getText().toString();
        String email = emailET.getText().toString();

        if (phone.isEmpty() || phone.contains("—")) {
            showToast(R.string.invalid_phone_message);
            return;
        }

        if (email.isEmpty()) {
            showToast(R.string.invalid_email_message);
            return;
        }

        phone = getFormattedPhone(phone);
        Log.i("phone",phone);
        Integer fixphone = 0;

        for(int k = 0;k < phonearray.size();k++){
            if(phonearray.get(k).equals(phone)){
                fixphone = 1;
            }
        }

        if(fixphone == 0){
            showToast(R.string.not_found_phone);
            return;
        }

        ShowProgressDialog();
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        String finalPhone = phone;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://webapi.bi-group.org/api/v1/MobileClient/Registration",
                response -> regUser(response),
                error -> showError(error)) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map headers = new HashMap();
                headers.put("Token", token);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                Map map = new HashMap();
                map.put("phone", finalPhone);
                map.put("iin", inn);
                map.put("email", email.isEmpty() ? null : email);
                String body = new Gson().toJson(map);
                return body.getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        requestQueue.add(stringRequest);


//        Intent intent = new Intent(this, ConfirmSMSActivity.class);
//
//        startActivity(intent);
    }

    private void regUser(String otvet) {

        RequestQueue requestQueue = Volley.newRequestQueue(this);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://webapi.bi-group.org/api/v1/MobileClient/SendRegistrationSms",
                response -> openConfirm(response),
                error -> showError(error)) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map headers = new HashMap();
                headers.put("Token", token);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                String body = "";
                try {
                    JSONObject jsonObject = new JSONObject(otvet);
                    contractID = jsonObject.getJSONObject("data").getString("contractId");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                String phone = phoneET.getText().toString();
                phone = getFormattedPhone(phone);
                Map map = new HashMap();
                map.put("contractId", contractID);
                map.put("phone", phone);
                map.put("iin", inn);
                map.put("email", emailET.getText().toString());
                body = new Gson().toJson(map);
                return body.getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        requestQueue.add(stringRequest);


    }

    private void openConfirm(String response) {
        System.out.println(response);
        HideProgressDialog();
        Intent intent = new Intent(this, ConfirmSMSActivity.class);
        intent.putExtra("data", response);
        intent.putExtra("reg", true);
        startActivity(intent);
    }

    private void showError(VolleyError error) {
        HideProgressDialog();
        if (error.networkResponse != null && error.networkResponse.statusCode / 100 == 4) {
            JSONObject jsonObject = null;
            System.out.println();
            try {
                jsonObject = new JSONObject(new String(error.networkResponse.data));
                Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();


            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(this, "Ошибка сети, проверьте подключение к интернету", Toast.LENGTH_LONG).show();
        }
    }



    public void ShowProgressDialog() {
        dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.progress_dialog_layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);
        b = dialogBuilder.create();
        b.show();
    }

    public void HideProgressDialog() {
        b.dismiss();
    }


}

