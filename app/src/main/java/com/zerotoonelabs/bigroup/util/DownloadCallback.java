package com.zerotoonelabs.bigroup.util;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.os.Environment;
import android.support.annotation.NonNull;

import com.zerotoonelabs.bigroup.AppExecutors;
import com.zerotoonelabs.bigroup.entity.Resource;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by temirlan on 30.11.17.
 */

public class DownloadCallback implements Callback<ResponseBody> {
    private final MutableLiveData<Resource<File>> result;

    private final AppExecutors appExecutors;
    private final Context applicationContext;
    private final String pdfName;

    public DownloadCallback(MutableLiveData<Resource<File>> result, AppExecutors appExecutors, Context applicationContext, String pdfName) {
        this.result = result;
        this.appExecutors = appExecutors;
        this.applicationContext = applicationContext;
        this.pdfName = pdfName;
    }

    @Override
    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        ResponseBody body = response.body();
        if (body != null) {
            appExecutors.diskIO().execute(() -> {
                boolean writtenToDisk = writeResponseBodyToDisk(applicationContext, pdfName, body, ".pdf");
                if (writtenToDisk) {
                    String fileName = File.separator + pdfName + ".pdf";
                    File file = new File(applicationContext.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
                            + fileName);
                    result.postValue(Resource.success(file));
                } else {
                    result.postValue(Resource.error("Запрошенный файл не найден.", null));
                }
            });
        } else {
            result.setValue(Resource.error("Запрошенный файл не найден.", null));
        }
    }

    @Override
    public void onFailure(Call<ResponseBody> call, Throwable t) {
        result.setValue(Resource.error(t.getMessage(), null));
    }

    private boolean writeResponseBodyToDisk(Context context, String pdfId, @NonNull ResponseBody body, String fileType) {
        try {
            File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS) + File.separator + pdfId + fileType);

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(file);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;
                }

                outputStream.flush();

                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }
}
