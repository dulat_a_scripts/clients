package com.zerotoonelabs.bigroup.util;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.zerotoonelabs.bigroup.R;

public class CommonDialog {
    private Context context;
    private Callback callback;

    public CommonDialog(Context context) {
        this.context = context;
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public void showThanksDialog() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_thanks);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.setCancelable(false);
        final ImageView img_thanks = dialog.findViewById(R.id.img_thanks);
        final ImageView img_close = dialog.findViewById(R.id.img_close);
        View.OnClickListener clickListener = v -> dialog.dismiss();
        img_thanks.setOnClickListener(clickListener);
        img_close.setOnClickListener(clickListener);
        dialog.show();
//        SharedPreferences sp = context.getSharedPreferences("userLogin", Context.MODE_PRIVATE);// PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean("firstLogin", false).apply();
//        boolean firstLogin = PreferenceManager.getDefaultSharedPreferences(context).getBoolean("firstLogin", true);
//        if (firstLogin) {
//
//        }
    }

    public interface Callback {
        void onClickImage();
    }
}
