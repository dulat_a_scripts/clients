package com.zerotoonelabs.bigroup.util;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.RecyclerView;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.transition.TransitionManager;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.ui.common.DataBoundListAdapter;

import timber.log.Timber;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.M;

public class CollapsibleCard extends FrameLayout {

    private boolean mExpanded = false;
    private TextView mCardTitle;
    private TextView mCardItemCount;
    private RecyclerView mCardList;
    private ImageView mExpandIcon;
    private final Transition mtoggle;
    private final String cardTitle;
    final View mRoot;
    private final int drawableStartId;

    public CollapsibleCard(Context context) {
        this(context, null);
    }

    public CollapsibleCard(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CollapsibleCard(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray arr = context.obtainStyledAttributes(attrs, R.styleable.CollapsibleCard, 0, 0);
        cardTitle = arr.getString(R.styleable.CollapsibleCard_cardTitle);
        drawableStartId = arr.getResourceId(R.styleable.CollapsibleCard_cardDrawableStart, 0);
        arr.recycle();
        mRoot = LayoutInflater.from(context)
                .inflate(R.layout.collapsible_card_content, this, true);
        View mTitleContainer = mRoot.findViewById(R.id.title_container);
        mCardTitle = mRoot.findViewById(R.id.card_title);
        mCardItemCount = mRoot.findViewById(R.id.card_item_count);
        mCardTitle.setText(cardTitle);
        if (drawableStartId != 0) {
            Drawable drawableStart = AppCompatResources.getDrawable(context, drawableStartId);
            mCardTitle.setCompoundDrawablesWithIntrinsicBounds(drawableStart, null, null, null);
            mCardTitle.setCompoundDrawablePadding(16);
            mCardTitle.setGravity(Gravity.CENTER_VERTICAL);
        }
        setTitleContentDescription(cardTitle);
        mCardList = mRoot.findViewById(R.id.card_description);
        mCardList.setHasFixedSize(true);
        mCardList.setNestedScrollingEnabled(false);

//        mCardList.setHtmlText(cardDescription);
        mExpandIcon = mRoot.findViewById(R.id.expand_icon);
        if (SDK_INT > M) {
            mExpandIcon.setImageTintList(
                    AppCompatResources.getColorStateList(context, R.color.collapsing_section));
        }
        mtoggle = TransitionInflater.from(getContext())
                .inflateTransition(R.transition.info_card_toggle);
        final OnClickListener expandClick = new OnClickListener() {
            @Override
            public void onClick(View v) {
                expand();
            }
        };
        mTitleContainer.setOnClickListener(expandClick);
    }

    public void expand() {
        mExpanded = !mExpanded;
        mtoggle.setDuration(mExpanded ? 300L : 200L);
        TransitionManager.beginDelayedTransition((ViewGroup) mRoot.getParent(), mtoggle);
        mCardList.setVisibility(mExpanded ? VISIBLE : GONE);
        mExpandIcon.setRotation(mExpanded ? 180f : 0f);
        // activated used to tint controls when expanded
        mExpandIcon.setActivated(mExpanded);
//            mCardTitle.setActivated(mExpanded);    чтобы текст CardView не менялся при раскрытии
        mCardItemCount.setActivated(mExpanded);
        setTitleContentDescription(cardTitle);
    }

    public void setCardList(@NonNull DataBoundListAdapter adapter) {
        mCardList.setAdapter(adapter);
    }

    public DataBoundListAdapter getCardList() {
        return (DataBoundListAdapter) mCardList.getAdapter();
    }

    public void setItemCount(int count) {
        mCardItemCount.setText(String.valueOf(count));
    }

    private void setTitleContentDescription(String cardTitle) {
        Resources res = getResources();
        mCardTitle.setContentDescription(cardTitle + ", " +
                (mExpanded ? res.getString(R.string.expanded) :
                        res.getString(R.string.collapsed)));
    }

    public void setCardTitle(String newTitle) {
        mCardTitle.setText(newTitle);
    }
}