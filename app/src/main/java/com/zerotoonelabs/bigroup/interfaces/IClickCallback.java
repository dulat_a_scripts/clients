package com.zerotoonelabs.bigroup.interfaces;

/**
 * Created by AKaltayev on 10.04.2018.
 */

public interface IClickCallback {
    void onItemClick(Object object);
    void onClick();
}
