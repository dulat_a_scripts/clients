package com.zerotoonelabs.bigroup;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.zerotoonelabs.bigroup.adapters.HistoryInvoiceAdapter;
import com.zerotoonelabs.bigroup.api.RetrofitApi;
import com.zerotoonelabs.bigroup.entity.PaymentHistory;
import com.zerotoonelabs.bigroup.entity.Utility;
import com.zerotoonelabs.bigroup.util.AutoClearedValue;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HistoryInvoiceActivity extends AppCompatActivity {
    int invoiceId = 0;
    Retrofit retrofitApi = null;
    RecyclerView recyclerView = null;
    private String token = "", date = "";
    String LOG = "HistoryInvoiceActivity";
    List<Utility> invoiceList = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(LOG, "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_invoice);
        Bundle args = getIntent().getExtras();
        if (args != null) {
            invoiceId = args.getInt("InvoiceId");
            token = args.getString("token");
            date = args.getString("date");
        }
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.action_bar_title);
        TextView title = findViewById(getResources().getIdentifier("actionbar_title", "id", getPackageName()));
        title.setText(date);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        recyclerView = findViewById(R.id.invoiceRecycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(HistoryInvoiceActivity.this));
        setData();
    }

    void setData () {
        Log.i(LOG, "setData()");
        retrofitApi = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(RetrofitApi.url)
                .build();
        RetrofitApi response = retrofitApi.create(RetrofitApi.class);
        response.getHistoryInvoice(token, invoiceId).enqueue(new Callback<List<Utility>>() {
            @Override
            public void onResponse(Call<List<Utility>> call, Response<List<Utility>> response) {
                if (response.body() != null) {
                    invoiceList = response.body();
                    recyclerView.setAdapter(new HistoryInvoiceAdapter(invoiceList));
                    recyclerView.getAdapter().notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<List<Utility>> call, Throwable t) {
                Toast.makeText(HistoryInvoiceActivity.this, t.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
