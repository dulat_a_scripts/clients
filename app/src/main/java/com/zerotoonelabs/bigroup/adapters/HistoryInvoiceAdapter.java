package com.zerotoonelabs.bigroup.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.entity.Utility;

import java.util.List;

/**
 * Created by zhan on 3/25/18.
 */

public class HistoryInvoiceAdapter extends RecyclerView.Adapter<HistoryInvoiceAdapter.ViewHolder> {
    private List<Utility> invoiceList = null;
    String LOG = "HistoryInvoiceAdapter";

    public HistoryInvoiceAdapter(List<Utility> invoiceList) {
        this.invoiceList = invoiceList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView uslugaText, oplachenoText, summaText;
        ImageView checked;
        public ViewHolder(View itemView) {
            super(itemView);
            uslugaText = itemView.findViewById(R.id.uslugaText);

            summaText = itemView.findViewById(R.id.summaText);

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i(LOG, "onCreateViewHolder()");
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_invoice_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder h, int position) {
        Log.i(LOG, "onBindViewHolder()");
        h.uslugaText.setText(invoiceList.get(position).getName());
        h.summaText.setText(invoiceList.get(position).getPaidAmount() + " тг");


    }

    @Override
    public int getItemCount() {
        return invoiceList.size();
    }
}
