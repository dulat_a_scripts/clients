package com.zerotoonelabs.bigroup.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.entity.Wooppay.DebitInfo;
import com.zerotoonelabs.bigroup.entity.Wooppay.WooppayCheck;
import com.zerotoonelabs.bigroup.entity.kassa24.Service;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by AKaltayev on 19.04.2018.
 */

public class Kassa24Adapter extends RecyclerView.Adapter<Kassa24Adapter.ViewHolder> {

    private List<Service> services;
    private ArrayList<Double> initAmounts;
    private Activity activity;
    private DecimalFormat df = new DecimalFormat("##.##");
    private Kassa24Adapter.OnDataChangeListener mOnDataChangeListener;

    public Kassa24Adapter(Activity activity, List<Service> services, Kassa24Adapter.OnDataChangeListener dataChangeListener) {
        this.services = services;
        this.activity = activity;
        this.mOnDataChangeListener = dataChangeListener;

        initAmounts = new ArrayList<>();
        for (Service service : services)
            initAmounts.add(Double.valueOf(service.getFixSum()));
    }

    public interface OnDataChangeListener{
        void onDataChanged(boolean trigger);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        Switch switchService;
        TextView nachislenoText, dolgText;
        EditText oplataText;
        View d1, d2;
        LinearLayout l1, l2, l3;
        public ViewHolder(View itemView) {
            super(itemView);
            switchService = itemView.findViewById(R.id.switchService);
            nachislenoText = itemView.findViewById(R.id.nachislenoText);
            dolgText = itemView.findViewById(R.id.dolgText);
            oplataText = itemView.findViewById(R.id.oplataText);
            d1 = itemView.findViewById(R.id.divider1);
            d2 = itemView.findViewById(R.id.divider2);
            l1 = itemView.findViewById(R.id.linearLayout1);
            l2 = itemView.findViewById(R.id.linearLayout2);
            l3 = itemView.findViewById(R.id.linearLayout3);
        }
    }
    @Override
    public Kassa24Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.monthly_payment_layout, parent, false);
        return new Kassa24Adapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(Kassa24Adapter.ViewHolder h, int position) {
        Service currentItem = services.get(position);

        h.switchService.setText(currentItem.getServiceName());
        h.nachislenoText.setText(formatTotalSum(initAmounts.get(position)) + " \u20B8");
        h.dolgText.setText(((currentItem.getDebtInfo() == null) ? "0" : currentItem.getDebtInfo()) + " \u20B8");
        h.oplataText.setText( String.valueOf(currentItem.getFixSum()) );

        h.switchService.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                h.oplataText.setText(String.valueOf(initAmounts.get(h.getAdapterPosition())));
                h.oplataText.setEnabled(true);
                //wooppayCheck.getDebtInfo().get(h.getAdapterPosition()).setAmount(currentItem.getAmount());

                h.d1.setVisibility(View.VISIBLE);
                h.d2.setVisibility(View.VISIBLE);
                h.l1.setVisibility(View.VISIBLE);
                h.l2.setVisibility(View.VISIBLE);
                h.l3.setVisibility(View.VISIBLE);
            } else {
                h.oplataText.setText("0");
                h.oplataText.setEnabled(false);
                //wooppayCheck.getDebtInfo().get(h.getAdapterPosition()).setAmount(0.0);

                h.d1.setVisibility(View.GONE);
                h.d2.setVisibility(View.GONE);
                h.l1.setVisibility(View.GONE);
                h.l2.setVisibility(View.GONE);
                h.l3.setVisibility(View.GONE);
            }
        });

        h.oplataText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().equals(""))
                    currentItem.setFixSum(charSequence.toString());
                else
                    currentItem.setFixSum("0.0");

                if (mOnDataChangeListener != null) {
                    mOnDataChangeListener.onDataChanged(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {}
        });
    }

    private String formatTotalSum(double value){
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();

        symbols.setGroupingSeparator(' ');
        formatter.setDecimalFormatSymbols(symbols);
        return formatter.format(value);
    }


    @Override
    public int getItemCount() {
        return services.size();
    }

    /**
     * Get a new object of WooppayCheck class
     * @return
     */
    public List<Service> getData() {
        return services;
    }

    public double getTotalAmount(){
        double initTotalSum = 0.0;
        for (Service service : services){
            initTotalSum += Double.valueOf(service.getFixSum());
        }
        return initTotalSum;
    }

    public String getTotalSum() {
        if (getTotalAmount() > 0.0)
            return activity.getString(R.string.pay_button_label2) + " " + formatTotalSum(getTotalAmount()) + " \u20B8";
        else
            return activity.getString(R.string.pay_button_label2) + " " + ((getTotalAmount() <= 0 ) ? 0 : df.format(getTotalAmount())) + " \u20B8";
    }
}