package com.zerotoonelabs.bigroup.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.entity.Utility;
import com.zerotoonelabs.bigroup.entity.Wooppay.DebitInfo;
import com.zerotoonelabs.bigroup.entity.Wooppay.WooppayCheck;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Created by zhan on 3/26/18.
 */

public class WooppayAdapter extends RecyclerView.Adapter<WooppayAdapter.ViewHolder> {

    private WooppayCheck wooppayCheck = null;
    //private WooppayCheck initWooppayCheck = null;

    private ArrayList<Double> initAmounts;

    Activity activity;
    DecimalFormat df = new DecimalFormat("##.##");
    OnDataChangeListener mOnDataChangeListener;

    public WooppayAdapter(WooppayCheck wooppayCheck, Activity activity, OnDataChangeListener dataChangeListener) {
        this.wooppayCheck = wooppayCheck;
        //this.initWooppayCheck = wooppayCheck;
        this.activity = activity;
        this.mOnDataChangeListener = dataChangeListener;

        initAmounts = new ArrayList<>();
        for (DebitInfo debitInfo : wooppayCheck.getDebtInfo())
            initAmounts.add(debitInfo.getAmount());
    }

    public interface OnDataChangeListener{
        void onDataChanged(boolean trigger);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        Switch switchService;
        TextView nachislenoText, dolgText;
        EditText oplataText;
        View d1, d2;
        LinearLayout l1, l2, l3;
        public ViewHolder(View itemView) {
            super(itemView);
            switchService = itemView.findViewById(R.id.switchService);
            nachislenoText = itemView.findViewById(R.id.nachislenoText);
            dolgText = itemView.findViewById(R.id.dolgText);
            oplataText = itemView.findViewById(R.id.oplataText);
            d1 = itemView.findViewById(R.id.divider1);
            d2 = itemView.findViewById(R.id.divider2);
            l1 = itemView.findViewById(R.id.linearLayout1);
            l2 = itemView.findViewById(R.id.linearLayout2);
            l3 = itemView.findViewById(R.id.linearLayout3);
        }
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.monthly_payment_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder h, int position) {
        DebitInfo currentItem = wooppayCheck.getDebtInfo().get(position);

        h.switchService.setText(currentItem.getServiceName());
        h.nachislenoText.setText(formatTotalSum(initAmounts.get(position)) + " \u20B8");
        h.dolgText.setText(((currentItem.getInvoiceNum() == null) ? "0" : currentItem.getInvoiceNum()) + " \u20B8");
        h.oplataText.setText( String.valueOf(wooppayCheck.getDebtInfo().get(position).getAmount()) );

        h.switchService.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                h.oplataText.setText(String.valueOf(initAmounts.get(h.getAdapterPosition())));
                h.oplataText.setEnabled(true);
                //wooppayCheck.getDebtInfo().get(h.getAdapterPosition()).setAmount(currentItem.getAmount());

                h.d1.setVisibility(View.VISIBLE);
                h.d2.setVisibility(View.VISIBLE);
                h.l1.setVisibility(View.VISIBLE);
                h.l2.setVisibility(View.VISIBLE);
                h.l3.setVisibility(View.VISIBLE);
            } else {
                h.oplataText.setText("0");
                h.oplataText.setEnabled(false);
                //wooppayCheck.getDebtInfo().get(h.getAdapterPosition()).setAmount(0.0);

                h.d1.setVisibility(View.GONE);
                h.d2.setVisibility(View.GONE);
                h.l1.setVisibility(View.GONE);
                h.l2.setVisibility(View.GONE);
                h.l3.setVisibility(View.GONE);
            }
        });

        h.oplataText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().equals(""))
                    wooppayCheck.getDebtInfo().get(h.getAdapterPosition()).setAmount(Double.valueOf(charSequence.toString()));
                else
                    wooppayCheck.getDebtInfo().get(h.getAdapterPosition()).setAmount(0.0);

                if (mOnDataChangeListener != null) {
                    mOnDataChangeListener.onDataChanged(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {}
        });
    }

    private String formatTotalSum(double value){
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();

        symbols.setGroupingSeparator(' ');
        formatter.setDecimalFormatSymbols(symbols);
        return formatter.format(value);
    }


    @Override
    public int getItemCount() {
        return wooppayCheck.getDebtInfo().size();
    }

    /**
     * Get a new object of WooppayCheck class
     * @return
     */
    public WooppayCheck getWooppayCheck() {
        double totalAmount = 0.0;
        //wooppayCheck.setAmount(new BigDecimal(totalAmount).setScale(2, RoundingMode.HALF_UP).doubleValue());
        for (DebitInfo debitInfo : wooppayCheck.getDebtInfo()){
            totalAmount += debitInfo.getAmount();
        }
        wooppayCheck.setAmount(totalAmount);
        return wooppayCheck;
    }

    public String getTotalSum() {
        double initTotalSum = 0.0;
        for (DebitInfo debitInfo : wooppayCheck.getDebtInfo()){
            initTotalSum += debitInfo.getAmount();
        }

        if (initTotalSum > 0.0)
            return activity.getString(R.string.pay_button_label2) + " " + formatTotalSum(initTotalSum) + " \u20B8";
        else
            return activity.getString(R.string.pay_button_label2) + " " + ((initTotalSum <= 0 ) ? 0 : df.format(initTotalSum)) + " \u20B8";
    }
}
