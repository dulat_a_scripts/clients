package com.zerotoonelabs.bigroup.adapters;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.databinding.FlatItemBinding;
import com.zerotoonelabs.bigroup.ui.common.DataBoundListAdapter;
import com.zerotoonelabs.bigroup.util.Objects;
import com.zerotoonelabs.bigroup.entity.Flat;

public class FlatListAdapter extends DataBoundListAdapter<Flat, FlatItemBinding> {
    private final ClickCallback itemClickCallback;

    public FlatListAdapter(ClickCallback itemClickCallback) {
        this.itemClickCallback = itemClickCallback;
    }

    @Override
    protected FlatItemBinding createBinding(ViewGroup parent) {
        FlatItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.flat_item,
                        parent, false);

        binding.getRoot().setOnClickListener(v -> {
            Flat flat = binding.getFlat();
            itemClickCallback.onClick(flat);
        });
        return binding;
    }

    @Override
    protected void bind(FlatItemBinding binding, Flat item, int position) {
        binding.setFlat(item);
    }


    @Override
    protected boolean areItemsTheSame(Flat oldItem, Flat newItem) {
        return Objects.equals(oldItem.getPropertyGuid(), newItem.getPropertyGuid());
    }

    @Override
    protected boolean areContentsTheSame(Flat oldItem, Flat newItem) {
        return Objects.equals(oldItem.isSelected(), newItem.isSelected());
    }

    public interface ClickCallback {
        void onClick(Flat flat);
    }
}