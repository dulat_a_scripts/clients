package com.zerotoonelabs.bigroup.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.entity.BiServiceEmployee;

import java.util.List;

/**
 * Created by temirlan on 17.11.17.
 */

public class BiServEmpoyeesAdapter extends RecyclerView.Adapter<BiServEmpoyeesAdapter.ViewHolder> {

    private List<BiServiceEmployee> empoyees;

    public BiServEmpoyeesAdapter(List<BiServiceEmployee> employees) {
        this.empoyees = employees;
    }

    @Override
    public BiServEmpoyeesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bi_serv_employee, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BiServEmpoyeesAdapter.ViewHolder holder, int position) {
        View view = holder.itemView;
        BiServiceEmployee employee = empoyees.get(position);
        TextView fullName = view.findViewById(R.id.employee_fullname);
        fullName.setText(employee.getFullName());
        TextView empPosition = view.findViewById(R.id.employee_position);
        empPosition.setText(employee.getEmployeePosition());
        TextView phone = view.findViewById(R.id.phone_number);
        if (!employee.getPhoneNumber().isEmpty()) {
            phone.setText(employee.getPhoneNumber());
        }
        TextView email = view.findViewById(R.id.email);
        if (!employee.getPhoneNumber().isEmpty()) {
            email.setText(employee.getPhoneNumber());
        }
        ImageView avatar = view.findViewById(R.id.avatar);
        if (employee.getAvatar() != null) {
            avatar.setImageBitmap(employee.getAvatar());
        }

    }

    @Override
    public int getItemCount() {
        return empoyees.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}


