package com.zerotoonelabs.bigroup.adapters;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zerotoonelabs.bigroup.Notification;
import com.zerotoonelabs.bigroup.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 13.09.2017.
 */

public class MyNotificationsAdapter extends RecyclerView.Adapter<MyNotificationsAdapter.ViewHolder> {

    private int type;
    private final ListItemClickListener mOnClickListener;
    private List<Notification> notificationList = new ArrayList<>();

    public MyNotificationsAdapter(ListItemClickListener mOnClickListener, List<Notification> notificationList, int type) {
        this.mOnClickListener = mOnClickListener;
        this.notificationList = notificationList;
        this.type = type;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (type) {
            case 1:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.technical_notif_item, parent, false);
                break;
            case 2:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.horizontal_technical_notif, parent, false);
                break;
            case 3:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.reciept_item, parent, false);
                break;
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.agreementandpayments_item, parent, false);
                break;
        }

        return new MyNotificationsAdapter.ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if(type != 4) {

            holder.body.setText(notificationList.get(position).getText());
            holder.topic.setText(notificationList.get(position).getType());

            if (type == 3) {
                if (notificationList.get(position).getProcess().equals("Оплачено"))
                    holder.progress.setTextColor(Color.parseColor("#6bda4f"));
                else
                    holder.progress.setTextColor(Color.parseColor("#e32a5c"));
                holder.progress.setText(notificationList.get(position).getProcess());
            }
        }else{
            holder.step.setText(notificationList.get(position).getStep());
            holder.date.setText(notificationList.get(position).getDate());
            holder.sum.setText(notificationList.get(position).getSum());
        }
    }

    @Override
    public int getItemCount() {
        return notificationList.size();
    }

    public interface ListItemClickListener {
        void onListItemClick(Notification notification);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView topic, body, progress;
        TextView date, step, sum;

        public ViewHolder(View itemView) {
            super(itemView);
            if (type != 4) {
                topic = (TextView) itemView.findViewById(R.id.topic);
                body = (TextView) itemView.findViewById(R.id.body);
                if (type == 3)
                    progress = (TextView) itemView.findViewById(R.id.progress);

            } else {
                date = (TextView) itemView.findViewById(R.id.date);
                sum = (TextView) itemView.findViewById(R.id.sum);
                step = (TextView) itemView.findViewById(R.id.step);
            }
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            mOnClickListener.onListItemClick(notificationList.get(getAdapterPosition()));
        }
    }
}
