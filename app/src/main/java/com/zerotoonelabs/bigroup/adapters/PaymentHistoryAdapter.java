package com.zerotoonelabs.bigroup.adapters;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.zerotoonelabs.bigroup.HistoryInvoiceActivity;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.entity.PaymentHistory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by zhan on 3/21/18.
 */

public class PaymentHistoryAdapter
        extends RecyclerView.Adapter<PaymentHistoryAdapter.ViewHolder> {
    private List<PaymentHistory> mData;
    private Context context = null;
    private String token = null;
    String LOG = "PaymentHistoryAdapter";

    public PaymentHistoryAdapter(List<PaymentHistory> mData, Context context, String token) {
        this.mData = mData;
        this.context = context;
        this.token = token;
    }



    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView date, price, status;
        public ViewHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.textView131);
            price = itemView.findViewById(R.id.textView137);
            status = itemView.findViewById(R.id.textView150);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Log.i(LOG, "onItemClick()");
            int pos = getAdapterPosition();
            Intent intent = new Intent(context, HistoryInvoiceActivity.class);
            intent.putExtra("InvoiceId", mData.get(pos).getId());
            String date = mData.get(pos).getTimeStamp().split("T")[0];
            try {
                intent.putExtra("date", new SimpleDateFormat("dd MMMM yyyy").format(new SimpleDateFormat("yyyy-MM-dd", Locale.US).parse(date)));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            intent.putExtra("token", token);
            context.startActivity(intent);
        }

//        @Override
//        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i(LOG, "onCreateViewHolder()");
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.u_bills_history_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder h, int position) {
        Log.i(LOG, "onBindViewHolder()");
        String date = mData.get(position).getTimeStamp().split("T")[0];
        try {
            h.date.setText( new SimpleDateFormat("dd MMMM yyyy").format(new SimpleDateFormat("yyyy-MM-dd", Locale.US).parse(date)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        h.price.setText( String.valueOf(mData.get(position).getPaymentSum()) + " тг");

        Integer payment_status = mData.get(position).getPaymentStatus();
        Integer processing_status = mData.get(position).getProcessingStatus();

        if(payment_status == 1 && processing_status == 0){
            h.status.setText("оплачено");
        }else if(payment_status == 0 && processing_status == 0){
            h.status.setText("в обработке");
        }else if(payment_status == 0 && processing_status == 1){
            h.status.setText("произошла ошибка");
        }




    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
