package com.zerotoonelabs.bigroup.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.api.PicasoClient;
import com.zerotoonelabs.bigroup.api.RestClient;
import com.zerotoonelabs.bigroup.api.RetrofitApi;
import com.zerotoonelabs.bigroup.authui.SingleFragmentActivity;
import com.zerotoonelabs.bigroup.entity.club.ClubTicket;
import com.zerotoonelabs.bigroup.entity.club.Partner;
import com.zerotoonelabs.bigroup.fragments.club.AllPartnersFragment;
import com.zerotoonelabs.bigroup.fragments.club.PartnerDetailsFragment;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;

/**
 * Created by AKaltayev on 05.04.2018.
 */

public class PartnersAdapter extends RecyclerView.Adapter<PartnersAdapter.ViewHolder> {

    private ArrayList<Partner> partners;
    private Context context;

    public PartnersAdapter(ArrayList<Partner> partners){
        this.partners = partners;
    }

    @Override
    public int getItemCount(){
        return partners.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        LinearLayout item;
        public TextView title;
        public TextView discount;
        public ImageView icon;
        public View divider;

        ViewHolder(View view){
            super(view);
            item = view.findViewById(R.id.item);
            title = item.findViewById(R.id.itemTitle);
            discount = item.findViewById(R.id.itemDiscount);
            icon = item.findViewById(R.id.itemIcon);
            divider = item.findViewById(R.id.divider);
        }
    }

    @Override
    public PartnersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.partner_item, parent, false);
        this.context = parent.getContext();
        return new PartnersAdapter.ViewHolder(view);
    }



    @Override
    public void onBindViewHolder(final PartnersAdapter.ViewHolder viewHolder, final int position){
        Partner partner = partners.get(position);

        Picasso.get()
                .load(partner.getAva())
                .into(viewHolder.icon, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError(Exception e) {
                        viewHolder.icon.setImageDrawable(context.getResources().getDrawable(R.drawable.logo_bi_club));
                    }
                });

        viewHolder.title.setText(partner.getName());
        viewHolder.discount.setText("Скидка от " + partner.getDiscountFrom() + " %");
        if (position == getItemCount() - 1)
            viewHolder.divider.setVisibility(View.GONE);
        else
            viewHolder.divider.setVisibility(View.VISIBLE);
        viewHolder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SingleFragmentActivity.class);
                intent.putExtra(SingleFragmentActivity.PARTNERS_DETAIL, true);
                intent.putExtra(PartnerDetailsFragment.PARTNER, partner);
                context.startActivity(intent);
            }
        });
    }
}