package com.zerotoonelabs.bigroup.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.zerotoonelabs.bigroup.R;

/**
 * Created by Daniyar on 16/08/2017.
 */

public class MyApartmentsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final ListItemClickListener mOnClickListener;

    public MyApartmentsAdapter(ListItemClickListener mOnClickListener) {
        this.mOnClickListener = (ListItemClickListener) mOnClickListener;
    }

    public interface ListItemClickListener {
        void onListItemClick(String indexNumber);
    }


    class CurrentAptVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        LinearLayout spravkaLinear, service_commentLinear, utilitiesLinear;
        ImageView imageView;
        public CurrentAptVH(View view) {
            super(view);
            service_commentLinear = (LinearLayout) view.findViewById(R.id.service_comment);
            spravkaLinear = (LinearLayout) view.findViewById(R.id.spravka);
            utilitiesLinear = (LinearLayout) view.findViewById(R.id.utilities);
            imageView = (ImageView) view.findViewById(R.id.apt_scheme);
            service_commentLinear.setOnClickListener(this);
            service_commentLinear.setTag(service_commentLinear.getId(),"two");
            spravkaLinear.setOnClickListener(this);
            spravkaLinear.setTag(spravkaLinear.getId(),"one");
            utilitiesLinear.setOnClickListener(this);
            utilitiesLinear.setTag(utilitiesLinear.getId(),"three");
            imageView.setOnClickListener(this);
            imageView.setTag(imageView.getId(), "four");

        }

        @Override
        public void onClick(View v) {
            mOnClickListener.onListItemClick((String) v.getTag(v.getId()));
        }
    }

    class BuyingAptVH extends RecyclerView.ViewHolder implements View.OnClickListener {




        public BuyingAptVH(View view) {
            super(view);
        }

        @Override
        public void onClick(View v) {

        }
    }

    class SellingAptVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        public SellingAptVH(View view) {
            super(view);
        }

        @Override
        public void onClick(View v) {

        }
    }

    @Override
    public int getItemViewType(int position) {
        // Just as an example, return 0 or 2 depending on position
        // Note that unlike in ListView adapters, types don't have to be contiguous
        return (position + 3) % 3;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)  {
        View view;
        switch (viewType) {
            case 0:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.current_apt_item, parent, false);
                return new CurrentAptVH(view);
            case 1:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.buying_apt_item, parent, false);
                return new BuyingAptVH(view);
            case 2:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.selling_apt_item, parent, false);
                return new SellingAptVH(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                CurrentAptVH currentAptVH = (CurrentAptVH) holder;
                break;
            case 1:
                BuyingAptVH buyingAptVH = (BuyingAptVH) holder;
                break;
            case 2:
                SellingAptVH sellingAptVH = (SellingAptVH) holder;
                break;
            default:
                CurrentAptVH currentAptVH1 = (CurrentAptVH) holder;
                break;
        }


    }

    @Override
    public int getItemCount() {
        return 3;
    }
}
