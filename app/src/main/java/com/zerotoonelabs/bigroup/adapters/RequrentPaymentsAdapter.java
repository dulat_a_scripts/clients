package com.zerotoonelabs.bigroup.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.authui.SingleFragmentActivity;
import com.zerotoonelabs.bigroup.entity.RequrentPayment;
import com.zerotoonelabs.bigroup.fragments.club.PartnerDetailsFragment;

import java.util.ArrayList;

/**
 * Created by AKaltayev on 13.04.2018.
 */

public class RequrentPaymentsAdapter extends RecyclerView.Adapter<RequrentPaymentsAdapter.ViewHolder> {

    private ArrayList<RequrentPayment> payments;
    private Context context;

    public RequrentPaymentsAdapter(ArrayList<RequrentPayment> payments) {
        this.payments = payments;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.requrent_p_item, parent, false);
        context = parent.getContext();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        RequrentPayment payment = payments.get(position);

        holder.desc.setText(payment.getDescription());
        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SingleFragmentActivity.class);
                intent.putExtra(SingleFragmentActivity.REQ_PAY_DETAIL, true);
                intent.putExtra("REQ_PAY", payment);
                context.startActivity(intent);
                Log.d("TEST", payment.toString());
            }
        });
    }

    @Override
    public int getItemCount() {
        return payments.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout item;
        TextView desc;

        public ViewHolder(View view) {
            super(view);
            item = (LinearLayout) view.findViewById(R.id.item);
            desc = (TextView) view.findViewById(R.id.desc);
        }
    }
}
