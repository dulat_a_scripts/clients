package com.zerotoonelabs.bigroup.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.zerotoonelabs.bigroup.ui.sales.StocksFragment;

/**
 * Created by Daniyar on 18/07/2017.
 */

public class SalesPagerAdapter extends FragmentStatePagerAdapter {

    public SalesPagerAdapter(FragmentManager supportFragmentManager) {
        super(supportFragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;

        switch (position) {
            case 0:
                fragment = StocksFragment.newInstance(1);
                break;
            case 1:
                fragment = StocksFragment.newInstance(2);
                break;
            default:
                fragment = StocksFragment.newInstance(1);
                break;
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String s = null;

        if (position == 0) {
            s = "Актуальные акции";
        } else {
            s = "Архив";
        }
        return s;
    }
}
