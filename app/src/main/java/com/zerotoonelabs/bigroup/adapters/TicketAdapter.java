package com.zerotoonelabs.bigroup.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.entity.club.ClubTicket;

import java.util.ArrayList;

/**
 * Created by AKaltayev on 04.04.2018.
 */

public class TicketAdapter extends RecyclerView.Adapter<TicketAdapter.ViewHolder> {

    private ArrayList<ClubTicket> clubTickets;

    public TicketAdapter(ArrayList<ClubTicket> clubTickets){
        this.clubTickets = clubTickets;
    }

    @Override
    public int getItemCount(){
        return clubTickets.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        LinearLayout item;
        public TextView title;
        public TextView amount;
        public View divider;

        ViewHolder(View view){
            super(view);
            item = view.findViewById(R.id.item);
            title = item.findViewById(R.id.itemTitle);
            amount = item.findViewById(R.id.itemAmount);
            divider = item.findViewById(R.id.divider);
        }
    }

    @Override
    public TicketAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ticket_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final TicketAdapter.ViewHolder viewHolder, final int position){
        ClubTicket ticket = clubTickets.get(position);

        viewHolder.title.setText(ticket.getTitle());
        viewHolder.amount.setText(ticket.getAmount());

        if (position == getItemCount() - 1)
            viewHolder.divider.setVisibility(View.GONE);
        else
            viewHolder.divider.setVisibility(View.VISIBLE);

        viewHolder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("TEST", "ticket_" + ticket.getTitle() + "_pressed");
            }
        });
    }
}
