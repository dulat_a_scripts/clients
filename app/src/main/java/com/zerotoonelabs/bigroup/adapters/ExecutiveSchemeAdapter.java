package com.zerotoonelabs.bigroup.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zerotoonelabs.bigroup.R;

/**
 * Created by Daniyar on 11/15/17.
 */

public class ExecutiveSchemeAdapter extends RecyclerView.Adapter<ExecutiveSchemeAdapter.ExecVH> {
    @Override
    public ExecVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.executive_item, parent, false);
        return new ExecVH(view);
    }

    @Override
    public void onBindViewHolder(ExecVH holder, int position) {
        holder.number.setText(position + 1 + "");

    }


    @Override
    public int getItemCount() {
        return 4;
    }


    class ExecVH extends RecyclerView.ViewHolder {
        public TextView number;

        public ExecVH(View itemView) {
            super(itemView);
            number = itemView.findViewById(R.id.number_tv);
        }
    }
}
