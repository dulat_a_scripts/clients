package com.zerotoonelabs.bigroup.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.TechnicalNotifActivity;

/**
 * Created by Daniyar on 16/08/2017.
 */

public class NotificationsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context mContext;


    public NotificationsAdapter(Context context) {
        mContext = context;
    }

    class TechnicalNotifVH extends RecyclerView.ViewHolder {
        public TechnicalNotifVH(View view) {
            super(view);
        }
    }

    class SimpleNotifVH extends RecyclerView.ViewHolder {
        public SimpleNotifVH(View view) {
            super(view);
        }
    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return 0;
        else
            return 1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case 0:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.technical_notif_item, parent, false);
                return new TechnicalNotifVH(view);
            case 1:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.simple_notif_item, parent, false);
                return new SimpleNotifVH(view);

        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                TechnicalNotifVH currentAptVH = (TechnicalNotifVH) holder;
                currentAptVH.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mContext.startActivity(new Intent(mContext, TechnicalNotifActivity.class));
                    }
                });
                break;
            case 1:
                SimpleNotifVH buyingAptVH = (SimpleNotifVH) holder;
                buyingAptVH.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mContext.startActivity(new Intent(mContext, SimpleNotifActivity.class));
                    }
                });
                break;
            default:
                TechnicalNotifVH currentAptVH1 = (TechnicalNotifVH) holder;
                break;
        }


    }

    @Override
    public int getItemCount() {
        return 3;
    }
}
