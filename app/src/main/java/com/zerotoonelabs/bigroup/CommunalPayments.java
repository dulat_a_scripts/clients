package com.zerotoonelabs.bigroup;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zerotoonelabs.bigroup.adapters.MyNotificationsAdapter;

import java.util.ArrayList;
import java.util.List;

public class CommunalPayments extends AppCompatActivity implements MyNotificationsAdapter.ListItemClickListener {

    private List<Notification> notificationList = new ArrayList<>(), goodReceipt = new ArrayList<>(), badReceipt = new ArrayList<>();
    private TextView notificationsCount, archiveText, receiptText;
    private RecyclerView recyclerView2;
    private RelativeLayout receipt, archive;
    private Integer receiptChecker = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_communal_payments);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        notificationsCount = (TextView) findViewById(R.id.noificationText);

        receipt = (RelativeLayout) findViewById(R.id.receipt);
        archive = (RelativeLayout) findViewById(R.id.archive);
        receiptText = (TextView) findViewById(R.id.receiptText);
        archiveText = (TextView) findViewById(R.id.archiveText);

        receipt.setBackgroundColor(Color.parseColor("#004a92"));


        Notification notification = new Notification("Коммунальные платежи", "У Вас новая квитанция на сумму 34 980 тг по счету 0989890", 1);
        notificationList.add(notification);
        notificationsCount.setText("Уведомлений: " + notificationList.size());
        int recyclerViewOrientation = LinearLayoutManager.VERTICAL;
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, recyclerViewOrientation, false);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycleView);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        MyNotificationsAdapter ad = new MyNotificationsAdapter(this, notificationList, 1);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(ad);

        notification = new Notification("Период: Август 2016", "Квитанция 0933920\n" +
                "На сумму 34 980 тг", 3);
        notification.setProcess("Оплачено");
        goodReceipt.add(notification);
        notification = new Notification("Период: Июль 2016","Квитанция 0933920\n" +
                "На сумму 34 980 тг",3);
        notification.setProcess("Оплачено");
        goodReceipt.add(notification);
        notification = new Notification("Период: Сентябрь 2016", "Квитанция 0933920\n" +
                "На сумму 34 980 тг", 3);
        notification.setProcess("Нужно оплатить до 10.10.2017");
        badReceipt.add(notification);

        LinearLayoutManager layoutManager2 = new LinearLayoutManager(this, recyclerViewOrientation, false);
        recyclerView2 = (RecyclerView) findViewById(R.id.secondRecycle);
        recyclerView2.setLayoutManager(layoutManager2);
        recyclerView2.setHasFixedSize(true);
        recyclerView2.setNestedScrollingEnabled(false);
        MyNotificationsAdapter ad2 = new MyNotificationsAdapter(this, badReceipt, 3);
        recyclerView2.setAdapter(ad2);


        receipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                receipt.setBackgroundColor(Color.parseColor("#004a92"));
                archive.setBackgroundColor(Color.parseColor("#ffffff"));
                receiptText.setTextColor(Color.parseColor("#ffffff"));
                archiveText.setTextColor(Color.parseColor("#004a92"));
                MyNotificationsAdapter ad2 = new MyNotificationsAdapter(CommunalPayments.this, badReceipt, 3);
                recyclerView2.setAdapter(ad2);
            }
        });

        archive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                archive.setBackgroundColor(Color.parseColor("#004a92"));
                receipt.setBackgroundColor(Color.parseColor("#ffffff"));
                archiveText.setTextColor(Color.parseColor("#ffffff"));
                receiptText.setTextColor(Color.parseColor("#004a92"));
                MyNotificationsAdapter ad2 = new MyNotificationsAdapter(CommunalPayments.this, goodReceipt, 3);
                recyclerView2.setAdapter(ad2);
            }
        });



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

    @Override
    public void onListItemClick(Notification notification) {

    }
}
