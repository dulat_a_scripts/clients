package com.zerotoonelabs.bigroup.api;

import com.jakewharton.picasso.OkHttp3Downloader;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by AKaltayev on 05.04.2018.
 */

public class RestClient {

    public static RetrofitApi getRestProvider(){
        /*OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(30, TimeUnit.SECONDS);
        builder.readTimeout(30, TimeUnit.SECONDS);
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.addInterceptor(logging);*/

        Retrofit retrofitApi = new Retrofit.Builder()
                .baseUrl(RetrofitApi.url)
                .client(getHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofitApi.create(RetrofitApi.class);
    }

    public static Kassa24Service getRestProviderKasss24(){
        /*OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(30, TimeUnit.SECONDS);
        builder.readTimeout(30, TimeUnit.SECONDS);
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.addInterceptor(logging);*/

        Retrofit retrofitApi = new Retrofit.Builder()
                .baseUrl("https://onepay.kassa24.kz/")
                .client(getHttpClient3())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofitApi.create(Kassa24Service.class);
    }

    public static RetrofitApi getRestProvider2(){
        /*OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(30, TimeUnit.SECONDS);
        builder.readTimeout(30, TimeUnit.SECONDS);
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.addInterceptor(logging);*/

        Retrofit retrofitApi = new Retrofit.Builder()
                .baseUrl(RetrofitApi.url)
                .client(getHttpClient2())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofitApi.create(RetrofitApi.class);
    }

    public static OkHttpClient getHttpClient2(){
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(30, TimeUnit.SECONDS);
        builder.readTimeout(30, TimeUnit.SECONDS);
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.addInterceptor(logging);
        builder.addInterceptor(new BasicAuthInterceptor("pk_348c635ba69b355d6f4dc75a4a205", "02a16349d37b79838a1d0310e21bd369"));

        return builder.build();
    }

    public static OkHttpClient getHttpClient3(){
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(30, TimeUnit.SECONDS);
        builder.readTimeout(30, TimeUnit.SECONDS);
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.addInterceptor(logging);
        builder.addInterceptor(new BasicAuthInterceptor("670", "bi2017gr02ou1p5"));

        return builder.build();
    }



    public static OkHttpClient getHttpClient(){
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(30, TimeUnit.SECONDS);
        builder.readTimeout(30, TimeUnit.SECONDS);
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.addInterceptor(logging);

        return builder.build();
    }

    public static OkHttpClient getHttpClient(String token){
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(chain -> {
                    Request newRequest = chain.request().newBuilder()
                            .addHeader("Token", token)
                            .build();
                    return chain.proceed(newRequest);
                })
                .build();

        return client;
    }


}
