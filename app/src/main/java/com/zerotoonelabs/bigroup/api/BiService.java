package com.zerotoonelabs.bigroup.api;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by temirlan on 30.11.17.
 */

public interface BiService {
    @GET("api/v1/MobileClient/GetBiServicePriceList?IfPdf=1")
    Call<ResponseBody> loadPriseListPdf(@Header("Token") String token);
}
