package com.zerotoonelabs.bigroup.api;

import com.google.gson.annotations.SerializedName;
import com.zerotoonelabs.bigroup.entity.ServiceProperty;

import java.util.List;

public class ServicePropertyResponse {

    @SerializedName("service_property")
    public List<ServiceProperty> serviceProperties;

    public ServicePropertyResponse(List<ServiceProperty> serviceProperties) {
        this.serviceProperties = serviceProperties;
    }
}