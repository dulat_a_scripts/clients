package com.zerotoonelabs.bigroup.api;

public class SalesRequestResponse {
    public String message;
    public String number;
    public String error;

    public SalesRequestResponse(String message, String number, String error) {
        this.message = message;
        this.number = number;
        this.error = error;
    }
}
