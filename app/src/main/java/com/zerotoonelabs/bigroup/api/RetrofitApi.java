package com.zerotoonelabs.bigroup.api;

import com.zerotoonelabs.bigroup.WooppayActivity;
import com.zerotoonelabs.bigroup.entity.Agreement;
import com.zerotoonelabs.bigroup.entity.AutoPayRequest;
import com.zerotoonelabs.bigroup.entity.BankAccount;
import com.zerotoonelabs.bigroup.entity.CPModel;
import com.zerotoonelabs.bigroup.entity.FlatImgResponse;
import com.zerotoonelabs.bigroup.entity.PaymentHistory;
import com.zerotoonelabs.bigroup.entity.PenaltyRequest;
import com.zerotoonelabs.bigroup.entity.PenaltyResponse;
import com.zerotoonelabs.bigroup.entity.ServiceId;
import com.zerotoonelabs.bigroup.entity.Utility;
import com.zerotoonelabs.bigroup.entity.Wooppay.AuthToken;
import com.zerotoonelabs.bigroup.entity.Wooppay.FrameForm;
import com.zerotoonelabs.bigroup.entity.Wooppay.GetScript;
import com.zerotoonelabs.bigroup.entity.Wooppay.Operation;
import com.zerotoonelabs.bigroup.entity.Wooppay.SendCheckRequest;
import com.zerotoonelabs.bigroup.entity.Wooppay.WooppayCheck;
import com.zerotoonelabs.bigroup.entity.club.ClientProfile;
import com.zerotoonelabs.bigroup.entity.club.Partner;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by zhan on 3/23/18.
 */

public interface RetrofitApi {

    public static String url = "http://webapi.bi-group.org";
    //public static String urlWooppay = "http://api.yii2-stage.test.wooppay.com";
    public static String urlWooppay = "https://api-core.wooppay.com";

    public static String contentType = "application/x-www-form-urlencoded; charset=utf-8";

    @POST("api/v1/MobileClient/PostCurrentPenalty")
    Call<PenaltyResponse> loadPenalty(@Header("Token") String token, @Body PenaltyRequest request);

    @Headers({

            "Content-Type: application/json; charset=utf-8"

    })
    //@FormUrlEncoded
    @GET("api/v1/MobileClient/GetPenaltyAgreementTextAndroid")
    Call<Agreement> getPenaltyAgreementText(@Header("Token")String token,
                @Query("date") String date, @Query("contract_number") String contract_number,
                @Query("sum") String sum,@Query("address") String address);

    @GET("api/v1/MobileClient/GetPaymentsMainPage")
    Call<PaymentScheduleResponse> loadPaymentSchedule(@Header("Token") String token, @Query("contractId") String contractId, @Query("property_id") int propertyId);



    //@POST("https://api.cloudpayments.ru/subscriptions/create")
    @POST("https://api.cloudpayments.kz/subscriptions/create")
    Call<ResponseBody> createAutoPay(@Header("Authorization") String autorization, @Body AutoPayRequest request);

    @FormUrlEncoded
   // @POST("https://api.cloudpayments.ru/subscriptions/find")
    @POST("https://api.cloudpayments.kz/subscriptions/find")
    Call<CPModel> getAutoPays(@Field("accountId") String accountId);
//xx
    @FormUrlEncoded
    //@POST("https://api.cloudpayments.ru/subscriptions/update")
    @POST("https://api.cloudpayments.kz/subscriptions/update")
    Call<ResponseBody> updateAutoPay(@Field("Id") String id, @Field("amount") double amount);

    @FormUrlEncoded
    //@POST("https://api.cloudpayments.ru/subscriptions/cancel")
    @POST("https://api.cloudpayments.kz/subscriptions/cancel")
    Call<ResponseBody> cancelAutoPay(@Field("Id") String id);




    @GET("api/v1/MobileClient/GetProfile")
    Call<ClientProfile> getProfile(@Header("Token") String token,
                                   @Query("contractId") String contractId);

    @GET("api/v1/MobileClient/GetBIClubQRCode")
    Call<ResponseBody> getClubCardQRCode(@Header("Token") String token,
                                         @Query("contractId") String contractId);

    @GET("api/v1/MobileClient/GetBIClubCompanies")
    Call<Partner[]> getClubPartners(@Header("Token") String token,
                                    @Query("contractId") String contractId);

    @GET("api/v1/MobileClient/GetBIClubCompanies")
    Call<Partner[]> getClubPartnersByType(@Header("Token") String token,
                                          @Query("contractId") String contractId,
                                          @Query("type") String type);

    @GET("/api/v1/MobileClient/GetActiveBankingAccount")
    Call<List<BankAccount>> getBankAccount(@Header("Token") String token,
                                           @Query("property_id") int propertyId,
                                           @Query("contractId") String contractId);

    @GET("/api/v1/MobileClient/GetHistoryInvoices")
    Call<List<PaymentHistory>> getPaymentHistory(@Header("Token") String token, @Query("ApplicationUserId") String ApplicationUserId,@Query("PropertyGuid") String PropertyGuid);

    @GET("/api/v1/MobileClient/GetHistoryInvoiceData")
    Call<List<Utility>> getHistoryInvoice(@Header("Token") String token, @Query("InvoiceId") int invoiceId);

    @FormUrlEncoded
    @POST("/v5/auth/pseudo")
    Call<AuthToken> getAuthToken(@Header("Content-Type") String contentType,
                                 @Field("login") String login,
                                 @Field("email") String email,
                                 @Field("parentLogin") String parentLogin);

    @FormUrlEncoded
    @POST("/v5/card/get-script")
    Call<GetScript> getScript(@Header("Authorization") String token,
                              @Field("load") String load,
                              @Field("success") String success,
                              @Field("error") String error,
                              @Field("authorisation_error") String authorisation_error,
                              @Field("form_send") String form_send);

    @FormUrlEncoded
    @POST("/v5/composite/get-check")
    Call<WooppayCheck> getWooppayData(
                                    @Header("Content-Type") String contentType,
                                    @Header("Authorization") String token,
                                      @Field("service_id") Integer serviceId,
                                      @Field("account") String account);

    @FormUrlEncoded
    @POST("/v5/composite/send-check")
    Call<Operation> sendWooppayData(@Header("Authorization") String token,
                                    @Field("service_id") Integer serviceId,

                                    @Field("check")  String wooppayCheck);//


    @GET("/v5/bill/{operationId}")
    Call<ResponseBody> getOperationBill(@Header("Authorization") String token,
                                        @Path("operationId") int operationId);

    @FormUrlEncoded
    @POST("/v5/action/pay")
    Call<FrameForm> payAction(@Header("Authorization") String token,
                              @Field("operationId") String operationId,
                              @Field("paymentType") String paymentType,
                              @Field("frameCSSWidth") Integer frameCSSWidth,
                              @Field("frameCSSHeight") Integer frameCSSHeight,
                              @Field("responseUrl") String responseUrl,
                              @Field("operationData") String operationData);

    @GET("api/v1/MobileClient/GetErcServiceId")
    Call<List<ServiceId>> getServiceIds(@Header("Token") String token);

    @GET("api/v1/MobileClient/GetParkingPicture")
    Call<ResponseBody> getParkingImage(@Header("Token") String token);

    @GET("api/v1/MobileClient/GetFlatPicture")
    Call<FlatImgResponse[]> getFlatImage(@Header("Token") String token, @Query("contractId") String contractId, @Query("property_id") int propertyId);
}
