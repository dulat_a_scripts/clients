package com.zerotoonelabs.bigroup.api;

import com.zerotoonelabs.bigroup.entity.PaymentResponse;
import com.zerotoonelabs.bigroup.entity.kassa24.CheckUserResponse;
import com.zerotoonelabs.bigroup.entity.kassa24.CheckUserResponse2;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface Kassa24Service {

    @FormUrlEncoded
    @POST("checkuser")
    Call<CheckUserResponse> checkUser(@Field("serviceId") String serviceId, @Field("userId") String userId);

    @FormUrlEncoded
    @POST("checkuser")
    Call<CheckUserResponse2> checkUser2(@Field("serviceId") String serviceId, @Field("userId") String userId);

    @FormUrlEncoded
    @POST("transaction")
    Call<PaymentResponse> transaction(@FieldMap Map<String, Object> data);

}
