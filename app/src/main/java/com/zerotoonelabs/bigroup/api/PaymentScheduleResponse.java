package com.zerotoonelabs.bigroup.api;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import static android.text.TextUtils.isEmpty;

public class PaymentScheduleResponse implements Serializable{
    @SerializedName("CheckStatus")
    private Boolean checkStatus;

    @SerializedName("TotalValue") // 1680000
    public String totalValue;

    public String formattedTotalValue;
    public String formattedPaidSum;
    public float paidSum; // 954000

    @SerializedName("Dept")
    public float debt;

    public String formattedDebt;
    public Payment payment;

    @SerializedName("CurrentPayment")
    public CurrentPayment currentPayment;

    public String buttonText;


    public PaymentScheduleResponse(Boolean checkStatus, String totalValue, String formattedTotalValue,
                                   String formattedPaidSum, float paidSum, int debt, String formattedDebt, Payment payment, CurrentPayment currentPayment, String buttonText) {
        this.checkStatus = checkStatus;
        this.totalValue = totalValue;
        this.formattedTotalValue = formattedTotalValue;
        this.formattedPaidSum = formattedPaidSum;
        this.paidSum = paidSum;
        this.debt = debt;
        this.formattedDebt = formattedDebt;
        this.payment = payment;
        this.currentPayment = currentPayment;
        this.buttonText = buttonText;
    }

    public boolean shouldPay() {
        if (checkStatus != null && checkStatus && currentPayment != null) {
            if ((!isEmpty(currentPayment.publicId)) && currentPayment.amount > 0
                    && (currentPayment.leftover > 0 || debt > 0)) {
                return true;
            }
        }
        return false;
    }

    public boolean isVisible() {
        if (isEmpty(totalValue)) return true;

        double dTotalValue = Double.parseDouble(totalValue);
        int totalValue = (int) dTotalValue;

        float paid = paidSum;

        return paid <= totalValue;
    }

    public static class Payment {
        @SerializedName("Id")
        public int id;
        @SerializedName("ContractId")
        public int contractId;
        @SerializedName("DueDate")
        public String dueDate;
        @SerializedName("Value")
        public float value;
        public String formattedValue;
        @SerializedName("Stage")
        public int stage;
        @SerializedName("StageName")
        public String stageName;
        @SerializedName("Leftover")
        public float leftover;
        public String formattedLeftover;
        @SerializedName("PublicId")
        public String publicId;
        @SerializedName("ApplicationUserId")
        public String applicationUserId;
        @SerializedName("Number")
        public String number;
        @SerializedName("OrganizationName")
        public String organizationName;
        @SerializedName("Iin")
        public String iin;
        @SerializedName("Email")
        public String email;

        public Payment(int id, int contractId, String dueDate, float value, int stage, String stageName, float leftover, String publicId, String applicationUserId, String number, String organizationName, String iin, String email) {
            this.id = id;
            this.contractId = contractId;
            this.dueDate = dueDate;
            this.value = value;
            this.stage = stage;
            this.stageName = stageName;
            this.leftover = leftover;
            this.publicId = publicId;
            this.applicationUserId = applicationUserId;
            this.number = number;
            this.organizationName = organizationName;
            this.iin = iin;
            this.email = email;
        }
    }

    public static class CurrentPayment {
        @SerializedName("Id")
        public int id;
        @SerializedName("Description")
        public String description;
        @SerializedName("Amount")
        public int amount;
        @SerializedName("PersonIIN")
        public String personIin;
        @SerializedName("Currency")
        public String currency;
        @SerializedName("ContractId")
        public int contractId;
        @SerializedName("PublicId")
        public String publicId;
        @SerializedName("Email")
        public String email;
        @SerializedName("DueDate")
        public String dueDate;
        @SerializedName("Leftover")
        public float leftover;

        public CurrentPayment(int id, String description, int amount, String personIin, String currency, int contractId, String publicId, String email, String dueDate, float leftover) {
            this.id = id;
            this.description = description;
            this.amount = amount;
            this.personIin = personIin;
            this.currency = currency;
            this.contractId = contractId;
            this.publicId = publicId;
            this.email = email;
            this.dueDate = dueDate;
            this.leftover = leftover;
        }
    }
}
