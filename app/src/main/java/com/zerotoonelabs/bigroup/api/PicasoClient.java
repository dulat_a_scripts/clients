package com.zerotoonelabs.bigroup.api;

import android.content.Context;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;
import com.zerotoonelabs.bigroup.BuildConfig;

import okhttp3.OkHttpClient;

/**
 * Created by AKaltayev on 05.04.2018.
 */

public class PicasoClient {

    private static Picasso sPicasso;

    public static Picasso getPicasso() {
        if (sPicasso != null) {
            return sPicasso;
        } else {
            throw new IllegalStateException("Picasso not initialized");
        }
    }

    /**
     * Initialized shared Picasso instance
     *
     * @param context    context
     * @param httpClient OkHTTP client
     *
     * @return
     */
    public static Picasso initRestPicasso(final Context context, OkHttpClient httpClient) {
        if (sPicasso == null) {
            Picasso.Builder builder = new Picasso.Builder(context);
            builder.downloader(new OkHttp3Downloader(httpClient));
            sPicasso = builder.build();
            sPicasso.setIndicatorsEnabled(false);
            sPicasso.setLoggingEnabled(BuildConfig.DEBUG);
        }
        return sPicasso;
    }
}
