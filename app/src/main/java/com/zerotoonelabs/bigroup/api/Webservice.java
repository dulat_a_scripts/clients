package com.zerotoonelabs.bigroup.api;

import android.arch.lifecycle.LiveData;

import com.google.gson.JsonObject;
import com.zerotoonelabs.bigroup.entity.Apartments.Apartment;
import com.zerotoonelabs.bigroup.entity.BankAccount;
import com.zerotoonelabs.bigroup.entity.BsDate;
import com.zerotoonelabs.bigroup.entity.BsTime;
import com.zerotoonelabs.bigroup.entity.BuildingSite;
import com.zerotoonelabs.bigroup.entity.Flat;
import com.zerotoonelabs.bigroup.entity.FlatScheme;
import com.zerotoonelabs.bigroup.entity.Instalment;
import com.zerotoonelabs.bigroup.entity.InstalmentHistory;
import com.zerotoonelabs.bigroup.entity.KeyTransfer;
import com.zerotoonelabs.bigroup.entity.Manager;
import com.zerotoonelabs.bigroup.entity.PaymentHistory;
import com.zerotoonelabs.bigroup.entity.ReBlock;
import com.zerotoonelabs.bigroup.entity.RealEstate;
import com.zerotoonelabs.bigroup.entity.SalesRequestHistory;
import com.zerotoonelabs.bigroup.entity.SalesRequestType;
import com.zerotoonelabs.bigroup.entity.ServiceId;
import com.zerotoonelabs.bigroup.entity.ServiceRequest;
import com.zerotoonelabs.bigroup.entity.StatusAuth.StatusAuth;
import com.zerotoonelabs.bigroup.entity.StatusDoc;
import com.zerotoonelabs.bigroup.entity.Utility;
import com.zerotoonelabs.bigroup.entity.VisitNumber;
import com.zerotoonelabs.bigroup.entity.blockvisit.BlockVisit;
import com.zerotoonelabs.bigroup.entity.statusesPred.StatusPred;

import java.util.HashMap;
import java.util.List;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface Webservice {




    @POST("api/v1/MobileClient/NewServiceStatement")
    LiveData<ApiResponse<ServiceRequest>> getServiceRequest(@Header("Token") String token, @Body RequestBody body);

    @POST("authenticate")
    LiveData<ApiResponse<String>> getToken(@Header("authorization") String token);

    @POST("authenticate")
    Call<String> getToken2(@Header("authorization") String auth);
//apartments
    @GET("api/v1/salesbigroup/real_estate?")
    LiveData<ApiResponse<List<RealEstate>>> getRealEstates(@Header("Token") String token);

    @GET("api/v1/SalesBIGroup/GetImage?ifimg=1")
    Call<String> getImage(@Header("Token") String token, @Query("path") String logoUrl);

    @GET("api/v1/SalesBIGroup/block/{id}")
    LiveData<ApiResponse<List<ReBlock>>> getBlocks(@Header("Token") String token, @Path("id") String id);

    @GET("api/v1/SalesBIGroup/BuidingBlock")
    LiveData<ApiResponse<List<BuildingSite>>> getBuildingSites(@Header("Token") String token);

//    @GET("api/v1/SalesBIGroup/VizitRezervation")
//    LiveData<ApiResponse<BsDate>> getBuildingSiteDates(@Header("Token") String token, @Query("block") String id);

    @GET("api/v1/SalesBIGroup/VizitRezervation")
    Call<BsDate> getBuildingSiteDates(@Header("Token") String token, @Query("block") String id);

//    @GET("api/v1/SalesBIGroup/VizitRezervation")
//    LiveData<ApiResponse<BsTime>> getBuildingSiteTimes(@Header("Token") String token, @Query("block") String id, @Query("date") String date);

    @GET("api/v1/SalesBIGroup/VizitRezervation")
    Call<BsTime> getBuildingSiteTimes(@Header("Token") String token, @Query("block") String id, @Query("date") String date);

    @POST("api/v1/SalesBIGroup/ReserveVizit")
    Call<JsonObject> getVisitObj(@Header("Token") String token, @Body RequestBody requestBody);

    @GET("api/v1/SalesBIGroup/Flats_all/")
    Call<List<Apartment>> getApartments2(@Header("Token") String token, @QueryMap HashMap<String, String> filter, @Query("page") int page);

    @GET("api/v1/SalesBIGroup/Flats_all/")
    LiveData<ApiResponse<List<Apartment>>> getApartments(@Header("Token") String token, @QueryMap HashMap<String, String> queries);

    @GET("api/v1/SalesBIGroup/BlockVisitDate")
    LiveData<ApiResponse<List<BlockVisit>>> getBlockVisits(@Header("Token") String token);

    @GET("api/v1/salesbigroup/city/")
    Call<List<String>> getCities(@Header("Token") String token);

    @GET("api/v1/salesbigroup/real_estate?")
    Call<List<BuildingSite>> getBuildingSites(@Header("Token") String token, @Query("gorod") String city);

    @GET("api/v1/salesbigroup/block/{guid}")
    Call<List<BuildingSite>> getBlocksFilter(@Header("Token") String token, @Path("guid") String guid);

    @POST("api/v1/SalesBIGroup/CallBack")
    Call<String> sendRequest(@Header("Token") String token, @Body RequestBody body);

    @GET("api/v1/MobileClient/realpropertylist")
    LiveData<ApiResponse<ServicePropertyResponse>> getServiceProperties(@Header("Token") String token);

    @GET("api/v1/SalesBIGroup/VizitRezervationNumer")
    LiveData<ApiResponse<List<VisitNumber>>> getVisitNumbers(@Header("Token") String token, @Query("block") String blockId);

    /*
    *   Flat related requests
    */
//    @GET("api/v1/MobileClient/GetFlat")
//    LiveData<ApiResponse<Flat[]>> getFlat(@Header("Token") String token, @Query("contractId") String contractId, @Query("property_id") int propertyId);

    @GET("api/v1/MobileClient/GetFlat")
    Call<Flat[]> getFlat(@Header("Token") String token, @Query("contractId") String contractId, @Query("property_id") int propertyId);

    @GET("api/v1/MobileClient/GetHappinessMng")
    Call<Manager[]> getManager(@Header("Token") String token, @Query("real_estate") String realEstateId, @Query("contractId") String contractId);

    @GET("api/v1/MobileClient/GetResponsibleMng")
    Call<Manager[]> getManager2(@Header("Token") String token, @Query("ResponsibleGuid") String managerId, @Query("contractId") String contractId);

    @GET("api/v1/MobileClient/GetFlatStatus")
    LiveData<ApiResponse<StatusAuth>> getFlatStatus(@Header("Token") String token, @Query("contractId") String contractId, @Query("property_id") String propertyId);

    @GET("api/v1/MobileClient/GetFlats")
    Call<List<Flat>> loadFlats(@Header("Token") String token, @Query("contractId") String contractId);

    @GET("api/v1/MobileClient/GetFlats")
    LiveData<ApiResponse<List<Flat>>> loadFlats2(@Header("Token") String token, @Query("contractId") String contractId);

    @GET("api/v1/MobileClient/GetActiveBankingAccount")
    Call<BankAccount[]> loadBankAccount(@Header("Token") String token, @Query("property_id") int propertyId, @Query("contractId") String contractId);

    @GET("api/v1/MobileClient/GetHistoryInvoices")
    Call<List<PaymentHistory>> loadPaymentHistory(@Header("Token") String token, @Query("BankingAccountId") int bankAccountId);

    @GET("api/v1/MobileClient/GetHistoryInvoiceData")
    Call<List<Utility>> loadUtilities(@Header("Token") String token, @Query("InvoiceId") int invoiceId);

    @GET("api/v1/MobileClient/GetFlatSchemes")
    Call<List<FlatScheme>> loadFlatSchemes(@Header("Token") String token, @Query("property_id") int propertyId, @Query("contractId") String contractId);

    @GET("api/v1/MobileClient/GetFlatSchemesFile/{id}?IfPdf=1")
    Call<ResponseBody> loadPdf(@Header("Token") String token, @Path("id") int pdfId);

    @GET("api/v1/MobileClient/GetBiServicePriceList?IfPdf=1")
    Call<ResponseBody> loadPriseListPdf(@Header("Token") String token);


    @GET("api/v1/MobileClient/getStatusDetails3")
    Call<List<KeyTransfer>> getKeyTransfer(@Header("Token") String token, @Query("apartmentID") String propertyId);

    @GET("api/v1/MobileClient/getStatusDetails5")
    LiveData<ApiResponse<List<StatusDoc>>> getStatusDoc(@Header("Token") String token, @Query("apartmentID") String propertyId);

    @GET("api/v1/MobileClient/GetApartmentSheet?IfPdf=1")
    Call<ResponseBody> loadApartmentSheetPdf(@Header("Token") String token, @Query("property_id") int propertyId);

    @GET("api/v1/MobileClient/GetDogovorFile?IfPdf=1&type=3")
    Call<ResponseBody> loadTechPassportPdf(@Header("Token") String token, @Query("contractId") String contractId, @Query("property_id") int propertyId);

    @GET("api/v1/MobileClient/GetDogovorFile?IfPdf=1")
    Call<ResponseBody> loadStatusPdf(@Header("Token") String token, @Query("contractId") String contractId, @Query("property_id") int propertyId, @Query("type") int type);

    @GET("api/v1/MobileClient/getStatusDetails1")
    LiveData<ApiResponse<List<StatusPred>>> getStatusPred(@Header("Token") String token, @Query("contractID") String contractId, @Query("apartmentID") String propertyId);

    @GET("api/v1/MobileClient/GetPaymentsMainPage")
    Call<PaymentScheduleResponse> loadPaymentSchedule(@Header("Token") String token, @Query("contractId") String contractId, @Query("property_id") int propertyId);

    @GET("api/v1/MobileClient/getGraphikPlatezheiNEW")
    Call<List<Instalment>> loadPaymentSchedules(@Header("Token") String token, @Query("contractID") String contractId, @Query("propertyGuid") String propertyGuid);

    @GET("api/v1/MobileClient/getIstoriyaPlatezheiNEW")
    Call<List<InstalmentHistory>> loadPaymentScheduleHistory(@Header("Token") String token, @Query("contractID") String contractId, @Query("propertyGuid") String propertyGuid);

    @GET("api/v1/MobileClient/getORKrequestTypes")
    Call<List<SalesRequestType>> loadSalesRequestTypes(@Header("Token") String token);

    @GET("api/v1/MobileClient/getORKrequestsHistory")
    Call<List<SalesRequestHistory>> loadSalesRequestsHistory(@Header("Token") String token, @Query("ApplicationUserId") String applicationUserId,
                                                             @Query("PropertyGuid") String propertyGuid);

    @POST("api/v1/MobileClient/NewStatement")
    Call<SalesRequestResponse> sendSalesRequest(@Header("Token") String token, @Body RequestBody body);

    @GET("api/v1/Administration/CheckToken")
    Call<String> checkToken(@Header("Token") String token);


    @GET("api/v1/MobileClient/GetErcServiceId")
    Call<List<ServiceId>> getServiceIds(@Header("Token") String token);
}