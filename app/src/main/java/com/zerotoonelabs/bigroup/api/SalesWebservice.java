package com.zerotoonelabs.bigroup.api;

import android.arch.lifecycle.LiveData;

import com.zerotoonelabs.bigroup.entity.News;
import com.zerotoonelabs.bigroup.entity.Stock;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SalesWebservice {

    @GET("stock")
    LiveData<ApiResponse<List<Stock>>> getStocks(@Query("lang") String lang);

    @GET("news")
    LiveData<ApiResponse<List<News>>> getNews(@Query("lang") String lang);
}
