package com.zerotoonelabs.bigroup;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Format phone according to specification
     */
    public String getFormattedPhone(String phone) {
        phone = phone
                .replace("(", "")
                .replace(")", "")
                .replace(" ", "-");
        phone = phone.substring(0, 12) + "-" + phone.substring(12, phone.length());

        return phone;
    }

    public void showToast(int messageId) {
        Toast.makeText(this, messageId, Toast.LENGTH_SHORT).show();
    }
}
