package com.zerotoonelabs.bigroup;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.adapters.Kassa24Adapter;
import com.zerotoonelabs.bigroup.adapters.WooppayAdapter;
import com.zerotoonelabs.bigroup.api.Kassa24Service;
import com.zerotoonelabs.bigroup.api.RestClient;
import com.zerotoonelabs.bigroup.api.RetrofitApi;
import com.zerotoonelabs.bigroup.entity.PaymentResponse;
import com.zerotoonelabs.bigroup.entity.Resource;
import com.zerotoonelabs.bigroup.entity.Utility;
import com.zerotoonelabs.bigroup.entity.kassa24.CheckUserResponse;
import com.zerotoonelabs.bigroup.entity.kassa24.CheckUserResponse2;
import com.zerotoonelabs.bigroup.entity.kassa24.Service;
import com.zerotoonelabs.bigroup.entity.kassa24.Service2;

import org.codehaus.jackson.map.ObjectMapper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class Kassa24Activity extends AppCompatActivity {

    private RecyclerView recyclerView = null;
    private ProgressBar loading;
    private Button payButton;

    private Kassa24Adapter adapter;

    private String serviceID;
    private String contractNumber;
    private String invoiceId;

    private ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kassa24);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");
        actionBar = getSupportActionBar();

        payButton = findViewById(R.id.payButton);
        loading = findViewById(R.id.loadingProgressBar);
        recyclerView = findViewById(R.id.monthlyRecyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        loading.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        payButton.setVisibility(View.GONE);

      //  if(Library.serviceId != null){
//            serviceID = Library.serviceId;
//            contractNumber = Library.serviceNumber;
       // }else{
            serviceID = getIntent().getStringExtra("serviceID");
            contractNumber = getIntent().getStringExtra("contractNumber");
      //  }

        Log.d("TEST", serviceID +"_"+contractNumber);

        RestClient
                .getRestProviderKasss24()
                .checkUser(serviceID, contractNumber)
                .enqueue(new Callback<CheckUserResponse>() {
                    @Override
                    public void onResponse(Call<CheckUserResponse> call, retrofit2.Response<CheckUserResponse> response) {
                        try {
                            if (response.body() != null) {
                                try {
                                    Date date = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).parse(response.body().invoices.invoice.formedDate);
                                    actionBar.setTitle(getMonthName(date) + new SimpleDateFormat(" yyyy", Locale.getDefault()).format(date));
                                } catch (Exception e){
                                    e.printStackTrace();
                                }

                                List<Service> services = response.body().invoices.invoice.services.service;
                                invoiceId = response.body().invoices.invoice.invoiceId;
                                adapter = new Kassa24Adapter(Kassa24Activity.this, services, dataChangeListener);
                                recyclerView.setAdapter(adapter);
                                recyclerView.getAdapter().notifyDataSetChanged();
                                loading.setVisibility(View.GONE);
                                payButton.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.VISIBLE);

                                // initial payButton text setting
                                payButton.setText(adapter.getTotalSum());
                            }
                        } catch (NullPointerException e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<CheckUserResponse> call, Throwable t) {


                        RestClient
                                .getRestProviderKasss24()
                                .checkUser2(serviceID, contractNumber).enqueue(new Callback<CheckUserResponse2>() {
                            @Override
                            public void onResponse(Call<CheckUserResponse2> call, Response<CheckUserResponse2> response) {

                                try {

                                    try {
                                        Date date = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).parse(response.body().invoices.invoice.formedDate);
                                        actionBar.setTitle(getMonthName(date) + new SimpleDateFormat(" yyyy", Locale.getDefault()).format(date));
                                    } catch (Exception e){
                                        e.printStackTrace();
                                    }

                                    Service2 service2 = response.body().invoices.invoice.services.service;
                                    List<Service> services = new ArrayList<>();
                                    Service service = new Service();
                                    service.setFixSum(service2.getFixSum());
                                    service.setDebtInfo(service2.getDebtInfo());
                                    service.setServiceName(service2.getServiceName());
                                    service.setServiceId(service2.getServiceId());
                                    services.add(service);
                                    invoiceId = response.body().invoices.invoice.invoiceId;
                                    adapter = new Kassa24Adapter(Kassa24Activity.this, services, dataChangeListener);
                                    recyclerView.setAdapter(adapter);
                                    recyclerView.getAdapter().notifyDataSetChanged();
                                    loading.setVisibility(View.GONE);
                                    payButton.setVisibility(View.VISIBLE);
                                    recyclerView.setVisibility(View.VISIBLE);

                                    // initial payButton text setting
                                    payButton.setText(adapter.getTotalSum());
                                } catch (NullPointerException e){
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(Call<CheckUserResponse2> call, Throwable t) {
                                Toast.makeText(Kassa24Activity.this, t.toString(), Toast.LENGTH_LONG).show();
                                loading.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.GONE);
                            }
                        });
                    }
                });

        payButton.setOnClickListener(v -> payAction());
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private String getMonthName(Date date){
        int index = date.getMonth();

        if (index == 0)
            return "Январь";
        if (index == 1)
            return "Февраль";
        if (index == 2)
            return "Март";
        if (index == 3)
            return "Апрель";
        if (index == 4)
            return "Май";
        if (index == 5)
            return "Июнь";
        if (index == 6)
            return "Июль";
        if (index == 7)
            return "Август";
        if (index == 8)
            return "Сентябрь";
        if (index == 9)
            return "Октябрь";
        if (index == 10)
            return "Ноябрь";
        if (index == 11)
            return "Декабрь";
        return "Ноябрь";
    }

    private void payAction(){
        if ( !(adapter.getTotalAmount() > 0)){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(" ")
                    .setIcon(getResources().getDrawable(R.drawable.ic_alert2))
                    .setMessage("Укажите сумму оплаты хотя бы для одной услуги")
                    .setPositiveButton("OK", (arg0, arg1) -> {
                        arg0.dismiss();
                    }).show();
            return;
        }

        Map<String, Object> map = new HashMap<>();
        map.put("serviceId", serviceID);
        map.put("userId", contractNumber);
        map.put("summ", adapter.getTotalAmount());
        map.put("successUrl", "https://www.google.com");

        List<Service> services = adapter.getData();

        for (int i = 1; i <= services.size(); i++) {
            Service service = services.get(i - 1);
            map.put("subservices[" + i + "][service]", service.getServiceId());
            map.put("subservices[" + i + "][constraint]", invoiceId);
            map.put("subservices[" + i + "][amount" + (i - 1) + "]", service.getTotalSum());
        }



        loading.setVisibility(View.VISIBLE);
        payButton.setVisibility(View.GONE);
//        recyclerView.setVisibility(View.GONE);
//        payButton.setVisibility(View.GONE);

        RestClient
                .getRestProviderKasss24()
                .transaction(map).enqueue(new Callback<PaymentResponse>() {
            @Override
            public void onResponse(@NonNull Call<PaymentResponse> call, @NonNull Response<PaymentResponse> response) {
                try {
                    loading.setVisibility(View.GONE);
                    payButton.setVisibility(View.VISIBLE);
                    Intent intent = new Intent(Kassa24Activity.this, WebViewActivity.class);
                    intent.putExtra("URL", response.body().formUrl);
                    startActivity(intent);
                    //zz
                } catch (NullPointerException e){
                    e.printStackTrace();
                    payButton.setVisibility(View.VISIBLE);
                    loading.setVisibility(View.GONE);
                    payButton.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(@NonNull Call<PaymentResponse> call, @NonNull Throwable t) {
                Log.d("TEST", t.getMessage() + "_");
                try {
                    loading.setVisibility(View.GONE);
                    payButton.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.VISIBLE);
                    payButton.setVisibility(View.VISIBLE);
                } catch (NullPointerException e){
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    Kassa24Adapter.OnDataChangeListener dataChangeListener = new Kassa24Adapter.OnDataChangeListener(){
        public void onDataChanged(boolean trigger){
            if(trigger) {
                payButton.setText(adapter.getTotalSum());
            }
        }
    };

}
