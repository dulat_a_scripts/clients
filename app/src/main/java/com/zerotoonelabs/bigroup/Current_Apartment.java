package com.zerotoonelabs.bigroup;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zerotoonelabs.bigroup.adapters.MyNotificationsAdapter;

import java.util.ArrayList;
import java.util.List;

public class Current_Apartment extends AppCompatActivity implements MyNotificationsAdapter.ListItemClickListener {

    private List<Notification> notificationList;
    private Integer typeOfRecycleView = 2;
    private LinearLayout typeOfRec;
    private ImageView imageOfType;
    private TextView textOfType, notificationsCount;
    private RelativeLayout communal, contract;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current__apartment);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        notificationList = new ArrayList<>();

        typeOfRec = (LinearLayout) findViewById(R.id.typeOfRecycle);
        imageOfType = (ImageView) findViewById(R.id.imageOfType);
        textOfType = (TextView) findViewById(R.id.textOfType);
        notificationsCount = (TextView) findViewById(R.id.noificationText);
        communal = (RelativeLayout) findViewById(R.id.communal);
        contract = (RelativeLayout) findViewById(R.id.contract);

        Notification notification = new Notification("Договор и оплата", "Вам нужно внести 400 000 тг по графику платежей до 08.07.2017", 1);
        notificationList.add(notification);
        notification = new Notification("Передача ключей", "Необходимо забрать ключи до 09.09.2017", 1);
        notificationList.add(notification);
        notification = new Notification("Коммунальные услуги", "Новая квитанция на сумму 12 456 тг. Необходимо оплатить до 27.08.2017", 1);
        notificationList.add(notification);
        notification = new Notification("Договор и оплата", "Вам начислена пеня в размере 42 000 тг за просрочку взносов.", 1);
        notificationList.add(notification);

        communal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Current_Apartment.this, CommunalPayments.class);
                startActivity(intent);
            }
        });
        contract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Current_Apartment.this, AgreementAndPayments.class);
                startActivity(intent);
            }
        });

        notificationsCount.setText("Уведомлений: " + notificationList.size());

        int recyclerViewOrientation = LinearLayoutManager.HORIZONTAL;
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, recyclerViewOrientation, false);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycleView);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        MyNotificationsAdapter ad = new MyNotificationsAdapter(Current_Apartment.this, notificationList, typeOfRecycleView);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(ad);

        typeOfRec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (typeOfRecycleView == 1) {
                    typeOfRecycleView = 2;
                    textOfType.setText("Показать все");
                    imageOfType.setRotation(-90);
                    int recyclerViewOrientation = LinearLayoutManager.HORIZONTAL;
                    LinearLayoutManager layoutManager = new LinearLayoutManager(Current_Apartment.this, recyclerViewOrientation, false);
                    RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycleView);
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setHasFixedSize(true);
                    MyNotificationsAdapter ad = new MyNotificationsAdapter(Current_Apartment.this, notificationList, typeOfRecycleView);
                    recyclerView.setNestedScrollingEnabled(false);
                    recyclerView.setAdapter(ad);
                } else {
                    typeOfRecycleView = 1;
                    textOfType.setText("Скрыть");
                    imageOfType.setRotation(90);
                    int recyclerViewOrientation = LinearLayoutManager.VERTICAL;
                    LinearLayoutManager layoutManager = new LinearLayoutManager(Current_Apartment.this, recyclerViewOrientation, false);
                    RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycleView);
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setHasFixedSize(true);
                    MyNotificationsAdapter ad = new MyNotificationsAdapter(Current_Apartment.this, notificationList, typeOfRecycleView);
                    recyclerView.setNestedScrollingEnabled(false);
                    recyclerView.setAdapter(ad);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

    @Override
    public void onListItemClick(Notification notification) {

    }
}
