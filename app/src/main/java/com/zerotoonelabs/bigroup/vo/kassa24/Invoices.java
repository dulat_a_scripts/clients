package com.zerotoonelabs.bigroup.vo.kassa24;

import java.util.List;

public class Invoices {
    public Invoice invoice;

    public static class Invoice {

        public Services services;
        public String invoiceId;

        public static class Services {
            public List<Service> service;
        }
    }
}
