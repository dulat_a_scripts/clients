
package com.zerotoonelabs.bigroup.vo.statusesPred;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Uslugi implements Parcelable
{

    @SerializedName("SostavUslug")
    @Expose
    private String sostavUslug;
    @SerializedName("Stoimost")
    @Expose
    private Float stoimost;
    public final static Creator<Uslugi> CREATOR = new Creator<Uslugi>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Uslugi createFromParcel(Parcel in) {
            return new Uslugi(in);
        }

        public Uslugi[] newArray(int size) {
            return (new Uslugi[size]);
        }

    }
    ;

    protected Uslugi(Parcel in) {
        this.sostavUslug = ((String) in.readValue((String.class.getClassLoader())));
        this.stoimost = ((Float) in.readValue((Float.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public Uslugi() {
    }

    /**
     * 
     * @param stoimost
     * @param sostavUslug
     */
    public Uslugi(String sostavUslug, Float stoimost) {
        super();
        this.sostavUslug = sostavUslug;
        this.stoimost = stoimost;
    }

    public String getSostavUslug() {
        return sostavUslug;
    }

    public void setSostavUslug(String sostavUslug) {
        this.sostavUslug = sostavUslug;
    }

    public Float getStoimost() {
        return stoimost;
    }

    public void setStoimost(Float stoimost) {
        this.stoimost = stoimost;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(sostavUslug);
        dest.writeValue(stoimost);
    }

    public int describeContents() {
        return  0;
    }

}
