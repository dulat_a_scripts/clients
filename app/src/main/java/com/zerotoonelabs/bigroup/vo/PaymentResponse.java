package com.zerotoonelabs.bigroup.vo;

import com.google.gson.annotations.SerializedName;

public class PaymentResponse {
    @SerializedName("orderId")
    public String orderId;
    @SerializedName("status")
    public int status;
    @SerializedName("errCode")
    public int errCode;
    public String formUrl;
    public Errors errors;

    public static class Errors {
        String[] serviceId;

        @Override
        public String toString() {
            StringBuilder errors = new StringBuilder();
            for (String error : serviceId) {
                errors.append(error);
            }
            return errors.toString();
        }
    }
}
