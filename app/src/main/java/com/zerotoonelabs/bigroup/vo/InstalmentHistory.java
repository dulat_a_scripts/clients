package com.zerotoonelabs.bigroup.vo;

import android.arch.persistence.room.Entity;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

@Entity
public class InstalmentHistory implements Comparable<InstalmentHistory> {
    @SerializedName("DataPlatezha")
    public String date;
    @SerializedName("SummaVznosa")
    public int amount;
    public String formattedAmount;
    @SerializedName("Details")
    public String details;
    public long timestamp;

    public InstalmentHistory(String date, int amount, String details, long timestamp) {
        this.date = date;
        this.amount = amount;
        this.details = details;
        this.timestamp = timestamp;
    }

    @Override
    public int compareTo(@NonNull InstalmentHistory o) {
        return compare(this.timestamp, o.timestamp);
    }

    private static int compare(long a, long b) {
        return a < b ? -1
                : 1;
    }
}
