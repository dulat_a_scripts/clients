package com.zerotoonelabs.bigroup.vo;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class Complaint implements Parcelable {

    public static final String UNKNOWN_TYPE_ID = "";

    @SerializedName("TypeId")
    public String typeId;

    @SerializedName("Message")
    public String message;

    @SerializedName("ReqPhoto")
    public String encodedImage;

    public transient Uri imageUri;

    public Complaint(String typeId, String message, @Nullable String encodedImage, @Nullable Uri imageUri) {
        this.typeId = typeId;
        this.message = message;
        this.encodedImage = encodedImage;
        this.imageUri = imageUri;
    }

    protected Complaint(Parcel in) {
        typeId = in.readString();
        message = in.readString();
        encodedImage = in.readString();
        imageUri = in.readParcelable(Uri.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(typeId);
        dest.writeString(message);
        dest.writeString(encodedImage);
        dest.writeParcelable(imageUri, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Complaint> CREATOR = new Creator<Complaint>() {
        @Override
        public Complaint createFromParcel(Parcel in) {
            return new Complaint(in);
        }

        @Override
        public Complaint[] newArray(int size) {
            return new Complaint[size];
        }
    };
}
