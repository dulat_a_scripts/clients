package com.zerotoonelabs.bigroup.vo;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.zerotoonelabs.bigroup.db.BiTypeConverters;

import java.util.List;

@Entity(tableName = "stocks")
@TypeConverters(BiTypeConverters.class)
public class Stock implements Parcelable {
    @NonNull
    @PrimaryKey
    public String id;
    @SerializedName("is_raw")
    public String isRaw;
    @SerializedName("is_visible")
    public String isVisible;
    public String title;
    public String description;
    public String start;
    public String end;
    public String sections;
    @SerializedName("Photo")
    public String imageUrl;
    @Ignore
    public Section[] section;
    public List<String> sectionImageUrls;

    public Stock(@NonNull String id, String isRaw, String isVisible, String title, String description,
                 String start, String end, String sections, String imageUrl, List<String> sectionImageUrls) {
        this.id = id;
        this.isRaw = isRaw;
        this.isVisible = isVisible;
        this.title = title;
        this.description = description;
        this.start = start;
        this.end = end;
        this.sections = sections;
        this.imageUrl = imageUrl;
        this.sectionImageUrls = sectionImageUrls;
    }

    public static class Section implements Parcelable {
        @SerializedName("section_name")
        public String sectionName;
        @SerializedName("file_path")
        public String imageUrl;

        public Section(String sectionName, String imageUrl) {
            this.sectionName = sectionName;
            this.imageUrl = imageUrl;
        }

        protected Section(Parcel in) {
            sectionName = in.readString();
            imageUrl = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(sectionName);
            dest.writeString(imageUrl);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<Section> CREATOR = new Creator<Section>() {
            @Override
            public Section createFromParcel(Parcel in) {
                return new Section(in);
            }

            @Override
            public Section[] newArray(int size) {
                return new Section[size];
            }
        };
    }

    protected Stock(Parcel in) {
        id = in.readString();
        isRaw = in.readString();
        isVisible = in.readString();
        title = in.readString();
        description = in.readString();
        start = in.readString();
        end = in.readString();
        sections = in.readString();
        imageUrl = in.readString();
        section = in.createTypedArray(Section.CREATOR);
        sectionImageUrls = in.createStringArrayList();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(isRaw);
        dest.writeString(isVisible);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(start);
        dest.writeString(end);
        dest.writeString(sections);
        dest.writeString(imageUrl);
        dest.writeTypedArray(section, flags);
        dest.writeStringList(sectionImageUrls);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Stock> CREATOR = new Creator<Stock>() {
        @Override
        public Stock createFromParcel(Parcel in) {
            return new Stock(in);
        }

        @Override
        public Stock[] newArray(int size) {
            return new Stock[size];
        }
    };
}
