package com.zerotoonelabs.bigroup.vo;

import com.google.gson.annotations.SerializedName;

public class FlatScheme {
    public int id;
    @SerializedName("SchemeName")
    public String name;

    public FlatScheme(int id, String name) {
        this.id = id;
        this.name = name;
    }
}
