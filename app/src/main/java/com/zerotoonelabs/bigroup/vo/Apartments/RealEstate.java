
package com.zerotoonelabs.bigroup.vo.Apartments;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RealEstate implements Parcelable {

    @SerializedName("guid")
    @Expose
    private String guid;
    @SerializedName("name")
    @Expose
    private String name;
    public final static Parcelable.Creator<RealEstate> CREATOR = new Creator<RealEstate>() {


        @SuppressWarnings({
                "unchecked"
        })
        public RealEstate createFromParcel(Parcel in) {
            return new RealEstate(in);
        }

        public RealEstate[] newArray(int size) {
            return (new RealEstate[size]);
        }

    };

    protected RealEstate(Parcel in) {
        this.guid = ((String) in.readValue((String.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
    }

    public RealEstate() {
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public RealEstate withGuid(String guid) {
        this.guid = guid;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RealEstate withName(String name) {
        this.name = name;
        return this;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(guid);
        dest.writeValue(name);
    }

    public int describeContents() {
        return 0;
    }

}
