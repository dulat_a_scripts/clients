package com.zerotoonelabs.bigroup.vo;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import static android.text.TextUtils.isEmpty;

@Entity
public class Manager {
    @SerializedName("Id")
    private int id;

    @PrimaryKey
    @NonNull
    @SerializedName("responsible_guid")
    private String managerId;
    @SerializedName("responsible_name")
    private String name;
    @Ignore
    @SerializedName("responsible_photo")
    private String photo;
    @SerializedName("responsible_number")
    private String phone;
    @SerializedName("responsible_mail")
    private String email;

    private String realEstateId;
    private String contractId;

    private boolean bool;

    public Manager() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(@NonNull String managerId) {
        this.managerId = managerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRealEstateId() {
        return realEstateId;
    }

    public void setRealEstateId(String realEstateId) {
        this.realEstateId = realEstateId;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public boolean isBool() {
        return bool;
    }

    public void setBool(boolean bool) {
        this.bool = bool;
    }

    @Override
    public String toString() {
        String result = "";
        if (!isEmpty(name)) result += name;
        if (!isEmpty(phone)) result += '\n' + phone;
        if (!isEmpty(email)) result += '\n' + email;
        return result;
    }
}