package com.zerotoonelabs.bigroup.vo;

import com.google.gson.annotations.SerializedName;

public class SalesRequestHistory {
    @SerializedName("Nomer")
    public String number;
    @SerializedName("DataPodachi")
    public String createdAt;
    @SerializedName("Status")
    public String status;
    @SerializedName("Spravka")
    public String pdfUrl;
    @SerializedName("VidZayavki")
    public String type;

    public SalesRequestHistory(String number, String createdAt, String status, String pdfUrl, String type) {
        this.number = number;
        this.createdAt = createdAt;
        this.status = status;
        this.pdfUrl = pdfUrl;
        this.type = type;
    }
}
