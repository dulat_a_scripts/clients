
package com.zerotoonelabs.bigroup.vo.Apartments;

import android.arch.persistence.room.Embedded;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Block implements Parcelable
{

    @SerializedName("guid")
    @Expose
    private String guid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("floors")
    @Expose
    private Integer floors;
    @SerializedName("real_estate")
    @Expose
    @Embedded(prefix = "real_estate")
    private RealEstate realEstate;
    public final static Parcelable.Creator<Block> CREATOR = new Creator<Block>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Block createFromParcel(Parcel in) {
            return new Block(in);
        }

        public Block[] newArray(int size) {
            return (new Block[size]);
        }

    }
    ;

    protected Block(Parcel in) {
        this.guid = ((String) in.readValue((String.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.floors = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.realEstate = ((RealEstate) in.readValue((RealEstate.class.getClassLoader())));
    }

    public Block() {
    }

    public void setFloors(Integer floors) {
        this.floors = floors;
    }

    public Integer getFloors() {
        return floors;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public Block withGuid(String guid) {
        this.guid = guid;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Block withName(String name) {
        this.name = name;
        return this;
    }

    public RealEstate getRealEstate() {
        return realEstate;
    }

    public void setRealEstate(RealEstate realEstate) {
        this.realEstate = realEstate;
    }

    public Block withRealEstate(RealEstate realEstate) {
        this.realEstate = realEstate;
        return this;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(guid);
        dest.writeValue(name);
        dest.writeValue(floors);
        dest.writeValue(realEstate);
    }

    public int describeContents() {
        return  0;
    }

}
