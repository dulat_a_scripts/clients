package com.zerotoonelabs.bigroup.vo;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

@Entity
public class BuildingSite implements Parcelable, Comparable<BuildingSite> {

    @PrimaryKey
    @NonNull
    @SerializedName("guid")
    public String id;
    public String name;

    public BuildingSite(@NonNull String id, String name) {
        this.id = id;
        this.name = name;
    }

    @Ignore
    protected BuildingSite(Parcel in) {
        id = in.readString();
        name = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BuildingSite> CREATOR = new Creator<BuildingSite>() {
        @Override
        public BuildingSite createFromParcel(Parcel in) {
            return new BuildingSite(in);
        }

        @Override
        public BuildingSite[] newArray(int size) {
            return new BuildingSite[size];
        }
    };

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;

        if (obj == null || getClass() != obj.getClass()) return false;

        BuildingSite buildingSite  = (BuildingSite) obj;
        if (id != null ? !id.equals(buildingSite.id) : buildingSite.id != null) {
            return false;
        }

        return name != null ? name.equals(buildingSite.name) : buildingSite.name == null;
    }

    @Override
    public int compareTo(@NonNull BuildingSite buildingSite) {
        return name.compareTo(buildingSite.name);
    }
}
