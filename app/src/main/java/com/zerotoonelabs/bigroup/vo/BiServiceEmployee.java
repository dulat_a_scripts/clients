package com.zerotoonelabs.bigroup.vo;

import android.graphics.Bitmap;
import android.support.annotation.Nullable;


/**
 * Created by temirlan on 17.11.17.
 */

public class BiServiceEmployee {
    private final String fullName;
    private String employeePosition;
    private final Bitmap avatar;
    private String phoneNumber;
    private String email;

    public BiServiceEmployee(String fullName, String employeePosition, @Nullable Bitmap avatar) {
        this.fullName = fullName;
        this.employeePosition = employeePosition;
        this.avatar = avatar;
    }

    public BiServiceEmployee(String fullName, String employeePosition, @Nullable Bitmap avatar, String phoneNumber, String email) {
        this.fullName = fullName;
        this.employeePosition = employeePosition;
        this.avatar = avatar;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public String getEmployeePosition() {
        return employeePosition;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public Bitmap getAvatar() {
        return avatar;
    }
}
