
package com.zerotoonelabs.bigroup.vo.statusesPred;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DopSoglashenie implements Parcelable
{

    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("summa")
    @Expose
    private Float summa;
    @SerializedName("Data")
    @Expose
    private String data;
    @SerializedName("VidDokumenta")
    @Expose
    private String vidDokumenta;
    public final static Creator<DopSoglashenie> CREATOR = new Creator<DopSoglashenie>() {


        @SuppressWarnings({
            "unchecked"
        })
        public DopSoglashenie createFromParcel(Parcel in) {
            return new DopSoglashenie(in);
        }

        public DopSoglashenie[] newArray(int size) {
            return (new DopSoglashenie[size]);
        }

    }
    ;

    protected DopSoglashenie(Parcel in) {
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.summa = ((Float) in.readValue((Float.class.getClassLoader())));
        this.data = ((String) in.readValue((String.class.getClassLoader())));
        this.vidDokumenta = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public DopSoglashenie() {
    }

    /**
     * 
     * @param summa
     * @param name
     * @param data
     * @param vidDokumenta
     */
    public DopSoglashenie(String name, Float summa, String data, String vidDokumenta) {
        super();
        this.name = name;
        this.summa = summa;
        this.data = data;
        this.vidDokumenta = vidDokumenta;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getSumma() {
        return summa;
    }

    public void setSumma(Float summa) {
        this.summa = summa;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getVidDokumenta() {
        return vidDokumenta;
    }

    public void setVidDokumenta(String vidDokumenta) {
        this.vidDokumenta = vidDokumenta;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(name);
        dest.writeValue(summa);
        dest.writeValue(data);
        dest.writeValue(vidDokumenta);
    }

    public int describeContents() {
        return  0;
    }

}
