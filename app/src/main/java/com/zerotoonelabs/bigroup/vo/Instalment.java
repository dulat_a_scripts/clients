package com.zerotoonelabs.bigroup.vo;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class Instalment implements Comparable<Instalment> {
    @SerializedName("Data")
    public String date;
    @SerializedName("SummaVznosa")
    public int amount;
    public String formattedAmount;
    @SerializedName("Etap")
    public String stage;
    public long timestamp;

    public Instalment(String date, int amount, String stage, long timestamp) {
        this.date = date;
        this.amount = amount;
        this.stage = stage;
        this.timestamp = timestamp;
    }

    @Override
    public int compareTo(@NonNull Instalment o) {
        return compare(this.timestamp, o.timestamp);
    }

    private static int compare(long a, long b) {
        return a < b ? -1
                : 1;
    }
}
