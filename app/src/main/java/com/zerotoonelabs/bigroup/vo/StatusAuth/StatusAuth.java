
package com.zerotoonelabs.bigroup.vo.StatusAuth;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StatusAuth implements Parcelable
{

    @SerializedName("Status1")
    @Expose
    private Status1 status1;
    @SerializedName("Status2")
    @Expose
    private Status2 status2;
    @SerializedName("Status3")
    @Expose
    private Status3 status3;
    @SerializedName("Status4")
    @Expose
    private Status4 status4;
    @SerializedName("Status5")
    @Expose
    private Status5 status5;
    @SerializedName("HideTabs")
    @Expose
    private Boolean hideTabs;
    public final static Parcelable.Creator<StatusAuth> CREATOR = new Creator<StatusAuth>() {


        @SuppressWarnings({
            "unchecked"
        })
        public StatusAuth createFromParcel(Parcel in) {
            return new StatusAuth(in);
        }

        public StatusAuth[] newArray(int size) {
            return (new StatusAuth[size]);
        }

    }
    ;

    protected StatusAuth(Parcel in) {
        this.status1 = ((Status1) in.readValue((Status1.class.getClassLoader())));
        this.status2 = ((Status2) in.readValue((Status2.class.getClassLoader())));
        this.status3 = ((Status3) in.readValue((Status3.class.getClassLoader())));
        this.status4 = ((Status4) in.readValue((Status4.class.getClassLoader())));
        this.status5 = ((Status5) in.readValue((Status5.class.getClassLoader())));
        this.hideTabs = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public StatusAuth() {
    }

    /**
     * 
     * @param hideTabs
     * @param status5
     * @param status4
     * @param status3
     * @param status1
     * @param status2
     */
    public StatusAuth(Status1 status1, Status2 status2, Status3 status3, Status4 status4, Status5 status5, Boolean hideTabs) {
        super();
        this.status1 = status1;
        this.status2 = status2;
        this.status3 = status3;
        this.status4 = status4;
        this.status5 = status5;
        this.hideTabs = hideTabs;
    }

    public Status1 getStatus1() {
        return status1;
    }

    public void setStatus1(Status1 status1) {
        this.status1 = status1;
    }

    public Status2 getStatus2() {
        return status2;
    }

    public void setStatus2(Status2 status2) {
        this.status2 = status2;
    }

    public Status3 getStatus3() {
        return status3;
    }

    public void setStatus3(Status3 status3) {
        this.status3 = status3;
    }

    public Status4 getStatus4() {
        return status4;
    }

    public void setStatus4(Status4 status4) {
        this.status4 = status4;
    }

    public Status5 getStatus5() {
        return status5;
    }

    public void setStatus5(Status5 status5) {
        this.status5 = status5;
    }

    public Boolean getHideTabs() {
        return hideTabs;
    }

    public void setHideTabs(Boolean hideTabs) {
        this.hideTabs = hideTabs;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status1);
        dest.writeValue(status2);
        dest.writeValue(status3);
        dest.writeValue(status4);
        dest.writeValue(status5);
        dest.writeValue(hideTabs);
    }

    public int describeContents() {
        return  0;
    }

}
