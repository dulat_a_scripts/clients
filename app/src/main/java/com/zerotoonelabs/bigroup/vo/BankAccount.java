package com.zerotoonelabs.bigroup.vo;

import com.google.gson.annotations.SerializedName;

public class BankAccount {

    @SerializedName("IsActive")
    private boolean active;
    @SerializedName("IsErcActive")
    private boolean ercActive;
    @SerializedName("Id")
    private int id;
    @SerializedName("Number")
    private String number;
    @SerializedName("ErcNumber")
    private String ercNumber;
    @SerializedName("ComplexName")
    private String complexName;
    @SerializedName("Address")
    private String address;
    @SerializedName("RoomNumber")
    private int roomNumber;
    @SerializedName("ApartmentNumber")
    private String apartmentNumber;
    @SerializedName("Builder")
    private String builder;
    @SerializedName("Square")
    private double square;
    @SerializedName("Contract")
    private String contract;
    @SerializedName("ContractNumber")
    private String contractNumber;
    @SerializedName("ErcContract")
    private String ercContract;
    @SerializedName("ApplicationUserId")
    private String applicationUserId;
    @SerializedName("ServiceCompanyId")
    private int serviceCompanyId;
    @SerializedName("PropertyGuid")
    private String propertyGuid;
    @SerializedName("RealEstateGuid")
    private String realEstateGuid;
    @SerializedName("ServiceId")
    private String serviceId;
    @SerializedName("GuaranteeComplete")
    private int guaranteeComplete;
    @SerializedName("RealServiceId")
    private String realServiceId;
    @SerializedName("RealServiceName")
    private String realServiceName;

    public BankAccount() {

    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isErcActive() {
        return ercActive;
    }

    public void setErcActive(boolean ercActive) {
        this.ercActive = ercActive;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getErcNumber() {
        return ercNumber;
    }

    public void setErcNumber(String ercNumber) {
        this.ercNumber = ercNumber;
    }

    public String getComplexName() {
        return complexName;
    }

    public void setComplexName(String complexName) {
        this.complexName = complexName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getApartmentNumber() {
        return apartmentNumber;
    }

    public void setApartmentNumber(String apartmentNumber) {
        this.apartmentNumber = apartmentNumber;
    }

    public String getBuilder() {
        return builder;
    }

    public void setBuilder(String builder) {
        this.builder = builder;
    }

    public double getSquare() {
        return square;
    }

    public void setSquare(double square) {
        this.square = square;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getErcContract() {
        return ercContract;
    }

    public void setErcContract(String ercContract) {
        this.ercContract = ercContract;
    }

    public String getApplicationUserId() {
        return applicationUserId;
    }

    public void setApplicationUserId(String applicationUserId) {
        this.applicationUserId = applicationUserId;
    }

    public int getServiceCompanyId() {
        return serviceCompanyId;
    }

    public void setServiceCompanyId(int serviceCompanyId) {
        this.serviceCompanyId = serviceCompanyId;
    }

    public String getPropertyGuid() {
        return propertyGuid;
    }

    public void setPropertyGuid(String propertyGuid) {
        this.propertyGuid = propertyGuid;
    }

    public String getRealEstateGuid() {
        return realEstateGuid;
    }

    public void setRealEstateGuid(String realEstateGuid) {
        this.realEstateGuid = realEstateGuid;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public int getGuaranteeComplete() {
        return guaranteeComplete;
    }

    public void setGuaranteeComplete(int guaranteeComplete) {
        this.guaranteeComplete = guaranteeComplete;
    }

    public String getRealServiceId() {
        return realServiceId;
    }

    public void setRealServiceId(String realServiceId) {
        this.realServiceId = realServiceId;
    }

    public String getRealServiceName() {
        return realServiceName;
    }

    public void setRealServiceName(String realServiceName) {
        this.realServiceName = realServiceName;
    }
}
