package com.zerotoonelabs.bigroup.vo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BiServiceContactData {

    @SerializedName("ManagerPhoto")
    @Expose
    private String managerPhoto;
    @SerializedName("ManagerObjecta")
    @Expose
    private String managerObjecta;
    @SerializedName("ManagerPhone")
    @Expose
    private String managerPhone;
    @SerializedName("ManagerMail")
    @Expose
    private String managerMail;
    @SerializedName("DirectorServSluzhbPhoto")
    @Expose
    private String directorServSluzhbPhoto;
    @SerializedName("DirectorServSluzhb")
    @Expose
    private String directorServSluzhb;
    @SerializedName("DirectorMail")
    @Expose
    private String directorMail;
    @SerializedName("ServiceCompanyName")
    @Expose
    private String serviceCompanyName;

    public String getManagerPhoto() {
        return managerPhoto;
    }

    public void setManagerPhoto(String managerPhoto) {
        this.managerPhoto = managerPhoto;
    }

    public String getManagerObjecta() {
        return managerObjecta;
    }

    public void setManagerObjecta(String managerObjecta) {
        this.managerObjecta = managerObjecta;
    }

    public String getManagerPhone() {
        return managerPhone;
    }

    public void setManagerPhone(String managerPhone) {
        this.managerPhone = managerPhone;
    }

    public String getManagerMail() {
        return managerMail;
    }

    public void setManagerMail(String managerMail) {
        this.managerMail = managerMail;
    }

    public String getDirectorServSluzhbPhoto() {
        return directorServSluzhbPhoto;
    }

    public void setDirectorServSluzhbPhoto(String directorServSluzhbPhoto) {
        this.directorServSluzhbPhoto = directorServSluzhbPhoto;
    }

    public String getDirectorServSluzhb() {
        return directorServSluzhb;
    }

    public void setDirectorServSluzhb(String directorServSluzhb) {
        this.directorServSluzhb = directorServSluzhb;
    }

    public String getDirectorMail() {
        return directorMail;
    }

    public void setDirectorMail(String directorMail) {
        this.directorMail = directorMail;
    }

    public String getServiceCompanyName() {
        return serviceCompanyName;
    }

    public void setServiceCompanyName(String serviceCompanyName) {
        this.serviceCompanyName = serviceCompanyName;
    }

}