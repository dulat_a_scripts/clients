
package com.zerotoonelabs.bigroup.vo.Apartments;

import android.arch.persistence.room.Embedded;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ApartmentBlock implements Parcelable {

    @SerializedName("guid")
    public String id;

    public String name;

    @SerializedName("floors")
    public int floors;

    @SerializedName("real_estate")
    @Embedded(prefix = "real_estate__")
    public RealEstate realEstate;

    public ApartmentBlock(String id, String name, int floors, RealEstate realEstate) {
        this.id = id;
        this.name = name;
        this.floors = floors;
        this.realEstate = realEstate;
    }

    public static class RealEstate implements Parcelable {
        @SerializedName("guid")
        public String id;
        public String name;

        public RealEstate(String id, String name) {
            this.id = id;
            this.name = name;
        }

        protected RealEstate(Parcel in) {
            id = in.readString();
            name = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(id);
            dest.writeString(name);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<RealEstate> CREATOR = new Creator<RealEstate>() {
            @Override
            public RealEstate createFromParcel(Parcel in) {
                return new RealEstate(in);
            }

            @Override
            public RealEstate[] newArray(int size) {
                return new RealEstate[size];
            }
        };
    }

    protected ApartmentBlock(Parcel in) {
        id = in.readString();
        name = in.readString();
        floors = in.readInt();
        realEstate = in.readParcelable(RealEstate.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeInt(floors);
        dest.writeParcelable(realEstate, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ApartmentBlock> CREATOR = new Creator<ApartmentBlock>() {
        @Override
        public ApartmentBlock createFromParcel(Parcel in) {
            return new ApartmentBlock(in);
        }

        @Override
        public ApartmentBlock[] newArray(int size) {
            return new ApartmentBlock[size];
        }
    };
}
