package com.zerotoonelabs.bigroup.vo;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

@Entity
public class VisitNumber {
    @PrimaryKey
    @NonNull
    @SerializedName("Ssylka")
    private String id;

    @SerializedName("Naimenovanie")
    private String name;

    private String blockId;

    public VisitNumber() {

    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBlockId() {
        return blockId;
    }

    public void setBlockId(String blockId) {
        this.blockId = blockId;
    }

    @Override
    public String toString() {
        return name;
    }
}
