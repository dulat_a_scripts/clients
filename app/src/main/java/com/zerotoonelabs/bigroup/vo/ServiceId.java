package com.zerotoonelabs.bigroup.vo;

import com.google.gson.annotations.SerializedName;

public class ServiceId {
    @SerializedName("Key")
    public String key;
    @SerializedName("Value")
    public String value;
    @SerializedName("Name")
    public String name;

    public ServiceId(String key, String value, String name) {
        this.key = key;
        this.value = value;
        this.name = name;
    }
}
