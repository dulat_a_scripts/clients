package com.zerotoonelabs.bigroup.vo;

public enum Status {
    SUCCESS,
    ERROR,
    LOADING
}
