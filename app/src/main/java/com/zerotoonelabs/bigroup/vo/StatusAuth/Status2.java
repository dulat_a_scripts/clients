
package com.zerotoonelabs.bigroup.vo.StatusAuth;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Status2 implements Parcelable
{

    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("Date")
    @Expose
    private String date;
    @SerializedName("Details")
    @Expose
    private Object details;
    public final static Parcelable.Creator<Status2> CREATOR = new Creator<Status2>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Status2 createFromParcel(Parcel in) {
            return new Status2(in);
        }

        public Status2 [] newArray(int size) {
            return (new Status2[size]);
        }

    }
    ;

    protected Status2(Parcel in) {
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.date = ((String) in.readValue((String.class.getClassLoader())));
        this.details = ((Object) in.readValue((Object.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public Status2() {
    }

    /**
     * 
     * @param details
     * @param status
     * @param date
     */
    public Status2(Integer status, String date, Object details) {
        super();
        this.status = status;
        this.date = date;
        this.details = details;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Object getDetails() {
        return details;
    }

    public void setDetails(Object details) {
        this.details = details;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(date);
        dest.writeValue(details);
    }

    public int describeContents() {
        return  0;
    }

}
