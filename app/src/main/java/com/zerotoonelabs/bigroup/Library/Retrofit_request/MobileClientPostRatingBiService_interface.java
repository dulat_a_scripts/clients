package com.zerotoonelabs.bigroup.Library.Retrofit_request;

import com.zerotoonelabs.bigroup.Library.Request_parsers.SendReiting_service;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface MobileClientPostRatingBiService_interface {


    @Headers({

            "Content-Type: application/json"

    })

    @POST("/api/v1/MobileClient/PostRatingBiService")
    Call<Object> getRequest(@Body SendReiting_service sendReiting_service, @Header("Token") String authToken);

}
