package com.zerotoonelabs.bigroup.Library.Retrofit_request;

import com.zerotoonelabs.bigroup.Library.Request_parsers.Send_Notification_pack;
import com.zerotoonelabs.bigroup.entity.Wooppay.WooppayCheck;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface Send_Notification {

    @POST("api/v1/MobileClient/PostNotifications")
    Call<Object> sendRequest(@Body Send_Notification_pack send_notification_pack, @Header("Token") String authToken);
}
