package com.zerotoonelabs.bigroup.Library.Request_parsers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Calculator_list {

    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Sum")
    @Expose
    private Double sum;
    @SerializedName("Measure")
    @Expose
    private String measure;
    @SerializedName("Number")
    @Expose
    private String number;

    @SerializedName("cat1")
    @Expose
    private Integer cat1;

    @SerializedName("cat2")
    @Expose
    private Integer cat2;

    @SerializedName("cat3")
    @Expose
    private Integer cat3;

    public Integer getCat1() {
        return cat1;
    }

    public void setCat1(Integer cat1) {
        this.cat1 = cat1;
    }

    public Integer getCat2() {
        return cat2;
    }

    public void setCat2(Integer cat2) {
        this.cat2 = cat2;
    }

    public Integer getCat3() {
        return cat3;
    }

    public void setCat3(Integer cat3) {
        this.cat3 = cat3;
    }



//    "cat1": 1,
//            "cat2": 0,
//            "cat3": 0

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getSum() {
        return sum;
    }

    public void setSum(Double sum) {
        this.sum = sum;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
