package com.zerotoonelabs.bigroup.Library.Retrofit_request;

import com.zerotoonelabs.bigroup.Library.Request_parsers.Parser_One;


import retrofit2.Call;

import retrofit2.http.GET;
import retrofit2.http.Header;


public interface GetWoopay_services_int {

    @GET("/v5/action/get-services")
    Call<Parser_One> getServices(

            @Header("Content-Type") String contentType,
            @Header("Authorization") String token

    );

}
