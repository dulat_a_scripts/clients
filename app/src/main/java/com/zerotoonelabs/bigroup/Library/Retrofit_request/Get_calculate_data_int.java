package com.zerotoonelabs.bigroup.Library.Retrofit_request;

import com.zerotoonelabs.bigroup.Library.Request_parsers.SerializeCalcData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface Get_calculate_data_int {

    @GET("api/v1/MobileClient/GetTarifCalculationAndroid")
        //api/v1/MobileClient/GetNotifications
    Call<SerializeCalcData> getData(@Header("Token") String token, @Query("property_id") String property_id);

}
