package com.zerotoonelabs.bigroup.Library.Retrofit_request;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface GetCities {

    @GET("api/v1/salesbigroup/city/")
    Call<List<String>> getCitiess(@Header("Token") String token);

}
