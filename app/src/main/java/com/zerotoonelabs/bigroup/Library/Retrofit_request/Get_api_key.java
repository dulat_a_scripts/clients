package com.zerotoonelabs.bigroup.Library.Retrofit_request;

import com.zerotoonelabs.bigroup.Library.Request_parsers.ApiKeyParser;
import com.zerotoonelabs.bigroup.Library.Request_parsers.SerializeCalcData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface Get_api_key {

    @GET("api/v1/MobileClient/GetCpAccountPass")
        //api/v1/MobileClient/GetNotifications
    Call<ApiKeyParser> getData(@Header("Token") String token, @Query("public_id") String public_id);

}
