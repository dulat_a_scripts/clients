package com.zerotoonelabs.bigroup.Library;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.authui.HomeActivity;

import java.util.HashMap;

public class Dialogmessage extends DialogFragment {

    AlertDialog dialog;

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.alert_message)
                .setPositiveButton(R.string.next_page, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(getActivity(), HomeActivity.class);
                        startActivity(intent);
                    }
                })
                .setNegativeButton(R.string.stop, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }

    //new Dialogmessage().Dialog("Оплата успешно произведена! ","отмена","главная",intent);
    public void Dialog(String message,String button1,String button2){

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        LayoutInflater inflater = this.getLayoutInflater();
//        View view = inflater.inflate(R.layout.apartment_fragment_action_bar, null);
//        builder.setView(view);
//        Button button = view.findViewById(R.id.button17);
//        Button button2 = view.findViewById(R.id.button18);
//        Button button3 = view.findViewById(R.id.button19);
//        Button button4 = view.findViewById(R.id.button20);
//        Button button5cancel = view.findViewById(R.id.button21);
        Dialog dialog = builder.create();

//        button5cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//
//            }
//        });

        Window window = dialog.getWindow();
        WindowManager.LayoutParams param = window.getAttributes();
        param.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
        window.setAttributes(param);
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.show();

        dialog.show();

    }


    public void newDialog(){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(Library.newcontext);
        builder1.setMessage("Write your message here.");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public void showactionmessagebar() {
//zz
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // LayoutInflater inflater = this.getLayoutInflater();
        // View view = inflater.inflate(R.layout.apartment_fragment_action_bar, null);
//        builder.setView(view);
//        Button button = view.findViewById(R.id.button17);
//        Button button2 = view.findViewById(R.id.button18);
//        Button button3 = view.findViewById(R.id.button19);
//        Button button4 = view.findViewById(R.id.button20);
//        Button button5cancel = view.findViewById(R.id.button21);
        Dialog dialog = builder.create();

        // Setting dialogview
        Window window = dialog.getWindow();
        WindowManager.LayoutParams param = window.getAttributes();
        param.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
        window.setAttributes(param);
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.show();
        // Setting dialogview

    }
}


