package com.zerotoonelabs.bigroup.Library.Retrofit_request;

import com.zerotoonelabs.bigroup.Library.Request_parsers.ChangePasswordParser;
import com.zerotoonelabs.bigroup.Library.Request_parsers.SendChangePassword;
import com.zerotoonelabs.bigroup.Library.Request_parsers.SendChangePhone;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface Change_phone_int {

    @POST("api/v1/MobileClient/Changephone")
    Call<Object> sendRequest(@Body SendChangePhone sendChangePhone, @Header("Token") String authToken);

}
