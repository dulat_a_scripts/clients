package com.zerotoonelabs.bigroup.Library.Retrofit_request;

import com.zerotoonelabs.bigroup.Library.Request_parsers.Get_flat_parser;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface GetFlat_int {

    @Headers({

            "Content-Type: application/json"

    })

    @GET("/api/v1/MobileClient/GetFlats")
    Call<List<Get_flat_parser>> getRequest(@Header("token") String authToken, @Query("contractId") String string);

}
