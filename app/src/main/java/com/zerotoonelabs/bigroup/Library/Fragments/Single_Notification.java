package com.zerotoonelabs.bigroup.Library.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.Library.Request_parsers.Send_Notification_pack;
import com.zerotoonelabs.bigroup.Library.Retrofit_request.Send_Notification;
import com.zerotoonelabs.bigroup.R;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

public class Single_Notification extends android.support.v4.app.Fragment{

    View view;
    WebView webView;
    Toolbar mToolbar;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        setupActionBar();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.single_notification_layout, container, false);
        webView = view.findViewById(R.id.web);
        WebSettings settings = webView.getSettings();
        settings.setDefaultTextEncodingName("utf-8");
        webView.loadData(Library.html, "text/html; charset=utf-8", "utf-8");

        setWritemessage();






        return view;
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        menu.clear();

        Boolean notif = false;

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            notif = bundle.getBoolean("boolean");
        }

        if(notif){
            inflater.inflate(R.menu.single_notification_icons_two, menu);
            fixfauvor = 1;
        }else{
            inflater.inflate(R.menu.single_notification_icons, menu);
            fixfauvor = 0;
        }




        super.onCreateOptionsMenu(menu, inflater);

    }

    Integer fixfauvor = 0;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == R.id.action_delete){

            String message = "Вы действительно хотите удалить?";
            String button1 = "отмена";
            String button2 = "подтвердить";

            AlertDialog.Builder builder1 = new AlertDialog.Builder(Library.context);
            builder1.setMessage(message);
            builder1.setCancelable(true);

            builder1.setPositiveButton(
                    button2,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Send_Notification_pack send_notification_pack = Library.send_notification_pack;
                            send_notification_pack.setIsDeleted(true);

                            Observable<Send_Notification_pack> obs = Observable.create(new ObservableOnSubscribe<Send_Notification_pack>() {
                                @Override
                                public void subscribe(ObservableEmitter<Send_Notification_pack> emitter) throws Exception {
                                    emitter.onNext(send_notification_pack);
                                }
                            });

                            obs.subscribe(Library.observer);

//                            Notification_fragments notification_fragmentss = new Notification_fragments();
//
//                            android.support.v4.app.FragmentManager fragmentManager = Library.fragmentManager;
//                            fragmentManager.beginTransaction().replace(R.id.containerAuth, notification_fragmentss).commit();

                        }
                    });

            builder1.setNegativeButton(
                    button1,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();


            Log.i("sss","delete");


        }else if(item.getItemId() == R.id.action_favorite){

            Log.i("sss","action_favorite");

            Send_Notification_pack send_notification_packs = Library.send_notification_pack;

            if(fixfauvor == 0){
                item.setIcon(R.drawable.star_y_b);
                send_notification_packs.setIsFavorite(true);
                fixfauvor = 1;
            }else{
                item.setIcon(R.drawable.star_white);
                send_notification_packs.setIsFavorite(false);
                fixfauvor = 0;
            }




            Observable<Send_Notification_pack> obsx = Observable.create(new ObservableOnSubscribe<Send_Notification_pack>() {
                @Override
                public void subscribe(ObservableEmitter<Send_Notification_pack> emitter) throws Exception {
                    emitter.onNext(send_notification_packs);
                }
            });

           obsx.subscribe(Library.observer);

        }

        return super.onOptionsItemSelected(item);
    }

    public void setWritemessage(){
        Send_Notification_pack send_notification_pack = Library.send_notification_pack;
        send_notification_pack.setIsRead(true);

        Observable<Send_Notification_pack> obs = Observable.create(new ObservableOnSubscribe<Send_Notification_pack>() {
            @Override
            public void subscribe(ObservableEmitter<Send_Notification_pack> emitter) throws Exception {
                emitter.onNext(send_notification_pack);
            }
        });

        obs.subscribe(Library.observer);
    }

    private void setupActionBar() {
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        Toolbar toolbar = ((Activity)Library.context).findViewById(R.id.toolbar);
        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Notification_fragments notification_fragmentss = new Notification_fragments();

                android.support.v4.app.FragmentManager fragmentManager = Library.fragmentManager;
                fragmentManager.beginTransaction().replace(R.id.containerAuth, notification_fragmentss).commit();
//                try{
//
//                }catch (Exception e){
//
//                }


            }
        });

        //actionBar.
    }


}
