package com.zerotoonelabs.bigroup.Library.DaggerModules;

import com.zerotoonelabs.bigroup.Library.Library;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class PreferenceModule {

    @Singleton
    @Provides
    public Boolean SavePhoneMemory(String name1,String name2){
        return Library.SavePhoneMemoryOne(name1,name2);
    }

    @Provides
    @Singleton
    public String GetPhoneMemory(String name1){
        String returnstring = Library.GetPhoneMemoryOne(name1);
        return  returnstring;
    }
}
