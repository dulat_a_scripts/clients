package com.zerotoonelabs.bigroup.Library.Retrofit_request;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface Getpdf_list_int {

    @GET("api/v1/MobileClient/GetApartmentSheet?IfPdf=1")
    Call<ResponseBody> getPdf(@Header("Token") String token, @Query("property_id") String propertyId);

}
