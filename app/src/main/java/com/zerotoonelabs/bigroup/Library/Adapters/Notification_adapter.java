package com.zerotoonelabs.bigroup.Library.Adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.zerotoonelabs.bigroup.Library.Fragments.Notification_fragments;
import com.zerotoonelabs.bigroup.Library.Fragments.Single_Notification;
import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.Library.Objects.SetTag_obj;
import com.zerotoonelabs.bigroup.Library.Request_parsers.Send_Notification_pack;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.authui.HomeActivity;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Set;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;


public class Notification_adapter extends RecyclerView.Adapter<Notification_adapter.MyViewHolder> {

    ArrayList<String> title = new ArrayList<>();
    ArrayList<String> date = new ArrayList<>();
    ArrayList<String> arrayList = new ArrayList<>();
    ArrayList<Boolean> booleans = new ArrayList<>();
    ArrayList<Integer> ids = new ArrayList<>();
    ArrayList<Boolean> is_del = new ArrayList<>();
    ArrayList<Boolean> is_fav = new ArrayList<>();
    Integer fixfavorite = 0;





    public Notification_adapter(ArrayList<String> titles, ArrayList<String> dates, ArrayList<String> arrayList, ArrayList<Boolean> booleans, ArrayList<Integer> ids, ArrayList<Boolean> is_del, ArrayList<Boolean> is_favorite){
        this.title = titles;
        this.date = dates;
        this.arrayList = arrayList;
        this.booleans = booleans;
        this.ids = ids;
        this.is_del = is_del;
        this.is_fav = is_favorite;
        //ids,booleansis_del,booleans_id_favorite
    }

    @Override
    public Notification_adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemview = LayoutInflater.from(parent.getContext()).inflate(R.layout.template_recycleview_notification,parent,false);

        return new Notification_adapter.MyViewHolder(itemview);
    }

    public void deleteItem(int index) {


        try{
            title.remove(index);
            date.remove(index);
            arrayList.remove(index);
            booleans.remove(index);
            ids.remove(index);
            is_del.remove(index);
            is_fav.remove(index);
            notifyItemRemoved(index);
        }catch(Exception e){

        }

       //// notifyDataSetChanged();

       // notifyItemRemoved(index);
    }

    ArrayList<Integer> deletearray = new ArrayList<>();

    public void addToDeleteArray(int position){

        try{
            deletearray.get(position);
        }catch (Exception e){
            deletearray.add(position);
        }

    }


    @Override
    public void onBindViewHolder(Notification_adapter.MyViewHolder holder, int position) {

        //holder.webView.getSettings().setJavaScriptEnabled(true);
        //holder.textView.setText(Html.fromHtml(arrayList.get(position)));

        //String texsst = arrayList.get(position).replace('\\', ' ');
        //holder.webView.loadData(texsst, "text/html", "en_US");

//        this.ids = ids;
//        this.is_del = is_del;
//        this.is_fav = is_favorite;





            if(is_del.get(position) == true){
                //.constraintLayout.setVisibility(View.GONE);
               // holder.itemView.setVisibility(View.GONE);
                //deleteItem(position);
            }else{
                holder.constraintLayout.setVisibility(View.VISIBLE);
                holder.itemView.setVisibility(View.VISIBLE);
            }


        if(is_fav.get(position) == true){
            holder.imageView.setImageResource(R.drawable.star_y);
        }else{
            holder.imageView.setImageResource(R.drawable.grey_star);
        }



        Send_Notification_pack send_notification_pack = new Send_Notification_pack();

        send_notification_pack.setIsDeleted(is_del.get(position));
        send_notification_pack.setIsFavorite(is_fav.get(position));
        send_notification_pack.setIsRead(booleans.get(position));
        send_notification_pack.setId(ids.get(position));

        holder.imageView.setTag(send_notification_pack);

        holder.imageViewtrash.setTag(send_notification_pack);

        holder.imageViewtrash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Send_Notification_pack hh = (Send_Notification_pack) v.getTag();
                hh.setIsDeleted(true);

                String message = "Вы действительно хотите удалить?";
                String button1 = "отмена";
                String button2 = "подтвердить";

                AlertDialog.Builder builder1 = new AlertDialog.Builder(Library.context);
                builder1.setMessage(message);
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        button2,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                    Observable<Send_Notification_pack> observables = Observable.create(new ObservableOnSubscribe<Send_Notification_pack>() {
                                        @Override
                                        public void subscribe(ObservableEmitter<Send_Notification_pack> emitter) throws Exception {
                                            emitter.onNext(hh);
                                        }
                                    });

                                holder.constraintLayout.setVisibility(View.GONE);
                                    observables.subscribe(Library.observer);


                                    //holder.imageViewtrash.setVisibility(View.GONE);
                            }
                        });

                builder1.setNegativeButton(
                        button1,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();


            }
        });



        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Send_Notification_pack hh = (Send_Notification_pack) v.getTag();

                if(hh.getIsFavorite() == true){
                    hh.setIsFavorite(false);
                    holder.imageView.setImageResource(R.drawable.grey_star);
                }else{
                    hh.setIsFavorite(true);
                    holder.imageView.setImageResource(R.drawable.star_y);
                }


                Observable<Send_Notification_pack> observable = Observable.create(new ObservableOnSubscribe<Send_Notification_pack>() {
                    @Override
                    public void subscribe(ObservableEmitter<Send_Notification_pack> emitter) throws Exception {
                        emitter.onNext(hh);
                    }
                });

                observable.subscribe(Library.observer);




            }
        });



        String newdate = "";

        for(int j = 0;j < date.get(position).length();j++){

            if(j < 10){
                newdate += String.valueOf(date.get(position).charAt(j));
            }

        }

        String formatting_text = arrayList.get(position).replace('\\', ' ');

        Document doc = Jsoup.parse(formatting_text);

        String Textdesc = doc.text();

        //Log.i("logging text",Textdesc);
        if(Textdesc.equals("")){
           // holder.constraintLayout.setVisibility(View.GONE);
           // holder.itemView.setVisibility(View.GONE);
           // deleteItem(position);
        }else if(Textdesc == null){
           // holder.constraintLayout.setVisibility(View.GONE);
           // holder.itemView.setVisibility(View.GONE);
            //deleteItem(position);
        }else if(Textdesc.isEmpty()){
            //holder.constraintLayout.setVisibility(View.GONE);
            //holder.itemView.setVisibility(View.GONE);
            //deleteItem(position);
        }else{
            holder.textView3.setText(Html.fromHtml(Textdesc));
        }




        //String newformat = formatting_text.replace("<img", "");
        //booleans
        String newtitle = "";
        if(!title.equals("")){
            newtitle = title.get(position);
        }else if(title != null){
            newtitle = title.get(position);
        }else{
            holder.constraintLayout.setVisibility(View.GONE);
            holder.itemView.setVisibility(View.GONE);
        }

       holder.textView.setText(newtitle);
//zz
        if(booleans.get(position) == true){
            holder.textView.setTextColor(context.getResources().getColor(R.color.light_black));
            holder.textView.setTextSize(16);
        }else{
            holder.textView.setTextColor(context.getResources().getColor(R.color.black));
            holder.textView.setTextSize(18);
        }

       holder.textView2.setText(newdate);
//xx
        SetTag_obj setTag_obj = new SetTag_obj();
        setTag_obj.setSend_Notification_pack(send_notification_pack);

        setTag_obj.setParam1(formatting_text);
        setTag_obj.setBoleanone(is_fav.get(position));

        holder.textView.setTag(setTag_obj);

        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               SetTag_obj setTag_obj1 = (SetTag_obj) v.getTag();
                String html = setTag_obj1.getParam1();
                Library.html = html;
                Library.send_notification_pack = setTag_obj1.getSend_Notification_pack();
                Library.notif = setTag_obj1.getBoleanone();

                Bundle bundle = new Bundle();
                bundle.putBoolean("boolean",setTag_obj1.getBoleanone());

                Single_Notification single_notification = new Single_Notification();
                single_notification.setArguments(bundle);

                FragmentManager fragmentManager = Library.fragmentManager;
                fragmentManager.
                        beginTransaction().
                       replace(R.id.containerAuth, single_notification,"toback").commitAllowingStateLoss();
                        //replace(R.id.containerAuth, single_notification,"single_notif").commitAllowingStateLoss();

            }
        });


        holder.textView3.setTag(setTag_obj);

        holder.textView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               SetTag_obj setTag_obj1 = (SetTag_obj) v.getTag();
                String html = setTag_obj1.getParam1();
                Library.html = html;
                Library.send_notification_pack = setTag_obj1.getSend_Notification_pack();
                Library.notif = setTag_obj1.getBoleanone();

                Bundle bundle = new Bundle();
                bundle.putBoolean("boolean",setTag_obj1.getBoleanone());


                Single_Notification single_notification = new Single_Notification();
                single_notification.setArguments(bundle);

                FragmentManager fragmentManager = Library.fragmentManager;
                fragmentManager.
                        beginTransaction().
                       replace(R.id.containerAuth, single_notification,"toback").commitAllowingStateLoss();
                        //replace(R.id.containerAuth, single_notification,"single_notif").commitAllowingStateLoss();

            }
        });



//        holder.webView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });

    }

    Context context;

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        WebView webView;
        TextView textView;
        TextView textView2;
        TextView textView3;
        ImageView imageView;
        ConstraintLayout constraintLayout;
        ImageView imageViewtrash;

        public MyViewHolder(View view) {
            super(view);
          //  webView = view.findViewById(R.id.notification_webview);
            textView = view.findViewById(R.id.textView98);
            textView2 = view.findViewById(R.id.textView105);
            textView3 = view.findViewById(R.id.textView107);
            imageView = view.findViewById(R.id.imageView14);
            constraintLayout = view.findViewById(R.id.gg);
            imageViewtrash = view.findViewById(R.id.imageView15);
            context = view.getContext();
        }
    }


}
