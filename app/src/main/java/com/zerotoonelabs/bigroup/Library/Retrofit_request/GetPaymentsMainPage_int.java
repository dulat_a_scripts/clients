package com.zerotoonelabs.bigroup.Library.Retrofit_request;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface GetPaymentsMainPage_int {

    @Headers({

            "Content-Type: application/json"

    })

    @GET("/api/v1/MobileClient/GetPaymentsMainPage")
    Call<Object> getRequest(@Query("property_id") Integer integer, @Query("contractId") Integer integer2, @Header("Token") String authToken);


}
