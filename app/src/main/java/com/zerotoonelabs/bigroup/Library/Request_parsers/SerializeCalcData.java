package com.zerotoonelabs.bigroup.Library.Request_parsers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SerializeCalcData {

    @SerializedName("totalSum")
    @Expose
    private Integer totalSum;
    @SerializedName("list")
    @Expose
    private List<Calculator_list> list = null;

    public Integer getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(Integer totalSum) {
        this.totalSum = totalSum;
    }

    public List<Calculator_list> getList() {
        return list;
    }

    public void setList(List<Calculator_list> list) {
        this.list = list;
    }
}
