package com.zerotoonelabs.bigroup.Library.Retrofit_request;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface GetCheckAccount {

    @Headers({

            "Content-Type: application/json"

    })

    @GET("/api/v1/MobileClient/GetCheckAccount")
    Call<Object> getRequest(@Query("contractNumber") String integer,@Query("contractorIin") String integer2,@Query("contractorUid") String integer3, @Header("Token") String authToken);


    //http://webapi.bi-group.org/api/v1/MobileClient/GetCheckAccount?contractNumber=PREX/C1/21/102&contractorIin=790906301185&contractorUid=5848484
}
