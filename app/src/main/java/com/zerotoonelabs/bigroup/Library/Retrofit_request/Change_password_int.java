package com.zerotoonelabs.bigroup.Library.Retrofit_request;

import com.zerotoonelabs.bigroup.Library.Request_parsers.ChangePasswordParser;
import com.zerotoonelabs.bigroup.Library.Request_parsers.SendChangePassword;
import com.zerotoonelabs.bigroup.Library.Request_parsers.Send_Notification_pack;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface Change_password_int {

   // @FormUrlEncoded
    @POST("api/v1/MobileClient/ChangePassword")
    Call<ChangePasswordParser> sendRequest(@Body SendChangePassword sendChangePassword, @Header("Token") String authToken);

}
