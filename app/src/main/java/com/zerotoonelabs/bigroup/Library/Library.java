package com.zerotoonelabs.bigroup.Library;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.zerotoonelabs.bigroup.Library.Request_parsers.Send_Notification_pack;
import com.zerotoonelabs.bigroup.Library.Retrofit_request.Send_Notification;
import com.zerotoonelabs.bigroup.entity.Flat;
import com.zerotoonelabs.bigroup.entity.Wooppay.WooppayCheck;

import java.util.ArrayList;
import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Library {



        public static Context context;
        public static String baseurl = "http://webapi.bi-group.org";
        public static String baseurlCloud = "https://api.cloudpayments.kz";
        public static HashMap<String, String> data;
        public static String savekvlist;
        public static String housename;
        public static ArrayList<String> srokarray;
        public static String html = null;
        public static Send_Notification_pack send_notification_pack;
        public static boolean notif = false;


        public static int latestpositionImage = 0;
        public static Integer filter_indicator = 0;
        public static Integer fix_gotov = 0;
        public static Integer Fixfilterpage = 0;
        public static Integer propertyid = 0;
        public static String publicid = "";
        public static String api_key = "";
        public static String StorageText = "";
        public static Integer StorageTextfix = 0;
        public static ArrayList<String> edittextar = new ArrayList<>();



        //test
//        public static String baseurlwoopay = "http://api.yii2-stage.test.wooppay.com";
//        public static String Woopay_parent_login = "BiGroupSub";
        //test

        //boevoi
        public static String Woopay_parent_login = "bigroup";
        public static String baseurlwoopay = "https://api-core.wooppay.com";
        public static android.support.v4.app.FragmentManager fragmentManager;
        //boevoi



        public static Context newcontext;

        public static SharedPreferences Phone_memory;

        //Woopay centered memory

        public static WooppayCheck wooppayCheck;
        public static String woopaytoken;
        //Woopay centered memory

        //backbutton
        //WebViewActivity
        public static Intent intent;
        public static String serviceId;
        public static String serviceNumber;
        public static View viewdialog;
        public static View activity;
        public static String token;
        public static String applicationUserId;
        public static Bitmap storeimage;
        public static Bitmap storeimagetwo;



        //backbutton

        public static Retrofit NewRetrofitclient(){


            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient httpClient = new OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .build();

            Retrofit client = new Retrofit.Builder()
                    .baseUrl(Library.baseurl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient)
                    .build();

            return client;


        }

        public static Retrofit NewRetrofitclient_Cloud(){


            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient httpClient = new OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .build();

            Retrofit client = new Retrofit.Builder()
                    .baseUrl(Library.baseurlCloud)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient)
                    .build();

            return client;


        }


        public static Retrofit NewRetrofitclient_woopay(){


            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient httpClient = new OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .build();

            Retrofit client = new Retrofit.Builder()
                    .baseUrl(Library.baseurlwoopay)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient)
                    .build();

            return client;


//            Retrofit client4 = Library.NewRetrofitclient();
//
//            GetProduct_list_int service4 = client4.create(GetProduct_list_int.class);
//
//            String tokenstring4 = Library.GetPhoneMemoryOne("token");
//
//            Call<Object> objectCall4 = service4.getData("Bearer " + tokenstring4);
//
//
//            try{
//
//                Gson gson = new Gson();
//
//                Library.response_products_list_string = gson.toJson(objectCall4.execute().body());
//                Library.response_products_list_raw = gson.toJson(objectCall4.execute().raw());
//
//
//                response = Library.response_products_list_string;
//
//                objectCall4.execute();
//
//            }catch (Exception e){
//
//            }


        }

        public static Boolean getjsonobject_one_bool(String new_parse_string,String convert_name_one){

            Log.i("r","r" + new_parse_string);

            try{
                JsonElement jelement = new JsonParser().parse(new_parse_string);
                JsonObject jobject = jelement.getAsJsonObject();
                //login
                JsonElement login = jobject.get(convert_name_one);
                Boolean val1 = login.getAsBoolean();

                return val1;
            }catch (Exception e){
                Log.i("exception","exception: " + e.toString());
                return false;
            }


        }


        public static String getjsonobject_one_string(String new_parse_string,String convert_name_one){

            Log.i("r","r" + new_parse_string);

            try{
                JsonElement jelement = new JsonParser().parse(new_parse_string);
                JsonObject jobject = jelement.getAsJsonObject();
                //login
                JsonElement login = jobject.get(convert_name_one);
                String val1 = login.getAsString();

                return val1;
            }catch (Exception e){
                Log.i("exception","exception: " + e.toString());
                return null;
            }


        }


        public static String getjsonobject_one_2_param(String new_parse_string,String convert_name_one,String convert_name_two){

            Log.i("r","r" + new_parse_string);

            try{
                JsonElement jelement = new JsonParser().parse(new_parse_string);
                JsonObject jobject = jelement.getAsJsonObject();
                //login
                JsonElement login = jobject.get(convert_name_one);

                JsonObject jsonObject = login.getAsJsonObject();

                String name = jsonObject.get(convert_name_two).getAsString();

                return name;
            }catch (Exception e){
                Log.i("exception","exception: " + e.toString());

                return "";
            }


        }


        public static String getjsonobject_one_getFlat(String new_parse_string,String convert_name_one,String convert_name_two){

            Log.i("r","r" + new_parse_string);

            try{
                JsonElement jelement = new JsonParser().parse(new_parse_string);

                JsonObject jj = jelement.getAsJsonObject();

                JsonArray jobject = jj.get(convert_name_one).getAsJsonArray();
                //login
                String hh = null;

                for(int y = 0;y < jobject.size();y++){
                    hh = jobject.get(y).getAsJsonObject().get(convert_name_two).getAsString();
                }


                return hh;
            }catch (Exception e){
                Log.i("exception","exception: " + e.toString());

                return "";
            }


        }

        public static Integer getjsonobject_one_2_param_int(String new_parse_string,String convert_name_one,String convert_name_two){

            Log.i("r","r" + new_parse_string);

            try{
                JsonElement jelement = new JsonParser().parse(new_parse_string);
                JsonObject jobject = jelement.getAsJsonObject();
                //login
                JsonElement login = jobject.get(convert_name_one);

                JsonObject jsonObject = login.getAsJsonObject();

                Integer name = jsonObject.get(convert_name_two).getAsInt();

                return name;
            }catch (Exception e){
                Log.i("exception","exception: " + e.toString());

                return null;
            }


        }


        public static Boolean getjsonobject_one_2_param_bool(String new_parse_string,String convert_name_one,String convert_name_two){

            Log.i("r","r" + new_parse_string);

            try{
                JsonElement jelement = new JsonParser().parse(new_parse_string);
                JsonObject jobject = jelement.getAsJsonObject();
                //login
                JsonElement login = jobject.get(convert_name_one);

                JsonObject jsonObject = login.getAsJsonObject();

                Boolean name = jsonObject.get(convert_name_two).getAsBoolean();

                return name;
            }catch (Exception e){
                Log.i("exception","exception: " + e.toString());

                return false;
            }


        }


    public static String getAsString(String new_parse_string){

        Log.i("r","r" + new_parse_string);

        try{

            JsonElement jelement = new JsonParser().parse(new_parse_string);
            String returnstring = jelement.getAsString();

            return returnstring;
        }catch (Exception e){
            Log.i("exception","exception: " + e.toString());
            return null;
        }


    }


    public static Integer getAsInt(String new_parse_string){

        Log.i("r","r" + new_parse_string);

        try{

            JsonElement jelement = new JsonParser().parse(new_parse_string);
            Integer returnstring = jelement.getAsInt();

            return returnstring;
        }catch (Exception e){
            Log.i("exception","exception: " + e.toString());
            return null;
        }


    }



    public static boolean SaveregistrationMemory(ArrayList<String>  newarray,ArrayList<Integer> newarrayint){

        String phone = newarray.get(0);
        String email = newarray.get(1);
        String role = newarray.get(2);
        String language = newarray.get(3);
        String codename = newarray.get(4);
        String activatetoken = newarray.get(5);
        String authkey = newarray.get(6);
        String login = newarray.get(7);

        Integer smscode = newarrayint.get(0);
//        Integer serviceplan = newarrayint.get(1);
//        Integer daysleft = newarrayint.get(2);
//        Integer createtime = newarrayint.get(3);
//        Integer updatetime = newarrayint.get(4);


        SharedPreferences sharedPref = Library.context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
//        editor.putString(Library.context.getString(R.string.memory_phone), phone);
//        editor.putString(Library.context.getString(R.string.memory_email), email);
//        editor.putString(Library.context.getString(R.string.memory_role), role);
//        editor.putString(Library.context.getString(R.string.memory_language), language);
//        editor.putString(Library.context.getString(R.string.memory_codename), codename);
//        editor.putString(Library.context.getString(R.string.memory_activatetoken), activatetoken);
//        editor.putString(Library.context.getString(R.string.memory_authkey), authkey);
//        editor.putString(Library.context.getString(R.string.memory_login), login);

//        editor.putInt(Library.context.getString(R.string.memory_smscode), smscode);
//        editor.putInt(Library.context.getString(R.string.memory_serviceplan), serviceplan);
//        editor.putInt(Library.context.getString(R.string.memory_daysleft), daysleft);
//        editor.putInt(Library.context.getString(R.string.memory_createtime), createtime);
//        editor.putInt(Library.context.getString(R.string.memory_updatetime), updatetime);

        editor.commit();


        return true;
    }



    public static boolean SavePhoneMemory(ArrayList<String>  savestring){
        savestring.toString();

        String expired = savestring.get(0);
        String role = savestring.get(1);
        String token = savestring.get(2);
        String username = savestring.get(3);
        String password = savestring.get(4);

        Log.i("pass",password);



        SharedPreferences sharedPref = Library.newcontext.getSharedPreferences("settings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
//        editor.putString(Library.context.getString(R.string.memory_expired), expired);
//        editor.putString(Library.context.getString(R.string.memory_role), role);
//        editor.putString(Library.context.getString(R.string.memory_token), token);
//        editor.putString(Library.context.getString(R.string.memory_username), username);
//        editor.putString(Library.context.getString(R.string.memory_password), password);
        editor.commit();

        return true;
    }

    public static byte[] storebyte;

        public static Flat flat;

    public static boolean SavePhoneMemoryOne(String savenametext,String savestring){
        savestring.toString();

        try{
            String newsavestring = savestring;

            SharedPreferences sharedPref = Library.newcontext.getSharedPreferences("settings", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(savenametext, newsavestring);

            editor.commit();
        }catch (Exception e){

        }



        return true;
    }

    public static String GetPhoneMemoryOne(String gettextinmomoryname){

        String newstring = null;

        SharedPreferences sharedPreff = Library.newcontext.getSharedPreferences("settings", Context.MODE_PRIVATE);

        newstring = sharedPreff.getString(gettextinmomoryname,"not");

        return newstring;
    }

    public static Integer GetPhoneMemoryOneInt(String gettextinmomoryname){

        Integer newstring = null;

        SharedPreferences sharedPreff = Library.newcontext.getSharedPreferences("settings", Context.MODE_PRIVATE);

        newstring = sharedPreff.getInt(gettextinmomoryname,0);

        return newstring;
    }


    public static ArrayList<String> GetPhoneMemory(){

        ArrayList <String> getmemory = new ArrayList<String>();

        SharedPreferences sharedPref = Library.newcontext.getSharedPreferences("settings", Context.MODE_PRIVATE);

//        getmemory.add(sharedPref.getString(Library.context.getString(R.string.memory_expired),null));
//        getmemory.add(sharedPref.getString(Library.context.getString(R.string.memory_role),null));
//        getmemory.add(sharedPref.getString(Library.context.getString(R.string.memory_token),null));
//        getmemory.add(sharedPref.getString(Library.context.getString(R.string.memory_username),null));
//        getmemory.add(sharedPref.getString(Library.context.getString(R.string.memory_password),null));

        return getmemory;

    }


    public static void showToast(Context context,String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static boolean containsUppercase(String str) {
        return str.codePoints().filter(Character::isUpperCase)
                .findFirst().isPresent();
    }
//    Set<Map.Entry<String, JsonElement>> entries = jobject.entrySet();//will return members of your object
//                for (Map.Entry<String, JsonElement> entry: entries) {
//        //System.out.println(entry.getKey());
//        // Log.i("S","s" + entry.getKey());
//        String jjj = entry.getKey();
//        String ddd = jjj;
//        base.add(ddd);
//    }

//observer

        public static Observer<Send_Notification_pack> observer = new Observer<Send_Notification_pack>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Send_Notification_pack send_notification_pack) {
                String userid = GetPhoneMemoryOne("userid");

                send_notification_pack.setApplicationUserId(userid);

                Retrofit client = NewRetrofitclient();
                Send_Notification send_notification = client.create(Send_Notification.class);
                String token = GetPhoneMemoryOne("token");
                Call<Object> objectCall = send_notification.sendRequest(send_notification_pack,token);

                objectCall.enqueue(new Callback<Object>() {
                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {

                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable t) {

                    }
                });

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }

        };





    public static io.reactivex.Observable<Integer> observable;



        //navigation next
//        ApartmentFilterFragment fragment = new ApartmentFilterFragment();
//        fragment.setTargetFragment(this, 0);
//    String tag = "Применить фильтр";
//
//        fragmentManager.beginTransaction()
//                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
//                .replace(R.id.flContent, fragment, tag)
//                .addToBackStack(null)
//                .commit();
        //navigation

        //navigation back

//        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//                fragmentManager.popBackStack();
        //navigation back






        //Send_Notification_pack
//

//observer





}
