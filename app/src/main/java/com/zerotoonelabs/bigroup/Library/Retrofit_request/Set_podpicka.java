package com.zerotoonelabs.bigroup.Library.Retrofit_request;

import com.zerotoonelabs.bigroup.entity.AutoPayRequest;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface Set_podpicka {

    @POST("subscriptions/create")
    Call<ResponseBody> createAutoPay(@Header("Authorization") String autorization, @Body AutoPayRequest request);

}
