package com.zerotoonelabs.bigroup.Library.Retrofit_request;

import android.arch.lifecycle.LiveData;

import com.zerotoonelabs.bigroup.api.ApiResponse;
import com.zerotoonelabs.bigroup.entity.Apartments.Apartment;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.QueryMap;

public interface GetApartment {

    @GET("api/v1/SalesBIGroup/Flats_all/")
    Call<List<Apartment>> getApartments(@Header("Token") String token, @QueryMap HashMap<String, String> queries);
   // LiveData<ApiResponse<List<Apartment>>> getApartments(@Header("Token") String token, @QueryMap HashMap<String, String> queries);

}
