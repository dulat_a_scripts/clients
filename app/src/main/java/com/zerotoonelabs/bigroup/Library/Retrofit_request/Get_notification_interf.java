package com.zerotoonelabs.bigroup.Library.Retrofit_request;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface Get_notification_interf {

    @GET("api/v1/MobileClient/GetNotifications")
    //api/v1/MobileClient/GetNotifications
    Call<Object> getArray(@Header("Token") String token, @Query("applicationUserId") String userid);
    
}
