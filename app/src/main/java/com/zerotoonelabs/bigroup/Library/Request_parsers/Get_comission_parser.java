package com.zerotoonelabs.bigroup.Library.Request_parsers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Get_comission_parser {

    @SerializedName("SberBankKomisiya")
    @Expose
    private Double sberBankKomisiya;
    @SerializedName("AmountSberBankTotal")
    @Expose
    private Double amountSberBankTotal;
    @SerializedName("AnotherBankKomisiya")
    @Expose
    private Double anotherBankKomisiya;
    @SerializedName("AmountAnotherBankTotal")
    @Expose
    private Double amountAnotherBankTotal;

    public Double getSberBankKomisiya() {
        return sberBankKomisiya;
    }

    public void setSberBankKomisiya(Double sberBankKomisiya) {
        this.sberBankKomisiya = sberBankKomisiya;
    }

    public Double getAmountSberBankTotal() {
        return amountSberBankTotal;
    }

    public void setAmountSberBankTotal(Double amountSberBankTotal) {
        this.amountSberBankTotal = amountSberBankTotal;
    }

    public Double getAnotherBankKomisiya() {
        return anotherBankKomisiya;
    }

    public void setAnotherBankKomisiya(Double anotherBankKomisiya) {
        this.anotherBankKomisiya = anotherBankKomisiya;
    }

    public Double getAmountAnotherBankTotal() {
        return amountAnotherBankTotal;
    }

    public void setAmountAnotherBankTotal(Double amountAnotherBankTotal) {
        this.amountAnotherBankTotal = amountAnotherBankTotal;
    }

}
