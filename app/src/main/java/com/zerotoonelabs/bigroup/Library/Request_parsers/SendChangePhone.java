package com.zerotoonelabs.bigroup.Library.Request_parsers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendChangePhone {

    @SerializedName("ui")
    @Expose
    private String userId;
    @SerializedName("phone")
    @Expose
    private String phoneNumber;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

}
