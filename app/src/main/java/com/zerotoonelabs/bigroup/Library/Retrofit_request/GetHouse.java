package com.zerotoonelabs.bigroup.Library.Retrofit_request;

import com.zerotoonelabs.bigroup.entity.BuildingSite;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface GetHouse {

    @GET("api/v1/salesbigroup/real_estate?")
    Call<List<BuildingSite>> getBuildingSites(@Header("Token") String token, @Query("gorod") String city);
}
