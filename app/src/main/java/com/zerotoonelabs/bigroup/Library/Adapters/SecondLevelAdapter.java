package com.zerotoonelabs.bigroup.Library.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.zerotoonelabs.bigroup.R;

import java.util.ArrayList;
import java.util.List;

public class SecondLevelAdapter extends BaseExpandableListAdapter {

    private Context context;


    ArrayList<ArrayList> data;

    ArrayList headers;


    public SecondLevelAdapter(Context context, ArrayList headers, ArrayList<ArrayList> data) {
        this.context = context;
        this.data = data;
        this.headers = headers;
    }

    @Override
    public Object getGroup(int groupPosition) {

        return headers.get(groupPosition);
    }

    @Override
    public int getGroupCount() {

        return headers.size();

    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {


        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.row_second, null);
        TextView text = (TextView) convertView.findViewById(R.id.textView148);
        TextView text2 = (TextView) convertView.findViewById(R.id.rowParentText);
        TextView text3 = (TextView) convertView.findViewById(R.id.textView130);

        String groupText = getGroup(groupPosition).toString();
        String ntext[];
        ntext = groupText.split("--");

        text.setText(ntext[0]);
        text2.setText(ntext[1]);
        text3.setText(ntext[2]);

        return convertView;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {

        ArrayList<String> childData;

        childData = data.get(groupPosition);


        return childData.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.row_third, null);

        TextView textView = (TextView) convertView.findViewById(R.id.textView148);
        TextView textView2 = (TextView) convertView.findViewById(R.id.rowParentText);
        TextView textView3 = (TextView) convertView.findViewById(R.id.textView130);

        ArrayList<String> childArray = data.get(groupPosition);

        String text = childArray.get(childPosition);
        String two[];
        two = text.split("--");
        textView.setText(two[0]);
        textView2.setText(two[1]);
        textView3.setText(two[2]);

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        ArrayList<String> children = new ArrayList<>();

        try{
            children = data.get(groupPosition);
        }catch (Exception e){

        }

        return children.size();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}
