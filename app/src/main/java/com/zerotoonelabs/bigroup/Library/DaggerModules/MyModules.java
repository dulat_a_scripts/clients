package com.zerotoonelabs.bigroup.Library.DaggerModules;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
//типы возвращаемых данных, также должны быть аннотированы @Providesаннотацией
@Module
public class MyModules {

    Application mapplication;

    public MyModules(Application application){
        mapplication = application;
    }

    @Provides
    @Singleton
    Application getApplication(){
        return mapplication;
    }
}
