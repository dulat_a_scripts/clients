package com.zerotoonelabs.bigroup.Library.Retrofit_request;

import com.zerotoonelabs.bigroup.entity.Wooppay.WooppayCheck;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;


public interface Send_Woopay_history {

    @Headers({

            "Content-Type: application/json"

    })

    @POST("/api/v1/MobileClient/PostPaymentFromMobile")
    Call<Object> getRequest(@Body WooppayCheck wooppayCheck, @Header("Token") String authToken);
}
