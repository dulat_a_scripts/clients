package com.zerotoonelabs.bigroup.Library.Request_parsers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendReiting_service {

    @SerializedName("DocumentNumber")
    @Expose
    private String documentNumber;

    @SerializedName("Remark")
    @Expose
    private String remark;

    @SerializedName("Assessment")
    @Expose
    private String Assessment;

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAssessment() {
        return Assessment;
    }

    public void setAssessment(String assessment) {
        Assessment = assessment;
    }



}
