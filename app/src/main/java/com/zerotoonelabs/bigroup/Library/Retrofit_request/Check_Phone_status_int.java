package com.zerotoonelabs.bigroup.Library.Retrofit_request;

import com.zerotoonelabs.bigroup.Library.Request_parsers.CheckPhoneStatus;
import com.zerotoonelabs.bigroup.entity.BuildingSite;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface Check_Phone_status_int {

    @GET("api/v1/MobileClient/PhoneNumber")
    Call<CheckPhoneStatus> check_inn(@Header("token") String token, @Query("iin") String inn);
}
