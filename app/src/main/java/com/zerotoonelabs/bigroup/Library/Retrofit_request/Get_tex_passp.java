package com.zerotoonelabs.bigroup.Library.Retrofit_request;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface Get_tex_passp {

    @GET("api/v1/MobileClient/GetDogovorFile?IfPdf=1&type=3")
    Call<ResponseBody> getPdf(@Header("Token") String token, @Query("contractId") String contractId, @Query("property_id") String propertyId);

}
