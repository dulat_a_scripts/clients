package com.zerotoonelabs.bigroup.Library.Request_parsers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Get_flat_parser {
    @SerializedName("property_id")
    @Expose
    private Integer propertyId;
    @SerializedName("real_estate")
    @Expose
    private String realEstate;
    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("pic_apartment_layout")
    @Expose
    private String picApartmentLayout;
    @SerializedName("propertyGuid")
    @Expose
    private String propertyGuid;

    public Integer getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Integer propertyId) {
        this.propertyId = propertyId;
    }

    public String getRealEstate() {
        return realEstate;
    }

    public void setRealEstate(String realEstate) {
        this.realEstate = realEstate;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPicApartmentLayout() {
        return picApartmentLayout;
    }

    public void setPicApartmentLayout(String picApartmentLayout) {
        this.picApartmentLayout = picApartmentLayout;
    }

    public String getPropertyGuid() {
        return propertyGuid;
    }

    public void setPropertyGuid(String propertyGuid) {
        this.propertyGuid = propertyGuid;
    }

}
