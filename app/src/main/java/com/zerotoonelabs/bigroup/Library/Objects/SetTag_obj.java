package com.zerotoonelabs.bigroup.Library.Objects;

import android.app.Dialog;
import android.content.Intent;

import com.zerotoonelabs.bigroup.Library.Request_parsers.Send_Notification_pack;

public class SetTag_obj {


    public String getParam1() {
        return param1;
    }

    public void setParam1(String param1) {
        this.param1 = param1;
    }

    public String getParam2() {
        return param2;
    }

    public void setParam2(String param2) {
        this.param2 = param2;
    }

    public Integer getIntparam() {
        return intparam;
    }

    public void setIntparam(Integer intparam) {
        this.intparam = intparam;
    }

    public Integer intparam;



    public Integer intparam2;

    public String param1;
    public String param2;
    public Boolean boleanone;
    public Boolean booleantwo;
    public Boolean booleanthree;
    public Integer id;
    public Send_Notification_pack Send_Notification_pack;
    public com.zerotoonelabs.bigroup.Library.Request_parsers.Send_Notification_pack getSend_Notification_pack() {
        return Send_Notification_pack;
    }

    public void setSend_Notification_pack(com.zerotoonelabs.bigroup.Library.Request_parsers.Send_Notification_pack send_Notification_pack) {
        Send_Notification_pack = send_Notification_pack;
    }



    public Integer getIntparam2() {
        return intparam2;
    }

    public void setIntparam2(Integer intparam2) {
        this.intparam2 = intparam2;
    }





    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }



    public Boolean getBoleanone() {
        return boleanone;
    }

    public void setBoleanone(Boolean boleanone) {
        this.boleanone = boleanone;
    }

    public Boolean getBooleantwo() {
        return booleantwo;
    }

    public void setBooleantwo(Boolean booleantwo) {
        this.booleantwo = booleantwo;
    }

    public Boolean getBooleanthree() {
        return booleanthree;
    }

    public void setBooleanthree(Boolean booleanthree) {
        this.booleanthree = booleanthree;
    }



    public Dialog getDialog() {
        return dialog;
    }

    public void setDialog(Dialog dialog) {
        this.dialog = dialog;
    }

    public Dialog dialog;

}
