package com.zerotoonelabs.bigroup.Library.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.zerotoonelabs.bigroup.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ThreeLevelListAdapter extends BaseExpandableListAdapter {

   // String[] parentHeaders;
    ArrayList<String> parentHeaders = new ArrayList<>();
    List<String[]> secondLevel;
    ArrayList<ArrayList> secondLeveltwo;
    private Context context;
    List<LinkedHashMap<String, ArrayList>> data;

    public ThreeLevelListAdapter(Context context, ArrayList<String> parentHeader, ArrayList secondLevel, List<LinkedHashMap<String, ArrayList>> data) {
        this.context = context;

        this.parentHeaders = parentHeader;

        //this.secondLevel = secondLevel;
        this.secondLeveltwo = secondLevel;
        //secondLeveltwo

        this.data = data;
    }

    @Override
    public int getGroupCount() {
        return parentHeaders.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {


        // no idea why this code is working

        return 1;

    }

    @Override
    public Object getGroup(int groupPosition) {

        return groupPosition;
    }

    @Override
    public Object getChild(int group, int child) {


        return child;


    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.row_first, null);
        TextView text = (TextView) convertView.findViewById(R.id.rowParentText);
        TextView text2 = (TextView) convertView.findViewById(R.id.textView130);
        TextView text3 = (TextView) convertView.findViewById(R.id.textView148);

        String convert = this.parentHeaders.get(groupPosition);
        String[] nerwstr;
        nerwstr = convert.split("--");

        text3.setText(nerwstr[0]);
        text.setText(nerwstr[1]);
        text2.setText(nerwstr[2]);

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final SecondLevelExpandableListView secondLevelELV = new SecondLevelExpandableListView(context);

        //String[] headers = secondLevel.get(groupPosition);
        ArrayList headers = new ArrayList();

        try{
            headers = secondLeveltwo.get(groupPosition);
        }catch (Exception e){

        }

        ArrayList childData = new ArrayList<>();
        HashMap<String, ArrayList> secondLevelData = new HashMap<>();
        Integer fix = 0;

        try{
            secondLevelData = data.get(groupPosition);
            fix = 1;
        }catch (Exception e){
            fix = 0;
        }


        if(fix == 1){

            for(String key : secondLevelData.keySet())
            {

                childData.add(secondLevelData.get(key));

            }



            secondLevelELV.setAdapter(new SecondLevelAdapter(context, headers,childData));

            secondLevelELV.setGroupIndicator(null);


            secondLevelELV.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                int previousGroup = -1;

                @Override
                public void onGroupExpand(int groupPosition) {
                    if(groupPosition != previousGroup)
                        secondLevelELV.collapseGroup(previousGroup);
                    previousGroup = groupPosition;
                }
            });

        }




        return secondLevelELV;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


}
