package com.zerotoonelabs.bigroup.Library.Retrofit_request;

import com.zerotoonelabs.bigroup.entity.Wooppay.AuthToken;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface Get_auth_token {

    @FormUrlEncoded
    @POST("/v5/auth/pseudo")
    Call<AuthToken> getAuthToken(
                                 @Header("Content-Type") String contentType,
                                 @Field("login") String login,
                                 @Field("email") String email,
                                 @Field("parentLogin") String parentLogin);


    }
