package com.zerotoonelabs.bigroup.Library.Request_parsers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ApiKeyParser {

    @SerializedName("accname")
    @Expose
    private String accname;
    @SerializedName("psw")
    @Expose
    private String psw;

    public String getAccname() {
        return accname;
    }

    public void setAccname(String accname) {
        this.accname = accname;
    }

    public String getPsw() {
        return psw;
    }

    public void setPsw(String psw) {
        this.psw = psw;
    }


}
