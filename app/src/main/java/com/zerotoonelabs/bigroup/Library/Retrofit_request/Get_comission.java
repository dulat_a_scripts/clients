package com.zerotoonelabs.bigroup.Library.Retrofit_request;

import com.zerotoonelabs.bigroup.Library.Request_parsers.Get_comission_parser;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface Get_comission {

    @GET("api/v1/MobileClient/GetBankKomisiya")
    Call<Get_comission_parser> getData(@Header("Token") String token, @Query("amount") Long amount);

}
