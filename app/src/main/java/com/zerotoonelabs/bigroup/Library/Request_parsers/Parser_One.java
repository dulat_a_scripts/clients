package com.zerotoonelabs.bigroup.Library.Request_parsers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Parser_One {

    @SerializedName("serviceIds")
    @Expose
    private List<ServiceId> serviceIds = null;

    public List<ServiceId> getServiceIds() {
        return serviceIds;
    }

    public void setServiceIds(List<ServiceId> serviceIds) {
        this.serviceIds = serviceIds;
    }

}
