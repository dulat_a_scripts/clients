package com.zerotoonelabs.bigroup.Library.Retrofit_request;

import com.zerotoonelabs.bigroup.Library.Request_parsers.ChangePasswordParser;
import com.zerotoonelabs.bigroup.Library.Request_parsers.SendChangeEmail;
import com.zerotoonelabs.bigroup.Library.Request_parsers.SendChangePassword;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface ChangeEmail_int {
    @POST("api/v1/MobileClient/Changeemail")
    Call<Object> sendRequest(@Body SendChangeEmail sendChangeEmail, @Header("Token") String authToken);
}
