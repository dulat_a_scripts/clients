package com.zerotoonelabs.bigroup.Library;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Instrumentation;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.zerotoonelabs.bigroup.R;
import java.util.ArrayList;


public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.MyViewHolder> {

    ArrayList<String> arrayList = new ArrayList<>();

    ArrayList<Bitmap> bitmaps = new ArrayList<Bitmap>();
    String[] mDataset;
    public Context context;
    private CallbackInterface mCallback;

    public interface CallbackInterface{

        void onHandleSelection(int position, String text);

    }

    public ServiceAdapter(Context context,ArrayList<String> arrayList,ArrayList<Bitmap> bitmaps){
        this.arrayList = arrayList;
        this.context = context;
        this.bitmaps = bitmaps;
        try{
            mCallback = (CallbackInterface) context;
        }catch (Exception e){

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemview = ((Activity)context).getCurrentFocus();
        if (itemview != null) {
            itemview.clearFocus();
        }

        itemview = LayoutInflater.from(parent.getContext()).inflate(R.layout.addservicenote_recycle_view,parent,false);

        return new MyViewHolder(itemview);
    }



    private static final int SELECTED_PIC = 1;
    int latestsendposition = 0;

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

//        addimage = view.findViewById(R.id.button23);
//        deleteposition = view.findViewById(R.id.delete_comment_button);
//        addposition = view.findViewById(R.id.button24);


        try{
            if(!arrayList.get(position).equals("1")){
                holder.editText.setText(arrayList.get(position));
            }else{
                holder.editText.setText("");
            }
        }catch (Exception e){

        }



        holder.editText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }
        });




//        if(bitmaps.get(position) == bitmaps.get(18)){
//            holder.delphoto.setVisibility(View.GONE);
//        }else{
//            holder.delphoto.setVisibility(View.VISIBLE);
//        }

        try{
            holder.imageView.setImageBitmap(bitmaps.get(position));
        }catch (Exception e){
            Library.showToast(context,"Фото загружено но оно не будет отображено так как оно имеет большой формат");
        }






        holder.addzamech.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onHandleSelection(position, "addzamech");
            }
        });

        holder.remove_zamech.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.editText.setText("");
               // holder.delphoto.setVisibility(View.GONE);
                mCallback.onHandleSelection(holder.getAdapterPosition(), "removezamech");

            }
        });


        holder.delphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //holder.delphoto.setVisibility(View.GONE);
                Bitmap no_image = BitmapFactory.decodeResource(context.getResources(),R.drawable.no_image);
                holder.imageView.setImageBitmap(no_image);
                mCallback.onHandleSelection(holder.getAdapterPosition(), "del_photo");

            }
        });


        holder.addimage.setOnClickListener(v -> {

            String yy = holder.editText.getText().toString();

            if(yy.length() > 2){
                Library.StorageText = yy;
                Library.StorageTextfix = 1;
            }else{
                Library.StorageTextfix = 0;
                holder.editText.setText("");
            }

            Library.latestpositionImage = holder.getAdapterPosition();
            mCallback.onHandleSelection(holder.getAdapterPosition(), "add_photo");

        });


        holder.editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try{
                    int posision = holder.getAdapterPosition();
                   Library.edittextar.set(posision,s.toString());

                   Log.i("array","ar " + Library.edittextar.toString());
                }catch (Exception e){
                    Library.edittextar.add(s.toString());
                }
            }
        });

    }

    public ArrayList<String> getArrayList() {


        int fix = 0;



        return Library.edittextar;



    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        EditText editText;
        ImageView addimage;
        ImageView delphoto;
        ImageView imageView;
        ConstraintLayout constraintLayout;

        Button addzamech;
        ImageView remove_zamech;

        public MyViewHolder(View view) {
            super(view);
            editText = view.findViewById(R.id.issueDescription);
            addimage = view.findViewById(R.id.imageView21);
            delphoto = view.findViewById(R.id.imageView17);
            imageView = view.findViewById(R.id.image_from_camera);
            addzamech = view.findViewById(R.id.button27);
            remove_zamech = view.findViewById(R.id.imageView22);
            constraintLayout = view.findViewById(R.id.con);

        }
    }


}
