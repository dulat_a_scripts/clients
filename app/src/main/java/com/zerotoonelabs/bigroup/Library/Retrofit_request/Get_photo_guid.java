package com.zerotoonelabs.bigroup.Library.Retrofit_request;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface Get_photo_guid {

    @GET("api/v1/MobileClient/GetResponsiblePhotoByGuid")
    Call<Object> getdecodeImage(@Header("Token") String token, @Query("ssylka") String user_guid);
}
