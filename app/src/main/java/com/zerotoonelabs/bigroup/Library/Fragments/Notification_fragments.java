package com.zerotoonelabs.bigroup.Library.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.zerotoonelabs.bigroup.Library.Adapters.Notification_adapter;
import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.Library.Retrofit_request.Get_notification_interf;
import com.zerotoonelabs.bigroup.Library.ServiceAdapter;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.authui.HomeActivity;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class Notification_fragments extends Fragment {

    View view;
    RecyclerView recyclerView;
    Notification_adapter notification_adapter;
    public Menu menus;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setupActionBar();
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.notification_layout, container, false);
        recyclerView = view.findViewById(R.id.recycleView);



        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        Library.context = getActivity();
        Library.fragmentManager = getActivity().getSupportFragmentManager();



        return view;
    }

    private void setupActionBar() {

        Toolbar mToolbar = getActivity().findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
        actionBar.setDisplayHomeAsUpEnabled(true);
        //actionBar.setTitle("");

        setHasOptionsMenu(true);



        //actionBar.
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        menu.clear();
        inflater.inflate(R.menu.notification_icons, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    Integer fixbutton = 0;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == R.id.star_id){


            if(fixbutton == 0){
                Log.i("dd","item_selected");

                Get_notifications();
                //notification_adapter.setFavorite(1);
                item.setIcon(R.drawable.star_y_b);
                fixbutton = 1;
                iffavorfix = 1;
            }else{
                Log.i("dd","item_selected");
                Get_notifications();
                //notification_adapter.setFavorite(0);
                item.setIcon(R.drawable.star_white);
                fixbutton = 0;
                iffavorfix = 0;
            }

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        Get_notifications();
        super.onResume();
    }

    Integer iffavorfix = 0;

    public void Get_notifications(){

        Retrofit client = Library.NewRetrofitclient();
        Get_notification_interf get_notification_interf = client.create(Get_notification_interf.class);
        String token = Library.GetPhoneMemoryOne("token");
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        String userid = sharedPreferences.getString("applicationUserId", null);

        Call<Object> getNotificationConverterCall = get_notification_interf.getArray(token,userid);

        getNotificationConverterCall.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {

                ArrayList<String> arrayList = new ArrayList<>();
                ArrayList<String> titles = new ArrayList<>();
                ArrayList<String> dates = new ArrayList<>();
                ArrayList<Boolean> booleansidread = new ArrayList<>();
                ArrayList<Boolean> booleansis_del = new ArrayList<>();
                ArrayList<Boolean> booleans_id_favorite = new ArrayList<>();
                ArrayList<Integer> ids = new ArrayList<>();


                Log.i("resp","resp:" + response.body());
                Gson gson = new Gson();
                String dd = gson.toJson(response.body());

                JsonParser parser = new JsonParser();
//                JsonObject rootObj = parser.parse(dd).getAsJsonObject();
//                JsonArray paymentsArray = rootObj.getAsJsonArray();

                JsonArray paymentsArray = new JsonArray();
                paymentsArray = parser.parse(dd).getAsJsonArray();

                for (JsonElement pa : paymentsArray) {
                    JsonObject paymentObj = pa.getAsJsonObject();
                    Integer Id = paymentObj.get("Id").getAsInt();
                    Boolean isread = paymentObj.get("IsRead").getAsBoolean();
                    String message = paymentObj.get("Message").getAsString();
                    String type = paymentObj.get("Type").getAsString();
                    String DateTime = paymentObj.get("DateTime").getAsString();
                    String ApplicationUserId = paymentObj.get("ApplicationUserId").getAsString();
                    Boolean IsDeleted = paymentObj.get("IsDeleted").getAsBoolean();
                    Boolean IsFavorite = paymentObj.get("IsFavorite").getAsBoolean();

                    Log.i("date",DateTime);
                    //2018-07-13T16:47:15.62
                    String newYear = "";
                    String newMonth = "";
                    String newDate = "";
                    for(int h = 0;h < DateTime.length();h++){
                        if(h < 4){
                            newYear += String.valueOf(DateTime.charAt(h));
                        }

                        if(h > 4 && h < 7){
                            newMonth += String.valueOf(DateTime.charAt(h));
                        }

                        if(h > 7 && h < 10){
                            newDate += String.valueOf(DateTime.charAt(h));
                        }

                    }

                    String Convertdate = newDate + "." + newMonth + "." + newYear;

                    //Log.i("",Convertdate);

                    DateTime = Convertdate;

                    if(IsDeleted == false){
                        if(iffavorfix == 0){
                            arrayList.add(message);
                            titles.add(type);
                            dates.add(DateTime);
                            booleansidread.add(isread);
                            ids.add(Id);
                            booleansis_del.add(IsDeleted);
                            booleans_id_favorite.add(IsFavorite);
                        }else{
                            if(IsFavorite == true){
                                arrayList.add(message);
                                titles.add(type);
                                dates.add(DateTime);
                                booleansidread.add(isread);
                                ids.add(Id);
                                booleansis_del.add(IsDeleted);
                                booleans_id_favorite.add(IsFavorite);
                            }

                        }

                    }


                }

//                if(is_del.get(position) == true){
//                    //.constraintLayout.setVisibility(View.GONE);
//                    // holder.itemView.setVisibility(View.GONE);
//                    //deleteItem(position);
//                }else{
//                    holder.constraintLayout.setVisibility(View.VISIBLE);
//                    holder.itemView.setVisibility(View.VISIBLE);
//                }

                notification_adapter = new Notification_adapter(titles,dates,arrayList,booleansidread,ids,booleansis_del,booleans_id_favorite);
                recyclerView.setAdapter(notification_adapter);

            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {

            }
        });

        Log.i("token","token:" + token + "|" + userid);



    }



}
