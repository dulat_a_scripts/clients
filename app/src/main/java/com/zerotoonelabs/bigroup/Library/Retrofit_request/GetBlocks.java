package com.zerotoonelabs.bigroup.Library.Retrofit_request;

import com.zerotoonelabs.bigroup.entity.BuildingSite;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

public interface GetBlocks {

    @GET("api/v1/salesbigroup/block/{guid}")
    Call<List<BuildingSite>> getBlocksFilter(@Header("Token") String token, @Path("guid") String guid);

}
