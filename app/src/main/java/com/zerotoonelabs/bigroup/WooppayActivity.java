package com.zerotoonelabs.bigroup;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.Library.Retrofit_request.Send_Woopay_history;
import com.zerotoonelabs.bigroup.adapters.HistoryInvoiceAdapter;
import com.zerotoonelabs.bigroup.adapters.WooppayAdapter;
import com.zerotoonelabs.bigroup.api.RestClient;
import com.zerotoonelabs.bigroup.api.RetrofitApi;
import com.zerotoonelabs.bigroup.authui.payment.BrowserActivity;
import com.zerotoonelabs.bigroup.entity.Utility;
import com.zerotoonelabs.bigroup.entity.Wooppay.GetScript;
import com.zerotoonelabs.bigroup.entity.Wooppay.Operation;
import com.zerotoonelabs.bigroup.entity.Wooppay.OperationData;
import com.zerotoonelabs.bigroup.entity.Wooppay.SendCheckRequest;
import com.zerotoonelabs.bigroup.entity.Wooppay.WooppayCheck;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WooppayActivity extends AppCompatActivity {

    String LOG = "WooppayActivity";
    Retrofit retrofitApi = null;
    String token;
    int service_id = 0;
    String account;
    List<Utility> invoiceList = new ArrayList<>();
    RecyclerView recyclerView = null;
    SharedPreferences sharedPreferences;
    ProgressBar loading;
    Button payButton;
    RetrofitApi response;
    String jsonString = "";
    ObjectMapper mapper = new ObjectMapper();
    WooppayAdapter adapter;
    Integer oper_id;

    WooppayCheck wooppayCheck;
    String responsesend;

    private ActionBar actionBar;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(LOG, "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wooppay);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(WooppayActivity.this);
        //token = sharedPreferences.getString("Authorization", null);
        token = Library.woopaytoken;
        Log.i("token","token" + token);
        service_id = sharedPreferences.getInt("service_id", 1883);
        account = sharedPreferences.getString("account", null);

        payButton = findViewById(R.id.payButton);
        loading = findViewById(R.id.loadingProgressBar);
        recyclerView = findViewById(R.id.monthlyRecyclerView);
        loading.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        payButton.setVisibility(View.GONE);
        recyclerView.setLayoutManager(new LinearLayoutManager(WooppayActivity.this));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");

        actionBar = getSupportActionBar();
    }

    private String formatTotalSum(double value){
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();

        symbols.setGroupingSeparator(' ');
        formatter.setDecimalFormatSymbols(symbols);
        return formatter.format(value);
    }

    WooppayAdapter.OnDataChangeListener dataChangeListener = new WooppayAdapter.OnDataChangeListener(){
        public void onDataChanged(boolean trigger){
            if(trigger) {
                payButton.setText(adapter.getTotalSum());
            }
        }
    };

    private String getMonthName(Date date){
        int index = date.getMonth();

        if (index == 0)
            return "Январь";
        if (index == 1)
            return "Февраль";
        if (index == 2)
            return "Март";
        if (index == 3)
            return "Апрель";
        if (index == 4)
            return "Май";
        if (index == 5)
            return "Июнь";
        if (index == 6)
            return "Июль";
        if (index == 7)
            return "Август";
        if (index == 8)
            return "Сентябрь";
        if (index == 9)
            return "Октябрь";
        if (index == 10)
            return "Ноябрь";
        if (index == 11)
            return "Декабрь";
        return "Ноябрь";
    }

    @Override
    protected void onResume() {
        Log.i(LOG, "onResume()");
        super.onResume();

        // Get Wooppaydata
        BiApplication.initializeRetrofitApi(RetrofitApi.urlWooppay).getWooppayData(
                RetrofitApi.contentType,
                token,
                service_id,
                account
        )
                .enqueue(new Callback<WooppayCheck>() {
                    @Override
                    public void onResponse(Call<WooppayCheck> call, Response<WooppayCheck> response) {
                        if (response.body() != null) {
                            adapter = new WooppayAdapter(response.body(), WooppayActivity.this, dataChangeListener);
                            recyclerView.setAdapter(adapter);
                            recyclerView.getAdapter().notifyDataSetChanged();
                            loading.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                            payButton.setVisibility(View.VISIBLE);

                            try {
                                Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(response.body().getInvoices().getFormedDate());
                                actionBar.setTitle(getMonthName(date) + new SimpleDateFormat(" yyyy", Locale.getDefault()).format(date));
                            } catch (Exception e){
                                e.printStackTrace();
                            }



                            // initial payButton text setting
                            payButton.setText(adapter.getTotalSum());
                        }
                    }

                    @Override
                    public void onFailure(Call<WooppayCheck> call, Throwable t) {
                        Toast.makeText(WooppayActivity.this, t.toString(), Toast.LENGTH_LONG).show();
                        loading.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.GONE);
                    }
                });

        String script = "<script type=\"text/javascript\">\n" +
                "    var functionName = 'rechargeReceiver';\n" +
                "    var frameType = 'recharge';\n" +
                "    var errors_text = '{\"e_04\":\"Карта заблокирована. Для снятия ограничений, позвоните в Колл-центр <b>вашего</b> банка.\",\"e_05\":\"Транзакция отклонена. Позвоните в Колл-центр <b>вашего</b> банка.\",\"e_07\":\"Карта заблокирована. Для снятия ограничений, позвоните в Колл-центр <b>вашего</b> банка.\",\"e_12\":\"Недействительная транзакция, перепроверьте введенные данные. В случае повторения ошибки попробуйте позже...\",\"e_14\":\"Недействительный номер карты.\",\"e_15\":\"Недействительный номер карты.\",\"e_19\":\"Ошибка авторизации.\",\"e_30\":\"Переданы неверные данные для оплаты/пополнения. Обратитесь в службу поддержки.\",\"e_36\":\"Карта заблокирована. Для снятия ограничений, позвоните в Колл-центр <b>вашего</b> банка.\",\"e_37\":\"По карте выставлены ограничения. Для снятия ограничений, позвоните в Колл-центр <b>вашего</b> банка.\",\"e_41\":\"Карта, числится в базе утерянных. Позвоните в Колл-центр <b>вашего</b> банка.\",\"e_43\":\"Карта, числится в базе утерянных. Позвоните в Колл-центр <b>вашего</b> банка.\",\"e_45\":\"Карта, числится в базе украденых. Позвоните в Колл-центр <b>вашего</b> банка, либо обратиться в ближайшее отделение полиции.\",\"e_51\":\"Недостаточно средств на карте.\",\"e_54\":\"Истёк срок действия карты.\",\"e_57\":\"Карта закрыта для интернет-транзакций. Обратитесь в <b>ваш</b> банк.\",\"e_58\":\"Операции с картами временно приостановлены. Попробуйте позже.\",\"e_61\":\"Сумма превышает допустимый суточный лимит. Можете обратиться в службу поддержки, либо завершить операцию завтра.\",\"e_62\":\"Карта заблокирована банком. Позвоните в Колл-центр <b>вашего</b> банка.\",\"e_91\":\"Ваш банк временно не доступен. Попробуйте оплатить позже.\",\"e_96\":\"Не установлен 3DSecure(SecureCode) либо сбой связи. Позвоните в Колл-центр <b>вашего</b> банка.\"}';\n" +
                "    var frame_result_load = '4';\n" +
                "    var frame_result_success = '1';\n" +
                "    var frame_result_error = '3';\n" +
                "    var frame_result_authorisation_error = '2';\n" +
                "    var frame_result_form_send = '8';\n" +
                "\n" +
                "    if (typeof functionName != \"function\") {\n" +
                "        functionName = function(event) {\n" +
                "            if (event.data) {\n" +
                "                var message = JSON.parse(event.data);\n" +
                "                if (message && message.source == frameType) {\n" +
                "                    var err_info = \"\";\n" +
                "                    if (message.data && typeof message.data.errorCode != \"undefined\") {\n" +
                "                        var errors_text = errors_text;\n" +
                "                        var err_key = \"e_\" + message.data.errorCode;\n" +
                "                        if (err_key in errors_text) {\n" +
                "                            err_info = errors_text[err_key];\n" +
                "                        }\n" +
                "                    }\n" +
                "                    if (message.status == frame_result_load) {\n" +
                "\n" +
                "                    } else if (message.status == frame_result_success) {\n" +
                "                        alert('success');\n" +
                "                    } else if (message.status == frame_result_error) {\n" +
                "                        if (err_info == \"\") {\n" +
                "                            err_info = \"Произошла ошибка. Скорее всего вы ввели некорректные данные карты\";\n" +
                "                        }\n" +
                "                        value\n" +
                "                    } else if (message.status == frame_result_authorisation_error) {\n" +
                "                        if (err_info == \"\") {\n" +
                "                            err_info = \"Произошла ошибка. Возможно вы ввели некорректные данные карты\";\n" +
                "                        }\n" +
                "                        value\n" +
                "                    } else if (message.status == frame_result_form_send) {\n" +
                "                        value\n" +
                "                    }\n" +
                "                }\n" +
                "            }\n" +
                "        };\n" +
                "        window.addEventListener(\"message\", functionName, false);\n" +
                "    }\n" +
                "</script>";
        sharedPreferences.edit().putString("javascript", script).commit();

//        BiApplication.initializeRetrofitApi(RetrofitApi.urlWooppay).getScript(
//                token,
//                "java_script_example",
//                "java_script_example",
//                "java_script_example",
//                "java_script_example",
//                "java_script_example"
//        ).enqueue(new Callback<GetScript>() {
//            @Override
//            public void onResponse(Call<GetScript> call, Response<GetScript> response) {
//                if (response.body() != null && response.code() == 200) {
//                    sharedPreferences.edit().putString("javascript", response.body().getScript()).commit();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<GetScript> call, Throwable t) {
//                Toast.makeText(WooppayActivity.this, t.toString(), Toast.LENGTH_LONG).show();
//            }
//        });

        // Method for pay action
        payButton.setOnClickListener(view -> {


            loading.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            payButton.setVisibility(View.GONE);

            Log.i(LOG, "payButton()");

            WooppayCheck wooppayCheck = ((WooppayAdapter)recyclerView.getAdapter()).getWooppayCheck();
            AppCompatActivity activity = this;
            if ( !(wooppayCheck.getAmount() > 0)){
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setTitle(" ")
                        .setIcon(getResources().getDrawable(R.drawable.ic_alert2))
                        .setMessage("Укажите сумму оплаты хотя бы для одной услуги")
                        .setPositiveButton("OK", (arg0, arg1) -> {
                            arg0.dismiss();
                            loading.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                            payButton.setVisibility(View.VISIBLE);
                        }).show();
                return;
            }

            try {
                jsonString = mapper.writeValueAsString(wooppayCheck);
                Library.wooppayCheck = wooppayCheck;
                //sharedPreferences.edit().putString("sendjson", jsonString).commit();

            } catch (IOException e) {
                e.printStackTrace();
            }

            //new Download().execute();

            loading.setVisibility(View.VISIBLE);
            //xx
            BiApplication.initializeRetrofitApi(RetrofitApi.urlWooppay).sendWooppayData(token, service_id, jsonString/*wooppayCheck.toString()*/)//
                    .enqueue(new Callback<Operation>() {
                        @Override
                        public void onResponse(Call<Operation> call, Response<Operation> response) {
                            if (response.body() != null && response.code() == 200) {
                                sharedPreferences.edit().putInt("operationId", response.body().getOperationId()).commit();
                                sharedPreferences.edit().putInt("txn_id", response.body().getOperationId()).commit();
                                try {
                                    OperationData op = response.body().getOperation_data();
                                    jsonString = mapper.writeValueAsString(op);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
//zz
                                loading.setVisibility(View.GONE);
                                Intent intent = new Intent(WooppayActivity.this, BrowserActivity.class);
                                intent.putExtra("url", "http://www.bi-group.org/success");
                                intent.putExtra("operationData", jsonString);
                                Library.Phone_memory.edit().putString("operationData", jsonString).commit();
                                //operationId
                                startActivity(intent);
                                activity.finish();
                            } else {
                                loading.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.VISIBLE);
                                payButton.setVisibility(View.VISIBLE);
                                if (response.code() == 500) {
                                    Toast.makeText(WooppayActivity.this, response.message().toString(), Toast.LENGTH_LONG).show();
                                } else
                                    Toast.makeText(WooppayActivity.this, response.message().toString(), Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Operation> call, Throwable t) {
                            loading.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                            payButton.setVisibility(View.VISIBLE);
                            Toast.makeText(WooppayActivity.this, t.toString(), Toast.LENGTH_LONG).show();
                        }
                    });
        });



            //operationId



    }

//zz


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i(LOG, "onOptionsItemSelected()");
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }


    public class Download extends AsyncTask {
        @Override
        protected String doInBackground(Object[] objects) {

            Retrofit client = Library.NewRetrofitclient();

            Send_Woopay_history send_woopay_history = client.create(Send_Woopay_history.class);

            String newtoken = Library.Phone_memory.getString("token", null);

            String sendjson = Library.Phone_memory.getString("sendjson", null);

            Integer txn_id = Library.Phone_memory.getInt("txn_id", 0);

            String propertyGuid = Library.Phone_memory.getString("propertyGuid", null);

            String ApplicationUserId = Library.Phone_memory.getString("applicationUserId", null);

            Integer serv_id = sharedPreferences.getInt("service_id", 1883);

            WooppayCheck wooppayChecking = Library.wooppayCheck;
            wooppayChecking.setTxn_id(txn_id.toString());
            wooppayChecking.setPropertyGuid(propertyGuid);
            wooppayChecking.setApplicationUserId(ApplicationUserId);
            wooppayChecking.setService_id(serv_id);


            Call<Object> objectCall2 = send_woopay_history.getRequest(wooppayChecking,newtoken);

            try {

                Gson gson = new Gson();

                responsesend = gson.toJson(objectCall2.execute().body());

                objectCall2.execute();



            } catch (Exception e) {

            }

            return "not";
        }

        @Override
        protected void onPostExecute(Object o) {

            Log.i("response","response" + responsesend);


            super.onPostExecute(o);
        }
    }



}
