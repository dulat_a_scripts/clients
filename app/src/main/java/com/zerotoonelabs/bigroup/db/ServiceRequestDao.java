package com.zerotoonelabs.bigroup.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.zerotoonelabs.bigroup.entity.ServiceProperty;
import com.zerotoonelabs.bigroup.entity.ServiceRequest;

import java.util.List;

@Dao
public interface ServiceRequestDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(ServiceRequest serviceRequest);

    @Query("SELECT * FROM ServiceRequest WHERE id =:id")
    LiveData<ServiceRequest> getServiceRequest(String id);

    @Query("SELECT * FROM ServiceRequest")
    LiveData<List<ServiceRequest>> getServiceRequests();


    @Query("SELECT * FROM ServiceProperty ORDER BY name ASC")
    LiveData<List<ServiceProperty>> getServiceProperties();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertProperties(List<ServiceProperty> list);
}