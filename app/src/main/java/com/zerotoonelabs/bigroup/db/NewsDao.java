package com.zerotoonelabs.bigroup.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.zerotoonelabs.bigroup.entity.News;

import java.util.List;

@Dao
public abstract class NewsDao implements BaseDao<News> {
    @Query("SELECT * FROM News ORDER BY timestamp DESC")
    public abstract LiveData<List<News>> getData();
}