package com.zerotoonelabs.bigroup.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.zerotoonelabs.bigroup.entity.Flat;
import com.zerotoonelabs.bigroup.entity.Manager;

import java.util.List;

@Dao
public abstract class FlatDao implements BaseDao<Flat> {
    @Query("SELECT * FROM Flat WHERE propertyId =:propertyId")
    public abstract LiveData<Flat> getFlat(int propertyId);

    @Query("SELECT * FROM Flat WHERE contractId =:contractId")
    public abstract LiveData<List<Flat>> getFlats(String contractId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertFlats(List<Flat> flats);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public abstract void updateFlats(List<Flat> flats);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public abstract int updateFlat(Flat flat);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertFlat(Flat flat);

    @Query("SELECT * FROM Manager WHERE realEstateId =:realEstateId AND contractId =:contractId")
    public abstract LiveData<Manager> getManager(String realEstateId, String contractId);

    @Query("SELECT * FROM Manager WHERE bool = 1 AND contractId =:contractId")
    public abstract LiveData<Manager> getManager(String contractId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertManager(Manager manager);
}
