package com.zerotoonelabs.bigroup.db;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.zerotoonelabs.bigroup.entity.Apartments.Apartment;
import com.zerotoonelabs.bigroup.entity.BsDate;
import com.zerotoonelabs.bigroup.entity.BsTime;
import com.zerotoonelabs.bigroup.entity.BuildingSite;
import com.zerotoonelabs.bigroup.entity.Flat;
import com.zerotoonelabs.bigroup.entity.Manager;
import com.zerotoonelabs.bigroup.entity.News;
import com.zerotoonelabs.bigroup.entity.ReBlock;
import com.zerotoonelabs.bigroup.entity.RealEstate;
import com.zerotoonelabs.bigroup.entity.ServiceProperty;
import com.zerotoonelabs.bigroup.entity.ServiceRequest;
import com.zerotoonelabs.bigroup.entity.Stock;
import com.zerotoonelabs.bigroup.entity.VisitNumber;
import com.zerotoonelabs.bigroup.entity.blockvisit.BlockVisit;

@Database(entities = {ServiceRequest.class, Stock.class, RealEstate.class, ReBlock.class,
        BuildingSite.class, BsDate.class, BsTime.class, News.class, Apartment.class,
        BlockVisit.class, ServiceProperty.class, VisitNumber.class, Flat.class, Manager.class}, version = 16,
        exportSchema = false)


public abstract class BiDb extends RoomDatabase {
    abstract public ServiceRequestDao applicationDao();

    abstract public SalesDao salesDao();

    abstract public RealEstateDao realEstateDao();

    abstract public NewsDao newsDao();

    abstract public ApartmentDao apartmentDao();

    abstract public BlockVisitDao blockVisitDao();

    abstract public FlatDao flatDao();

}
