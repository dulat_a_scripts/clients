package com.zerotoonelabs.bigroup.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.zerotoonelabs.bigroup.entity.Stock;

import java.util.List;

@Dao
public abstract class SalesDao implements BaseDao<Stock> {
    /**
     * Get all data from the Data table.
     */
    @Query("SELECT * FROM Stocks WHERE isVisible == 1")
    public abstract LiveData<List<Stock>> getTab1Data();

    @Query("SELECT * FROM Stocks WHERE isVisible == 0")
    public abstract LiveData<List<Stock>> getTab2Data();
}
