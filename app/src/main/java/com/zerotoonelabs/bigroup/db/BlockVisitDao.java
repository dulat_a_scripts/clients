package com.zerotoonelabs.bigroup.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.zerotoonelabs.bigroup.entity.VisitNumber;
import com.zerotoonelabs.bigroup.entity.blockvisit.BlockVisit;

import java.util.List;

/**
 * Created by mac on 08.11.2017.
 */


@Dao
public abstract class BlockVisitDao implements BaseDao<BlockVisit> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(List<BlockVisit> blockVisits);

    @Query("SELECT * FROM blockvisit")
    public abstract LiveData<List<BlockVisit>> getData();

    @Query("SELECT * FROM VisitNumber WHERE blockId =:blockId")
    public abstract LiveData<List<VisitNumber>> getVisitNumbers(String blockId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertVisitNumbers(List<VisitNumber> list);
}