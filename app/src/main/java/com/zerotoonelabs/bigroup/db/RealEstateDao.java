package com.zerotoonelabs.bigroup.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.zerotoonelabs.bigroup.entity.BsDate;
import com.zerotoonelabs.bigroup.entity.BsTime;
import com.zerotoonelabs.bigroup.entity.BuildingSite;
import com.zerotoonelabs.bigroup.entity.ReBlock;
import com.zerotoonelabs.bigroup.entity.RealEstate;

import java.util.List;

@Dao
public abstract class RealEstateDao implements BaseDao<RealEstate> {
    /**
     * Get all data from the Data table.
     */
    @Query("SELECT * FROM RealEstate")
    public abstract LiveData<List<RealEstate>> getData();

    @Query("SELECT * FROM RealEstate WHERE id =:id")
    public abstract RealEstate getRealEstate(String id);

    @Query("SELECT * FROM BuildingSite")
    public abstract LiveData<List<BuildingSite>> getBuildingSites();

    @Query("SELECT * FROM RealEstate WHERE city = :city OR status = :status")
    public abstract LiveData<List<RealEstate>> getData(String city, String status);

    @Query("SELECT * FROM blocks WHERE realEstate_guid = :id ORDER BY `group` ASC")
    public abstract LiveData<List<ReBlock>> getBlocks(String id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertBlocks(List<ReBlock> blocks);

    /*
    * BuildingSite related statements
    */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertBuildingSites(List<BuildingSite> items);

    /*
    * BsDate related queries
    */
    @Query("SELECT * FROM BsDate WHERE bsId = :id")
    public abstract LiveData<BsDate> getBuildingSiteDate(String id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertBsDate(BsDate item);

    /*
    * BsTime related queries
    */
    @Query("SELECT * FROM BsTime WHERE bsId = :id AND date = :date")
    public abstract LiveData<BsTime> getBuildingSiteTimes(String id, String date);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertBsTime(BsTime item);
}