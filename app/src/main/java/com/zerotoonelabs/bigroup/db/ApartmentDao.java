package com.zerotoonelabs.bigroup.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.zerotoonelabs.bigroup.entity.Apartments.Apartment;

import java.util.List;

@Dao
public abstract class ApartmentDao implements BaseDao<Apartment> {

    @Query("SELECT * FROM Apartment")
    public abstract LiveData<List<Apartment>> getApartments();

    @Query("SELECT * FROM Apartment WHERE id =:id")
    public abstract Apartment getApartmentById(String id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract long insertApartment(Apartment apartment);

    @Delete
    public abstract void removeApartment(Apartment apartment);

}