package com.zerotoonelabs.bigroup;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class BroadcastingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_broadcasting);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
