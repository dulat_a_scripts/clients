package com.zerotoonelabs.bigroup.entity.Wooppay;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by zhan on 3/26/18.
 */

public class WooppayCheck {
    @SerializedName("txn_id")
    @Expose
    String txn_id;

    @SerializedName("service_id")
    @Expose
    Integer service_id;



    @SerializedName("ApplicationUserId")
    @Expose
    String ApplicationUserId;



    @SerializedName("PropertyGuid")
    @Expose
    String PropertyGuid;

    @SerializedName("OperationId")
    @Expose
    String OperationId;



    @SerializedName("account")
    @Expose
    String account;
    @SerializedName("address")
    @Expose
    String address;
    @SerializedName("amount")
    @Expose
    double amount;
    @SerializedName("debtInfo")
    @Expose
    List<DebitInfo> debtInfo;

    @SerializedName("commission")
    @Expose
    Double commission;

    @SerializedName("invoices")
    @Expose
    Invoices invoices;

    @SerializedName("privateData")
    @Expose
    PrivateData privateData;

    @Override
    public String toString() {
        return "WooppayCheck{" +
                "txn_id='" + txn_id + '\'' +
                ", service_id=" + service_id +
                ", account='" + account + '\'' +
                ", address='" + address + '\'' +
                ", amount=" + amount +
                ", debtInfo=" + debtInfo +
                ", commission=" + commission +
                ", invoices=" + invoices +
                ", privateData=" + privateData +
                '}';
    }

    public WooppayCheck(String txn_id, Integer service_id, String account, String address, Double amount, List<DebitInfo> debtInfo, Double commission, Invoices invoices) {
        this.txn_id = txn_id;
        this.service_id = service_id;
        this.account = account;
        this.address = address;
        this.amount = amount;
        this.debtInfo = debtInfo;
        this.commission = commission;
        this.invoices = invoices;
    }


    public String getOperationId() {
        return OperationId;
    }

    public void setOperationId(String operationId) {
        OperationId = operationId;
    }

    public String getApplicationUserId() {
        return ApplicationUserId;
    }

    public void setApplicationUserId(String applicationUserId) {
        ApplicationUserId = applicationUserId;
    }

    public String getPropertyGuid() {
        return PropertyGuid;
    }

    public void setPropertyGuid(String propertyGuid) {
        PropertyGuid = propertyGuid;
    }

    public PrivateData getPrivateData() {
        return privateData;
    }

    public void setPrivateData(PrivateData privateData) {
        this.privateData = privateData;
    }

    public Integer getService_id() {
        return service_id;
    }

    public void setService_id(Integer service_id) {
        this.service_id = service_id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Double getCommission() {
        return commission;
    }

    public void setCommission(Double commission) {
        this.commission = commission;
    }

    public Invoices getInvoices() {
        return invoices;
    }

    public void setInvoices(Invoices invoices) {
        this.invoices = invoices;
    }

    public String getTxn_id() {
        return txn_id;
    }

    public void setTxn_id(String txn_id) {
        this.txn_id = txn_id;
    }


    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public List<DebitInfo> getDebtInfo() {
        return debtInfo;
    }

    public void setDebtInfo(List<DebitInfo> debtInfo) {
        this.debtInfo = debtInfo;
    }
}
