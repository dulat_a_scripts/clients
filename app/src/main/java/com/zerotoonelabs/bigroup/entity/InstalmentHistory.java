package com.zerotoonelabs.bigroup.entity;

import android.arch.persistence.room.Entity;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

@Entity
public class InstalmentHistory implements Comparable<InstalmentHistory> {
    @SerializedName("DataPlatezha")
    public String date;
    @SerializedName("SummaVznosa")
    public float amount;
    public String formattedAmount;
    @SerializedName("Details")
    public String details;
    public long timestamp;
    //public String fDate;

    public InstalmentHistory(String date, int amount, String details, long timestamp) {
        this.date = date;
        this.amount = amount;
        this.details = details;
        this.timestamp = timestamp;

        /*try {
            this.fDate = new SimpleDateFormat("dd MMMM yyyy").format(new SimpleDateFormat("dd.MM.yyyy", Locale.US).parse(date));
        } catch (ParseException e){
            e.printStackTrace();
            this.fDate = date;
        }*/
    }

    @Override
    public int compareTo(@NonNull InstalmentHistory o) {
        return compare(this.timestamp, o.timestamp);
    }

    private static int compare(long a, long b) {
        return a < b ? -1
                : 1;
    }
}
