package com.zerotoonelabs.bigroup.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import static android.text.TextUtils.isEmpty;

@Entity
public class Flat {
    @SerializedName("contracter_guid")
    private String contracterGuid;
    @SerializedName("property_id")
    private int propertyId;
    @SerializedName("real_estate")
    private String realEstateName;
    private String address;
    @SerializedName("Builder")
    private String builderName;
    @SerializedName("ContractNumber")
    private String contractNumber;

    @SerializedName("DateKeyDelivery")
    private String dateKeyDelivery;
    @SerializedName("number")
    private String flatNumber;
    @SerializedName("Rooms")
    private int roomsCount;
    @SerializedName("AreaInside")
    private double area1;
    @SerializedName("AreaOutside")
    private double area2;
    @SerializedName("CameraCode")
    private String cameraId;

    @PrimaryKey
    @NonNull
    @SerializedName("propertyGuid")
    private String propertyGuid;

    @SerializedName("ResponsibleGuid")
    private String responsibleGuid;

    @SerializedName("pic_apartment_layout")
    private String realEstateImageUrl;
    @SerializedName("RealEstateGuid")
    private String realEstateGuid;
    @SerializedName("ServiceId")
    private String serviceId;
    @SerializedName("Value")
    private int value;
    @SerializedName("tContractType")
    private int contractType;
    @SerializedName("GarantiyaDo")
    private String warranty;
    @SerializedName("Vizitable")
    private boolean visitable;

    @SerializedName("CheckStatus")
    private boolean checkStatus;

    @SerializedName("type")
    private String type;
    private boolean selected;
    private String contractId;

    @Override
    public String toString() {
        return "Flat{" +
                "contracterGuid='" + contracterGuid + '\'' +
                ", propertyId=" + propertyId +
                ", realEstateName='" + realEstateName + '\'' +
                ", address='" + address + '\'' +
                ", builderName='" + builderName + '\'' +
                ", contractNumber='" + contractNumber + '\'' +
                ", dateKeyDelivery='" + dateKeyDelivery + '\'' +
                ", flatNumber='" + flatNumber + '\'' +
                ", roomsCount=" + roomsCount +
                ", area1=" + area1 +
                ", area2=" + area2 +
                ", cameraId='" + cameraId + '\'' +
                ", propertyGuid='" + propertyGuid + '\'' +
                ", responsibleGuid='" + responsibleGuid + '\'' +
                ", realEstateImageUrl='" + realEstateImageUrl + '\'' +
                ", realEstateGuid='" + realEstateGuid + '\'' +
                ", serviceId='" + serviceId + '\'' +
                ", value=" + value +
                ", contractType=" + contractType +
                ", warranty='" + warranty + '\'' +
                ", visitable=" + visitable +
                ", checkStatus=" + checkStatus +
                ", type='" + type + '\'' +
                ", selected=" + selected +
                ", contractId='" + contractId + '\'' +
                '}';
    }

    public Flat() {

    }

    public boolean isCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(boolean checkStatus) {
        this.checkStatus = checkStatus;
    }

    public String getContracterGuid() {
        return contracterGuid;
    }

    public void setContracterGuid(String contracterGuid) {
        this.contracterGuid = contracterGuid;
    }

    public int getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(int propertyId) {
        this.propertyId = propertyId;
    }

    public String getRealEstateName() {
        return realEstateName;
    }

    public void setRealEstateName(String realEstateName) {
        this.realEstateName = realEstateName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBuilderName() {
        return builderName;
    }

    public void setBuilderName(String builderName) {
        this.builderName = builderName;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getDateKeyDelivery() {
        return dateKeyDelivery;
    }

    public void setDateKeyDelivery(String dateKeyDelivery) {
        this.dateKeyDelivery = dateKeyDelivery;
    }

    public String getFlatNumber() {
        return flatNumber;
    }

    public void setFlatNumber(String flatNumber) {
        this.flatNumber = flatNumber;
    }

    public int getRoomsCount() {
        return roomsCount;
    }

    public void setRoomsCount(int roomsCount) {
        this.roomsCount = roomsCount;
    }

    public double getArea1() {
        return area1;
    }

    public void setArea1(double area1) {
        this.area1 = area1;
    }

    public double getArea2() {
        return area2;
    }

    public void setArea2(double area2) {
        this.area2 = area2;
    }

    public String getCameraId() {
        return cameraId;
    }

    public void setCameraId(String cameraId) {
        this.cameraId = cameraId;
    }

    @NonNull
    public String getPropertyGuid() {
        return propertyGuid;
    }

    public void setPropertyGuid(@NonNull String propertyGuid) {
        this.propertyGuid = propertyGuid;
    }

    public String getResponsibleGuid() {
        return responsibleGuid;
    }

    public void setResponsibleGuid(String responsibleGuid) {
        this.responsibleGuid = responsibleGuid;
    }

    public String getRealEstateImageUrl() {
        return realEstateImageUrl;
    }

    public void setRealEstateImageUrl(String realEstateImageUrl) {
        this.realEstateImageUrl = realEstateImageUrl;
    }

    public String getRealEstateGuid() {
        return realEstateGuid;
    }

    public void setRealEstateGuid(String realEstateGuid) {
        this.realEstateGuid = realEstateGuid;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getContractType() {
        return contractType;
    }

    public void setContractType(int contractType) {
        this.contractType = contractType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getWarranty() {
        return warranty;
    }

    public void setWarranty(String warranty) {
        this.warranty = warranty;
    }

    public boolean isVisitable() {
        return visitable;
    }

    public void setVisitable(boolean visitable) {
        this.visitable = visitable;
    }

    public String getFlatTitle() {
        String title = "";
        if (!isEmpty(realEstateName)) {
            title += realEstateName + " ";
        }
        if (!isEmpty(type)) {
            title += "\n" + type.toLowerCase() + " ";
        }
        if (!isEmpty(flatNumber)) {
            title += flatNumber;
        }
        return title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Flat repoId = (Flat) o;

        return propertyGuid.equals(repoId.propertyGuid);
    }
}
