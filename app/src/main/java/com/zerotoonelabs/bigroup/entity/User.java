package com.zerotoonelabs.bigroup.entity;

/**
 * Created by Daniyar on 11/17/17.
 */


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;

import com.google.gson.annotations.SerializedName;

import static android.text.TextUtils.isEmpty;

public class User implements Parcelable {

    @SerializedName("ApplicationUserId")
    private String applicationUserId;
    @SerializedName("Guid1C")
    private String guid1C;
    @SerializedName("UserName")
    private String userName;
    @SerializedName("LastName")
    private String lastName;
    @SerializedName("FirstName")
    private String firstName;
    @SerializedName("MiddleName")
    private String middleName;
    @SerializedName("Phone")
    private String phone;
    @SerializedName("Email")
    private String email;
    @SerializedName("Iin")
    private String iin;
    @SerializedName("SecretCode")
    private String secretCode;
    @SerializedName("Ava")
    private String ava;
    @SerializedName("BiCardType")
    private String biCardType;
    @SerializedName("Discount")
    private int discount;
    @SerializedName("DiscountOffice")
    private int discountOffice;

    protected User(Parcel in) {
        applicationUserId = in.readString();
        guid1C = in.readString();
        userName = in.readString();
        lastName = in.readString();
        firstName = in.readString();
        middleName = in.readString();
        phone = in.readString();
        email = in.readString();
        iin = in.readString();
        secretCode = in.readString();
        ava = in.readString();
        biCardType = in.readString();
        discount = in.readInt();
        discountOffice = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(applicationUserId);
        dest.writeString(guid1C);
        dest.writeString(userName);
        dest.writeString(lastName);
        dest.writeString(firstName);
        dest.writeString(middleName);
        dest.writeString(phone);
        dest.writeString(email);
        dest.writeString(iin);
        dest.writeString(secretCode);
        dest.writeString(ava);
        dest.writeString(biCardType);
        dest.writeInt(discount);
        dest.writeInt(discountOffice);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getFullName() {
        String result = "";
        if (lastName != null) result += lastName + " ";
        if (firstName != null) result += firstName + " ";
        if (middleName != null) result += middleName;
        return result;
    }

    public String getApplicationUserId() {
        return applicationUserId;
    }

    public void setApplicationUserId(String applicationUserId) {
        this.applicationUserId = applicationUserId;
    }

    public String getGuid1C() {
        return guid1C;
    }

    public void setGuid1C(String guid1C) {
        this.guid1C = guid1C;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIin() {
        return iin;
    }

    public void setIin(String iin) {
        this.iin = iin;
    }

    public String getSecretCode() {
        return secretCode;
    }

    public void setSecretCode(String secretCode) {
        this.secretCode = secretCode;
    }

    public String getAva() {
        return ava;
    }

    public void setAva(String ava) {
        this.ava = ava;
    }

    public String getBiCardType() {
        return biCardType;
    }

    public void setBiCardType(String biCardType) {
        this.biCardType = biCardType;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getDiscountOffice() {
        return discountOffice;
    }

    public void setDiscountOffice(int discountOffice) {
        this.discountOffice = discountOffice;
    }

    public Bitmap getBitmapAva() {
        if (isEmpty(ava)) return null;

        byte[] decodedByte = Base64.decode(ava, Base64.NO_WRAP);
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }
}