
package com.zerotoonelabs.bigroup.entity.statusesPred;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Contract implements Parcelable
{

    @SerializedName("NomerDogovora")
    @Expose
    private String nomerDogovora;
    @SerializedName("SummaDogovora")
    @Expose
    private Float summaDogovora;
    public final static Creator<Contract> CREATOR = new Creator<Contract>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Contract createFromParcel(Parcel in) {
            return new Contract(in);
        }

        public Contract[] newArray(int size) {
            return (new Contract[size]);
        }

    }
    ;

    protected Contract(Parcel in) {
        this.nomerDogovora = ((String) in.readValue((String.class.getClassLoader())));
        this.summaDogovora = ((Float) in.readValue((Float.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public Contract() {
    }

    /**
     * 
     * @param summaDogovora
     * @param nomerDogovora
     */
    public Contract(String nomerDogovora, Float summaDogovora) {
        super();
        this.nomerDogovora = nomerDogovora;
        this.summaDogovora = summaDogovora;
    }

    public String getNomerDogovora() {
        return nomerDogovora;
    }

    public void setNomerDogovora(String nomerDogovora) {
        this.nomerDogovora = nomerDogovora;
    }

    public Float getSummaDogovora() {
        return summaDogovora;
    }

    public void setSummaDogovora(Float summaDogovora) {
        this.summaDogovora = summaDogovora;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(nomerDogovora);
        dest.writeValue(summaDogovora);
    }

    public int describeContents() {
        return  0;
    }

}
