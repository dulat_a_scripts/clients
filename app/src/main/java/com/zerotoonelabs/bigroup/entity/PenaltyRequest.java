package com.zerotoonelabs.bigroup.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import retrofit2.http.Query;

/**
 * Created by AKaltayev on 12.04.2018.
 */

public class PenaltyRequest implements Serializable{

    @SerializedName("contracter_guid")
    private String contracterGuid;
    @SerializedName("dogovor_number")
    private String contractNumber;
    @SerializedName("property_id")
    private String propertyId;

    public String getContracterGuid() {
        return contracterGuid;
    }

    public void setContracterGuid(String contracterGuid) {
        this.contracterGuid = contracterGuid;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }
}
