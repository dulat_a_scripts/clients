package com.zerotoonelabs.bigroup.entity.Wooppay;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by zhan on 3/29/18.
 */

public class Fields {
    @SerializedName("amount")
    @Expose
    Amount amount;
    @SerializedName("account")
    @Expose
    String account;
    @SerializedName("button")
    @Expose
    Boolean button;

    public Amount getAmount() {
        return amount;
    }

    public void setAmount(Amount amount) {
        this.amount = amount;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Boolean getButton() {
        return button;
    }

    public void setButton(Boolean button) {
        this.button = button;
    }
}
