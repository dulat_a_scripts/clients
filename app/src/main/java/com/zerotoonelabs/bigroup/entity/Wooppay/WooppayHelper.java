package com.zerotoonelabs.bigroup.entity.Wooppay;

import com.zerotoonelabs.bigroup.api.RetrofitApi;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by zhan on 3/31/18.
 */

public class WooppayHelper {
    private static final String ENDPOINT = "http://test.com";

    private static OkHttpClient httpClient = new OkHttpClient();
    private static WooppayHelper instance = new WooppayHelper();
    private RetrofitApi service;


    private WooppayHelper() {

        Retrofit retrofit = createAdapter().build();
        service = retrofit.create(RetrofitApi.class);
    }

    public static WooppayHelper getInstance() {
        return instance;
    }

    public static Retrofit.Builder createAdapter() {

//        httpClient.setReadTimeout(60, TimeUnit.SECONDS);
//        httpClient.setConnectTimeout(60, TimeUnit.SECONDS);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.interceptors().add(interceptor);

        return new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create());
    }

//    public Call<List<CategoryModel>> getAllCategory() {
//        return service.getAllCategory();
//    }
}
