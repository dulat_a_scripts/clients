package com.zerotoonelabs.bigroup.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by AKaltayev on 12.04.2018.
 */

public class PenaltyResponse implements Serializable {

    @SerializedName("summfine")
    private double summfine;

    @SerializedName("paymentsfine")
    private PaymentsFineElement[] paymentsfine;

    @SerializedName("graphfine")
    private PaymentsFineElement[] graphfine;

    public PaymentsFineElement[] getGraphfine() {
        return graphfine;
    }

    public void setGraphfine(PaymentsFineElement[] graphfine) {
        this.graphfine = graphfine;
    }

    public double getSummfine() {
        return summfine;
    }

    public void setSummfine(double summfine) {
        this.summfine = summfine;
    }

    public PaymentsFineElement[] getPaymentsfine() {
        return paymentsfine;
    }

    public void setPaymentsfine(PaymentsFineElement[] paymentsfine) {
        this.paymentsfine = paymentsfine;
    }
}
