
package com.zerotoonelabs.bigroup.entity.StatusAuth;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Status4 implements Parcelable
{

    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("Date")
    @Expose
    private String date;
    @SerializedName("Details")
    @Expose
    private Object details;
    public final static Parcelable.Creator<Status4> CREATOR = new Creator<Status4>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Status4 createFromParcel(Parcel in) {
            return new Status4(in);
        }

        public Status4 [] newArray(int size) {
            return (new Status4[size]);
        }

    }
    ;

    protected Status4(Parcel in) {
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.date = ((String) in.readValue((String.class.getClassLoader())));
        this.details = ((Object) in.readValue((Object.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public Status4() {
    }

    /**
     * 
     * @param details
     * @param status
     * @param date
     */
    public Status4(Integer status, String date, Object details) {
        super();
        this.status = status;
        this.date = date;
        this.details = details;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Object getDetails() {
        return details;
    }

    public void setDetails(Object details) {
        this.details = details;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(date);
        dest.writeValue(details);
    }

    public int describeContents() {
        return  0;
    }

}
