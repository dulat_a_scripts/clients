package com.zerotoonelabs.bigroup.entity.club;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by AKaltayev on 05.04.2018.
 */

public class ClientProfile implements Serializable {

    @SerializedName("Discount")
    @Expose
    private BigDecimal Discount;
    @SerializedName("DiscountOffice")
    @Expose
    private BigDecimal DiscountOffice;
    @SerializedName("BiCardType")
    @Expose
    private String BiCardType;

    public BigDecimal getDiscount() {
        return Discount;
    }

    public void setDiscount(BigDecimal discount) {
        Discount = discount;
    }

    public BigDecimal getDiscountOffice() {
        return DiscountOffice;
    }

    public void setDiscountOffice(BigDecimal discountOffice) {
        DiscountOffice = discountOffice;
    }

    public String getBiCardType() {
        return BiCardType;
    }

    public void setBiCardType(String biCardType) {
        BiCardType = biCardType;
    }
}
