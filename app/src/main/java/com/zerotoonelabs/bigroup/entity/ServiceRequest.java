package com.zerotoonelabs.bigroup.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@Entity(primaryKeys = {"id"})
public class ServiceRequest implements Parcelable {
    @SerializedName("fio")
    private String fullName;
    @SerializedName("E-mail")
    private String email;
    private String phone;
    @SerializedName("iin")
    private String uid;
    private String object;
    @SerializedName("number_property")
    private String apartmentNo;

    @Ignore
    @SerializedName("complaint_list")
    private List<Complaint> complaints;

    private String status;
    @NonNull
    @SerializedName("number")
    private String id;
    private String dateTime;

    public ServiceRequest() {

    }

    @Ignore
    public ServiceRequest(String fullName, String email, String phone, String uid, String object, String apartmentNo) {
        this.fullName = fullName;
        this.email = email;
        this.phone = phone;
        this.uid = uid;
        this.object = object;
        this.apartmentNo = apartmentNo;
    }

    @Ignore
    protected ServiceRequest(Parcel in) {
        fullName = in.readString();
        email = in.readString();
        phone = in.readString();
        uid = in.readString();
        object = in.readString();
        apartmentNo = in.readString();
        complaints = in.createTypedArrayList(Complaint.CREATOR);
        status = in.readString();
        id = in.readString();
        dateTime = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(fullName);
        dest.writeString(email);
        dest.writeString(phone);
        dest.writeString(uid);
        dest.writeString(object);
        dest.writeString(apartmentNo);
        dest.writeTypedList(complaints);
        dest.writeString(status);
        dest.writeString(id);
        dest.writeString(dateTime);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ServiceRequest> CREATOR = new Creator<ServiceRequest>() {
        @Override
        public ServiceRequest createFromParcel(Parcel in) {
            return new ServiceRequest(in);
        }

        @Override
        public ServiceRequest[] newArray(int size) {
            return new ServiceRequest[size];
        }
    };

    public boolean isEmpty() {
        return TextUtils.isEmpty(fullName) || TextUtils.isEmpty(email) || TextUtils.isEmpty(phone)
                || TextUtils.isEmpty(uid) || TextUtils.isEmpty(object) || TextUtils.isEmpty(apartmentNo);
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getApartmentNo() {
        return apartmentNo;
    }

    public void setApartmentNo(String apartmentNo) {
        this.apartmentNo = apartmentNo;
    }

    public List<Complaint> getComplaints() {
        return complaints;
    }

    public void setComplaints(List<Complaint> complaints) {
        this.complaints = complaints;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }
}
