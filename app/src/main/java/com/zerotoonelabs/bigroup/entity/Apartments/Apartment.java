
package com.zerotoonelabs.bigroup.entity.Apartments;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

@Entity
public class Apartment implements Parcelable {
    @SerializedName("deadline")
    private String deadline;
    @PrimaryKey
    @NonNull
    @SerializedName("guid")
    private String id;
    private String number;
    private int floor;

    private String formattedTitle;

    @Ignore
    @SerializedName("price")
    private double priceF;

    private long priceL;

    private String formattedPrice;

    @SerializedName("price_m")
    @Ignore
    private double priceMF;
    private long priceM;

    private String formattedPriceM;

    private int entrance;

    private double square;

    @SerializedName("rooms_count")
    private int roomsCount;

    private String layout;

    @SerializedName("pic_apartment_layout")
    private String listImageUrl;

    @SerializedName("ceiling_height")
    private String ceilingHeight;

    @SerializedName("for_sale")
    private boolean forSale;

    private String status;

    @SerializedName("finishing_repair")
    private Boolean finishingRepair;

    @SerializedName("group")
    private int group;

    private String groupName;

    private boolean parking;

    @SerializedName("Put_PlanovEtaja")
    private String imageUrl;

    @Embedded(prefix = "type_")
    private Type type;

    @SerializedName("block")
    @Embedded(prefix = "block_")
    private ApartmentBlock block;


    public boolean favourite = false;

    public Apartment() {
    }

    protected Apartment(Parcel in) {
        deadline = in.readString();
        id = in.readString();
        number = in.readString();
        floor = in.readInt();
        formattedTitle = in.readString();
        priceF = in.readDouble();
        priceL = in.readLong();
        formattedPrice = in.readString();
        priceMF = in.readDouble();
        priceM = in.readLong();
        formattedPriceM = in.readString();
        entrance = in.readInt();
        square = in.readDouble();
        roomsCount = in.readInt();
        layout = in.readString();
        listImageUrl = in.readString();
        ceilingHeight = in.readString();
        forSale = in.readByte() != 0;
        status = in.readString();
        byte tmpFinishingRepair = in.readByte();
        finishingRepair = tmpFinishingRepair == 0 ? null : tmpFinishingRepair == 1;
        group = in.readInt();
        groupName = in.readString();
        parking = in.readByte() != 0;
        imageUrl = in.readString();
        type = in.readParcelable(Type.class.getClassLoader());
        block = in.readParcelable(ApartmentBlock.class.getClassLoader());
        favourite = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(deadline);
        dest.writeString(id);
        dest.writeString(number);
        dest.writeInt(floor);
        dest.writeString(formattedTitle);
        dest.writeDouble(priceF);
        dest.writeLong(priceL);
        dest.writeString(formattedPrice);
        dest.writeDouble(priceMF);
        dest.writeLong(priceM);
        dest.writeString(formattedPriceM);
        dest.writeInt(entrance);
        dest.writeDouble(square);
        dest.writeInt(roomsCount);
        dest.writeString(layout);
        dest.writeString(listImageUrl);
        dest.writeString(ceilingHeight);
        dest.writeByte((byte) (forSale ? 1 : 0));
        dest.writeString(status);
        dest.writeByte((byte) (finishingRepair == null ? 0 : finishingRepair ? 1 : 2));
        dest.writeInt(group);
        dest.writeString(groupName);
        dest.writeByte((byte) (parking ? 1 : 0));
        dest.writeString(imageUrl);
        dest.writeParcelable(type, flags);
        dest.writeParcelable(block, flags);
        dest.writeByte((byte) (favourite ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Apartment> CREATOR = new Creator<Apartment>() {
        @Override
        public Apartment createFromParcel(Parcel in) {
            return new Apartment(in);
        }

        @Override
        public Apartment[] newArray(int size) {
            return new Apartment[size];
        }
    };

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public String getFormattedTitle() {
        return formattedTitle;
    }

    public void setFormattedTitle(String formattedTitle) {
        this.formattedTitle = formattedTitle;
    }

    public double getPriceF() {
        return priceF;
    }

    public void setPriceF(double priceF) {
        this.priceF = priceF;
    }

    public long getPriceL() {
        return priceL;
    }

    public void setPriceL(long price) {
        this.priceL = price;
    }

    public String getFormattedPrice() {
        return formattedPrice;
    }

    public void setFormattedPrice(String formattedPrice) {
        this.formattedPrice = formattedPrice;
    }

    public double getPriceMF() {
        return priceMF;
    }

    public void setPriceMF(double priceMF) {
        this.priceMF = priceMF;
    }

    public long getPriceM() {
        return priceM;
    }

    public void setPriceM(long priceM) {
        this.priceM = priceM;
    }

    public String getFormattedPriceM() {
        return formattedPriceM;
    }

    public void setFormattedPriceM(String formattedPriceM) {
        this.formattedPriceM = formattedPriceM;
    }

    public int getEntrance() {
        return entrance;
    }

    public void setEntrance(int entrance) {
        this.entrance = entrance;
    }

    public double getSquare() {
        return square;
    }

    public void setSquare(double square) {
        this.square = square;
    }

    public int getRoomsCount() {
        return roomsCount;
    }

    public void setRoomsCount(int roomsCount) {
        this.roomsCount = roomsCount;
    }

    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    public String getListImageUrl() {
        return listImageUrl;
    }

    public void setListImageUrl(String listImageUrl) {
        this.listImageUrl = listImageUrl;
    }

    public String getCeilingHeight() {
        return ceilingHeight;
    }

    public void setCeilingHeight(String ceilingHeight) {
        this.ceilingHeight = ceilingHeight;
    }

    public boolean isForSale() {
        return forSale;
    }

    public void setForSale(boolean forSale) {
        this.forSale = forSale;
    }

    public String getStatus() {

        if(status.equals("")){
            status = "Строящиеся";
        }

        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getFinishingRepair() {
        return finishingRepair;
    }

    public void setFinishingRepair(Boolean finishingRepair) {
        this.finishingRepair = finishingRepair;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public boolean isParking() {
        return parking;
    }

    public void setParking(boolean parking) {
        this.parking = parking;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public ApartmentBlock getBlock() {
        return block;
    }

    public void setBlock(ApartmentBlock block) {
        this.block = block;
    }

    public String getTitle() {
        String description;
        String type = getType().name;
        int count = getRoomsCount();
        switch (count) {
            case 1:
                description = "Одно";
                break;
            case 2:
                description = "Двух";
                break;
            case 3:
                description = "Трех";
                break;
            case 4:
                description = "Четырех";
                break;
            case 5:
                description = "Пяти";
                break;
            case 6:
                description = "Шести";
                break;
            case 7:
                description = "Семи";
                break;
            case 8:
                description = "Восьми";
                break;
            case 9:
                description = "Девяти";
                break;
            default:
                description = "Десяти";
                break;
        }
        if (type != null) {
            description += type.equals("flat") ? "комнатная квартира - " : "комнатный офис - ";
        }
        description += getSquare() + " м²";
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Apartment apartment = (Apartment) o;

        if (id != null ? !id.equals(apartment.id) : apartment.id != null) {
            return false;
        }

        return formattedTitle != null ? formattedTitle.equals(apartment.formattedTitle) : apartment.formattedTitle == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (formattedTitle != null ? formattedTitle.hashCode() : 0);
        return result;
    }
}
