package com.zerotoonelabs.bigroup.entity.Wooppay;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by zhan on 3/30/18.
 */

public class FrameForm {
    @SerializedName("frameform")
    @Expose
    String frameform;

    public String getFrameform() {
        return frameform;
    }

    public void setFrameform(String frameform) {
        this.frameform = frameform;
    }
}
