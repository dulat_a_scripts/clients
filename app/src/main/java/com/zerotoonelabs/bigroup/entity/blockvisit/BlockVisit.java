package com.zerotoonelabs.bigroup.entity.blockvisit;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.zerotoonelabs.bigroup.entity.RealEstate;

@Entity(tableName = "blockvisit")
public class BlockVisit implements Parcelable {
    @PrimaryKey
    @NonNull
    @SerializedName("guid")
    private String guid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("logo_web")
    @Expose
    private String logoWeb;
    @SerializedName("start_sale_date")
    @Expose
    private String startSaleDate;
    @SerializedName("floors")
    @Expose
    private Integer floors;
    @SerializedName("deadline")
    @Expose
    private String deadline;
    @SerializedName("pic_apartment_sheets")
    @Expose
    private String picApartmentSheets;
    @SerializedName("pic_office_sheets")
    @Expose
    private String picOfficeSheets;
    @SerializedName("pic_storage_sheets")
    @Expose
    private String picStorageSheets;
    @SerializedName("pic_cap_sheets")
    @Expose
    private String picCapSheets;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("happiness_manager")
    @Expose
    private String happinessManager;
    @SerializedName("happiness_manager_tel")
    @Expose
    private String happinessManagerTel;
    @SerializedName("visit_date")
    @Expose
    private String visitDate;
    @SerializedName("visit_date_2")
    @Expose
    private String visitDate2;
    @SerializedName("doc_date")
    @Expose
    private String docDate;
    @SerializedName("parking")
    @Expose
    private Boolean parking;
    @SerializedName("commissioned")
    @Expose
    private Boolean commissioned;
    @SerializedName("finish_contract")
    @Expose
    private Boolean finishContract;
    @SerializedName("StateCommission")
    @Expose
    private String stateCommission;
    @SerializedName("SMR_deadline_Plan")
    @Expose
    private String sMRDeadlinePlan;
    @SerializedName("SMR_deadline_Fact")
    @Expose
    private String sMRDeadlineFact;
    @SerializedName("group")
    @Expose
    private Integer group;
    @SerializedName("key_handover")
    @Expose
    private Boolean keyHandover;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("groupName")
    @Expose
    private String groupName;
    @SerializedName("real_estate")
    @Expose
    @Embedded(prefix = "real_estate_")
    private RealEstate realEstate;
    @SerializedName("Put_FotoVeb")
    @Expose
    private String putFotoVeb;
    public final static Parcelable.Creator<BlockVisit> CREATOR = new Creator<BlockVisit>() {


        @SuppressWarnings({
                "unchecked"
        })
        public BlockVisit createFromParcel(Parcel in) {
            return new BlockVisit(in);
        }

        public BlockVisit[] newArray(int size) {
            return (new BlockVisit[size]);
        }

    };

    protected BlockVisit(Parcel in) {
        this.guid = ((String) in.readValue((String.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.logo = ((String) in.readValue((String.class.getClassLoader())));
        this.logoWeb = ((String) in.readValue((String.class.getClassLoader())));
        this.startSaleDate = ((String) in.readValue((String.class.getClassLoader())));
        this.floors = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.deadline = ((String) in.readValue((String.class.getClassLoader())));
        this.picApartmentSheets = ((String) in.readValue((String.class.getClassLoader())));
        this.picOfficeSheets = ((String) in.readValue((String.class.getClassLoader())));
        this.picStorageSheets = ((String) in.readValue((String.class.getClassLoader())));
        this.picCapSheets = ((String) in.readValue((String.class.getClassLoader())));
        this.address = ((String) in.readValue((String.class.getClassLoader())));
        this.happinessManager = ((String) in.readValue((String.class.getClassLoader())));
        this.happinessManagerTel = ((String) in.readValue((String.class.getClassLoader())));
        this.visitDate = ((String) in.readValue((String.class.getClassLoader())));
        this.visitDate2 = ((String) in.readValue((String.class.getClassLoader())));
        this.docDate = ((String) in.readValue((String.class.getClassLoader())));
        this.parking = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.commissioned = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.finishContract = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.stateCommission = ((String) in.readValue((String.class.getClassLoader())));
        this.sMRDeadlinePlan = ((String) in.readValue((String.class.getClassLoader())));
        this.sMRDeadlineFact = ((String) in.readValue((String.class.getClassLoader())));
        this.group = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.keyHandover = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        this.groupName = ((String) in.readValue((String.class.getClassLoader())));
        this.realEstate = ((RealEstate) in.readValue((RealEstate.class.getClassLoader())));
        this.putFotoVeb = ((String) in.readValue((String.class.getClassLoader())));
    }

    public BlockVisit() {
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getLogoWeb() {
        return logoWeb;
    }

    public void setLogoWeb(String logoWeb) {
        this.logoWeb = logoWeb;
    }

    public String getStartSaleDate() {
        return startSaleDate;
    }

    public void setStartSaleDate(String startSaleDate) {
        this.startSaleDate = startSaleDate;
    }

    public Integer getFloors() {
        return floors;
    }

    public void setFloors(Integer floors) {
        this.floors = floors;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public String getPicApartmentSheets() {
        return picApartmentSheets;
    }

    public void setPicApartmentSheets(String picApartmentSheets) {
        this.picApartmentSheets = picApartmentSheets;
    }

    public String getPicOfficeSheets() {
        return picOfficeSheets;
    }

    public void setPicOfficeSheets(String picOfficeSheets) {
        this.picOfficeSheets = picOfficeSheets;
    }

    public String getPicStorageSheets() {
        return picStorageSheets;
    }

    public void setPicStorageSheets(String picStorageSheets) {
        this.picStorageSheets = picStorageSheets;
    }

    public String getPicCapSheets() {
        return picCapSheets;
    }

    public void setPicCapSheets(String picCapSheets) {
        this.picCapSheets = picCapSheets;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getHappinessManager() {
        return happinessManager;
    }

    public void setHappinessManager(String happinessManager) {
        this.happinessManager = happinessManager;
    }

    public String getHappinessManagerTel() {
        return happinessManagerTel;
    }

    public void setHappinessManagerTel(String happinessManagerTel) {
        this.happinessManagerTel = happinessManagerTel;
    }

    public String getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(String visitDate) {
        this.visitDate = visitDate;
    }

    public String getVisitDate2() {
        return visitDate2;
    }

    public void setVisitDate2(String visitDate2) {
        this.visitDate2 = visitDate2;
    }

    public String getDocDate() {
        return docDate;
    }

    public void setDocDate(String docDate) {
        this.docDate = docDate;
    }

    public Boolean getParking() {
        return parking;
    }

    public void setParking(Boolean parking) {
        this.parking = parking;
    }

    public Boolean getCommissioned() {
        return commissioned;
    }

    public void setCommissioned(Boolean commissioned) {
        this.commissioned = commissioned;
    }

    public Boolean getFinishContract() {
        return finishContract;
    }

    public void setFinishContract(Boolean finishContract) {
        this.finishContract = finishContract;
    }

    public String getStateCommission() {
        return stateCommission;
    }

    public void setStateCommission(String stateCommission) {
        this.stateCommission = stateCommission;
    }

    public String getSMRDeadlinePlan() {
        return sMRDeadlinePlan;
    }

    public void setSMRDeadlinePlan(String sMRDeadlinePlan) {
        this.sMRDeadlinePlan = sMRDeadlinePlan;
    }

    public String getSMRDeadlineFact() {
        return sMRDeadlineFact;
    }

    public void setSMRDeadlineFact(String sMRDeadlineFact) {
        this.sMRDeadlineFact = sMRDeadlineFact;
    }

    public Integer getGroup() {
        return group;
    }

    public void setGroup(Integer group) {
        this.group = group;
    }

    public Boolean getKeyHandover() {
        return keyHandover;
    }

    public void setKeyHandover(Boolean keyHandover) {
        this.keyHandover = keyHandover;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public RealEstate getRealEstate() {
        return realEstate;
    }

    public void setRealEstate(RealEstate realEstate) {
        this.realEstate = realEstate;
    }

    public String getPutFotoVeb() {
        return putFotoVeb;
    }

    public void setPutFotoVeb(String putFotoVeb) {
        this.putFotoVeb = putFotoVeb;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(guid);
        dest.writeValue(name);
        dest.writeValue(logo);
        dest.writeValue(logoWeb);
        dest.writeValue(startSaleDate);
        dest.writeValue(floors);
        dest.writeValue(deadline);
        dest.writeValue(picApartmentSheets);
        dest.writeValue(picOfficeSheets);
        dest.writeValue(picStorageSheets);
        dest.writeValue(picCapSheets);
        dest.writeValue(address);
        dest.writeValue(happinessManager);
        dest.writeValue(happinessManagerTel);
        dest.writeValue(visitDate);
        dest.writeValue(visitDate2);
        dest.writeValue(docDate);
        dest.writeValue(parking);
        dest.writeValue(commissioned);
        dest.writeValue(finishContract);
        dest.writeValue(stateCommission);
        dest.writeValue(sMRDeadlinePlan);
        dest.writeValue(sMRDeadlineFact);
        dest.writeValue(group);
        dest.writeValue(keyHandover);
        dest.writeValue(status);
        dest.writeValue(groupName);
        dest.writeValue(realEstate);
        dest.writeValue(putFotoVeb);
    }

    public int describeContents() {
        return 0;
    }

}