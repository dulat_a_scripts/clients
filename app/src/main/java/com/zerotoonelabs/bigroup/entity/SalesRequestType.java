package com.zerotoonelabs.bigroup.entity;

import com.google.gson.annotations.SerializedName;

public class SalesRequestType {
    @SerializedName("id")
    public int id;
    @SerializedName("Guid")
    public String guid;
    @SerializedName("Title")
    public String title;
    @SerializedName("TitleKz")
    public String titleKk;

    public SalesRequestType(int id, String guid, String title, String titleKk) {
        this.id = id;
        this.guid = guid;
        this.title = title;
        this.titleKk = titleKk;
    }

    @Override
    public String toString() {
        return title;
    }
}
