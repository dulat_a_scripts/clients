package com.zerotoonelabs.bigroup.entity;

public class Constants {

    /**
     * Тестовый publicId для подключения, его вам нужно получить в личном кабинете на сайте cloudpayments.ru
     */
    public static final String publicId = "pk_7ed64898ee108664f973446e22ff4";

    public static final String currency = "USD";
    public static final String invoiceId = "testInvoiceId";
    public static final String accountId = "madim@example.com";
}
