package com.zerotoonelabs.bigroup.entity.Wooppay;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by zhan on 3/29/18.
 */

public class Amount {
    @SerializedName("id_7")
    @Expose
    Double id_7;
    @SerializedName("id_8")
    @Expose
    Double id_8;
    @SerializedName("id_13")
    @Expose
    Double id_13;

    public Double getId_7() {
        return id_7;
    }

    public void setId_7(Double id_7) {
        this.id_7 = id_7;
    }

    public Double getId_8() {
        return id_8;
    }

    public void setId_8(Double id_8) {
        this.id_8 = id_8;
    }

    public Double getId_13() {
        return id_13;
    }

    public void setId_13(Double id_13) {
        this.id_13 = id_13;
    }
}
