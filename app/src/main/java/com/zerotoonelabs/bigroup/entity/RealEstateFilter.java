package com.zerotoonelabs.bigroup.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class RealEstateFilter implements Parcelable {
    private String city;
    private int status;

    public RealEstateFilter() {
    }

    protected RealEstateFilter(Parcel in) {
        city = in.readString();
        status = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(city);
        dest.writeInt(status);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RealEstateFilter> CREATOR = new Creator<RealEstateFilter>() {
        @Override
        public RealEstateFilter createFromParcel(Parcel in) {
            return new RealEstateFilter(in);
        }

        @Override
        public RealEstateFilter[] newArray(int size) {
            return new RealEstateFilter[size];
        }
    };

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
