
package com.zerotoonelabs.bigroup.entity.Apartments;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Type implements Parcelable {
    public String name;

    @SerializedName("is_living")
    public boolean living;

    public Type(String name, boolean living) {
        this.name = name;
        this.living = living;
    }

    protected Type(Parcel in) {
        name = in.readString();
        living = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeByte((byte) (living ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Type> CREATOR = new Creator<Type>() {
        @Override
        public Type createFromParcel(Parcel in) {
            return new Type(in);
        }

        @Override
        public Type[] newArray(int size) {
            return new Type[size];
        }
    };
}
