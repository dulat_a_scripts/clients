package com.zerotoonelabs.bigroup.entity;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import com.zerotoonelabs.bigroup.BR;

public class ApartmentFilter extends BaseObservable implements Parcelable {
    private String city;
    private BuildingSite object;
    private BuildingSite block;

    private boolean flat;
    private boolean office;

    private boolean room1;
    private boolean room2;
    private boolean room3;
    private boolean room4;
    private boolean room5;
    private boolean room6;

    private int minPrice = 4000000;
    private int maxPrice = 250000000;
    private int minArea = 0;
    private int maxArea = 300;
    private int minFloor = 1;
    private int maxFloor = 24;

    private boolean deadline1;
    private boolean deadline2;
    private boolean deadline3;
    private boolean deadline4;
    private boolean deadline5;
    private boolean deadline6;

    private boolean sortByPriceAsc = true;
    private boolean sortByAreaAsc = true;

    public ApartmentFilter() {

    }

    protected ApartmentFilter(Parcel in) {
        city = in.readString();
        object = in.readParcelable(BuildingSite.class.getClassLoader());
        block = in.readParcelable(BuildingSite.class.getClassLoader());
        flat = in.readByte() != 0;
        office = in.readByte() != 0;
        room1 = in.readByte() != 0;
        room2 = in.readByte() != 0;
        room3 = in.readByte() != 0;
        room4 = in.readByte() != 0;
        room5 = in.readByte() != 0;
        room6 = in.readByte() != 0;
        minPrice = in.readInt();
        maxPrice = in.readInt();
        minArea = in.readInt();
        maxArea = in.readInt();
        minFloor = in.readInt();
        maxFloor = in.readInt();
        deadline1 = in.readByte() != 0;
        deadline2 = in.readByte() != 0;
        deadline3 = in.readByte() != 0;
        deadline4 = in.readByte() != 0;
        deadline5 = in.readByte() != 0;
        deadline6 = in.readByte() != 0;
        sortByPriceAsc = in.readByte() != 0;
        sortByAreaAsc = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(city);
        dest.writeParcelable(object, flags);
        dest.writeParcelable(block, flags);
        dest.writeByte((byte) (flat ? 1 : 0));
        dest.writeByte((byte) (office ? 1 : 0));
        dest.writeByte((byte) (room1 ? 1 : 0));
        dest.writeByte((byte) (room2 ? 1 : 0));
        dest.writeByte((byte) (room3 ? 1 : 0));
        dest.writeByte((byte) (room4 ? 1 : 0));
        dest.writeByte((byte) (room5 ? 1 : 0));
        dest.writeByte((byte) (room6 ? 1 : 0));
        dest.writeInt(minPrice);
        dest.writeInt(maxPrice);
        dest.writeInt(minArea);
        dest.writeInt(maxArea);
        dest.writeInt(minFloor);
        dest.writeInt(maxFloor);
        dest.writeByte((byte) (deadline1 ? 1 : 0));
        dest.writeByte((byte) (deadline2 ? 1 : 0));
        dest.writeByte((byte) (deadline3 ? 1 : 0));
        dest.writeByte((byte) (deadline4 ? 1 : 0));
        dest.writeByte((byte) (deadline5 ? 1 : 0));
        dest.writeByte((byte) (deadline6 ? 1 : 0));
        dest.writeByte((byte) (sortByPriceAsc ? 1 : 0));
        dest.writeByte((byte) (sortByAreaAsc ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ApartmentFilter> CREATOR = new Creator<ApartmentFilter>() {
        @Override
        public ApartmentFilter createFromParcel(Parcel in) {
            return new ApartmentFilter(in);
        }

        @Override
        public ApartmentFilter[] newArray(int size) {
            return new ApartmentFilter[size];
        }
    };

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public BuildingSite getObject() {
        return object;
    }

    public void setObject(BuildingSite object) {
        this.object = object;
    }

    public BuildingSite getBlock() {
        return block;
    }

    public void setBlock(BuildingSite block) {
        this.block = block;
    }

    @Bindable
    public boolean isFlat() {
        return flat;
    }

    public void setFlat(boolean flat) {
        this.flat = flat;
        notifyPropertyChanged(BR.flat);
    }

    @Bindable
    public boolean isOffice() {
        return office;
    }

    public void setOffice(boolean office) {
        this.office = office;
        notifyPropertyChanged(BR.office);
    }

    @Bindable
    public boolean isRoom1() {
        return room1;
    }

    public void setRoom1(boolean room1) {
        this.room1 = room1;
        notifyPropertyChanged(BR.room1);
    }

    @Bindable
    public boolean isRoom2() {
        return room2;
    }

    public void setRoom2(boolean room2) {
        this.room2 = room2;
        notifyPropertyChanged(BR.room2);
    }

    @Bindable
    public boolean isRoom3() {
        return room3;
    }

    public void setRoom3(boolean room3) {
        this.room3 = room3;
        notifyPropertyChanged(BR.room3);
    }

    @Bindable
    public boolean isRoom4() {
        return room4;
    }

    public void setRoom4(boolean room4) {
        this.room4 = room4;
        notifyPropertyChanged(BR.room4);
    }

    @Bindable
    public boolean isRoom5() {
        return room5;
    }

    public void setRoom5(boolean room5) {
        this.room5 = room5;
        notifyPropertyChanged(BR.room5);
    }

    @Bindable
    public boolean isRoom6() {
        return room6;
    }

    public void setRoom6(boolean room6) {
        this.room6 = room6;
        notifyPropertyChanged(BR.room6);
    }

    @Bindable
    public int getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(int minPrice) {
        this.minPrice = minPrice;
        notifyPropertyChanged(BR.minPrice);
    }

    @Bindable
    public int getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(int maxPrice) {
        this.maxPrice = maxPrice;
        notifyPropertyChanged(BR.maxPrice);
    }

    @Bindable
    public int getMinArea() {
        return minArea;
    }

    public void setMinArea(int minArea) {
        this.minArea = minArea;
        notifyPropertyChanged(BR.minArea);
    }

    @Bindable
    public int getMaxArea() {
        return maxArea;
    }

    public void setMaxArea(int maxArea) {
        this.maxArea = maxArea;
        notifyPropertyChanged(BR.maxArea);
    }

    @Bindable
    public int getMinFloor() {
        return minFloor;
    }

    public void setMinFloor(int minFloor) {
        this.minFloor = minFloor;
        notifyPropertyChanged(BR.minFloor);
    }

    @Bindable
    public int getMaxFloor() {
        return maxFloor;
    }

    public void setMaxFloor(int maxFloor) {
        this.maxFloor = maxFloor;
        notifyPropertyChanged(BR.maxFloor);
    }

    @Bindable
    public boolean isDeadline1() {
        return deadline1;
    }

    public void setDeadline1(boolean deadline1) {
        this.deadline1 = deadline1;
        notifyPropertyChanged(BR.deadline1);
    }

    @Bindable
    public boolean isDeadline2() {
        return deadline2;
    }

    public void setDeadline2(boolean deadline2) {
        this.deadline2 = deadline2;
        notifyPropertyChanged(BR.deadline2);
    }

    @Bindable
    public boolean isDeadline3() {
        return deadline3;
    }

    public void setDeadline3(boolean deadline3) {
        this.deadline3 = deadline3;
        notifyPropertyChanged(BR.deadline3);
    }

    @Bindable
    public boolean isDeadline4() {
        return deadline4;
    }

    public void setDeadline4(boolean deadline4) {
        this.deadline4 = deadline4;
        notifyPropertyChanged(BR.deadline4);
    }

    @Bindable
    public boolean isDeadline5() {
        return deadline5;
    }

    public void setDeadline5(boolean deadline5) {
        this.deadline5 = deadline5;
        notifyPropertyChanged(BR.deadline5);
    }

    @Bindable
    public boolean isDeadline6() {
        return deadline6;
    }

    public void setDeadline6(boolean deadline6) {
        this.deadline6 = deadline6;
        notifyPropertyChanged(BR.deadline6);
    }

    @Bindable
    public boolean isSortByPriceAsc() {
        return sortByPriceAsc;
    }

    public void setSortByPriceAsc(boolean sortByPriceAsc) {
        this.sortByPriceAsc = sortByPriceAsc;
        notifyPropertyChanged(BR.sortByPriceAsc);
    }

    @Bindable
    public boolean isSortByAreaAsc() {
        return sortByAreaAsc;
    }

    public void setSortByAreaAsc(boolean sortByAreaAsc) {
        this.sortByAreaAsc = sortByAreaAsc;
        notifyPropertyChanged(BR.sortByAreaAsc);
    }
}
