package com.zerotoonelabs.bigroup.entity;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class PaymentHistory implements Comparable<PaymentHistory>{

    @SerializedName("Id")
    private int id;
    @SerializedName("Number")
    private String number;
    @SerializedName("CalculationDate")
    private String calculationDate;
    @SerializedName("CalculationSum")
    private double calculationSum;
    @SerializedName("PaymentSum")
    private double paymentSum;
    @SerializedName("CalculationState")
    private int calculationState;
    @SerializedName("BankingAccountId")
    private int bankingAccountId;
    @SerializedName("CalculationDueDate")
    private String calculationDueDate;
    @SerializedName("BankingAccountType")
    private String bankingAccountType;
    @SerializedName("ProcessingStatus")
    private int processingStatus;
    @SerializedName("PaymentStatus")
    private int paymentStatus;
    @SerializedName("IsFinal")
    private boolean isFinal;
    @SerializedName("NumTransaction")
    private String numTransaction;
    @SerializedName("TimeStamp")
    private String timeStamp;
    @SerializedName("OrderId")
    private String orderId;
    @SerializedName("FormUrl")
    private String formUrl;

    @Override
    public int compareTo(@NonNull PaymentHistory o) {
        if (this.getId() > o.getId())
            return -1;
        else if (this.getId() < o.getId())
            return 1;
        else
            return 0;
    }

    public PaymentHistory() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCalculationDate() {
        return calculationDate;
    }

    public void setCalculationDate(String calculationDate) {
        this.calculationDate = calculationDate;
    }

    public double getCalculationSum() {
        return calculationSum;
    }

    public void setCalculationSum(double calculationSum) {
        this.calculationSum = calculationSum;
    }

    public double getPaymentSum() {
        return paymentSum;
    }

    public void setPaymentSum(double paymentSum) {
        this.paymentSum = paymentSum;
    }

    public int getCalculationState() {
        return calculationState;
    }

    public void setCalculationState(int calculationState) {
        this.calculationState = calculationState;
    }

    public int getBankingAccountId() {
        return bankingAccountId;
    }

    public void setBankingAccountId(int bankingAccountId) {
        this.bankingAccountId = bankingAccountId;
    }

    public String getCalculationDueDate() {
        return calculationDueDate;
    }

    public void setCalculationDueDate(String calculationDueDate) {
        this.calculationDueDate = calculationDueDate;
    }

    public String getBankingAccountType() {
        return bankingAccountType;
    }

    public void setBankingAccountType(String bankingAccountType) {
        this.bankingAccountType = bankingAccountType;
    }

    public int getProcessingStatus() {
        return processingStatus;
    }

    public void setProcessingStatus(int processingStatus) {
        this.processingStatus = processingStatus;
    }

    public int getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(int paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public boolean isFinal() {
        return isFinal;
    }

    public void setFinal(boolean aFinal) {
        isFinal = aFinal;
    }

    public String getNumTransaction() {
        return numTransaction;
    }

    public void setNumTransaction(String numTransaction) {
        this.numTransaction = numTransaction;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getFormUrl() {
        return formUrl;
    }

    public void setFormUrl(String formUrl) {
        this.formUrl = formUrl;
    }
}
