package com.zerotoonelabs.bigroup.entity;

import com.google.gson.annotations.SerializedName;

public class Utility {

    @SerializedName("Id")
    private int id;
    @SerializedName("Name")
    private String name;
    @SerializedName("BeginningOfMonthTotal")
    private double beginningOfMonthTotal;
    @SerializedName("EndOfMonthTotal")
    private double endOfMonthTotal;
    @SerializedName("Amount")
    private double amount;
    @SerializedName("PreviousMonthDebtSum")
    private double previousMonthDebtSum;
    @SerializedName("CurrentMonthDebtSum")
    private double currentMonthDebtSum;
    @SerializedName("UnitOfMeasurement")
    private String unitOfMeasurement;
    @SerializedName("PricePerUnit")
    private double pricePerUnit;
    @SerializedName("PaidAmount")
    private double paidAmount;
    @SerializedName("Discount")
    private double discount;
    @SerializedName("Fine")
    private double fine;
    @SerializedName("ToPay")
    private double toPay;
    @SerializedName("Payable")
    private double payable;
    @SerializedName("InvoiceId")
    private int invoiceId;
    @SerializedName("ServiceId")
    private int serviceId;
    @SerializedName("IsCounterService")
    private boolean isCounterService;

    private int totalSum;

    public Utility() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBeginningOfMonthTotal() {
        return beginningOfMonthTotal;
    }

    public void setBeginningOfMonthTotal(int beginningOfMonthTotal) {
        this.beginningOfMonthTotal = beginningOfMonthTotal;
    }

    public double getEndOfMonthTotal() {
        return endOfMonthTotal;
    }

    public void setEndOfMonthTotal(int endOfMonthTotal) {
        this.endOfMonthTotal = endOfMonthTotal;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getPreviousMonthDebtSum() {
        return previousMonthDebtSum;
    }

    public void setPreviousMonthDebtSum(int previousMonthDebtSum) {
        this.previousMonthDebtSum = previousMonthDebtSum;
    }

    public double getCurrentMonthDebtSum() {
        return currentMonthDebtSum;
    }

    public void setCurrentMonthDebtSum(int currentMonthDebtSum) {
        this.currentMonthDebtSum = currentMonthDebtSum;
    }

    public String getUnitOfMeasurement() {
        return unitOfMeasurement;
    }

    public void setUnitOfMeasurement(String unitOfMeasurement) {
        this.unitOfMeasurement = unitOfMeasurement;
    }

    public double getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(double pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    public double getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(double paidAmount) {
        this.paidAmount = paidAmount;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public double getFine() {
        return fine;
    }

    public void setFine(int fine) {
        this.fine = fine;
    }

    public double getToPay() {
        return toPay;
    }

    public void setToPay(int toPay) {
        this.toPay = toPay;
    }

    public double getPayable() {
        return payable;
    }

    public void setPayable(double payable) {
        this.payable = payable;
    }

    public int getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(int invoiceId) {
        this.invoiceId = invoiceId;
    }

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    public boolean isCounterService() {
        return isCounterService;
    }

    public void setCounterService(boolean counterService) {
        isCounterService = counterService;
    }

    public int getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(int totalSum) {
        this.totalSum = totalSum;
    }
}