package com.zerotoonelabs.bigroup.entity;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class Instalment implements Comparable<Instalment> {
    @SerializedName("Data")
    public String date;
    @SerializedName("SummaVznosa")
    public int amount;
    public String formattedAmount;
    @SerializedName("Etap")
    public String stage;
    public long timestamp;

    //public String fDate;

    public Instalment(String date, int amount, String stage, long timestamp) {
        this.date = date;
        this.amount = amount;
        this.stage = stage;
        this.timestamp = timestamp;

       /* try {
            this.fDate = new SimpleDateFormat("dd MMMM yyyy").format(new SimpleDateFormat("dd.MM.yyyy", Locale.US).parse(date));
        } catch (ParseException e){
            e.printStackTrace();
            this.fDate = date;
        }*/
    }

    @Override
    public int compareTo(@NonNull Instalment o) {
        return compare(this.timestamp, o.timestamp);
    }

    private static int compare(long a, long b) {
        return a < b ? -1
                : 1;
    }
}
