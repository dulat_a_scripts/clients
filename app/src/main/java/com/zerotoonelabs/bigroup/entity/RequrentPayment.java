package com.zerotoonelabs.bigroup.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by AKaltayev on 13.04.2018.
 */

public class RequrentPayment implements Serializable {

    @SerializedName("Id")
    private String id;
    @SerializedName("AccountId")
    private String accountId;
    @SerializedName("Description")
    private String description;
    @SerializedName("Email")
    private String email;
    @SerializedName("Amount")
    private double amount;
    @SerializedName("Currency")
    private String currency;
    @SerializedName("Interval")
    private String interval;
    @SerializedName("Period")
    private int period;
    @SerializedName("Status")
    private String status;
    @SerializedName("NextTransactionDateIso")
    private String nextTransactionDate;
    @SerializedName("SuccessfulTransactionsNumber")
    private int successfulTransactionsNumber;

    @Override
    public String toString() {
        return "RequrentPayment{" +
                "id='" + id + '\'' +
                ", accountId='" + accountId + '\'' +
                ", description='" + description + '\'' +
                ", email='" + email + '\'' +
                ", amount=" + amount +
                ", currency='" + currency + '\'' +
                ", interval='" + interval + '\'' +
                ", period=" + period +
                ", status='" + status + '\'' +
                ", nextTransactionDate='" + nextTransactionDate + '\'' +
                ", successfulTransactionsNumber=" + successfulTransactionsNumber +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getInterval() {
        return interval;
    }

    public void setInterval(String interval) {
        this.interval = interval;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNextTransactionDate() {
        return nextTransactionDate;
    }

    public void setNextTransactionDate(String nextTransactionDate) {
        this.nextTransactionDate = nextTransactionDate;
    }

    public int getSuccessfulTransactionsNumber() {
        return successfulTransactionsNumber;
    }

    public void setSuccessfulTransactionsNumber(int successfulTransactionsNumber) {
        this.successfulTransactionsNumber = successfulTransactionsNumber;
    }
}
