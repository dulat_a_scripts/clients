package com.zerotoonelabs.bigroup.entity.Wooppay;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by zhan on 3/29/18.
 */

public class OperationData {
    @SerializedName("paymentInfo")
    @Expose
    PaymentInfo paymentInfo;
    @SerializedName("amount")
    @Expose
    String amount;

    public PaymentInfo getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(PaymentInfo paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
