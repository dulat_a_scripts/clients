package com.zerotoonelabs.bigroup.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by AKaltayev on 13.04.2018.
 */

public class CPModel implements Serializable {

    @SerializedName("Model")
    private RequrentPayment[] model;

    public RequrentPayment[] getModel() {
        return model;
    }

    public void setModel(RequrentPayment[] model) {
        this.model = model;
    }
}
