package com.zerotoonelabs.bigroup.entity;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "blocks")
public class ReBlock {

    @NonNull
    @PrimaryKey
    @SerializedName("guid")
    public String blockId;
    public String name;
    public int group;
    @SerializedName("start_sale_date")
    public String saleDate;

    @SerializedName("floors")
    public int floorsCount;

    @SerializedName("parking")
    public boolean hasParking;

    public String deadline;

    @SerializedName("StateComission")
    public String stateComission;
    public String logo;

    @SerializedName("logo_web")
    public String logoWeb;

    @SerializedName("pic_apartment_sheets")
    public String apartmentSheets;

    @SerializedName("pic_office_sheets")
    public String officeSheets;

    @SerializedName("pic_storage_sheets")
    public String storageSheets;

    @SerializedName("pic_cap_sheets")
    public String capSheets;

    public String address;

    @SerializedName("happiness_manager")
    public String managerName;

    @SerializedName("happiness_manager_tel")
    public String managerPhoneNumber;

    @SerializedName("visit_date")
    public String firstVisitDate;

    @SerializedName("visit_date_2")
    public String secondVisitDate;

    @SerializedName("doc_date")
    public String documentsDate;

    @SerializedName("comissioned")
    public boolean isComissioned;

    @SerializedName("finish_contract")
    public boolean hasFinishContract;

    @SerializedName("key_handover")
    public boolean isKeyHanded;

    @SerializedName("Status")
    public String status;

    @SerializedName("real_estate")
    @Embedded(prefix = "realEstate_")
    public RealEstate realEstate;






    public ReBlock(@NonNull String blockId, String name, int group, String saleDate, int floorsCount, boolean hasParking, String deadline, String stateComission, String logo, String logoWeb, String apartmentSheets, String officeSheets, String storageSheets, String capSheets, String address, String managerName, String managerPhoneNumber, String firstVisitDate, String secondVisitDate, String documentsDate, boolean isComissioned, boolean hasFinishContract, boolean isKeyHanded, String status, RealEstate realEstate) {


        this.blockId = blockId;
        this.name = name;
        this.group = group;
        this.saleDate = saleDate;
        this.floorsCount = floorsCount;
        this.hasParking = hasParking;
        this.deadline = deadline;
        this.stateComission = stateComission;
        this.logo = logo;
        this.logoWeb = logoWeb;
        this.apartmentSheets = apartmentSheets;
        this.officeSheets = officeSheets;
        this.storageSheets = storageSheets;
        this.capSheets = capSheets;
        this.address = address;
        this.managerName = managerName;
        this.managerPhoneNumber = managerPhoneNumber;
        this.firstVisitDate = firstVisitDate;
        this.secondVisitDate = secondVisitDate;
        this.documentsDate = documentsDate;
        this.isComissioned = isComissioned;
        this.hasFinishContract = hasFinishContract;
        this.isKeyHanded = isKeyHanded;
        this.status = status;
        this.realEstate = realEstate;
    }

    public static class RealEstate {
        public String guid;
        public String name;

        public RealEstate(String guid, String name) {
            this.guid = guid;
            this.name = name;
        }
    }
}
