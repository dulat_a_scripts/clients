package com.zerotoonelabs.bigroup.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BiServiceContactData {

    @SerializedName("ManagerGuid")
    @Expose
    private String managerGuid;
    @SerializedName("ManagerPhoto")
    @Expose
    private Object managerPhoto;
    @SerializedName("ManagerObjecta")
    @Expose
    private String managerObjecta;
    @SerializedName("ManagerPhone")
    @Expose
    private String managerPhone;
    @SerializedName("ManagerMail")
    @Expose
    private String managerMail;
    @SerializedName("DirectorServSluzhbPhoto")
    @Expose
    private Object directorServSluzhbPhoto;
    @SerializedName("DirectorGuid")
    @Expose
    private String directorGuid;
    @SerializedName("DirectorServSluzhb")
    @Expose
    private String directorServSluzhb;
    @SerializedName("DirectorMail")
    @Expose
    private String directorMail;
    @SerializedName("ServiceCompanyName")
    @Expose
    private String serviceCompanyName;

    public String getManagerGuid() {
        return managerGuid;
    }

    public void setManagerGuid(String managerGuid) {
        this.managerGuid = managerGuid;
    }

    public Object getManagerPhoto() {
        return managerPhoto;
    }

    public void setManagerPhoto(Object managerPhoto) {
        this.managerPhoto = managerPhoto;
    }

    public String getManagerObjecta() {
        return managerObjecta;
    }

    public void setManagerObjecta(String managerObjecta) {
        this.managerObjecta = managerObjecta;
    }

    public String getManagerPhone() {
        return managerPhone;
    }

    public void setManagerPhone(String managerPhone) {
        this.managerPhone = managerPhone;
    }

    public String getManagerMail() {
        return managerMail;
    }

    public void setManagerMail(String managerMail) {
        this.managerMail = managerMail;
    }

    public Object getDirectorServSluzhbPhoto() {
        return directorServSluzhbPhoto;
    }

    public void setDirectorServSluzhbPhoto(Object directorServSluzhbPhoto) {
        this.directorServSluzhbPhoto = directorServSluzhbPhoto;
    }

    public String getDirectorGuid() {
        return directorGuid;
    }

    public void setDirectorGuid(String directorGuid) {
        this.directorGuid = directorGuid;
    }

    public String getDirectorServSluzhb() {
        return directorServSluzhb;
    }

    public void setDirectorServSluzhb(String directorServSluzhb) {
        this.directorServSluzhb = directorServSluzhb;
    }

    public String getDirectorMail() {
        return directorMail;
    }

    public void setDirectorMail(String directorMail) {
        this.directorMail = directorMail;
    }

    public String getServiceCompanyName() {
        return serviceCompanyName;
    }

    public void setServiceCompanyName(String serviceCompanyName) {
        this.serviceCompanyName = serviceCompanyName;
    }

}