package com.zerotoonelabs.bigroup.entity.club;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by AKaltayev on 05.04.2018.
 */

public class Partner implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Organization")
    @Expose
    private String Organization;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("Web")
    @Expose
    private String web;
    @SerializedName("ActivityField")
    @Expose
    private String activityField;
    @SerializedName("DiscountFrom")
    @Expose
    private double discountFrom;
    @SerializedName("ava")
    @Expose
    private String ava;

    public String getAva() {
        return ava;
    }

    public void setAva(String ava) {
        this.ava = ava;
    }

    public double getDiscountFrom() {
        return discountFrom;
    }

    public void setDiscountFrom(double discountFrom) {
        this.discountFrom = discountFrom;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrganization() {
        return Organization;
    }

    public void setOrganization(String organization) {
        Organization = organization;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getActivityField() {
        return activityField;
    }

    public void setActivityField(String activityField) {
        this.activityField = activityField;
    }
}
