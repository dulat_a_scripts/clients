package com.zerotoonelabs.bigroup.entity.kassa24;

import java.util.List;

/**
 * Created by AKaltayev on 24.04.2018.
 */

public class Invoices2 {
    public Invoice2 invoice;



    public static class Invoice2 {

        public Invoices2.Invoice2.Services services;
        public String invoiceId;
        public String formedDate;

        public static class Services {
            public Service2 service;
        }
    }
}
