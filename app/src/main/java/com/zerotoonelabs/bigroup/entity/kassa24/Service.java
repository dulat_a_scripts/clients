package com.zerotoonelabs.bigroup.entity.kassa24;

import com.google.gson.annotations.SerializedName;

public class Service {
    @SerializedName("serviceId")
    private String serviceId;
    @SerializedName("serviceName")
    private String serviceName;
    @SerializedName("IsCounterService")
    private String isCounterService;
    @SerializedName("measure")
    private Object measure;
    @SerializedName("tariff")
    private String tariff;
    @SerializedName("debtInfo")
    private String debtInfo;
    @SerializedName("fixSum")
    private String fixSum;
    @SerializedName("fixCount")
    private String fixCount;
    @SerializedName("prevCount")
    private String prevCount;
    @SerializedName("lastCount")
    private String lastCount;

    private int totalSum;

    public Service() {

    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getIsCounterService() {
        return isCounterService;
    }

    public void setIsCounterService(String isCounterService) {
        this.isCounterService = isCounterService;
    }

    public Object getMeasure() {
        return measure;
    }

    public void setMeasure(Object measure) {
        this.measure = measure;
    }

    public String getTariff() {
        return tariff;
    }

    public void setTariff(String tariff) {
        this.tariff = tariff;
    }

    public String getDebtInfo() {
        return debtInfo;
    }

    public void setDebtInfo(String debtInfo) {
        this.debtInfo = debtInfo;
    }

    public String getFixSum() {
        return fixSum;
    }

    public void setFixSum(String fixSum) {
        this.fixSum = fixSum;
    }

    public String getFixCount() {
        return fixCount;
    }

    public void setFixCount(String fixCount) {
        this.fixCount = fixCount;
    }

    public String getPrevCount() {
        return prevCount;
    }

    public void setPrevCount(String prevCount) {
        this.prevCount = prevCount;
    }

    public String getLastCount() {
        return lastCount;
    }

    public void setLastCount(String lastCount) {
        this.lastCount = lastCount;
    }

    public int getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(int totalSum) {
        this.totalSum = totalSum;
    }
}
