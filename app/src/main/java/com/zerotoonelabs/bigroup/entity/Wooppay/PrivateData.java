package com.zerotoonelabs.bigroup.entity.Wooppay;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by zhan on 3/31/18.
 */

public class PrivateData {
    @SerializedName("Количество проживающих")
    @Expose
    String Количество_проживающих;
    @SerializedName("Отапливаемая площадь")
    @Expose
    String Отапливаемая_площадь;

    @Override
    public String toString() {
        return "PrivateData{" +
                "Количество_проживающих='" + Количество_проживающих + '\'' +
                ", Отапливаемая_площадь='" + Отапливаемая_площадь + '\'' +
                '}';
    }

    public String getКоличество_проживающих() {
        return Количество_проживающих;
    }

    public void setКоличество_проживающих(String количество_проживающих) {
        Количество_проживающих = количество_проживающих;
    }

    public String getОтапливаемая_площадь() {
        return Отапливаемая_площадь;
    }

    public void setОтапливаемая_площадь(String отапливаемая_площадь) {
        Отапливаемая_площадь = отапливаемая_площадь;
    }
}
