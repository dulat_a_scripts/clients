
package com.zerotoonelabs.bigroup.entity.statusesPred;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Platezhi implements Parcelable
{

    @SerializedName("DataPlatezha")
    @Expose
    private String dataPlatezha;
    @SerializedName("SummaPlatezha")
    @Expose
    private Float summaPlatezha;
    @SerializedName("PlatezhDocument")
    @Expose
    private String platezhDocument;
    public final static Creator<Platezhi> CREATOR = new Creator<Platezhi>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Platezhi createFromParcel(Parcel in) {
            return new Platezhi(in);
        }

        public Platezhi[] newArray(int size) {
            return (new Platezhi[size]);
        }

    }
    ;

    protected Platezhi(Parcel in) {
        this.dataPlatezha = ((String) in.readValue((String.class.getClassLoader())));
        this.summaPlatezha = ((Float) in.readValue((Float.class.getClassLoader())));
        this.platezhDocument = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public Platezhi() {
    }

    /**
     * 
     * @param platezhDocument
     * @param dataPlatezha
     * @param summaPlatezha
     */
    public Platezhi(String dataPlatezha, Float summaPlatezha, String platezhDocument) {
        super();
        this.dataPlatezha = dataPlatezha;
        this.summaPlatezha = summaPlatezha;
        this.platezhDocument = platezhDocument;
    }

    public String getDataPlatezha() {
        return dataPlatezha;
    }

    public void setDataPlatezha(String dataPlatezha) {
        this.dataPlatezha = dataPlatezha;
    }

    public Float getSummaPlatezha() {
        return summaPlatezha;
    }

    public void setSummaPlatezha(Float summaPlatezha) {
        this.summaPlatezha = summaPlatezha;
    }

    public String getPlatezhDocument() {
        return platezhDocument;
    }

    public void setPlatezhDocument(String platezhDocument) {
        this.platezhDocument = platezhDocument;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(dataPlatezha);
        dest.writeValue(summaPlatezha);
        dest.writeValue(platezhDocument);
    }

    public int describeContents() {
        return  0;
    }

}
