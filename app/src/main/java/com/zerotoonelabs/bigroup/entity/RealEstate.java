package com.zerotoonelabs.bigroup.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

@Entity
public class RealEstate implements Parcelable {
    @NonNull
    @PrimaryKey
    @SerializedName("guid")
    private String id;
    private String name;
    private String website;

    //    @Ignore
    @SerializedName("logo")
    private String logoUrl;

    //    @Ignore
    @SerializedName("logo_web")
    private String logoWebUrl;

    private String status;
    private String city;
    private String address;
    @SerializedName("num_apt")
    private int numApt;
    @SerializedName("num_premises")
    private int numPremises;

    @Ignore
    @SerializedName("price_m")
    private int priceM;
    private String formattedPrice;

    @SerializedName("number_of_parking")
    private int numberOfParking;

    @SerializedName("Put_Foto")
    private String photoUrl;

    @SerializedName("Put_FotoVeb")
    private String photoWebUrl;

    @SerializedName("OpisanieJK")
    private String description;
    @SerializedName("KratkoeOpisanieJK")
    private String shortDescription;

    @Ignore
    @SerializedName("GPSLocation")
    private String latlong;

    private double latitude;
    private double longitude;


    public RealEstate() {

    }

    @Ignore
    protected RealEstate(Parcel in) {
        id = in.readString();
        name = in.readString();
        website = in.readString();
        logoUrl = in.readString();
        logoWebUrl = in.readString();
        status = in.readString();
        city = in.readString();
        address = in.readString();
        numApt = in.readInt();
        numPremises = in.readInt();
        priceM = in.readInt();
        formattedPrice = in.readString();
        numberOfParking = in.readInt();
        photoUrl = in.readString();
        photoWebUrl = in.readString();
        description = in.readString();
        shortDescription = in.readString();
        latlong = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(website);
        dest.writeString(logoUrl);
        dest.writeString(logoWebUrl);
        dest.writeString(status);
        dest.writeString(city);
        dest.writeString(address);
        dest.writeInt(numApt);
        dest.writeInt(numPremises);
        dest.writeInt(priceM);
        dest.writeString(formattedPrice);
        dest.writeInt(numberOfParking);
        dest.writeString(photoUrl);
        dest.writeString(photoWebUrl);
        dest.writeString(description);
        dest.writeString(shortDescription);
        dest.writeString(latlong);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Ignore
    public static final Creator<RealEstate> CREATOR = new Creator<RealEstate>() {
        @Override
        public RealEstate createFromParcel(Parcel in) {
            return new RealEstate(in);
        }

        @Override
        public RealEstate[] newArray(int size) {
            return new RealEstate[size];
        }
    };

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getLogoWebUrl() {
        return logoWebUrl;
    }

    public void setLogoWebUrl(String logoWebUrl) {
        this.logoWebUrl = logoWebUrl;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getNumApt() {
        return numApt;
    }

    public void setNumApt(int numApt) {
        this.numApt = numApt;
    }

    public int getNumPremises() {
        return numPremises;
    }

    public void setNumPremises(int numPremises) {
        this.numPremises = numPremises;
    }

    public int getPriceM() {
        return priceM;
    }

    public void setPriceM(int priceM) {
        this.priceM = priceM;
    }

    public int getNumberOfParking() {
        return numberOfParking;
    }

    public void setNumberOfParking(int numberOfParking) {
        this.numberOfParking = numberOfParking;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getPhotoWebUrl() {
        return photoWebUrl;
    }

    public void setPhotoWebUrl(String photoWebUrl) {
        this.photoWebUrl = photoWebUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getFormattedPrice() {
        return formattedPrice;
    }

    public void setFormattedPrice(String formattedPrice) {
        this.formattedPrice = formattedPrice;
    }

    public String getLatlong() {
        return latlong;
    }

    public void setLatlong(String latlong) {
        this.latlong = latlong;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
