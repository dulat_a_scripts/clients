
package com.zerotoonelabs.bigroup.entity.statusesPred;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StatusPred implements Parcelable
{

    @SerializedName("Contract")
    @Expose
    private List<Contract> contract = null;
    @SerializedName("DopSoglashenie")
    @Expose
    private List<DopSoglashenie> dopSoglashenie = null;
    @SerializedName("Uslugi")
    @Expose
    private List<Uslugi> uslugi = null;
    @SerializedName("Platezhi")
    @Expose
    private List<Platezhi> platezhi = null;
    public final static Creator<StatusPred> CREATOR = new Creator<StatusPred>() {


        @SuppressWarnings({
            "unchecked"
        })
        public StatusPred createFromParcel(Parcel in) {
            return new StatusPred(in);
        }

        public StatusPred[] newArray(int size) {
            return (new StatusPred[size]);
        }

    }
    ;

    protected StatusPred(Parcel in) {
        in.readList(this.contract, (Contract.class.getClassLoader()));
        in.readList(this.dopSoglashenie, (DopSoglashenie.class.getClassLoader()));
        in.readList(this.uslugi, (Uslugi.class.getClassLoader()));
        in.readList(this.platezhi, (Platezhi.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public StatusPred() {
    }

    /**
     * 
     * @param dopSoglashenie
     * @param contract
     * @param uslugi
     * @param platezhi
     */
    public StatusPred(List<Contract> contract, List<DopSoglashenie> dopSoglashenie, List<Uslugi> uslugi, List<Platezhi> platezhi) {
        super();
        this.contract = contract;
        this.dopSoglashenie = dopSoglashenie;
        this.uslugi = uslugi;
        this.platezhi = platezhi;
    }

    public List<Contract> getContract() {
        return contract;
    }

    public void setContract(List<Contract> contract) {
        this.contract = contract;
    }

    public List<DopSoglashenie> getDopSoglashenie() {
        return dopSoglashenie;
    }

    public void setDopSoglashenie(List<DopSoglashenie> dopSoglashenie) {
        this.dopSoglashenie = dopSoglashenie;
    }

    public List<Uslugi> getUslugi() {
        return uslugi;
    }

    public void setUslugi(List<Uslugi> uslugi) {
        this.uslugi = uslugi;
    }

    public List<Platezhi> getPlatezhi() {
        return platezhi;
    }

    public void setPlatezhi(List<Platezhi> platezhi) {
        this.platezhi = platezhi;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(contract);
        dest.writeList(dopSoglashenie);
        dest.writeList(uslugi);
        dest.writeList(platezhi);
    }

    public int describeContents() {
        return  0;
    }

}
