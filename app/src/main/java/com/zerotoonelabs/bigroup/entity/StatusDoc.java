
package com.zerotoonelabs.bigroup.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StatusDoc implements Parcelable
{

    @SerializedName("DataPodpisaniyaDogovora")
    @Expose
    private String dataPodpisaniyaDogovora;
    @SerializedName("StoimostPomesheniya")
    @Expose
    private Float stoimostPomesheniya;
    @SerializedName("NomerPoTehpasportu")
    @Expose
    private String nomerPoTehpasportu;
    @SerializedName("PoleznayaPloshadBTI")
    @Expose
    private Float poleznayaPloshadBTI;
    @SerializedName("PloshadBalkonovBTI")
    @Expose
    private Float ploshadBalkonovBTI;
    @SerializedName("ZhilayaPloshadBTI")
    @Expose
    private Float zhilayaPloshadBTI;
    public final static Parcelable.Creator<StatusDoc> CREATOR = new Creator<StatusDoc>() {


        @SuppressWarnings({
                "unchecked"
        })
        public StatusDoc createFromParcel(Parcel in) {
            return new StatusDoc(in);
        }

        public StatusDoc[] newArray(int size) {
            return (new StatusDoc[size]);
        }

    }
            ;

    protected StatusDoc(Parcel in) {
        this.dataPodpisaniyaDogovora = ((String) in.readValue((String.class.getClassLoader())));
        this.stoimostPomesheniya = ((Float) in.readValue((Float.class.getClassLoader())));
        this.nomerPoTehpasportu = ((String) in.readValue((String.class.getClassLoader())));
        this.poleznayaPloshadBTI = ((Float) in.readValue((Float.class.getClassLoader())));
        this.ploshadBalkonovBTI = ((Float) in.readValue((Float.class.getClassLoader())));
        this.zhilayaPloshadBTI = ((Float) in.readValue((Float.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public StatusDoc() {
    }

    /**
     *
     * @param nomerPoTehpasportu
     * @param poleznayaPloshadBTI
     * @param stoimostPomesheniya
     * @param dataPodpisaniyaDogovora
     * @param ploshadBalkonovBTI
     * @param zhilayaPloshadBTI
     */
    public StatusDoc(String dataPodpisaniyaDogovora, Float stoimostPomesheniya, String nomerPoTehpasportu, Float poleznayaPloshadBTI, Float ploshadBalkonovBTI, Float zhilayaPloshadBTI) {
        super();
        this.dataPodpisaniyaDogovora = dataPodpisaniyaDogovora;
        this.stoimostPomesheniya = stoimostPomesheniya;
        this.nomerPoTehpasportu = nomerPoTehpasportu;
        this.poleznayaPloshadBTI = poleznayaPloshadBTI;
        this.ploshadBalkonovBTI = ploshadBalkonovBTI;
        this.zhilayaPloshadBTI = zhilayaPloshadBTI;
    }

    public String getDataPodpisaniyaDogovora() {
        return dataPodpisaniyaDogovora;
    }

    public void setDataPodpisaniyaDogovora(String dataPodpisaniyaDogovora) {
        this.dataPodpisaniyaDogovora = dataPodpisaniyaDogovora;
    }

    public Float getStoimostPomesheniya() {
        return stoimostPomesheniya;
    }

    public void setStoimostPomesheniya(Float stoimostPomesheniya) {
        this.stoimostPomesheniya = stoimostPomesheniya;
    }

    public String getNomerPoTehpasportu() {
        return nomerPoTehpasportu;
    }

    public void setNomerPoTehpasportu(String nomerPoTehpasportu) {
        this.nomerPoTehpasportu = nomerPoTehpasportu;
    }

    public Float getPoleznayaPloshadBTI() {
        return poleznayaPloshadBTI;
    }

    public void setPoleznayaPloshadBTI(Float poleznayaPloshadBTI) {
        this.poleznayaPloshadBTI = poleznayaPloshadBTI;
    }

    public Float getPloshadBalkonovBTI() {
        return ploshadBalkonovBTI;
    }

    public void setPloshadBalkonovBTI(Float ploshadBalkonovBTI) {
        this.ploshadBalkonovBTI = ploshadBalkonovBTI;
    }

    public Float getZhilayaPloshadBTI() {
        return zhilayaPloshadBTI;
    }

    public void setZhilayaPloshadBTI(Float zhilayaPloshadBTI) {
        this.zhilayaPloshadBTI = zhilayaPloshadBTI;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(dataPodpisaniyaDogovora);
        dest.writeValue(stoimostPomesheniya);
        dest.writeValue(nomerPoTehpasportu);
        dest.writeValue(poleznayaPloshadBTI);
        dest.writeValue(ploshadBalkonovBTI);
        dest.writeValue(zhilayaPloshadBTI);
    }

    public int describeContents() {
        return  0;
    }


}
