package com.zerotoonelabs.bigroup.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "news")
public class News implements Parcelable {
    @NonNull
    @SerializedName("ID")
    @PrimaryKey
    private String id;
    private String title;
    @SerializedName("anoncement")
    private String anoncement;
    private String content;
    private String start;
    private String end;
    @SerializedName("anonce_photo_path")
    private String anoncePhotoPath;

    private long timestamp;



    public News() {
    }

    protected News(Parcel in) {
        id = in.readString();
        title = in.readString();
        anoncement = in.readString();
        content = in.readString();
        start = in.readString();
        end = in.readString();
        anoncePhotoPath = in.readString();
        timestamp = in.readLong();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(anoncement);
        dest.writeString(content);
        dest.writeString(start);
        dest.writeString(end);
        dest.writeString(anoncePhotoPath);
        dest.writeLong(timestamp);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<News> CREATOR = new Creator<News>() {
        @Override
        public News createFromParcel(Parcel in) {
            return new News(in);
        }

        @Override
        public News[] newArray(int size) {
            return new News[size];
        }
    };

    public void setId(@NonNull String id) {
        this.id = id;
    }

    @NonNull
    public String getId() {
        return id;
    }


    public News withID(String iD) {
        this.id = iD;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public News withTitle(String title) {
        this.title = title;
        return this;
    }

    public String getAnoncement() {
        return anoncement;
    }

    public void setAnoncement(String anoncement) {
        this.anoncement = anoncement;
    }

    public News withAnoncement(String anoncement) {
        this.anoncement = anoncement;
        return this;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public News withContent(String content) {
        this.content = content;
        return this;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public News withStart(String start) {
        this.start = start;
        return this;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public News withEnd(String end) {
        this.end = end;
        return this;
    }

    public String getAnoncePhotoPath() {
        return anoncePhotoPath;
    }

    public void setAnoncePhotoPath(String anoncePhotoPath) {
        this.anoncePhotoPath = anoncePhotoPath;
    }

    public News withAnoncePhotoPath(String anoncePhotoPath) {
        this.anoncePhotoPath = anoncePhotoPath;
        return this;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}