package com.zerotoonelabs.bigroup.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by AKaltayev on 12.04.2018.
 */

public class PaymentsFineElement implements Serializable {

    @SerializedName("cell")
    private Object[] cell;

    public Object[] getCell() {
        return cell;
    }

    public void setCell(Object[] cell) {
        this.cell = cell;
    }
}
