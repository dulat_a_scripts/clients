package com.zerotoonelabs.bigroup.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by AKaltayev on 17.04.2018.
 */

public class CloudPayRequest implements Serializable {

    private String description;
    private String currency;
    private String invoiceId;
    private String accountId;
    private String publicId;
    private double amount;

    private boolean isSaveCard;
    private boolean isAutoPay;
    private Date autoPayDate;
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isSaveCard() {
        return isSaveCard;
    }

    public void setSaveCard(boolean saveCard) {
        isSaveCard = saveCard;
    }

    public boolean isAutoPay() {
        return isAutoPay;
    }

    public void setAutoPay(boolean autoPay) {
        isAutoPay = autoPay;
    }

    public Date getAutoPayDate() {
        return autoPayDate;
    }

    public void setAutoPayDate(Date autoPayDate) {
        this.autoPayDate = autoPayDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
