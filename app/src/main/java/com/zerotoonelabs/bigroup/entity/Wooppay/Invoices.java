package com.zerotoonelabs.bigroup.entity.Wooppay;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by zhan on 3/27/18.
 */

public class Invoices {
    @SerializedName("formedDate")
    @Expose
    String formedDate;
    @SerializedName("invoiceId")
    @Expose
    String invoiceId;
    @SerializedName("expireDate")
    @Expose
    String expireDate;

    @Override
    public String toString() {
        return "Invoices{" +
                "formedDate='" + formedDate + '\'' +
                ", invoiceId='" + invoiceId + '\'' +
                ", expireDate='" + expireDate + '\'' +
                '}';
    }

    public String getFormedDate() {
        return formedDate;
    }

    public void setFormedDate(String formedDate) {
        this.formedDate = formedDate;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }
}
