package com.zerotoonelabs.bigroup.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BiServiceApplication {

    @SerializedName("nomer")
    @Expose
    private String nomer;
    @SerializedName("Opisanie")
    @Expose
    private String opisanie;
    @SerializedName("Data")
    @Expose
    private String data;
    @SerializedName("Mark")
    @Expose
    private int mark;
    @SerializedName("Status")
    @Expose
    private String status;

    @SerializedName("Remark")
    @Expose
    private String remark;




    @Override
    public String toString() {
        return "BiServiceApplication{" +
                "nomer='" + nomer + '\'' +
                ", opisanie='" + opisanie + '\'' +
                ", data='" + data + '\'' +
                ", mark=" + mark +
                ", status='" + status + '\'' +
                '}';
    }



    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getNomer() {
        return nomer;
    }

    public void setNomer(String nomer) {
        this.nomer = nomer;
    }

    public String getOpisanie() {
        return opisanie;
    }

    public void setOpisanie(String opisanie) {
        this.opisanie = opisanie;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}