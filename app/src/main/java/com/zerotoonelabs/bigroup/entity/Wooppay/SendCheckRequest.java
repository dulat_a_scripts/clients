package com.zerotoonelabs.bigroup.entity.Wooppay;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by AKaltayev on 08.04.2018.
 */

public class SendCheckRequest implements Serializable {

    @SerializedName("service_id")
    @Expose
    private String serviceId;

    @SerializedName("check")
    @Expose
    private WooppayCheck check;

    public SendCheckRequest() {
    }

    public SendCheckRequest(String serviceId, WooppayCheck check) {
        this.serviceId = serviceId;
        this.check = check;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public WooppayCheck getCheck() {
        return check;
    }

    public void setCheck(WooppayCheck check) {
        this.check = check;
    }
}
