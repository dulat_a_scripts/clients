package com.zerotoonelabs.bigroup.entity;

public enum Status {
    SUCCESS,
    ERROR,
    LOADING
}
