
package com.zerotoonelabs.bigroup.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KeyTransfer implements Parcelable
{

    @SerializedName("NomerDogovora")
    @Expose
    private String nomerDogovora;
    @SerializedName("SummaDogovora")
    @Expose
    private Integer summaDogovora;
    @SerializedName("DataPodpisDopSoglashDlyaKluchei")
    @Expose
    private String dataPodpisDopSoglashDlyaKluchei;
    @SerializedName("DataNachPeredachiKluchei")
    @Expose
    private String dataNachPeredachiKluchei;
    public final static Creator<KeyTransfer> CREATOR = new Creator<KeyTransfer>() {


        @SuppressWarnings({
            "unchecked"
        })
        public KeyTransfer createFromParcel(Parcel in) {
            return new KeyTransfer(in);
        }

        public KeyTransfer[] newArray(int size) {
            return (new KeyTransfer[size]);
        }

    }
    ;

    protected KeyTransfer(Parcel in) {
        this.nomerDogovora = ((String) in.readValue((String.class.getClassLoader())));
        this.summaDogovora = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.dataPodpisDopSoglashDlyaKluchei = ((String) in.readValue((String.class.getClassLoader())));
        this.dataNachPeredachiKluchei = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public KeyTransfer() {
    }

    /**
     * 
     * @param summaDogovora
     * @param dataNachPeredachiKluchei
     * @param dataPodpisDopSoglashDlyaKluchei
     * @param nomerDogovora
     */
    public KeyTransfer(String nomerDogovora, Integer summaDogovora, String dataPodpisDopSoglashDlyaKluchei, String dataNachPeredachiKluchei) {
        super();
        this.nomerDogovora = nomerDogovora;
        this.summaDogovora = summaDogovora;
        this.dataPodpisDopSoglashDlyaKluchei = dataPodpisDopSoglashDlyaKluchei;
        this.dataNachPeredachiKluchei = dataNachPeredachiKluchei;
    }

    public String getNomerDogovora() {
        return nomerDogovora;
    }

    public void setNomerDogovora(String nomerDogovora) {
        this.nomerDogovora = nomerDogovora;
    }

    public Integer getSummaDogovora() {
        return summaDogovora;
    }

    public void setSummaDogovora(Integer summaDogovora) {
        this.summaDogovora = summaDogovora;
    }

    public String getDataPodpisDopSoglashDlyaKluchei() {
        return dataPodpisDopSoglashDlyaKluchei;
    }

    public void setDataPodpisDopSoglashDlyaKluchei(String dataPodpisDopSoglashDlyaKluchei) {
        this.dataPodpisDopSoglashDlyaKluchei = dataPodpisDopSoglashDlyaKluchei;
    }

    public String getDataNachPeredachiKluchei() {
        return dataNachPeredachiKluchei;
    }

    public void setDataNachPeredachiKluchei(String dataNachPeredachiKluchei) {
        this.dataNachPeredachiKluchei = dataNachPeredachiKluchei;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(nomerDogovora);
        dest.writeValue(summaDogovora);
        dest.writeValue(dataPodpisDopSoglashDlyaKluchei);
        dest.writeValue(dataNachPeredachiKluchei);
    }

    public int describeContents() {
        return  0;
    }

}
