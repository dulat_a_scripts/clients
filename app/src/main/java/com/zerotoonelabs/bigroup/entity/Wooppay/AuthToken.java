package com.zerotoonelabs.bigroup.entity.Wooppay;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zhan on 3/26/18.
 */

public class AuthToken {
    @SerializedName("token")
    String authorization;

    public String getAuthorization() {
        return authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }
}
