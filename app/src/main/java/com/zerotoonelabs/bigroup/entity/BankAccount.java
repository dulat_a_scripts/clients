package com.zerotoonelabs.bigroup.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BankAccount {


        @SerializedName("IsActive")
        @Expose
        private Boolean isActive;
        @SerializedName("IsErcActive")
        @Expose
        private Boolean isErcActive;
        @SerializedName("Id")
        @Expose
        private Integer id;
        @SerializedName("Number")
        @Expose
        private String number;
        @SerializedName("ErcNumber")
        @Expose
        private String ercNumber;
        @SerializedName("ComplexName")
        @Expose
        private String complexName;
        @SerializedName("Address")
        @Expose
        private String address;
        @SerializedName("RoomNumber")
        @Expose
        private Integer roomNumber;
        @SerializedName("ApartmentNumber")
        @Expose
        private String apartmentNumber;
        @SerializedName("Builder")
        @Expose
        private String builder;
        @SerializedName("Square")
        @Expose
        private Double square;
        @SerializedName("Contract")
        @Expose
        private String contract;
        @SerializedName("ContractNumber")
        @Expose
        private String contractNumber;
        @SerializedName("ErcContract")
        @Expose
        private String ercContract;
        @SerializedName("ApplicationUserId")
        @Expose
        private String applicationUserId;
        @SerializedName("ServiceCompanyId")
        @Expose
        private Integer serviceCompanyId;
        @SerializedName("PropertyGuid")
        @Expose
        private String propertyGuid;
        @SerializedName("RealEstateGuid")
        @Expose
        private String realEstateGuid;
        @SerializedName("ServiceId")
        @Expose
        private String serviceId;
        @SerializedName("GuaranteeComplete")
        @Expose
        private Integer guaranteeComplete;
        @SerializedName("RealServiceId")
        @Expose
        private String realServiceId;
        @SerializedName("RealServiceName")
        @Expose
        private String realServiceName;
        @SerializedName("IsAlsecoActive")
        @Expose
        private Boolean isAlsecoActive;
        @SerializedName("AlsecoNumber")
        @Expose
        private String alsecoNumber;

        public Boolean getIsActive() {
            return isActive;
        }

        public void setIsActive(Boolean isActive) {
            this.isActive = isActive;
        }

        public Boolean getIsErcActive() {
            return isErcActive;
        }

        public void setIsErcActive(Boolean isErcActive) {
            this.isErcActive = isErcActive;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getErcNumber() {
            return ercNumber;
        }

        public void setErcNumber(String ercNumber) {
            this.ercNumber = ercNumber;
        }

        public String getComplexName() {
            return complexName;
        }

        public void setComplexName(String complexName) {
            this.complexName = complexName;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public Integer getRoomNumber() {
            return roomNumber;
        }

        public void setRoomNumber(Integer roomNumber) {
            this.roomNumber = roomNumber;
        }

        public String getApartmentNumber() {
            return apartmentNumber;
        }

        public void setApartmentNumber(String apartmentNumber) {
            this.apartmentNumber = apartmentNumber;
        }

        public String getBuilder() {
            return builder;
        }

        public void setBuilder(String builder) {
            this.builder = builder;
        }

        public Double getSquare() {
            return square;
        }

        public void setSquare(Double square) {
            this.square = square;
        }

        public String getContract() {
            return contract;
        }

        public void setContract(String contract) {
            this.contract = contract;
        }

        public String getContractNumber() {
            return contractNumber;
        }

        public void setContractNumber(String contractNumber) {
            this.contractNumber = contractNumber;
        }

        public String getErcContract() {
            return ercContract;
        }

        public void setErcContract(String ercContract) {
            this.ercContract = ercContract;
        }

        public String getApplicationUserId() {
            return applicationUserId;
        }

        public void setApplicationUserId(String applicationUserId) {
            this.applicationUserId = applicationUserId;
        }

        public Integer getServiceCompanyId() {
            return serviceCompanyId;
        }

        public void setServiceCompanyId(Integer serviceCompanyId) {
            this.serviceCompanyId = serviceCompanyId;
        }

        public String getPropertyGuid() {
            return propertyGuid;
        }

        public void setPropertyGuid(String propertyGuid) {
            this.propertyGuid = propertyGuid;
        }

        public String getRealEstateGuid() {
            return realEstateGuid;
        }

        public void setRealEstateGuid(String realEstateGuid) {
            this.realEstateGuid = realEstateGuid;
        }

        public String getServiceId() {
            return serviceId;
        }

        public void setServiceId(String serviceId) {
            this.serviceId = serviceId;
        }

        public Integer getGuaranteeComplete() {
            return guaranteeComplete;
        }

        public void setGuaranteeComplete(Integer guaranteeComplete) {
            this.guaranteeComplete = guaranteeComplete;
        }

        public String getRealServiceId() {
            return realServiceId;
        }

        public void setRealServiceId(String realServiceId) {
            this.realServiceId = realServiceId;
        }

        public String getRealServiceName() {
            return realServiceName;
        }

        public void setRealServiceName(String realServiceName) {
            this.realServiceName = realServiceName;
        }

        public Boolean getIsAlsecoActive() {
            return isAlsecoActive;
        }

        public void setIsAlsecoActive(Boolean isAlsecoActive) {
            this.isAlsecoActive = isAlsecoActive;
        }

        public String getAlsecoNumber() {
            return alsecoNumber;
        }

        public void setAlsecoNumber(String alsecoNumber) {
            this.alsecoNumber = alsecoNumber;
        }


}
