package com.zerotoonelabs.bigroup.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by AKaltayev on 12.04.2018.
 */

public class Agreement implements Serializable{

    @SerializedName("title")
    public String title;
    @SerializedName("body")
    public String body;
    @SerializedName("agree")
    public String agree;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getAgree() {
        return agree;
    }

    public void setAgree(String agree) {
        this.agree = agree;
    }
}
