package com.zerotoonelabs.bigroup.entity.kassa24;

import com.google.gson.annotations.SerializedName;

/**
 * Created by AKaltayev on 24.04.2018.
 */

public class Service2 {

    @SerializedName("serviceId")
    private String serviceId;

    @SerializedName("serviceName")
    private String serviceName;
    @SerializedName("balance")
    private String debtInfo;
    @SerializedName("amount")
    private String fixSum;

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getDebtInfo() {
        return debtInfo;
    }

    public void setDebtInfo(String debtInfo) {
        this.debtInfo = debtInfo;
    }

    public String getFixSum() {
        return fixSum;
    }

    public void setFixSum(String fixSum) {
        this.fixSum = fixSum;
    }
}
