package com.zerotoonelabs.bigroup.entity.Wooppay;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by zhan on 3/26/18.
 */

public class DebitInfo {
    @SerializedName("amount")
    @Expose
    double amount;
    @SerializedName("invoiceNum")
    @Expose
    String invoiceNum;
    @SerializedName("serviceName")
    @Expose
    String serviceName;
    @SerializedName("invoiceId")
    @Expose
    String invoiceId;
    @SerializedName("serviceId")
    @Expose
    Integer serviceId;

    @Override
    public String toString() {
        return "DebitInfo{" +
                "amount=" + amount +
                ", invoiceNum='" + invoiceNum + '\'' +
                ", serviceName='" + serviceName + '\'' +
                ", invoiceId='" + invoiceId + '\'' +
                ", serviceId=" + serviceId +
                '}';
    }

    public String getInvoiceNum() {
        return invoiceNum;
    }

    public void setInvoiceNum(String invoiceNum) {
        this.invoiceNum = invoiceNum;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Integer getServiceId() {
        return serviceId;
    }

    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }
}
