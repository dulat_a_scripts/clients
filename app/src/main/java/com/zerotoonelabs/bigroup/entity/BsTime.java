package com.zerotoonelabs.bigroup.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.zerotoonelabs.bigroup.db.BiTypeConverters;

import java.util.List;

@Entity
@TypeConverters(BiTypeConverters.class)

public class BsTime {
    @PrimaryKey
    @NonNull
    public String bsId;
    public String date;
    @SerializedName("time")
    public List<String> times;

    public BsTime(@NonNull String bsId, String date, List<String> times) {
        this.bsId = bsId;
        this.date = date;
        this.times = times;
    }
}
