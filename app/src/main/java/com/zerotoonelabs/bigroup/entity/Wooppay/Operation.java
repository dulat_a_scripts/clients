package com.zerotoonelabs.bigroup.entity.Wooppay;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by zhan on 3/27/18.
 */

public class Operation {
    @SerializedName("operation_id")
    @Expose
    int operationId;
    @SerializedName("operation_data")
    @Expose
    OperationData operation_data;

    public OperationData getOperation_data() {
        return operation_data;
    }

    public void setOperation_data(OperationData operation_data) {
        this.operation_data = operation_data;
    }

    public int getOperationId() {
        return operationId;
    }

    public void setOperationId(int operationId) {
        this.operationId = operationId;
    }
}
