package com.zerotoonelabs.bigroup.entity.Wooppay;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by zhan on 3/29/18.
 */

public class PaymentInfo {
    @SerializedName("service_id")
    @Expose
    String service_id;
    @SerializedName("fields")
    @Expose
    Fields fields;
    @SerializedName("bodyAmount")
    @Expose
    BodyAmount bodyAmount;

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public Fields getFields() {
        return fields;
    }

    public void setFields(Fields fields) {
        this.fields = fields;
    }

    public BodyAmount getBodyAmount() {
        return bodyAmount;
    }

    public void setBodyAmount(BodyAmount bodyAmount) {
        this.bodyAmount = bodyAmount;
    }
}
