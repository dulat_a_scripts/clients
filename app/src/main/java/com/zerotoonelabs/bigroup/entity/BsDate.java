package com.zerotoonelabs.bigroup.entity;
/*
* Entity that stores list of dates for specific @link BuildingSite
*/

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.zerotoonelabs.bigroup.db.BiTypeConverters;

import java.util.List;

@Entity
@TypeConverters(BiTypeConverters.class)
public class BsDate {
    @PrimaryKey
    @NonNull
    public String bsId;
    @SerializedName("day")
    public List<String> dates;

    public BsDate(@NonNull String bsId, List<String> dates) {
        this.bsId = bsId;
        this.dates = dates;
    }
}
