package com.zerotoonelabs.bigroup.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by AKaltayev on 25.04.2018.
 */

public class FlatImgResponse {

    @SerializedName("FlatImage")
    private String FlatImage;

    public String getFlatImage() {
        return FlatImage;
    }

    public void setFlatImage(String flatImage) {
        FlatImage = flatImage;
    }
}
