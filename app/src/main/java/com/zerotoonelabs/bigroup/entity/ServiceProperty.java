package com.zerotoonelabs.bigroup.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

@Entity
public class ServiceProperty {

    @PrimaryKey
    @NonNull
    @SerializedName("service_property_id")
    public String id;

    @SerializedName("name_service_property")
    public String name;

    @SerializedName("PometkaUdaleniya")
    public boolean deleted;

    public ServiceProperty(@NonNull String id, String name, boolean deleted) {
        this.id = id;
        this.name = name;
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return name;
    }
}
