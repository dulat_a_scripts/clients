package com.zerotoonelabs.bigroup.entity.club;

import java.io.Serializable;

/**
 * Created by AKaltayev on 04.04.2018.
 */

public class ClubTicket implements Serializable {

    private String title;
    private String amount;

    public ClubTicket(String title, String amount) {
        this.title = title;
        this.amount = amount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
