package com.zerotoonelabs.bigroup.entity.kassa24;

/**
 * Created by AKaltayev on 24.04.2018.
 */

public class CheckUserResponse2 {
    public Invoices2 invoices;

    public Invoices2 getInvoices() {
        return invoices;
    }

    public void setInvoices(Invoices2 invoices) {
        this.invoices = invoices;
    }
}
