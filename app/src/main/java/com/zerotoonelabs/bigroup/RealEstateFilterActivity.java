package com.zerotoonelabs.bigroup;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;

public class RealEstateFilterActivity extends AppCompatActivity {

    public static final String STATUS_KEY = "status";
    public static final String CITY_KEY = "city";
    private Spinner city, status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.realestate_filter_act);

        setupActionBar();

        city = findViewById(R.id.city_spinner_object);
        status = findViewById(R.id.status_spinner);
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void closeActivity() {
        String url = "http://webapi.bi-group.org/api/v1/salesbigroup/real_estate?";
        String cityText = city.getSelectedItem().toString();
        String statusText = status.getSelectedItem().toString();
        if (statusText.equals("Построенные"))
            statusText = "1";
        else
            statusText = "0";
        statusText = "vveden=" + statusText;
        cityText = "gorod=" + cityText;
        url += cityText + "&" + statusText;
        SharedPreferences sPref = getSharedPreferences("abc", MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putString("ObjectUrl", url);
        ed.apply();
        finish();
    }

    public void applyFilterPressed(View view) {
        Intent data = new Intent();

        String cityText = city.getSelectedItem().toString();
        int selectedStatus = status.getSelectedItemPosition();

        data.putExtra(CITY_KEY, cityText);
        data.putExtra(STATUS_KEY, selectedStatus);
        setResult(RESULT_OK, data);
        onBackPressed();
    }
}
