package com.zerotoonelabs.bigroup.fragment;

import android.arch.lifecycle.ViewModelProvider;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.zerotoonelabs.bigroup.CalculationActivity;
import com.zerotoonelabs.bigroup.Kassa24Activity;
import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.Library.Request_parsers.Parser_One;
import com.zerotoonelabs.bigroup.Library.Retrofit_request.GetWoopay_services_int;
import com.zerotoonelabs.bigroup.Library.Retrofit_request.Get_auth_token;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.WooppayActivity;
import com.zerotoonelabs.bigroup.api.Kassa24Service;
import com.zerotoonelabs.bigroup.api.RestClient;
import com.zerotoonelabs.bigroup.api.RetrofitApi;
import com.zerotoonelabs.bigroup.authui.payment.BrowserActivity;

import com.zerotoonelabs.bigroup.authui.payment.QuickPayFragment;
import com.zerotoonelabs.bigroup.entity.BankAccount;
import com.zerotoonelabs.bigroup.entity.PaymentHistory;
import com.zerotoonelabs.bigroup.entity.PaymentResponse;
import com.zerotoonelabs.bigroup.entity.ServiceId;
import com.zerotoonelabs.bigroup.entity.Wooppay.AuthToken;
import com.zerotoonelabs.bigroup.entity.kassa24.CheckUserResponse;
import com.zerotoonelabs.bigroup.interfaces.IClickCallback;
import com.zerotoonelabs.bigroup.vo.Status;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by zhan on 3/23/18.
 */

public class PayQuickFragment extends Fragment {

    String LOG = "PayQuickFragment";
    private static final String TOKEN_KEY = "token";
    private static final String CONTRACT_KEY = "contract_id";
    private static final String PROPERTY_KEY = "property_id";
    private static final String INVOICE_KEY = "InvoiceId";
    private static final int BROWSER_RC = 12;
    String token = "", contractId = "";
    int propertyId = 0;
    Retrofit retrofitApi = null;
    List<BankAccount> bankAccountList = new ArrayList<>();
    SharedPreferences sharedPreferences = null;
    RetrofitApi response;

    private View view;

    private String serviceNumber;
    //private String ercNumber;
    private String serviceId;

    String woopaytoken = null;

    String number = null;
    String ErcNumber = null;
    String AlsecoNumber = null;

    ProgressBar progressBar;

    //private boolean isAlsecoActive = false;
    //private String alsecoNumber;
    //private String alsecoServiceId = "138";

    private BroadcastReceiver syncFinishedReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            int propertyId = intent.getIntExtra("propertyId", 1);
            Library.SavePhoneMemoryOne("propertyid",String.valueOf(propertyId));
            updateUI(propertyId);
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i(LOG, "onCreate()");
        View v = inflater.inflate(R.layout.payment_fragment, container, false);


        progressBar = v.findViewById(R.id.progressBar3);

        progressBar.setVisibility(View.GONE);



        setHasOptionsMenu(true);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());


            return v;


    }

    @Override
    public void onDestroyOptionsMenu() {
        super.onDestroyOptionsMenu();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        Log.i(LOG, "onCreateOptionsMenu()");
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.action_calculator, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i(LOG, "onOptionsItemSelected()");
        switch (item.getItemId()) {
            case R.id.actionCalculator:
                Intent intent = new Intent(getActivity(), CalculationActivity.class);
                startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = view;

        Bundle args = getArguments();
        if (args != null && args.containsKey(TOKEN_KEY) && args.containsKey(CONTRACT_KEY) && args.containsKey(PROPERTY_KEY)) {
            token = args.getString(TOKEN_KEY);
            contractId = args.getString(CONTRACT_KEY);
            propertyId = args.getInt(PROPERTY_KEY);
            Library.SavePhoneMemoryOne("propertyid",String.valueOf(propertyId));
        }

        updateUI(propertyId);

        view.findViewById(R.id.wooppaySelect).setOnClickListener(view1 -> {


            retrofitApi = Library.NewRetrofitclient_woopay();

            progressBar.setVisibility(View.VISIBLE);

            Get_auth_token get_auth_token = retrofitApi.create(Get_auth_token.class);

            Call<AuthToken> call = get_auth_token.getAuthToken(
                    RetrofitApi.contentType,
                    sharedPreferences.getString("login", null),
                    sharedPreferences.getString("email", null),
                    Library.Woopay_parent_login

            );
//"bigroup"
            call.enqueue(new Callback<AuthToken>() {
                @Override
                public void onResponse(Call<AuthToken> call, retrofit2.Response<AuthToken> response) {
                    if (response.body() != null) {
                        woopaytoken = response.body().getAuthorization();
                        Library.woopaytoken = response.body().getAuthorization();
                        sharedPreferences.edit().putString("Authorization", response.body().getAuthorization()).commit();

                        Download();

                    }
                }

                @Override
                public void onFailure(Call<AuthToken> call, Throwable t) {
                    Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });



        });

//zz
        view.findViewById(R.id.kassaSelect).setOnClickListener(view1 -> {
            Intent intent = new Intent(getActivity(), Kassa24Activity.class);
            Library.intent = intent;
            Library.serviceId = serviceId;
            Library.serviceNumber = serviceNumber;
            intent.putExtra("serviceID", serviceId);
            intent.putExtra("contractNumber", serviceNumber);
            startActivity(intent);
        });
    }

    public void Download(){

        Retrofit client = Library.NewRetrofitclient_woopay();

        GetWoopay_services_int getWoopay_services_int = client.create(GetWoopay_services_int.class);

        Call<Parser_One> parser_oneCall = getWoopay_services_int.getServices(RetrofitApi.contentType,woopaytoken);

        parser_oneCall.enqueue(new Callback<Parser_One>() {
            @Override
            public void onResponse(Call<Parser_One> call, retrofit2.Response<Parser_One> response) {
                List<com.zerotoonelabs.bigroup.Library.Request_parsers.ServiceId> serviceIds = response.body().getServiceIds();

                for(int i = 0;i < serviceIds.size();i++){

                    String name = serviceIds.get(i).getName();
                    Integer id = serviceIds.get(i).getId();
                    if(name.equals("erc")){
                  //  if(name.equals("erc_m")){
                        sharedPreferences.edit().putInt("service_id", id ).commit();
                        Log.i("d","d" + id);

                        progressBar.setVisibility(View.GONE);

                        Intent intent = new Intent(getActivity(), WooppayActivity.class);
                        startActivity(intent);



                    }

                }


            }

            @Override
            public void onFailure(Call<Parser_One> call, Throwable t) {

                progressBar.setVisibility(View.GONE);
                sharedPreferences.edit().putInt("service_id", 1883 ).commit();
                Intent intent = new Intent(getActivity(), WooppayActivity.class);
                startActivity(intent);
            }
        });

    }

    private void openBrowser(String url) {
        if (url == null) {
            return;
        }

        Intent browserIntent = new Intent(getActivity(), BrowserActivity.class);
        browserIntent.putExtra(BrowserActivity.URL_KEY, url);
        startActivityForResult(browserIntent, BROWSER_RC);
    }

    public static PayQuickFragment newInstance(String token, int param1, String contractId) {
        PayQuickFragment fragment = new PayQuickFragment();
        Bundle args = new Bundle();
        args.putString(TOKEN_KEY, token);
        args.putString(CONTRACT_KEY, contractId);
        args.putInt(PROPERTY_KEY, param1);
        fragment.setArguments(args);
        return fragment;
    }







    private void updateUI(int propertyId){
        Log.d("TEST", "_getInvoice");
        Log.d("TEST", contractId+"_"+propertyId);

        retrofitApi = new Retrofit.Builder()
                .baseUrl(RetrofitApi.url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        response = retrofitApi.create(RetrofitApi.class);

        view.findViewById(R.id.wooppaySelect).setVisibility(View.GONE);
        view.findViewById(R.id.kassaSelect).setVisibility(View.GONE);

        progressBar.setVisibility(View.VISIBLE);
//zz
        RestClient.getRestProvider().getBankAccount(token, propertyId, contractId).enqueue(new Callback<List<BankAccount>>() {
            @Override
            public void onResponse(Call<List<BankAccount>> call, retrofit2.Response<List<BankAccount>> response) {
                bankAccountList = response.body();

                if (bankAccountList != null && bankAccountList.size() > 0) {

                    try{
                        //sharedPreferences.edit().putInt("service_id", 1883 ).commit();
                        sharedPreferences.edit().putString("account", bankAccountList.get(0).getErcNumber()).commit();
                        sharedPreferences.edit().putString("serviceGUID", bankAccountList.get(0).getServiceId()).commit();
                        sharedPreferences.edit().putInt("BankingAccountId", bankAccountList.get(0).getId()).commit();
                    }catch (Exception e){

                    }


                    ((TextView)view.findViewById(R.id.addrText)).setText(bankAccountList.get(0).getAddress());

                    // new logic


                    //ekranizasiya

                    try{
                        number = bankAccountList.get(0).getNumber();
                        if(number.equals("") || number.equals(null) || number.equals("0")){
                            number = "";
                        }
                    }catch (Exception e){
                        number = "";
                    }


                    try{
                        ErcNumber = bankAccountList.get(0).getErcNumber();
                        if(ErcNumber.equals("") || ErcNumber.equals(null) || ErcNumber.equals("0")){
                            ErcNumber = "";
                        }
                    }catch (Exception e){
                        ErcNumber = "";
                    }


                    TextView textuvedom = view.findViewById(R.id.textuvedom);

                    textuvedom.setText("");

                    view.findViewById(R.id.wooppaySelect).setVisibility(View.GONE);
                    view.findViewById(R.id.kassaSelect).setVisibility(View.GONE);

                    //ekranizasiya



                        if(!number.equals("")){     //esli lis schet ne pustoi kassa 24
                            //Вы можете оплатить коммунальные услуги УСО через Кассу 24
                            //отображать квитанцию Сервиса
                            //number
                            textuvedom.setText("Вы можете оплатить коммунальные услуги УСО через Кассу 24");
                            view.findViewById(R.id.kassaSelect).setVisibility(View.VISIBLE);

                            serviceNumber = bankAccountList.get(0).getNumber();
                            serviceId = "2518";
                            ((TextView)view.findViewById(R.id.ercNumText)).setText(serviceNumber);
                            //number
                        }else if(number.equals("")){
                            textuvedom.setText("Лицевой счет отсутствует");
                        }




                        if(!ErcNumber.equals("")){  //esli ers ne pustoi
                            //то отображать квитанцию ЕРЦ
                            //ers
                            textuvedom.setText("Вы можете оплатить коммунальные услуги УСО через Woopay");
                            view.findViewById(R.id.wooppaySelect).setVisibility(View.VISIBLE);
                            serviceNumber = bankAccountList.get(0).getErcNumber();
                            serviceId = "138";
                            ((TextView)view.findViewById(R.id.ercNumText)).setText(serviceNumber);
                            //ers
                        }else if(ErcNumber.equals("")){
                            textuvedom.setText("Лицевой счет ЕРЦ отсутствует");

                        }

                    if(!ErcNumber.equals("") && !number.equals("")){ //esli vse ne pustie vse knopki
                        //отображать квитанцию ЕРЦ
                        //ers
                        textuvedom.setText("");
                        view.findViewById(R.id.wooppaySelect).setVisibility(View.VISIBLE);
                        view.findViewById(R.id.kassaSelect).setVisibility(View.VISIBLE);
                        serviceNumber = bankAccountList.get(0).getErcNumber();
                        serviceId = "138";
                        ((TextView)view.findViewById(R.id.ercNumText)).setText(serviceNumber);
                        //ers

                    }else if(ErcNumber.equals("") && number.equals("")){ //esli pustie

                        textuvedom.setText("Для отображения квитанции, заключите договор УСO c BI Service");
                        view.findViewById(R.id.wooppaySelect).setVisibility(View.GONE);
                        view.findViewById(R.id.kassaSelect).setVisibility(View.GONE);
                        LinearLayout lineo =  view.findViewById(R.id.mainView);
                        lineo.setVisibility(View.GONE);

                    }



                    progressBar.setVisibility(View.GONE);





//                    if (isErc == true){
//                        view.findViewById(R.id.wooppaySelect).setVisibility(View.VISIBLE);
//                        view.findViewById(R.id.kassaSelect).setVisibility(View.VISIBLE);
//                        serviceNumber = bankAccountList.get(0).getErcNumber();
//                        serviceId = "138";
//                        ((TextView)view.findViewById(R.id.ercNumText)).setText(serviceNumber);
//                    }
//                    if (isServiceActive){
//                        view.findViewById(R.id.kassaSelect).setVisibility(View.VISIBLE);
//                        view.findViewById(R.id.wooppaySelect).setVisibility(View.GONE);
//                        serviceNumber = bankAccountList.get(0).getNumber();
//                        serviceId = "2518";
//                        ((TextView)view.findViewById(R.id.ercNumText)).setText(serviceNumber);
//                    }
                }
            }

            @Override
            public void onFailure(Call<List<BankAccount>> call, Throwable t) {
                Toast.makeText(getActivity(), t.toString(), Toast.LENGTH_LONG).show();
            }
        });


    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        getContext().registerReceiver(syncFinishedReceiver, new IntentFilter("UPDATE_UI"));
    }

    @Override
    public void onPause() {
        super.onPause();
        getContext().unregisterReceiver(syncFinishedReceiver);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
