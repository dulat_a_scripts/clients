package com.zerotoonelabs.bigroup.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.zerotoonelabs.bigroup.HistoryInvoiceActivity;
import com.zerotoonelabs.bigroup.Library.Library;
import com.zerotoonelabs.bigroup.R;
import com.zerotoonelabs.bigroup.adapters.PaymentHistoryAdapter;
import com.zerotoonelabs.bigroup.api.RestClient;
import com.zerotoonelabs.bigroup.api.RetrofitApi;
import com.zerotoonelabs.bigroup.entity.PaymentHistory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by zhan on 3/21/18.
 */

public class HistoryPaymentFragment extends Fragment {

    static String LOG = "HistoryPaymentFragment";
    private static final String TOKEN_KEY = "token";
    private static final String CONTRACT_KEY = "contract_id";
    private static final String PROPERTY_KEY = "property_id";
    private static final String ACCOUNT_ID = "BankingAccountId";
    String token = "", contractId = "";
    int bankAccountId = 0, propertyId = 0;
    SharedPreferences sharedPreferences = null;
    //Retrofit retrofitApi = null;
    RecyclerView recyclerView = null;
    View view = null;

    public static HistoryPaymentFragment newInstance(String token, int propertyId, String contractId) {
        Log.i(LOG, "newInstance()");
        HistoryPaymentFragment fragment = new HistoryPaymentFragment();
        Bundle args = new Bundle();
        args.putString(TOKEN_KEY, token);
        args.putString(CONTRACT_KEY, contractId);
        args.putInt(PROPERTY_KEY, propertyId);
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i(LOG, "onCreateView()");
        View v = inflater.inflate(R.layout.utility_bills_history_frag, container, false);
        recyclerView = v.findViewById(R.id.utility_bills_list);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Log.i(LOG, "onViewCreated()");
        super.onViewCreated(view, savedInstanceState);
        Bundle args = getArguments();
        //xx
        if (args != null && args.containsKey(TOKEN_KEY) && args.containsKey(CONTRACT_KEY) && args.containsKey(PROPERTY_KEY)) {
            token = args.getString(TOKEN_KEY);
            contractId = args.getString(CONTRACT_KEY);
            propertyId = args.getInt(PROPERTY_KEY);
        }
        this.view = view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.i(LOG, "onAttach()");
    }

    @Override
    public void onStart() {
        Log.i(LOG, "onStart()");
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(LOG, "onResume()");


        Log.i("d","d" + contractId + " | " + propertyId);



        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        bankAccountId = sharedPreferences.getInt(ACCOUNT_ID, 0);
        if (bankAccountId > 0) {
            /*retrofitApi = new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(RetrofitApi.url)
                    .build();
            RetrofitApi response = retrofitApi.create(RetrofitApi.class);*/

            String ApplicationUserId = Library.Phone_memory.getString("applicationUserId", null);
            String propertyGuid = Library.Phone_memory.getString("propertyGuid", null);
            ///api/v1/MobileClient/GetHistoryInvoices
            RestClient.getRestProvider().getPaymentHistory(token, ApplicationUserId,propertyGuid).enqueue(new Callback<List<PaymentHistory>>() {
                @Override
                public void onResponse(Call<List<PaymentHistory>> call, Response<List<PaymentHistory>> response) {
                    if (response.body() != null && response.body().size() > 0) {
                        view.findViewById(R.id.emptyList).setVisibility(View.GONE);
                        view.findViewById(R.id.progressBar).setVisibility(View.GONE);

                        ArrayList<PaymentHistory> paymentHistories = new ArrayList<>();
                        paymentHistories.addAll(response.body());
                        Collections.sort(paymentHistories);

                        recyclerView.setAdapter(new PaymentHistoryAdapter(paymentHistories, view.getContext(), token));
                        recyclerView.getAdapter().notifyDataSetChanged();
                    } else {
                        view.findViewById(R.id.progressBar).setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<List<PaymentHistory>> call, Throwable t) {
                    Toast.makeText(getActivity(), t.toString(), Toast.LENGTH_LONG).show();
                    view.findViewById(R.id.progressBar).setVisibility(View.GONE);
                }
            });
        } else
            view.findViewById(R.id.progressBar).setVisibility(View.GONE);
    }

    @Override
    public void onStop() {
        Log.i(LOG, "onStop()");
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(LOG, "onDestroy()");
    }

    @Override
    public void onDetach() {
        Log.i(LOG, "onDetach()");
        super.onDetach();
    }
}
